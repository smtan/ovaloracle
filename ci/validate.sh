#!/bin/sh

find oval -type f -iname "*.json" -exec sh -c 'for f; do echo "$f"; check-jsonschema --schemafile ci/schema.json "$f" || exit 1; done' sh {} +
