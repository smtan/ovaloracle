{
  "Title": "RHSA-2020:4500: bind security, bug fix, and enhancement update (Moderate)",
  "Description": "The Berkeley Internet Name Domain (BIND) is an implementation of the Domain Name System (DNS) protocols. BIND includes a DNS server (named); a resolver library (routines for applications to use when interfacing with DNS); and tools for verifying that the DNS server is operating correctly.\nThe following packages have been upgraded to a later upstream version: bind (9.11.20). (BZ#1818785)\nSecurity Fix(es):\n* bind: asterisk character in an empty non-terminal can cause an assertion failure in rbtdb.c (CVE-2020-8619)\n* bind: truncated TSIG response can lead to an assertion failure (CVE-2020-8622)\n* bind: remotely triggerable assertion failure in pk11.c (CVE-2020-8623)\n* bind: incorrect enforcement of update-policy rules of type \"subdomain\" (CVE-2020-8624)\nFor more details about the security issue(s), including the impact, a CVSS score, acknowledgments, and other related information, refer to the CVE page(s) listed in the References section.\nAdditional Changes:\nFor detailed information on changes in this release, see the Red Hat Enterprise Linux 8.3 Release Notes linked from the References section.",
  "Platform": [
    "Red Hat Enterprise Linux 8"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2020:4500",
      "ID": "RHSA-2020:4500"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-8619",
      "ID": "CVE-2020-8619"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-8622",
      "ID": "CVE-2020-8622"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-8623",
      "ID": "CVE-2020-8623"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-8624",
      "ID": "CVE-2020-8624"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "Oracle Linux 8 is installed"
              },
              {
                "Comment": "Red Hat CoreOS 4 is installed"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "bind is earlier than 32:9.11.20-5.el8"
                  },
                  {
                    "Comment": "bind is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "bind-chroot is earlier than 32:9.11.20-5.el8"
                  },
                  {
                    "Comment": "bind-chroot is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "bind-devel is earlier than 32:9.11.20-5.el8"
                  },
                  {
                    "Comment": "bind-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "bind-export-devel is earlier than 32:9.11.20-5.el8"
                  },
                  {
                    "Comment": "bind-export-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "bind-export-libs is earlier than 32:9.11.20-5.el8"
                  },
                  {
                    "Comment": "bind-export-libs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "bind-libs is earlier than 32:9.11.20-5.el8"
                  },
                  {
                    "Comment": "bind-libs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "bind-libs-lite is earlier than 32:9.11.20-5.el8"
                  },
                  {
                    "Comment": "bind-libs-lite is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "bind-license is earlier than 32:9.11.20-5.el8"
                  },
                  {
                    "Comment": "bind-license is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "bind-lite-devel is earlier than 32:9.11.20-5.el8"
                  },
                  {
                    "Comment": "bind-lite-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "bind-pkcs11 is earlier than 32:9.11.20-5.el8"
                  },
                  {
                    "Comment": "bind-pkcs11 is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "bind-pkcs11-devel is earlier than 32:9.11.20-5.el8"
                  },
                  {
                    "Comment": "bind-pkcs11-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "bind-pkcs11-libs is earlier than 32:9.11.20-5.el8"
                  },
                  {
                    "Comment": "bind-pkcs11-libs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "bind-pkcs11-utils is earlier than 32:9.11.20-5.el8"
                  },
                  {
                    "Comment": "bind-pkcs11-utils is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "bind-sdb is earlier than 32:9.11.20-5.el8"
                  },
                  {
                    "Comment": "bind-sdb is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "bind-sdb-chroot is earlier than 32:9.11.20-5.el8"
                  },
                  {
                    "Comment": "bind-sdb-chroot is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "bind-utils is earlier than 32:9.11.20-5.el8"
                  },
                  {
                    "Comment": "bind-utils is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "python3-bind is earlier than 32:9.11.20-5.el8"
                  },
                  {
                    "Comment": "python3-bind is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [

        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 8 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-8619"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-8622"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-8623"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-8624"
    }
  ],
  "Issued": {
    "Date": "2020-11-04"
  }
}