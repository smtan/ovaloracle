{
  "Title": "RHSA-2020:5500: mariadb:10.3 security, bug fix, and enhancement update (Important)",
  "Description": "MariaDB is a multi-user, multi-threaded SQL database server that is binary compatible with MySQL. \nThe following packages have been upgraded to a later upstream version: mariadb (10.3.27), galera (25.3.31). (BZ#1899082, BZ#1899086)\nSecurity Fix(es):\n* mariadb: Insufficient SST method name check leading to code injection in mysql-wsrep (CVE-2020-15180)\n* mysql: InnoDB unspecified vulnerability (CPU Oct 2019) (CVE-2019-2938)\n* mysql: Server: Optimizer unspecified vulnerability (CPU Oct 2019) (CVE-2019-2974)\n* mysql: C API unspecified vulnerability (CPU Apr 2020) (CVE-2020-2752)\n* mysql: InnoDB unspecified vulnerability (CPU Apr 2020) (CVE-2020-2760)\n* mysql: Server: DML unspecified vulnerability (CPU Apr 2020) (CVE-2020-2780)\n* mysql: Server: Stored Procedure unspecified vulnerability (CPU Apr 2020) (CVE-2020-2812)\n* mysql: InnoDB unspecified vulnerability (CPU Apr 2020) (CVE-2020-2814)\n* mariadb-connector-c: Improper validation of content in a OK packet received from server (CVE-2020-13249)\n* mysql: Server: FTS unspecified vulnerability (CPU Oct 2020) (CVE-2020-14765)\n* mysql: InnoDB unspecified vulnerability (CPU Oct 2020) (CVE-2020-14776)\n* mysql: Server: FTS unspecified vulnerability (CPU Oct 2020) (CVE-2020-14789)\n* mysql: Server: Locking unspecified vulnerability (CPU Oct 2020) (CVE-2020-14812)\n* mysql: C API unspecified vulnerability (CPU Jan 2020) (CVE-2020-2574)\nFor more details about the security issue(s), including the impact, a CVSS score, acknowledgments, and other related information, refer to the CVE page(s) listed in the References section.\nBug Fix(es):\n* FTBFS: -D_GLIBCXX_ASSERTIONS (BZ#1899009)\n* Queries with entity_id IN ('1', '2', …, '70000') run much slower in MariaDB 10.3 than on MariaDB 10.1 (BZ#1899017)\n* Cleanup race with wsrep_rsync_sst_tunnel may prevent full galera cluster bootstrap (BZ#1899021)\n* There are undeclared file conflicts in several mariadb and mysql packages (BZ#1899077)",
  "Platform": [
    "Red Hat Enterprise Linux 8"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2020:5500",
      "ID": "RHSA-2020:5500"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-2938",
      "ID": "CVE-2019-2938"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-2974",
      "ID": "CVE-2019-2974"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-13249",
      "ID": "CVE-2020-13249"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-14765",
      "ID": "CVE-2020-14765"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-14776",
      "ID": "CVE-2020-14776"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-14789",
      "ID": "CVE-2020-14789"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-14812",
      "ID": "CVE-2020-14812"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-15180",
      "ID": "CVE-2020-15180"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-2574",
      "ID": "CVE-2020-2574"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-2752",
      "ID": "CVE-2020-2752"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-2760",
      "ID": "CVE-2020-2760"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-2780",
      "ID": "CVE-2020-2780"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-2812",
      "ID": "CVE-2020-2812"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-2814",
      "ID": "CVE-2020-2814"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2021-2022",
      "ID": "CVE-2021-2022"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2021-2144",
      "ID": "CVE-2021-2144"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2021-2194",
      "ID": "CVE-2021-2194"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "Oracle Linux 8 is installed"
              },
              {
                "Comment": "Red Hat CoreOS 4 is installed"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [
                  {
                    "Operator": "OR",
                    "Criterias": [
                      {
                        "Operator": "AND",
                        "Criterias": [

                        ],
                        "Criterions": [
                          {
                            "Comment": "Judy is earlier than 0:1.0.5-18.module+el8+2765+cfa4f87b"
                          },
                          {
                            "Comment": "Judy is signed with Red Hat redhatrelease2 key"
                          }
                        ]
                      },
                      {
                        "Operator": "AND",
                        "Criterias": [

                        ],
                        "Criterions": [
                          {
                            "Comment": "galera is earlier than 0:25.3.31-1.module+el8.3.0+8843+3f4e42f6"
                          },
                          {
                            "Comment": "galera is signed with Red Hat redhatrelease2 key"
                          }
                        ]
                      },
                      {
                        "Operator": "AND",
                        "Criterias": [

                        ],
                        "Criterions": [
                          {
                            "Comment": "mariadb is earlier than 3:10.3.27-3.module+el8.3.0+8972+5e3224e9"
                          },
                          {
                            "Comment": "mariadb is signed with Red Hat redhatrelease2 key"
                          }
                        ]
                      },
                      {
                        "Operator": "AND",
                        "Criterias": [

                        ],
                        "Criterions": [
                          {
                            "Comment": "mariadb-backup is earlier than 3:10.3.27-3.module+el8.3.0+8972+5e3224e9"
                          },
                          {
                            "Comment": "mariadb-backup is signed with Red Hat redhatrelease2 key"
                          }
                        ]
                      },
                      {
                        "Operator": "AND",
                        "Criterias": [

                        ],
                        "Criterions": [
                          {
                            "Comment": "mariadb-common is earlier than 3:10.3.27-3.module+el8.3.0+8972+5e3224e9"
                          },
                          {
                            "Comment": "mariadb-common is signed with Red Hat redhatrelease2 key"
                          }
                        ]
                      },
                      {
                        "Operator": "AND",
                        "Criterias": [

                        ],
                        "Criterions": [
                          {
                            "Comment": "mariadb-devel is earlier than 3:10.3.27-3.module+el8.3.0+8972+5e3224e9"
                          },
                          {
                            "Comment": "mariadb-devel is signed with Red Hat redhatrelease2 key"
                          }
                        ]
                      },
                      {
                        "Operator": "AND",
                        "Criterias": [

                        ],
                        "Criterions": [
                          {
                            "Comment": "mariadb-embedded is earlier than 3:10.3.27-3.module+el8.3.0+8972+5e3224e9"
                          },
                          {
                            "Comment": "mariadb-embedded is signed with Red Hat redhatrelease2 key"
                          }
                        ]
                      },
                      {
                        "Operator": "AND",
                        "Criterias": [

                        ],
                        "Criterions": [
                          {
                            "Comment": "mariadb-embedded-devel is earlier than 3:10.3.27-3.module+el8.3.0+8972+5e3224e9"
                          },
                          {
                            "Comment": "mariadb-embedded-devel is signed with Red Hat redhatrelease2 key"
                          }
                        ]
                      },
                      {
                        "Operator": "AND",
                        "Criterias": [

                        ],
                        "Criterions": [
                          {
                            "Comment": "mariadb-errmsg is earlier than 3:10.3.27-3.module+el8.3.0+8972+5e3224e9"
                          },
                          {
                            "Comment": "mariadb-errmsg is signed with Red Hat redhatrelease2 key"
                          }
                        ]
                      },
                      {
                        "Operator": "AND",
                        "Criterias": [

                        ],
                        "Criterions": [
                          {
                            "Comment": "mariadb-gssapi-server is earlier than 3:10.3.27-3.module+el8.3.0+8972+5e3224e9"
                          },
                          {
                            "Comment": "mariadb-gssapi-server is signed with Red Hat redhatrelease2 key"
                          }
                        ]
                      },
                      {
                        "Operator": "AND",
                        "Criterias": [

                        ],
                        "Criterions": [
                          {
                            "Comment": "mariadb-oqgraph-engine is earlier than 3:10.3.27-3.module+el8.3.0+8972+5e3224e9"
                          },
                          {
                            "Comment": "mariadb-oqgraph-engine is signed with Red Hat redhatrelease2 key"
                          }
                        ]
                      },
                      {
                        "Operator": "AND",
                        "Criterias": [

                        ],
                        "Criterions": [
                          {
                            "Comment": "mariadb-server is earlier than 3:10.3.27-3.module+el8.3.0+8972+5e3224e9"
                          },
                          {
                            "Comment": "mariadb-server is signed with Red Hat redhatrelease2 key"
                          }
                        ]
                      },
                      {
                        "Operator": "AND",
                        "Criterias": [

                        ],
                        "Criterions": [
                          {
                            "Comment": "mariadb-server-galera is earlier than 3:10.3.27-3.module+el8.3.0+8972+5e3224e9"
                          },
                          {
                            "Comment": "mariadb-server-galera is signed with Red Hat redhatrelease2 key"
                          }
                        ]
                      },
                      {
                        "Operator": "AND",
                        "Criterias": [

                        ],
                        "Criterions": [
                          {
                            "Comment": "mariadb-server-utils is earlier than 3:10.3.27-3.module+el8.3.0+8972+5e3224e9"
                          },
                          {
                            "Comment": "mariadb-server-utils is signed with Red Hat redhatrelease2 key"
                          }
                        ]
                      },
                      {
                        "Operator": "AND",
                        "Criterias": [

                        ],
                        "Criterions": [
                          {
                            "Comment": "mariadb-test is earlier than 3:10.3.27-3.module+el8.3.0+8972+5e3224e9"
                          },
                          {
                            "Comment": "mariadb-test is signed with Red Hat redhatrelease2 key"
                          }
                        ]
                      }
                    ],
                    "Criterions": [

                    ]
                  }
                ],
                "Criterions": [
                  {
                    "Comment": "Module mariadb:10.3 is enabled"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [

        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 8 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-2938"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-2974"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-13249"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-14765"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-14776"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-14789"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-14812"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-15180"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-2574"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-2752"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-2760"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-2780"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-2812"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-2814"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2021-2022"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2021-2144"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2021-2194"
    }
  ],
  "Issued": {
    "Date": "2020-12-15"
  }
}