{
  "Title": "RHSA-2020:4289: kernel-rt security and bug fix update (Important)",
  "Description": "The kernel-rt packages provide the Real Time Linux Kernel, which enables fine-tuning for systems with extremely high determinism requirements.\nSecurity Fix(es):\n* kernel: net: bluetooth: type confusion while processing AMP packets (CVE-2020-12351)\n* kernel: net: bluetooth: information leak when processing certain AMP packets (CVE-2020-12352)\n* kernel: metadata validator in XFS may cause an inode with a valid, user-creatable extended attribute to be flagged as corrupt (CVE-2020-14385)\n* kernel: memory corruption in net/packet/af_packet.c leads to elevation of privilege (CVE-2020-14386)\n* kernel: kernel: buffer over write in vgacon_scroll (CVE-2020-14331)\nFor more details about the security issue(s), including the impact, a CVSS score, acknowledgments, and other related information, refer to the CVE page(s) listed in the References section.\nBug Fix(es):\n* kernel-rt: update RT source tree to the RHEL-8.2.z Batch#4 source tree (BZ#1877921)",
  "Platform": [
    "Red Hat Enterprise Linux 8"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2020:4289",
      "ID": "RHSA-2020:4289"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-12351",
      "ID": "CVE-2020-12351"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-12352",
      "ID": "CVE-2020-12352"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-14331",
      "ID": "CVE-2020-14331"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-14385",
      "ID": "CVE-2020-14385"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-14386",
      "ID": "CVE-2020-14386"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "Oracle Linux 8 is installed"
              },
              {
                "Comment": "Red Hat CoreOS 4 is installed"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel-rt earlier than 0:4.18.0-193.28.1.rt13.77.el8_2 is currently running"
              },
              {
                "Comment": "kernel-rt earlier than 0:4.18.0-193.28.1.rt13.77.el8_2 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt is earlier than 0:4.18.0-193.28.1.rt13.77.el8_2"
                  },
                  {
                    "Comment": "kernel-rt is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-core is earlier than 0:4.18.0-193.28.1.rt13.77.el8_2"
                  },
                  {
                    "Comment": "kernel-rt-core is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug is earlier than 0:4.18.0-193.28.1.rt13.77.el8_2"
                  },
                  {
                    "Comment": "kernel-rt-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug-core is earlier than 0:4.18.0-193.28.1.rt13.77.el8_2"
                  },
                  {
                    "Comment": "kernel-rt-debug-core is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug-devel is earlier than 0:4.18.0-193.28.1.rt13.77.el8_2"
                  },
                  {
                    "Comment": "kernel-rt-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug-kvm is earlier than 0:4.18.0-193.28.1.rt13.77.el8_2"
                  },
                  {
                    "Comment": "kernel-rt-debug-kvm is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug-modules is earlier than 0:4.18.0-193.28.1.rt13.77.el8_2"
                  },
                  {
                    "Comment": "kernel-rt-debug-modules is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug-modules-extra is earlier than 0:4.18.0-193.28.1.rt13.77.el8_2"
                  },
                  {
                    "Comment": "kernel-rt-debug-modules-extra is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-devel is earlier than 0:4.18.0-193.28.1.rt13.77.el8_2"
                  },
                  {
                    "Comment": "kernel-rt-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-kvm is earlier than 0:4.18.0-193.28.1.rt13.77.el8_2"
                  },
                  {
                    "Comment": "kernel-rt-kvm is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-modules is earlier than 0:4.18.0-193.28.1.rt13.77.el8_2"
                  },
                  {
                    "Comment": "kernel-rt-modules is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-modules-extra is earlier than 0:4.18.0-193.28.1.rt13.77.el8_2"
                  },
                  {
                    "Comment": "kernel-rt-modules-extra is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [

        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 8 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-12351"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-12352"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-14331"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-14385"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-14386"
    }
  ],
  "Issued": {
    "Date": "2020-10-20"
  }
}