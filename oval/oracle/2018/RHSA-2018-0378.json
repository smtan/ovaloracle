{
  "Title": "RHSA-2018:0378: ruby security update (Important)",
  "Description": "Ruby is an extensible, interpreted, object-oriented, scripting language. It has features to process text files and to perform system management tasks.\nSecurity Fix(es):\n* It was discovered that the Net::FTP module did not properly process filenames in combination with certain operations. A remote attacker could exploit this flaw to execute arbitrary commands by setting up a malicious FTP server and tricking a user or Ruby application into downloading files with specially crafted names using the Net::FTP module. (CVE-2017-17405)\n* A buffer underflow was found in ruby's sprintf function. An attacker, with ability to control its format string parameter, could send a specially crafted string that would disclose heap memory or crash the interpreter. (CVE-2017-0898)\n* It was found that rubygems did not sanitize gem names during installation of a given gem. A specially crafted gem could use this flaw to install files outside of the regular directory. (CVE-2017-0901)\n* A vulnerability was found where rubygems did not sanitize DNS responses when requesting the hostname of the rubygems server for a domain, via a _rubygems._tcp DNS SRV query. An attacker with the ability to manipulate DNS responses could direct the gem command towards a different domain. (CVE-2017-0902)\n* A vulnerability was found where the rubygems module was vulnerable to an unsafe YAML deserialization when inspecting a gem. Applications inspecting gem files without installing them can be tricked to execute arbitrary code in the context of the ruby interpreter. (CVE-2017-0903)\n* It was found that WEBrick did not sanitize all its log messages. If logs were printed in a terminal, an attacker could interact with the terminal via the use of escape sequences. (CVE-2017-10784)\n* It was found that the decode method of the OpenSSL::ASN1 module was vulnerable to buffer underrun. An attacker could pass a specially crafted string to the application in order to crash the ruby interpreter, causing a denial of service. (CVE-2017-14033)\n* A vulnerability was found where rubygems did not properly sanitize gems' specification text. A specially crafted gem could interact with the terminal via the use of escape sequences. (CVE-2017-0899)\n* It was found that rubygems could use an excessive amount of CPU while parsing a sufficiently long gem summary. A specially crafted gem from a gem repository could freeze gem commands attempting to parse its summary. (CVE-2017-0900)\n* A buffer overflow vulnerability was found in the JSON extension of ruby. An attacker with the ability to pass a specially crafted JSON input to the extension could use this flaw to expose the interpreter's heap memory. (CVE-2017-14064)\n* The \"lazy_initialize\" function in lib/resolv.rb did not properly process certain filenames. A remote attacker could possibly exploit this flaw to inject and execute arbitrary commands. (CVE-2017-17790)",
  "Platform": [
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2018:0378",
      "ID": "RHSA-2018:0378"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-0898",
      "ID": "CVE-2017-0898"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-0899",
      "ID": "CVE-2017-0899"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-0900",
      "ID": "CVE-2017-0900"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-0901",
      "ID": "CVE-2017-0901"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-0902",
      "ID": "CVE-2017-0902"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-0903",
      "ID": "CVE-2017-0903"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-10784",
      "ID": "CVE-2017-10784"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-14033",
      "ID": "CVE-2017-14033"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-14064",
      "ID": "CVE-2017-14064"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-17405",
      "ID": "CVE-2017-17405"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-17790",
      "ID": "CVE-2017-17790"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ruby is earlier than 0:2.0.0.648-33.el7_4"
                  },
                  {
                    "Comment": "ruby is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ruby-devel is earlier than 0:2.0.0.648-33.el7_4"
                  },
                  {
                    "Comment": "ruby-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ruby-doc is earlier than 0:2.0.0.648-33.el7_4"
                  },
                  {
                    "Comment": "ruby-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ruby-irb is earlier than 0:2.0.0.648-33.el7_4"
                  },
                  {
                    "Comment": "ruby-irb is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ruby-libs is earlier than 0:2.0.0.648-33.el7_4"
                  },
                  {
                    "Comment": "ruby-libs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ruby-tcltk is earlier than 0:2.0.0.648-33.el7_4"
                  },
                  {
                    "Comment": "ruby-tcltk is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "rubygem-bigdecimal is earlier than 0:1.2.0-33.el7_4"
                  },
                  {
                    "Comment": "rubygem-bigdecimal is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "rubygem-io-console is earlier than 0:0.4.2-33.el7_4"
                  },
                  {
                    "Comment": "rubygem-io-console is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "rubygem-json is earlier than 0:1.7.7-33.el7_4"
                  },
                  {
                    "Comment": "rubygem-json is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "rubygem-minitest is earlier than 0:4.3.2-33.el7_4"
                  },
                  {
                    "Comment": "rubygem-minitest is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "rubygem-psych is earlier than 0:2.0.0-33.el7_4"
                  },
                  {
                    "Comment": "rubygem-psych is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "rubygem-rake is earlier than 0:0.9.6-33.el7_4"
                  },
                  {
                    "Comment": "rubygem-rake is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "rubygem-rdoc is earlier than 0:4.0.0-33.el7_4"
                  },
                  {
                    "Comment": "rubygem-rdoc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "rubygems is earlier than 0:2.0.14.1-33.el7_4"
                  },
                  {
                    "Comment": "rubygems is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "rubygems-devel is earlier than 0:2.0.14.1-33.el7_4"
                  },
                  {
                    "Comment": "rubygems-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 7 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-0898"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-0899"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-0900"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-0901"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-0902"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-0903"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-10784"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-14033"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-14064"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-17405"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-17790"
    }
  ],
  "Issued": {
    "Date": "2018-02-28"
  }
}