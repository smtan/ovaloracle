{
  "Title": "RHSA-2018:1062: kernel security, bug fix, and enhancement update (Important)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux operating system.\nSecurity Fix(es):\n* hw: cpu: speculative execution permission faults handling (CVE-2017-5754, Important, KVM for Power)\n* kernel: Buffer overflow in firewire driver via crafted incoming packets (CVE-2016-8633, Important)\n* kernel: Use-after-free vulnerability in DCCP socket (CVE-2017-8824, Important)\n* Kernel: kvm: nVMX: L2 guest could access hardware(L0) CR8 register (CVE-2017-12154, Important)\n* kernel: v4l2: disabled memory access protection mechanism allowing privilege escalation (CVE-2017-13166, Important)\n* kernel: media: use-after-free in [tuner-xc2028] media driver (CVE-2016-7913, Moderate)\n* kernel: drm/vmwgfx: fix integer overflow in vmw_surface_define_ioctl() (CVE-2017-7294, Moderate)\n* kernel: Incorrect type conversion for size during dma allocation (CVE-2017-9725, Moderate)\n* kernel: memory leak when merging buffers in SCSI IO vectors (CVE-2017-12190, Moderate)\n* kernel: vfs: BUG in truncate_inode_pages_range() and fuse client (CVE-2017-15121, Moderate)\n* kernel: Use-after-free in userfaultfd_event_wait_completion function in userfaultfd.c (CVE-2017-15126, Moderate)\n* kernel: net: double-free and memory corruption in get_net_ns_by_id() (CVE-2017-15129, Moderate)\n* kernel: Use-after-free in snd_seq_ioctl_create_port() (CVE-2017-15265, Moderate)\n* kernel: Missing capabilities check in net/netfilter/nfnetlink_cthelper.c allows for unprivileged access to systemwide nfnl_cthelper_list structure (CVE-2017-17448, Moderate)\n* kernel: Missing namespace check in net/netlink/af_netlink.c allows for network monitors to observe systemwide activity (CVE-2017-17449, Moderate)\n* kernel: Unallocated memory access by malicious USB device via bNumInterfaces overflow (CVE-2017-17558, Moderate)\n* kernel: netfilter: use-after-free in tcpmss_mangle_packet function in net/netfilter/xt_TCPMSS.c (CVE-2017-18017, Moderate)\n* kernel: Race condition in drivers/md/dm.c:dm_get_from_kobject() allows local users to cause a denial of service (CVE-2017-18203, Moderate)\n* kernel: kvm: Reachable BUG() on out-of-bounds guest IRQ (CVE-2017-1000252, Moderate)\n* Kernel: KVM: DoS via write flood to I/O port 0x80 (CVE-2017-1000407, Moderate)\n* kernel: Stack information leak in the EFS element (CVE-2017-1000410, Moderate)\n* kernel: Kernel address information leak in drivers/acpi/sbshc.c:acpi_smbus_hc_add() function potentially allowing KASLR bypass (CVE-2018-5750, Moderate)\n* kernel: Race condition in sound system can lead to denial of service (CVE-2018-1000004, Moderate)\n* kernel: multiple Low security impact security issues (CVE-2016-3672, CVE-2017-14140, CVE-2017-15116, CVE-2017-15127, CVE-2018-6927, Low)\nRed Hat would like to thank Eyal Itkin for reporting CVE-2016-8633; Google Project Zero for reporting CVE-2017-5754; Mohamed Ghannam for reporting CVE-2017-8824; Jim Mattson (Google.com) for reporting CVE-2017-12154; Vitaly Mayatskih for reporting CVE-2017-12190; Andrea Arcangeli (Engineering) for reporting CVE-2017-15126; Kirill Tkhai for reporting CVE-2017-15129; Jan H. Schönherr (Amazon) for reporting CVE-2017-1000252; and Armis Labs for reporting CVE-2017-1000410. The CVE-2017-15121 issue was discovered by Miklos Szeredi (Red Hat) and the CVE-2017-15116 issue was discovered by ChunYu Wang (Red Hat).\nFor more details about the security issue(s), including the impact, a CVSS score, and other related information, refer to the CVE page(s) listed in the References section.\nAdditional Changes:\nFor detailed information on changes in this release, see the Red Hat Enterprise Linux 7.5 Release Notes linked from the References section.",
  "Platform": [
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2018:1062",
      "ID": "RHSA-2018:1062"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-3672",
      "ID": "CVE-2016-3672"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-7913",
      "ID": "CVE-2016-7913"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-8633",
      "ID": "CVE-2016-8633"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-1000252",
      "ID": "CVE-2017-1000252"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-1000407",
      "ID": "CVE-2017-1000407"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-1000410",
      "ID": "CVE-2017-1000410"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-12154",
      "ID": "CVE-2017-12154"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-12190",
      "ID": "CVE-2017-12190"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-13166",
      "ID": "CVE-2017-13166"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-13305",
      "ID": "CVE-2017-13305"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-14140",
      "ID": "CVE-2017-14140"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-15116",
      "ID": "CVE-2017-15116"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-15121",
      "ID": "CVE-2017-15121"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-15126",
      "ID": "CVE-2017-15126"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-15127",
      "ID": "CVE-2017-15127"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-15129",
      "ID": "CVE-2017-15129"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-15265",
      "ID": "CVE-2017-15265"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-15274",
      "ID": "CVE-2017-15274"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-17448",
      "ID": "CVE-2017-17448"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-17449",
      "ID": "CVE-2017-17449"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-17558",
      "ID": "CVE-2017-17558"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-18017",
      "ID": "CVE-2017-18017"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-18203",
      "ID": "CVE-2017-18203"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-18270",
      "ID": "CVE-2017-18270"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-5715",
      "ID": "CVE-2017-5715"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-5754",
      "ID": "CVE-2017-5754"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-7294",
      "ID": "CVE-2017-7294"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-8824",
      "ID": "CVE-2017-8824"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-9725",
      "ID": "CVE-2017-9725"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-1000004",
      "ID": "CVE-2018-1000004"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-1066",
      "ID": "CVE-2018-1066"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-5750",
      "ID": "CVE-2018-5750"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-6927",
      "ID": "CVE-2018-6927"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:3.10.0-862.el7 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:3.10.0-862.el7 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:3.10.0-862.el7"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-abi-whitelists is earlier than 0:3.10.0-862.el7"
                  },
                  {
                    "Comment": "kernel-abi-whitelists is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-bootwrapper is earlier than 0:3.10.0-862.el7"
                  },
                  {
                    "Comment": "kernel-bootwrapper is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:3.10.0-862.el7"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:3.10.0-862.el7"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:3.10.0-862.el7"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:3.10.0-862.el7"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:3.10.0-862.el7"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:3.10.0-862.el7"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:3.10.0-862.el7"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-tools is earlier than 0:3.10.0-862.el7"
                  },
                  {
                    "Comment": "kernel-tools is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-tools-libs is earlier than 0:3.10.0-862.el7"
                  },
                  {
                    "Comment": "kernel-tools-libs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-tools-libs-devel is earlier than 0:3.10.0-862.el7"
                  },
                  {
                    "Comment": "kernel-tools-libs-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perf is earlier than 0:3.10.0-862.el7"
                  },
                  {
                    "Comment": "perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "python-perf is earlier than 0:3.10.0-862.el7"
                  },
                  {
                    "Comment": "python-perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 7 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-3672"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-7913"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-8633"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-1000252"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-1000407"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-1000410"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-12154"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-12190"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-13166"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-13305"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-14140"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-15116"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-15121"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-15126"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-15127"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-15129"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-15265"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-15274"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-17448"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-17449"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-17558"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-18017"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-18203"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-18270"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-5715"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-5754"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-7294"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-8824"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-9725"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-1000004"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-1066"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-5750"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-6927"
    }
  ],
  "Issued": {
    "Date": "2018-04-10"
  }
}