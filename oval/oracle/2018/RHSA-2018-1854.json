{
  "Title": "RHSA-2018:1854: kernel security and bug fix update (Important)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux operating system.\nSecurity Fix(es):\n* An industry-wide issue was found in the way many modern microprocessor designs have implemented speculative execution of Load & Store instructions (a commonly used performance optimization). It relies on the presence of a precisely-defined instruction sequence in the privileged code as well as the fact that memory read from address to which a recent memory write has occurred may see an older value and subsequently cause an update into the microprocessor's data cache even for speculatively executed instructions that never actually commit (retire). As a result, an unprivileged attacker could use this flaw to read privileged memory by conducting targeted cache side-channel attacks. (CVE-2018-3639, PowerPC)\n* kernel: net/packet: overflow in check for priv area size (CVE-2017-7308)\n* kernel: AIO interface didn't use rw_verify_area() for checking mandatory locking on files and size of access (CVE-2012-6701)\n* kernel: AIO write triggers integer overflow in some protocols (CVE-2015-8830)\n* kernel: Null pointer dereference via keyctl (CVE-2016-8650)\n* kernel: ping socket / AF_LLC connect() sin_family race (CVE-2017-2671)\n* kernel: Race condition between multiple sys_perf_event_open() calls (CVE-2017-6001)\n* kernel: Incorrect error handling in the set_mempolicy and mbind compat syscalls in mm/mempolicy.c (CVE-2017-7616)\n* kernel: mm subsystem does not properly enforce the CONFIG_STRICT_DEVMEM protection mechanism (CVE-2017-7889)\n* kernel: Double free in the inet_csk_clone_lock function in net/ipv4/inet_connection_sock.c (CVE-2017-8890)\n* kernel: net: sctp_v6_create_accept_sk function mishandles inheritance (CVE-2017-9075)\n* kernel: net: IPv6 DCCP implementation mishandles inheritance (CVE-2017-9076)\n* kernel: net: tcp_v6_syn_recv_sock function mishandles inheritance (CVE-2017-9077)\n* kernel: memory leak when merging buffers in SCSI IO vectors (CVE-2017-12190)\n* kernel: vfs: BUG in truncate_inode_pages_range() and fuse client (CVE-2017-15121)\n* kernel: Race condition in drivers/md/dm.c:dm_get_from_kobject() allows local users to cause a denial of service (CVE-2017-18203)\n* kernel: a null pointer dereference in net/dccp/output.c:dccp_write_xmit() leads to a system crash (CVE-2018-1130)\n* kernel: Missing length check of payload in net/sctp/sm_make_chunk.c:_sctp_make_chunk() function allows denial of service (CVE-2018-5803)\nFor more details about the security issue(s), including the impact, a CVSS score, and other related information, refer to the CVE page(s) listed in the References section.\nRed Hat would like to thank Ken Johnson (Microsoft Security Response Center) and Jann Horn (Google Project Zero) for reporting CVE-2018-3639; Vitaly Mayatskih for reporting CVE-2017-12190; and Evgenii Shatokhin (Virtuozzo Team) for reporting CVE-2018-1130. The CVE-2017-15121 issue was discovered by Miklos Szeredi (Red Hat).\nAdditional Changes:\nFor detailed information on changes in this release, see the Red Hat Enterprise Linux 6.10 Release Notes and Red Hat Enterprise Linux 6.10 Technical Notes linked from the References section.",
  "Platform": [
    "Red Hat Enterprise Linux 6"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2018:1854",
      "ID": "RHSA-2018:1854"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-6701",
      "ID": "CVE-2012-6701"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-8830",
      "ID": "CVE-2015-8830"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-8650",
      "ID": "CVE-2016-8650"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-12190",
      "ID": "CVE-2017-12190"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-15121",
      "ID": "CVE-2017-15121"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-18203",
      "ID": "CVE-2017-18203"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-2671",
      "ID": "CVE-2017-2671"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-6001",
      "ID": "CVE-2017-6001"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-7308",
      "ID": "CVE-2017-7308"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-7616",
      "ID": "CVE-2017-7616"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-7889",
      "ID": "CVE-2017-7889"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-8890",
      "ID": "CVE-2017-8890"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-9075",
      "ID": "CVE-2017-9075"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-9076",
      "ID": "CVE-2017-9076"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-9077",
      "ID": "CVE-2017-9077"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-1130",
      "ID": "CVE-2018-1130"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-3639",
      "ID": "CVE-2018-3639"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-5803",
      "ID": "CVE-2018-5803"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.32-754.el6 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.32-754.el6 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.32-754.el6"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-abi-whitelists is earlier than 0:2.6.32-754.el6"
                  },
                  {
                    "Comment": "kernel-abi-whitelists is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-bootwrapper is earlier than 0:2.6.32-754.el6"
                  },
                  {
                    "Comment": "kernel-bootwrapper is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:2.6.32-754.el6"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:2.6.32-754.el6"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.32-754.el6"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.32-754.el6"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-firmware is earlier than 0:2.6.32-754.el6"
                  },
                  {
                    "Comment": "kernel-firmware is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:2.6.32-754.el6"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:2.6.32-754.el6"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:2.6.32-754.el6"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perf is earlier than 0:2.6.32-754.el6"
                  },
                  {
                    "Comment": "perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "python-perf is earlier than 0:2.6.32-754.el6"
                  },
                  {
                    "Comment": "python-perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-6701"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-8830"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-8650"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-12190"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-15121"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-18203"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-2671"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-6001"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-7308"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-7616"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-7889"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-8890"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-9075"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-9076"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-9077"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-1130"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-3639"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-5803"
    }
  ],
  "Issued": {
    "Date": "2018-06-19"
  }
}