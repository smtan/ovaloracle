{
  "Title": "RHSA-2018:2395: kernel-rt security and bug fix update (Important)",
  "Description": "The kernel-rt packages provide the Real Time Linux Kernel, which enables fine-tuning for systems with extremely high determinism requirements.\nSecurity Fix(es):\n* Modern operating systems implement virtualization of physical memory to efficiently use available system resources and provide inter-domain protection through access control and isolation. The L1TF issue was found in the way the x86 microprocessor designs have implemented speculative execution of instructions (a commonly used performance optimisation) in combination with handling of page-faults caused by terminated virtual to physical address resolving process. As a result, an unprivileged attacker could use this flaw to read privileged memory of the kernel or other processes and/or cross guest/host boundaries to read host memory by conducting targeted cache side-channel attacks. (CVE-2018-3620, CVE-2018-3646)\n* An industry-wide issue was found in the way many modern microprocessor designs have implemented speculative execution of instructions past bounds check. The flaw relies on the presence of a precisely-defined instruction sequence in the privileged code and the fact that memory writes occur to an address which depends on the untrusted value. Such writes cause an update into the microprocessor's data cache even for speculatively executed instructions that never actually commit (retire). As a result, an unprivileged attacker could use this flaw to influence speculative execution and/or read privileged memory by conducting targeted cache side-channel attacks. (CVE-2018-3693)\n* A flaw named SegmentSmack was found in the way the Linux kernel handled specially crafted TCP packets. A remote attacker could use this flaw to trigger time and calculation expensive calls to tcp_collapse_ofo_queue() and tcp_prune_ofo_queue() functions by sending specially modified packets within ongoing TCP sessions which could lead to a CPU saturation and hence a denial of service on the system. Maintaining the denial of service condition requires continuous two-way TCP sessions to a reachable open port, thus the attacks cannot be performed using spoofed IP addresses. (CVE-2018-5390)\n* kernel: crypto: privilege escalation in skcipher_recvmsg function (CVE-2017-13215)\n* kernel: mm: use-after-free in do_get_mempolicy function allows local DoS or other unspecified impact (CVE-2018-10675)\n* kernel: race condition in snd_seq_write() may lead to UAF or OOB access (CVE-2018-7566)\nFor more details about the security issue(s), including the impact, a CVSS score, and other related information, refer to the CVE page(s) listed in the References section.\nRed Hat would like to thank Intel OSSIRT (Intel.com) for reporting CVE-2018-3620 and CVE-2018-3646; Vladimir Kiriansky (MIT) and Carl Waldspurger (Carl Waldspurger Consulting) for reporting CVE-2018-3693; and Juha-Matti Tilli (Aalto University, Department of Communications and Networking and Nokia Bell Labs) for reporting CVE-2018-5390.\nBug Fix(es):\n* The kernel-rt packages have been upgraded to the 3.10.0-862.10.2 source tree, which provides a number of bug fixes over the previous version. (BZ#1594915)",
  "Platform": [
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2018:2395",
      "ID": "RHSA-2018:2395"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-13215",
      "ID": "CVE-2017-13215"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-10675",
      "ID": "CVE-2018-10675"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-3620",
      "ID": "CVE-2018-3620"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-3646",
      "ID": "CVE-2018-3646"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-3693",
      "ID": "CVE-2018-3693"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-5390",
      "ID": "CVE-2018-5390"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-7566",
      "ID": "CVE-2018-7566"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel-rt earlier than 0:3.10.0-862.11.6.rt56.819.el7 is currently running"
              },
              {
                "Comment": "kernel-rt earlier than 0:3.10.0-862.11.6.rt56.819.el7 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt is earlier than 0:3.10.0-862.11.6.rt56.819.el7"
                  },
                  {
                    "Comment": "kernel-rt is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug is earlier than 0:3.10.0-862.11.6.rt56.819.el7"
                  },
                  {
                    "Comment": "kernel-rt-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug-devel is earlier than 0:3.10.0-862.11.6.rt56.819.el7"
                  },
                  {
                    "Comment": "kernel-rt-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug-kvm is earlier than 0:3.10.0-862.11.6.rt56.819.el7"
                  },
                  {
                    "Comment": "kernel-rt-debug-kvm is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-devel is earlier than 0:3.10.0-862.11.6.rt56.819.el7"
                  },
                  {
                    "Comment": "kernel-rt-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-doc is earlier than 0:3.10.0-862.11.6.rt56.819.el7"
                  },
                  {
                    "Comment": "kernel-rt-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-kvm is earlier than 0:3.10.0-862.11.6.rt56.819.el7"
                  },
                  {
                    "Comment": "kernel-rt-kvm is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-trace is earlier than 0:3.10.0-862.11.6.rt56.819.el7"
                  },
                  {
                    "Comment": "kernel-rt-trace is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-trace-devel is earlier than 0:3.10.0-862.11.6.rt56.819.el7"
                  },
                  {
                    "Comment": "kernel-rt-trace-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-trace-kvm is earlier than 0:3.10.0-862.11.6.rt56.819.el7"
                  },
                  {
                    "Comment": "kernel-rt-trace-kvm is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 7 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-13215"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-10675"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-3620"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-3646"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-3693"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-5390"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-7566"
    }
  ],
  "Issued": {
    "Date": "2018-08-14"
  }
}