{
  "Title": "RHSA-2018:0349: java-1.7.0-openjdk security update (Important)",
  "Description": "The java-1.7.0-openjdk packages provide the OpenJDK 7 Java Runtime Environment and the OpenJDK 7 Java Software Development Kit.\nSecurity Fix(es):\n* A flaw was found in the AWT component of OpenJDK. An untrusted Java application or applet could use this flaw to bypass certain Java sandbox restrictions. (CVE-2018-2641)\n* It was discovered that the LDAPCertStore class in the JNDI component of OpenJDK failed to securely handle LDAP referrals. An attacker could possibly use this flaw to make it fetch attacker controlled certificate data. (CVE-2018-2633)\n* The JGSS component of OpenJDK ignores the value of the javax.security.auth.useSubjectCredsOnly property when using HTTP/SPNEGO authentication and always uses global credentials. It was discovered that this could cause global credentials to be unexpectedly used by an untrusted Java application. (CVE-2018-2634)\n* It was discovered that the JMX component of OpenJDK failed to properly set the deserialization filter for the SingleEntryRegistry in certain cases. A remote attacker could possibly use this flaw to bypass intended deserialization restrictions. (CVE-2018-2637)\n* It was discovered that the LDAP component of OpenJDK failed to properly encode special characters in user names when adding them to an LDAP search query. A remote attacker could possibly use this flaw to manipulate LDAP queries performed by the LdapLoginModule class. (CVE-2018-2588)\n* It was discovered that the DNS client implementation in the JNDI component of OpenJDK did not use random source ports when sending out DNS queries. This could make it easier for a remote attacker to spoof responses to those queries. (CVE-2018-2599)\n* It was discovered that the I18n component of OpenJDK could use an untrusted search path when loading resource bundle classes. A local attacker could possibly use this flaw to execute arbitrary code as another local user by making their Java application load an attacker controlled class file. (CVE-2018-2602)\n* It was discovered that the Libraries component of OpenJDK failed to sufficiently limit the amount of memory allocated when reading DER encoded input. A remote attacker could possibly use this flaw to make a Java application use an excessive amount of memory if it parsed attacker supplied DER encoded input. (CVE-2018-2603)\n* It was discovered that the key agreement implementations in the JCE component of OpenJDK did not guarantee sufficient strength of used keys to adequately protect generated shared secret. This could make it easier to break data encryption by attacking key agreement rather than the encryption using the negotiated secret. (CVE-2018-2618)\n* It was discovered that the JGSS component of OpenJDK failed to properly handle GSS context in the native GSS library wrapper in certain cases. A remote attacker could possibly make a Java application using JGSS to use a previously freed context. (CVE-2018-2629)\n* It was discovered that multiple classes in the Libraries, AWT, and JNDI components of OpenJDK did not sufficiently validate input when creating object instances from the serialized form. A specially-crafted input could cause a Java application to create objects with an inconsistent state or use an excessive amount of memory when deserialized. (CVE-2018-2663, CVE-2018-2677, CVE-2018-2678)\n* It was discovered that multiple encryption key classes in the Libraries component of OpenJDK did not properly synchronize access to their internal data. This could possibly cause a multi-threaded Java application to apply weak encryption to data because of the use of a key that was zeroed out. (CVE-2018-2579)",
  "Platform": [
    "Red Hat Enterprise Linux 6",
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2018:0349",
      "ID": "RHSA-2018:0349"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-2579",
      "ID": "CVE-2018-2579"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-2588",
      "ID": "CVE-2018-2588"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-2599",
      "ID": "CVE-2018-2599"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-2602",
      "ID": "CVE-2018-2602"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-2603",
      "ID": "CVE-2018-2603"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-2618",
      "ID": "CVE-2018-2618"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-2629",
      "ID": "CVE-2018-2629"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-2633",
      "ID": "CVE-2018-2633"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-2634",
      "ID": "CVE-2018-2634"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-2637",
      "ID": "CVE-2018-2637"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-2641",
      "ID": "CVE-2018-2641"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-2663",
      "ID": "CVE-2018-2663"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-2677",
      "ID": "CVE-2018-2677"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-2678",
      "ID": "CVE-2018-2678"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk is earlier than 1:1.7.0.171-2.6.13.0.el6_9"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk-demo is earlier than 1:1.7.0.171-2.6.13.0.el6_9"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk-demo is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk-devel is earlier than 1:1.7.0.171-2.6.13.0.el6_9"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk-javadoc is earlier than 1:1.7.0.171-2.6.13.0.el6_9"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk-javadoc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk-src is earlier than 1:1.7.0.171-2.6.13.0.el6_9"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk-src is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      },
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk is earlier than 1:1.7.0.171-2.6.13.0.el7_4"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk-accessibility is earlier than 1:1.7.0.171-2.6.13.0.el7_4"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk-accessibility is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk-demo is earlier than 1:1.7.0.171-2.6.13.0.el7_4"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk-demo is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk-devel is earlier than 1:1.7.0.171-2.6.13.0.el7_4"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk-headless is earlier than 1:1.7.0.171-2.6.13.0.el7_4"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk-headless is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk-javadoc is earlier than 1:1.7.0.171-2.6.13.0.el7_4"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk-javadoc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk-src is earlier than 1:1.7.0.171-2.6.13.0.el7_4"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk-src is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-2579"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-2588"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-2599"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-2602"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-2603"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-2618"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-2629"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-2633"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-2634"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-2637"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-2641"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-2663"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-2677"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-2678"
    }
  ],
  "Issued": {
    "Date": "2018-02-26"
  }
}