{
  "Title": "RHSA-2009:1024: Red Hat Enterprise Linux 4.8 kernel security and bug fix update (Important)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux\noperating system.\nSecurity Fixes:\n* the exit_notify() function in the Linux kernel did not properly reset the\nexit signal if a process executed a set user ID (setuid) application before\nexiting. This could allow a local, unprivileged user to elevate their\nprivileges. (CVE-2009-1337, Important)\n* the Linux kernel implementation of the Network File System (NFS) did not\nproperly initialize the file name limit in the nfs_server data structure.\nThis flaw could possibly lead to a denial of service on a client mounting\nan NFS share. (CVE-2009-1336, Moderate)\nBug Fixes and Enhancements:\nKernel Feature Support:\n* added a new allowable value to \"/proc/sys/kernel/wake_balance\" to allow\nthe scheduler to run the thread on any available CPU rather than scheduling\nit on the optimal CPU.\n* added \"max_writeback_pages\" tunable parameter to /proc/sys/vm/ to allow\nthe maximum number of modified pages kupdate writes to disk, per iteration\nper run.\n* added \"swap_token_timeout\" tunable parameter to /proc/sys/vm/ to provide\na valid hold time for the swap out protection token.\n* added diskdump support to sata_svw driver.\n* limited physical memory to 64GB for 32-bit kernels running on systems\nwith more than 64GB of physical memory to prevent boot failures.\n* improved reliability of autofs.\n* added support for 'rdattr_error' in NFSv4 readdir requests.\n* fixed various short packet handling issues for NFSv4 readdir and sunrpc.\n* fixed several CIFS bugs.\nNetworking and IPv6 Enablement:\n* added router solicitation support.\n* enforced sg requires tx csum in ethtool.\nPlatform Support:\nx86, AMD64, Intel 64, IBM System z\n* added support for a new Intel chipset.\n* added initialization vendor info in boot_cpu_data.\n* added support for N_Port ID Virtualization (NPIV) for IBM System z guests\nusing zFCP.\n* added HDMI support for some AMD and ATI chipsets.\n* updated HDA driver in ALSA to latest upstream as of 2008-07-22.\n* added support for affected_cpus for cpufreq.\n* removed polling timer from i8042.\n* fixed PM-Timer when using the ASUS A8V Deluxe motherboard.\n* backported usbfs_mutex in usbfs.\n64-bit PowerPC:\n* updated eHEA driver from version 0078-04 to 0078-08.\n* updated logging of checksum errors in the eHEA driver.\nNetwork Driver Updates:\n* updated forcedeth driver to latest upstream version 0.61.\n* fixed various e1000 issues when using Intel ESB2 hardware.\n* updated e1000e driver to upstream version 0.3.3.3-k6.\n* updated igb to upstream version 1.2.45-k2.\n* updated tg3 to upstream version 3.96.\n* updated ixgbe to upstream version 1.3.18-k4.\n* updated bnx2 to upstream version 1.7.9.\n* updated bnx2x to upstream version 1.45.23.\n* fixed bugs and added enhancements for the NetXen NX2031 and NX3031\nproducts.\n* updated Realtek r8169 driver to support newer network chipsets. All\nvariants of RTL810x/RTL8168(9) are now supported.\nStorage Driver Updates:\n* fixed various SCSI issues. Also, the SCSI sd driver now calls the\nrevalidate_disk wrapper.\n* fixed a dmraid reduced I/O delay bug in certain configurations.\n* removed quirk aac_quirk_scsi_32 for some aacraid controllers.\n* updated FCP driver on IBM System z systems with support for\npoint-to-point connections.\n* updated lpfc to version 8.0.16.46.\n* updated megaraid_sas to version 4.01-RH1.\n* updated MPT Fusion driver to version 3.12.29.00rh.\n* updated qla2xxx firmware to 4.06.01 for 4GB/s and 8GB/s adapters.\n* updated qla2xxx driver to version 8.02.09.00.04.08-d.\n* fixed sata_nv in libsata to disable ADMA mode by default.\nMiscellaneous Updates:\n* upgraded OpenFabrics Alliance Enterprise Distribution (OFED) to version\n1.4.\n* added driver support and fixes for various Wacom tablets.\nUsers should install this update, which resolves these issues and adds\nthese enhancements.",
  "Platform": [
    "Red Hat Enterprise Linux 4"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2009:1024",
      "ID": "RHSA-2009:1024"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2009-1336",
      "ID": "CVE-2009-1336"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2009-1337",
      "ID": "CVE-2009-1337"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.9-89.EL is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.9-89.EL is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.9-89.EL"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.9-89.EL"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.9-89.EL"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-hugemem is earlier than 0:2.6.9-89.EL"
                  },
                  {
                    "Comment": "kernel-hugemem is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-hugemem-devel is earlier than 0:2.6.9-89.EL"
                  },
                  {
                    "Comment": "kernel-hugemem-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-largesmp is earlier than 0:2.6.9-89.EL"
                  },
                  {
                    "Comment": "kernel-largesmp is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-largesmp-devel is earlier than 0:2.6.9-89.EL"
                  },
                  {
                    "Comment": "kernel-largesmp-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-smp is earlier than 0:2.6.9-89.EL"
                  },
                  {
                    "Comment": "kernel-smp is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-smp-devel is earlier than 0:2.6.9-89.EL"
                  },
                  {
                    "Comment": "kernel-smp-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xenU is earlier than 0:2.6.9-89.EL"
                  },
                  {
                    "Comment": "kernel-xenU is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xenU-devel is earlier than 0:2.6.9-89.EL"
                  },
                  {
                    "Comment": "kernel-xenU-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 4 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 4 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2009-1336"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2009-1337"
    }
  ],
  "Issued": {
    "Date": "2009-05-18"
  }
}