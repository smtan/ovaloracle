{
  "Title": "RHSA-2009:1132: kernel security and bug fix update (Important)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux\noperating system.\nThese updated packages fix the following security issues:\n* a flaw was found in the Intel PRO/1000 network driver in the Linux\nkernel. Frames with sizes near the MTU of an interface may be split across\nmultiple hardware receive descriptors. Receipt of such a frame could leak\nthrough a validation check, leading to a corruption of the length check. A\nremote attacker could use this flaw to send a specially-crafted packet that\nwould cause a denial of service. (CVE-2009-1385, Important)\n* the Linux kernel Network File System daemon (nfsd) implementation did not\ndrop the CAP_MKNOD capability when handling requests from local,\nunprivileged users. This flaw could possibly lead to an information leak or\nprivilege escalation. (CVE-2009-1072, Moderate)\n* Frank Filz reported the NFSv4 client was missing a file permission check\nfor the execute bit in some situations. This could allow local,\nunprivileged users to run non-executable files on NFSv4 mounted file\nsystems. (CVE-2009-1630, Moderate)\n* a missing check was found in the hypervisor_callback() function in the\nLinux kernel provided by the kernel-xen package. This could cause a denial\nof service of a 32-bit guest if an application running in that guest\naccesses a certain memory location in the kernel. (CVE-2009-1758, Moderate)\n* a flaw was found in the AGPGART driver. The agp_generic_alloc_page() and\nagp_generic_alloc_pages() functions did not zero out the memory pages they\nallocate, which may later be available to user-space processes. This flaw\ncould possibly lead to an information leak. (CVE-2009-1192, Low)\nThese updated packages also fix the following bugs:\n* \"/proc/[pid]/maps\" and \"/proc/[pid]/smaps\" can only be read by processes\nable to use the ptrace() call on a given process; however, certain\ninformation from \"/proc/[pid]/stat\" and \"/proc/[pid]/wchan\" could be used\nto reconstruct memory maps, making it possible to bypass the Address Space\nLayout Randomization (ASLR) security feature. This update addresses this\nissue. (BZ#499549)\n* in some situations, the link count was not decreased when renaming unused\nfiles on NFS mounted file systems. This may have resulted in poor\nperformance. With this update, the link count is decreased in these\nsituations, the same as is done for other file operations, such as unlink\nand rmdir. (BZ#501802)\n* tcp_ack() cleared the probes_out variable even if there were outstanding\npackets. When low TCP keepalive intervals were used, this bug may have\ncaused problems, such as connections terminating, when using remote tools\nsuch as rsh and rlogin. (BZ#501754)\n* off-by-one errors in the time normalization code could have caused\nclock_gettime() to return one billion nanoseconds, rather than adding an\nextra second. This bug could have caused the name service cache daemon\n(nscd) to consume excessive CPU resources. (BZ#501800)\n* a system panic could occur when one thread read \"/proc/bus/input/devices\"\nwhile another was removing a device. With this update, a mutex has been\nadded to protect the input_dev_list and input_handler_list variables, which\nresolves this issue. (BZ#501804)\n* using netdump may have caused a kernel deadlock on some systems.\n(BZ#504565)\n* the file system mask, which lists capabilities for users with a file\nsystem user ID (fsuid) of 0, was missing the CAP_MKNOD and\nCAP_LINUX_IMMUTABLE capabilities. This could, potentially, allow users with\nan fsuid other than 0 to perform actions on some file system types that\nwould otherwise be prevented. This update adds these capabilities. (BZ#497269)\nAll Red Hat Enterprise Linux 4 users should upgrade to these updated\npackages, which contain backported patches to resolve these issues. Note:\nThe system must be rebooted for this update to take effect.",
  "Platform": [
    "Red Hat Enterprise Linux 4"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2009:1132",
      "ID": "RHSA-2009:1132"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2009-1072",
      "ID": "CVE-2009-1072"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2009-1192",
      "ID": "CVE-2009-1192"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2009-1385",
      "ID": "CVE-2009-1385"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2009-1630",
      "ID": "CVE-2009-1630"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2009-1758",
      "ID": "CVE-2009-1758"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.9-89.0.3.EL is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.9-89.0.3.EL is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.9-89.0.3.EL"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.9-89.0.3.EL"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.9-89.0.3.EL"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-hugemem is earlier than 0:2.6.9-89.0.3.EL"
                  },
                  {
                    "Comment": "kernel-hugemem is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-hugemem-devel is earlier than 0:2.6.9-89.0.3.EL"
                  },
                  {
                    "Comment": "kernel-hugemem-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-largesmp is earlier than 0:2.6.9-89.0.3.EL"
                  },
                  {
                    "Comment": "kernel-largesmp is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-largesmp-devel is earlier than 0:2.6.9-89.0.3.EL"
                  },
                  {
                    "Comment": "kernel-largesmp-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-smp is earlier than 0:2.6.9-89.0.3.EL"
                  },
                  {
                    "Comment": "kernel-smp is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-smp-devel is earlier than 0:2.6.9-89.0.3.EL"
                  },
                  {
                    "Comment": "kernel-smp-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xenU is earlier than 0:2.6.9-89.0.3.EL"
                  },
                  {
                    "Comment": "kernel-xenU is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xenU-devel is earlier than 0:2.6.9-89.0.3.EL"
                  },
                  {
                    "Comment": "kernel-xenU-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 4 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 4 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2009-1072"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2009-1192"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2009-1385"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2009-1630"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2009-1758"
    }
  ],
  "Issued": {
    "Date": "2009-06-30"
  }
}