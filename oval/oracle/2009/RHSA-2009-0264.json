{
  "Title": "RHSA-2009:0264: kernel security update (Important)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux\noperating system.\nThis update addresses the following security issues:\n* a memory leak in keyctl handling. A local user could use this flaw to\ndeplete kernel memory, eventually leading to a denial of service. \n(CVE-2009-0031, Important)\n* a buffer overflow in the Linux kernel Partial Reliable Stream Control\nTransmission Protocol (PR-SCTP) implementation. This could, potentially,\nlead to a denial of service if a Forward-TSN chunk is received with a large\nstream ID. (CVE-2009-0065, Important)\n* a flaw when handling heavy network traffic on an SMP system with many\ncores. An attacker who could send a large amount of network traffic could\ncreate a denial of service. (CVE-2008-5713, Important)\n* the code for the HFS and HFS Plus (HFS+) file systems failed to properly\nhandle corrupted data structures. This could, potentially, lead to a local\ndenial of service. (CVE-2008-4933, CVE-2008-5025, Low)\n* a flaw was found in the HFS Plus (HFS+) file system implementation. This\ncould, potentially, lead to a local denial of service when write operations\nare performed. (CVE-2008-4934, Low)\nIn addition, these updated packages fix the following bugs:\n* when using the nfsd daemon in a clustered setup, kernel panics appeared\nseemingly at random. These panics were caused by a race condition in\nthe device-mapper mirror target. \n* the clock_gettime(CLOCK_THREAD_CPUTIME_ID, ) syscall returned a smaller\ntimespec value than the result of previous clock_gettime() function\nexecution, which resulted in a negative, and nonsensical, elapsed time value.\n* nfs_create_rpc_client was called with a \"flavor\" parameter which was\nusually ignored and ended up unconditionally creating the RPC client with\nan AUTH_UNIX flavor. This caused problems on AUTH_GSS mounts when the\ncredentials needed to be refreshed. The credops did not match the\nauthorization type, which resulted in the credops dereferencing an\nincorrect part of the AUTH_UNIX rpc_auth struct.\n* when copy_user_c terminated prematurely due to reading beyond the end of\nthe user buffer and the kernel jumped to the exception table entry, the rsi\nregister was not cleared. This resulted in exiting back to user code with\ngarbage in the rsi register.\n* the hexdump data in s390dbf traces was incomplete. The length of the data\ntraced was incorrect and the SAN payload was read from a different place\nthen it was written to.\n* when using connected mode (CM) in IPoIB on ehca2 hardware, it was not\npossible to transmit any data.\n* when an application called fork() and pthread_create() many times and, at\nsome point, a thread forked a child and then attempted to call the\nsetpgid() function, then this function failed and returned and ESRCH error\nvalue.\nUsers should upgrade to these updated packages, which contain backported\npatches to correct these issues. Note: for this update to take effect, the\nsystem must be rebooted.",
  "Platform": [
    "Red Hat Enterprise Linux 5"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2009:0264",
      "ID": "RHSA-2009:0264"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-4933",
      "ID": "CVE-2008-4933"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-4934",
      "ID": "CVE-2008-4934"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-5025",
      "ID": "CVE-2008-5025"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-5713",
      "ID": "CVE-2008-5713"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2009-0031",
      "ID": "CVE-2009-0031"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2009-0065",
      "ID": "CVE-2009-0065"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.18-128.1.1.el5 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.18-128.1.1.el5 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.18-128.1.1.el5"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-PAE is earlier than 0:2.6.18-128.1.1.el5"
                  },
                  {
                    "Comment": "kernel-PAE is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-PAE-devel is earlier than 0:2.6.18-128.1.1.el5"
                  },
                  {
                    "Comment": "kernel-PAE-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:2.6.18-128.1.1.el5"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:2.6.18-128.1.1.el5"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.18-128.1.1.el5"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.18-128.1.1.el5"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:2.6.18-128.1.1.el5"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:2.6.18-128.1.1.el5"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:2.6.18-128.1.1.el5"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xen is earlier than 0:2.6.18-128.1.1.el5"
                  },
                  {
                    "Comment": "kernel-xen is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xen-devel is earlier than 0:2.6.18-128.1.1.el5"
                  },
                  {
                    "Comment": "kernel-xen-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 5 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 5 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-4933"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-4934"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-5025"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-5713"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2009-0031"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2009-0065"
    }
  ],
  "Issued": {
    "Date": "2009-02-10"
  }
}