{
  "Title": "RHSA-2009:0429: cups security update (Important)",
  "Description": "The Common UNIX® Printing System (CUPS) provides a portable printing layer\nfor UNIX operating systems.\nMultiple integer overflow flaws were found in the CUPS JBIG2 decoder. An\nattacker could create a malicious PDF file that would cause CUPS to crash\nor, potentially, execute arbitrary code as the \"lp\" user if the file was\nprinted. (CVE-2009-0147, CVE-2009-1179)\nMultiple buffer overflow flaws were found in the CUPS JBIG2 decoder. An\nattacker could create a malicious PDF file that would cause CUPS to crash\nor, potentially, execute arbitrary code as the \"lp\" user if the file was\nprinted. (CVE-2009-0146, CVE-2009-1182)\nMultiple flaws were found in the CUPS JBIG2 decoder that could lead to the\nfreeing of arbitrary memory. An attacker could create a malicious PDF file\nthat would cause CUPS to crash or, potentially, execute arbitrary code\nas the \"lp\" user if the file was printed. (CVE-2009-0166, CVE-2009-1180)\nMultiple input validation flaws were found in the CUPS JBIG2 decoder. An\nattacker could create a malicious PDF file that would cause CUPS to crash\nor, potentially, execute arbitrary code as the \"lp\" user if the file was\nprinted. (CVE-2009-0800)\nAn integer overflow flaw, leading to a heap-based buffer overflow, was\ndiscovered in the Tagged Image File Format (TIFF) decoding routines used by\nthe CUPS image-converting filters, \"imagetops\" and \"imagetoraster\". An\nattacker could create a malicious TIFF file that could, potentially,\nexecute arbitrary code as the \"lp\" user if the file was printed.\n(CVE-2009-0163)\nMultiple denial of service flaws were found in the CUPS JBIG2 decoder. An\nattacker could create a malicious PDF file that would cause CUPS to crash\nwhen printed. (CVE-2009-0799, CVE-2009-1181, CVE-2009-1183)\nRed Hat would like to thank Aaron Sigel, Braden Thomas and Drew Yao of\nthe Apple Product Security team, and Will Dormann of the CERT/CC for\nresponsibly reporting these flaws.\nUsers of cups are advised to upgrade to these updated packages, which\ncontain backported patches to correct these issues. After installing the\nupdate, the cupsd daemon will be restarted automatically.",
  "Platform": [
    "Red Hat Enterprise Linux 4",
    "Red Hat Enterprise Linux 5"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2009:0429",
      "ID": "RHSA-2009:0429"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2009-0146",
      "ID": "CVE-2009-0146"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2009-0147",
      "ID": "CVE-2009-0147"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2009-0163",
      "ID": "CVE-2009-0163"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2009-0166",
      "ID": "CVE-2009-0166"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2009-0195",
      "ID": "CVE-2009-0195"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2009-0799",
      "ID": "CVE-2009-0799"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2009-0800",
      "ID": "CVE-2009-0800"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2009-1179",
      "ID": "CVE-2009-1179"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2009-1180",
      "ID": "CVE-2009-1180"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2009-1181",
      "ID": "CVE-2009-1181"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2009-1182",
      "ID": "CVE-2009-1182"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2009-1183",
      "ID": "CVE-2009-1183"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "cups is earlier than 1:1.1.22-0.rc1.9.27.el4_7.5"
                  },
                  {
                    "Comment": "cups is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "cups-devel is earlier than 1:1.1.22-0.rc1.9.27.el4_7.5"
                  },
                  {
                    "Comment": "cups-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "cups-libs is earlier than 1:1.1.22-0.rc1.9.27.el4_7.5"
                  },
                  {
                    "Comment": "cups-libs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 4 is installed"
          }
        ]
      },
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "cups is earlier than 1:1.3.7-8.el5_3.4"
                  },
                  {
                    "Comment": "cups is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "cups-devel is earlier than 1:1.3.7-8.el5_3.4"
                  },
                  {
                    "Comment": "cups-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "cups-libs is earlier than 1:1.3.7-8.el5_3.4"
                  },
                  {
                    "Comment": "cups-libs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "cups-lpd is earlier than 1:1.3.7-8.el5_3.4"
                  },
                  {
                    "Comment": "cups-lpd is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 5 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 4 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2009-0146"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2009-0147"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2009-0163"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2009-0166"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2009-0195"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2009-0799"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2009-0800"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2009-1179"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2009-1180"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2009-1181"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2009-1182"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2009-1183"
    }
  ],
  "Issued": {
    "Date": "2009-04-16"
  }
}