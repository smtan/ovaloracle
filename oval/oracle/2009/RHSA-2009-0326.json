{
  "Title": "RHSA-2009:0326: kernel security and bug fix update (Important)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux\noperating system.\nSecurity fixes:\n* memory leaks were found on some error paths in the icmp_send()\nfunction in the Linux kernel. This could, potentially, cause the network\nconnectivity to cease. (CVE-2009-0778, Important)\n* Chris Evans reported a deficiency in the clone() system call when called\nwith the CLONE_PARENT flag. This flaw permits the caller (the parent\nprocess) to indicate an arbitrary signal it wants to receive when its child\nprocess exits. This could lead to a denial of service of the parent\nprocess. (CVE-2009-0028, Moderate)\n* an off-by-one underflow flaw was found in the eCryptfs subsystem. This\ncould potentially cause a local denial of service when the readlink()\nfunction returned an error. (CVE-2009-0269, Moderate)\n* a deficiency was found in the Remote BIOS Update (RBU) driver for Dell\nsystems. This could allow a local, unprivileged user to cause a denial of\nservice by reading zero bytes from the image_type or packet_size files in\n\"/sys/devices/platform/dell_rbu/\". (CVE-2009-0322, Moderate)\n* an inverted logic flaw was found in the SysKonnect FDDI PCI adapter\ndriver, allowing driver statistics to be reset only when the CAP_NET_ADMIN\ncapability was absent (local, unprivileged users could reset driver\nstatistics). (CVE-2009-0675, Moderate)\n* the sock_getsockopt() function in the Linux kernel did not properly\ninitialize a data structure that can be directly returned to user-space\nwhen the getsockopt() function is called with SO_BSDCOMPAT optname set.\nThis flaw could possibly lead to memory disclosure.\n(CVE-2009-0676, Moderate)\n* the ext2 and ext3 file system code failed to properly handle corrupted\ndata structures, leading to a possible local denial of service when read\nor write operations were performed on a specially-crafted file system.\n(CVE-2008-3528, Low)\n* a deficiency was found in the libATA implementation. This could,\npotentially, lead to a local denial of service. Note: by default, the\n\"/dev/sg*\" devices are accessible only to the root user.\n(CVE-2008-5700, Low)\nBug fixes:\n* a bug in aic94xx may have caused kernel panics during boot on some\nsystems with certain SATA disks. (BZ#485909)\n* a word endianness problem in the qla2xx driver on PowerPC-based machines\nmay have corrupted flash-based devices. (BZ#485908)\n* a memory leak in pipe() may have caused a system deadlock. The workaround\nin Section 1.5, Known Issues, of the Red Hat Enterprise Linux 5.3 Release\nNotes Updates, which involved manually allocating extra file descriptors to\nprocesses calling do_pipe, is no longer necessary. (BZ#481576)\n* CPU soft-lockups in the network rate estimator. (BZ#481746)\n* bugs in the ixgbe driver caused it to function unreliably on some\nsystems with 16 or more CPU cores. (BZ#483210)\n* the iwl4965 driver may have caused a kernel panic. (BZ#483206)\n* a bug caused NFS attributes to not update for some long-lived NFS\nmounted file systems. (BZ#483201)\n* unmounting a GFS2 file system may have caused a panic. (BZ#485910)\n* a bug in ptrace() may have caused a panic when single stepping a target.\n(BZ#487394)\n* on some 64-bit systems, notsc was incorrectly set at boot, causing slow\ngettimeofday() calls. (BZ#488239)\n* do_machine_check() cleared all Machine Check Exception (MCE) status\nregisters, preventing the BIOS from using them to determine the cause of\ncertain panics and errors. (BZ#490433)\n* scaling problems caused performance problems for LAPI applications.\n(BZ#489457)\n* a panic may have occurred on systems using certain Intel WiFi Link 5000\nproducts when booting with the RF Kill switch on. (BZ#489846)\n* the TSC is invariant with C/P/T states, and always runs at constant\nfrequency from now on. (BZ#489310)\nAll users should upgrade to these updated packages, which contain\nbackported patches to correct these issues. The system must be rebooted for\nthis update to take effect.",
  "Platform": [
    "Red Hat Enterprise Linux 5"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2009:0326",
      "ID": "RHSA-2009:0326"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-3528",
      "ID": "CVE-2008-3528"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-5700",
      "ID": "CVE-2008-5700"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2009-0028",
      "ID": "CVE-2009-0028"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2009-0269",
      "ID": "CVE-2009-0269"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2009-0322",
      "ID": "CVE-2009-0322"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2009-0675",
      "ID": "CVE-2009-0675"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2009-0676",
      "ID": "CVE-2009-0676"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2009-0778",
      "ID": "CVE-2009-0778"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.18-128.1.6.el5 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.18-128.1.6.el5 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.18-128.1.6.el5"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-PAE is earlier than 0:2.6.18-128.1.6.el5"
                  },
                  {
                    "Comment": "kernel-PAE is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-PAE-devel is earlier than 0:2.6.18-128.1.6.el5"
                  },
                  {
                    "Comment": "kernel-PAE-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:2.6.18-128.1.6.el5"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:2.6.18-128.1.6.el5"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.18-128.1.6.el5"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.18-128.1.6.el5"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:2.6.18-128.1.6.el5"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:2.6.18-128.1.6.el5"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:2.6.18-128.1.6.el5"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xen is earlier than 0:2.6.18-128.1.6.el5"
                  },
                  {
                    "Comment": "kernel-xen is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xen-devel is earlier than 0:2.6.18-128.1.6.el5"
                  },
                  {
                    "Comment": "kernel-xen-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 5 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 5 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-3528"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-5700"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2009-0028"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2009-0269"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2009-0322"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2009-0675"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2009-0676"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2009-0778"
    }
  ],
  "Issued": {
    "Date": "2009-04-01"
  }
}