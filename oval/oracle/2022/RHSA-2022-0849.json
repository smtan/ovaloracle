{
  "Title": "RHSA-2022:0849: kpatch-patch security update (Important)",
  "Description": "This is a kernel live patch module which is automatically loaded by the RPM post-install script to modify the code of a running kernel.\nSecurity Fix(es):\n* kernel: Use After Free in unix_gc() which could result in a local privilege escalation (CVE-2021-0920)\n* kernel: local privilege escalation by exploiting the fsconfig syscall parameter leads to container breakout (CVE-2021-4154)\n* kernel: possible privileges escalation due to missing TLB flush (CVE-2022-0330)\n* kernel: remote stack overflow via kernel panic on systems using TIPC may lead to DoS (CVE-2022-0435)\n* kernel: cgroups v1 release_agent feature may allow privilege escalation (CVE-2022-0492)\n* kernel: failing usercopy allows for use-after-free exploitation (CVE-2022-22942)\nFor more details about the security issue(s), including the impact, a CVSS score, acknowledgments, and other related information, refer to the CVE page(s) listed in the References section.",
  "Platform": [
    "Red Hat Enterprise Linux 8"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2022:0849",
      "ID": "RHSA-2022:0849"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2021-0920",
      "ID": "CVE-2021-0920"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2021-4154",
      "ID": "CVE-2021-4154"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2022-0330",
      "ID": "CVE-2022-0330"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2022-0435",
      "ID": "CVE-2022-0435"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2022-0492",
      "ID": "CVE-2022-0492"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2022-22942",
      "ID": "CVE-2022-22942"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "Oracle Linux 8 is installed"
              },
              {
                "Comment": "Red Hat CoreOS 4 is installed"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [
                  {
                    "Operator": "OR",
                    "Criterias": [

                    ],
                    "Criterions": [
                      {
                        "Comment": "kernel version 0:4.18.0-348.el8 is currently running"
                      },
                      {
                        "Comment": "kernel version 0:4.18.0-348.el8 is set to boot up on next boot"
                      }
                    ]
                  },
                  {
                    "Operator": "AND",
                    "Criterias": [
                      {
                        "Operator": "OR",
                        "Criterias": [
                          {
                            "Operator": "AND",
                            "Criterias": [

                            ],
                            "Criterions": [
                              {
                                "Comment": "kpatch-patch-4_18_0-348 is earlier than 0:1-3.el8"
                              },
                              {
                                "Comment": "kpatch-patch-4_18_0-348 is signed with Red Hat redhatrelease2 key"
                              }
                            ]
                          }
                        ],
                        "Criterions": [
                          {
                            "Comment": "kpatch-patch not installed for 0:4.18.0-348.el8"
                          }
                        ]
                      }
                    ],
                    "Criterions": [
                      {
                        "Comment": "kernel version equals 0:4.18.0-348.el8"
                      },
                      {
                        "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                      }
                    ]
                  }
                ],
                "Criterions": [

                ]
              },
              {
                "Operator": "AND",
                "Criterias": [
                  {
                    "Operator": "OR",
                    "Criterias": [

                    ],
                    "Criterions": [
                      {
                        "Comment": "kernel version 0:4.18.0-348.2.1.el8_5 is currently running"
                      },
                      {
                        "Comment": "kernel version 0:4.18.0-348.2.1.el8_5 is set to boot up on next boot"
                      }
                    ]
                  },
                  {
                    "Operator": "AND",
                    "Criterias": [
                      {
                        "Operator": "OR",
                        "Criterias": [
                          {
                            "Operator": "AND",
                            "Criterias": [

                            ],
                            "Criterions": [
                              {
                                "Comment": "kpatch-patch-4_18_0-348_2_1 is earlier than 0:1-2.el8_5"
                              },
                              {
                                "Comment": "kpatch-patch-4_18_0-348_2_1 is signed with Red Hat redhatrelease2 key"
                              }
                            ]
                          }
                        ],
                        "Criterions": [
                          {
                            "Comment": "kpatch-patch not installed for 0:4.18.0-348.2.1.el8_5"
                          }
                        ]
                      }
                    ],
                    "Criterions": [
                      {
                        "Comment": "kernel version equals 0:4.18.0-348.2.1.el8_5"
                      },
                      {
                        "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                      }
                    ]
                  }
                ],
                "Criterions": [

                ]
              },
              {
                "Operator": "AND",
                "Criterias": [
                  {
                    "Operator": "OR",
                    "Criterias": [

                    ],
                    "Criterions": [
                      {
                        "Comment": "kernel version 0:4.18.0-348.7.1.el8_5 is currently running"
                      },
                      {
                        "Comment": "kernel version 0:4.18.0-348.7.1.el8_5 is set to boot up on next boot"
                      }
                    ]
                  },
                  {
                    "Operator": "AND",
                    "Criterias": [
                      {
                        "Operator": "OR",
                        "Criterias": [
                          {
                            "Operator": "AND",
                            "Criterias": [

                            ],
                            "Criterions": [
                              {
                                "Comment": "kpatch-patch-4_18_0-348_7_1 is earlier than 0:1-2.el8_5"
                              },
                              {
                                "Comment": "kpatch-patch-4_18_0-348_7_1 is signed with Red Hat redhatrelease2 key"
                              }
                            ]
                          }
                        ],
                        "Criterions": [
                          {
                            "Comment": "kpatch-patch not installed for 0:4.18.0-348.7.1.el8_5"
                          }
                        ]
                      }
                    ],
                    "Criterions": [
                      {
                        "Comment": "kernel version equals 0:4.18.0-348.7.1.el8_5"
                      },
                      {
                        "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                      }
                    ]
                  }
                ],
                "Criterions": [

                ]
              },
              {
                "Operator": "AND",
                "Criterias": [
                  {
                    "Operator": "OR",
                    "Criterias": [

                    ],
                    "Criterions": [
                      {
                        "Comment": "kernel version 0:4.18.0-348.12.2.el8_5 is currently running"
                      },
                      {
                        "Comment": "kernel version 0:4.18.0-348.12.2.el8_5 is set to boot up on next boot"
                      }
                    ]
                  },
                  {
                    "Operator": "AND",
                    "Criterias": [
                      {
                        "Operator": "OR",
                        "Criterias": [
                          {
                            "Operator": "AND",
                            "Criterias": [

                            ],
                            "Criterions": [
                              {
                                "Comment": "kpatch-patch-4_18_0-348_12_2 is earlier than 0:1-1.el8_5"
                              },
                              {
                                "Comment": "kpatch-patch-4_18_0-348_12_2 is signed with Red Hat redhatrelease2 key"
                              }
                            ]
                          }
                        ],
                        "Criterions": [
                          {
                            "Comment": "kpatch-patch not installed for 0:4.18.0-348.12.2.el8_5"
                          }
                        ]
                      }
                    ],
                    "Criterions": [
                      {
                        "Comment": "kernel version equals 0:4.18.0-348.12.2.el8_5"
                      },
                      {
                        "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                      }
                    ]
                  }
                ],
                "Criterions": [

                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [

        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 8 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2021-0920"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2021-4154"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2022-0330"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2022-0435"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2022-0492"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2022-22942"
    }
  ],
  "Issued": {
    "Date": "2022-03-14"
  }
}