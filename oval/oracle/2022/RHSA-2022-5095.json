{
  "Title": "RHSA-2022:5095: grub2, mokutil, shim, and shim-unsigned-x64 security update (Important)",
  "Description": "The grub2 packages provide version 2 of the Grand Unified Boot Loader (GRUB), a highly configurable and customizable boot loader with modular architecture. The packages support a variety of kernel formats, file systems, computer architectures, and hardware devices.\nThe shim package contains a first-stage UEFI boot loader that handles chaining to a trusted full boot loader under secure boot environments.\nSecurity Fix(es):\n* grub2: Integer underflow in grub_net_recv_ip4_packets (CVE-2022-28733)\n* grub2: Crafted PNG grayscale images may lead to out-of-bounds write in heap (CVE-2021-3695)\n* grub2: Crafted PNG image may lead to out-of-bound write during huffman table handling (CVE-2021-3696)\n* grub2: Crafted JPEG image can lead to buffer underflow write in the heap (CVE-2021-3697)\n* grub2: Out-of-bound write when handling split HTTP headers (CVE-2022-28734)\n* grub2: shim_lock verifier allows non-kernel files to be loaded (CVE-2022-28735)\n* grub2: use-after-free in grub_cmd_chainloader() (CVE-2022-28736)\n* shim: Buffer overflow when loading crafted EFI images (CVE-2022-28737)\nFor more details about the security issue(s), including the impact, a CVSS score, acknowledgments, and other related information, refer to the CVE page(s) listed in the References section.",
  "Platform": [
    "Red Hat Enterprise Linux 8"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2022:5095",
      "ID": "RHSA-2022:5095"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2021-3695",
      "ID": "CVE-2021-3695"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2021-3696",
      "ID": "CVE-2021-3696"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2021-3697",
      "ID": "CVE-2021-3697"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2022-28733",
      "ID": "CVE-2022-28733"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2022-28734",
      "ID": "CVE-2022-28734"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2022-28735",
      "ID": "CVE-2022-28735"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2022-28736",
      "ID": "CVE-2022-28736"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2022-28737",
      "ID": "CVE-2022-28737"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "Oracle Linux 8 is installed"
              },
              {
                "Comment": "Red Hat CoreOS 4 is installed"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "grub2-common is earlier than 1:2.02-123.el8_6.8"
                  },
                  {
                    "Comment": "grub2-common is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "grub2-efi-aa64 is earlier than 1:2.02-123.el8_6.8"
                  },
                  {
                    "Comment": "grub2-efi-aa64 is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "grub2-efi-aa64-cdboot is earlier than 1:2.02-123.el8_6.8"
                  },
                  {
                    "Comment": "grub2-efi-aa64-cdboot is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "grub2-efi-aa64-modules is earlier than 1:2.02-123.el8_6.8"
                  },
                  {
                    "Comment": "grub2-efi-aa64-modules is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "grub2-efi-ia32 is earlier than 1:2.02-123.el8_6.8"
                  },
                  {
                    "Comment": "grub2-efi-ia32 is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "grub2-efi-ia32-cdboot is earlier than 1:2.02-123.el8_6.8"
                  },
                  {
                    "Comment": "grub2-efi-ia32-cdboot is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "grub2-efi-ia32-modules is earlier than 1:2.02-123.el8_6.8"
                  },
                  {
                    "Comment": "grub2-efi-ia32-modules is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "grub2-efi-x64 is earlier than 1:2.02-123.el8_6.8"
                  },
                  {
                    "Comment": "grub2-efi-x64 is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "grub2-efi-x64-cdboot is earlier than 1:2.02-123.el8_6.8"
                  },
                  {
                    "Comment": "grub2-efi-x64-cdboot is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "grub2-efi-x64-modules is earlier than 1:2.02-123.el8_6.8"
                  },
                  {
                    "Comment": "grub2-efi-x64-modules is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "grub2-pc is earlier than 1:2.02-123.el8_6.8"
                  },
                  {
                    "Comment": "grub2-pc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "grub2-pc-modules is earlier than 1:2.02-123.el8_6.8"
                  },
                  {
                    "Comment": "grub2-pc-modules is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "grub2-ppc64le is earlier than 1:2.02-123.el8_6.8"
                  },
                  {
                    "Comment": "grub2-ppc64le is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "grub2-ppc64le-modules is earlier than 1:2.02-123.el8_6.8"
                  },
                  {
                    "Comment": "grub2-ppc64le-modules is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "grub2-tools is earlier than 1:2.02-123.el8_6.8"
                  },
                  {
                    "Comment": "grub2-tools is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "grub2-tools-efi is earlier than 1:2.02-123.el8_6.8"
                  },
                  {
                    "Comment": "grub2-tools-efi is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "grub2-tools-extra is earlier than 1:2.02-123.el8_6.8"
                  },
                  {
                    "Comment": "grub2-tools-extra is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "grub2-tools-minimal is earlier than 1:2.02-123.el8_6.8"
                  },
                  {
                    "Comment": "grub2-tools-minimal is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "shim-unsigned-x64 is earlier than 0:15.6-1.el8"
                  },
                  {
                    "Comment": "shim-unsigned-x64 is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "shim-aa64 is earlier than 0:15.6-1.el8"
                  },
                  {
                    "Comment": "shim-aa64 is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "shim-ia32 is earlier than 0:15.6-1.el8"
                  },
                  {
                    "Comment": "shim-ia32 is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "shim-x64 is earlier than 0:15.6-1.el8"
                  },
                  {
                    "Comment": "shim-x64 is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [

        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 8 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2021-3695"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2021-3696"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2021-3697"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2022-28733"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2022-28734"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2022-28735"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2022-28736"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2022-28737"
    }
  ],
  "Issued": {
    "Date": "2022-06-16"
  }
}