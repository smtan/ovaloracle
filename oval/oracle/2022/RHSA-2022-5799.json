{
  "Title": "RHSA-2022:5799: go-toolset and golang security and bug fix update (Important)",
  "Description": "Go Toolset provides the Go programming language tools and libraries. Go is alternatively known as golang. \nThe golang packages provide the Go programming language compiler.\nSecurity Fix(es):\n* golang: compress/gzip: stack exhaustion in Reader.Read (CVE-2022-30631)\n* golang: net/http: improper sanitization of Transfer-Encoding header (CVE-2022-1705)\n* golang: go/parser: stack exhaustion in all Parse* functions (CVE-2022-1962)\n* golang: encoding/xml: stack exhaustion in Decoder.Skip (CVE-2022-28131)\n* golang: io/fs: stack exhaustion in Glob (CVE-2022-30630)\n* golang: path/filepath: stack exhaustion in Glob (CVE-2022-30632)\n* golang: encoding/xml: stack exhaustion in Unmarshal (CVE-2022-30633)\n* golang: encoding/gob: stack exhaustion in Decoder.Decode (CVE-2022-30635)\n* golang: net/http/httputil: NewSingleHostReverseProxy - omit X-Forwarded-For not working (CVE-2022-32148)\nFor more details about the security issue(s), including the impact, a CVSS score, acknowledgments, and other related information, refer to the CVE page(s) listed in the References section.\nBug Fix(es):\n* Clean up dist-git patches (BZ#2109174)\n* Update Go to version 1.17.12 (BZ#2109183)",
  "Platform": [
    "Red Hat Enterprise Linux 9"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2022:5799",
      "ID": "RHSA-2022:5799"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2022-1705",
      "ID": "CVE-2022-1705"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2022-1962",
      "ID": "CVE-2022-1962"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2022-24675",
      "ID": "CVE-2022-24675"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2022-24921",
      "ID": "CVE-2022-24921"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2022-28131",
      "ID": "CVE-2022-28131"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2022-28327",
      "ID": "CVE-2022-28327"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2022-29526",
      "ID": "CVE-2022-29526"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2022-30629",
      "ID": "CVE-2022-30629"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2022-30630",
      "ID": "CVE-2022-30630"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2022-30631",
      "ID": "CVE-2022-30631"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2022-30632",
      "ID": "CVE-2022-30632"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2022-30633",
      "ID": "CVE-2022-30633"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2022-30635",
      "ID": "CVE-2022-30635"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2022-32148",
      "ID": "CVE-2022-32148"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "golang is earlier than 0:1.17.12-1.el9_0"
                  },
                  {
                    "Comment": "golang is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "golang-bin is earlier than 0:1.17.12-1.el9_0"
                  },
                  {
                    "Comment": "golang-bin is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "golang-docs is earlier than 0:1.17.12-1.el9_0"
                  },
                  {
                    "Comment": "golang-docs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "golang-misc is earlier than 0:1.17.12-1.el9_0"
                  },
                  {
                    "Comment": "golang-misc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "golang-race is earlier than 0:1.17.12-1.el9_0"
                  },
                  {
                    "Comment": "golang-race is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "golang-src is earlier than 0:1.17.12-1.el9_0"
                  },
                  {
                    "Comment": "golang-src is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "golang-tests is earlier than 0:1.17.12-1.el9_0"
                  },
                  {
                    "Comment": "golang-tests is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "go-toolset is earlier than 0:1.17.12-1.el9_0"
                  },
                  {
                    "Comment": "go-toolset is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 9 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 9 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2022-1705"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2022-1962"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2022-24675"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2022-24921"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2022-28131"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2022-28327"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2022-29526"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2022-30629"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2022-30630"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2022-30631"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2022-30632"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2022-30633"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2022-30635"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2022-32148"
    }
  ],
  "Issued": {
    "Date": "2022-08-01"
  }
}