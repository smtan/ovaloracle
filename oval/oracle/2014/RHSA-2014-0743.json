{
  "Title": "RHSA-2014:0743: qemu-kvm security and bug fix update (Moderate)",
  "Description": "KVM (Kernel-based Virtual Machine) is a full virtualization solution for\nLinux on AMD64 and Intel 64 systems. The qemu-kvm package provides the\nuser-space component for running virtual machines using KVM.\nMultiple buffer overflow, input validation, and out-of-bounds write flaws\nwere found in the way the virtio, virtio-net, virtio-scsi, and usb drivers\nof QEMU handled state loading after migration. A user able to alter the\nsavevm data (either on the disk or over the wire during migration) could\nuse either of these flaws to corrupt QEMU process memory on the\n(destination) host, which could potentially result in arbitrary code\nexecution on the host with the privileges of the QEMU process.\n(CVE-2013-4148, CVE-2013-4151, CVE-2013-4535, CVE-2013-4536, CVE-2013-4541,\nCVE-2013-4542, CVE-2013-6399, CVE-2014-0182, CVE-2014-3461)\nAn out-of-bounds memory access flaw was found in the way QEMU's IDE device\ndriver handled the execution of SMART EXECUTE OFFLINE commands.\nA privileged guest user could use this flaw to corrupt QEMU process memory\non the host, which could potentially result in arbitrary code execution on\nthe host with the privileges of the QEMU process. (CVE-2014-2894)\nThe CVE-2013-4148, CVE-2013-4151, CVE-2013-4535, CVE-2013-4536,\nCVE-2013-4541, CVE-2013-4542, CVE-2013-6399, CVE-2014-0182, and\nCVE-2014-3461 issues were discovered by Michael S. Tsirkin of Red Hat,\nAnthony Liguori, and Michael Roth.\nThis update also fixes the following bugs:\n* Previously, under certain circumstances, libvirt failed to start guests\nwhich used a non-zero PCI domain and SR-IOV Virtual Functions (VFs), and\nreturned the following error message:\nCan't assign device inside non-zero PCI segment as this KVM module doesn't\nsupport it.\nThis update fixes this issue and guests using the aforementioned\nconfiguration no longer fail to start. (BZ#1099941)\n* Due to an incorrect initialization of the cpus_sts bitmap, which holds\nthe enablement status of a vCPU, libvirt could fail to start a guest with\nan unusual vCPU topology (for example, a guest with three cores and two\nsockets). With this update, the initialization of cpus_sts has been\ncorrected, and libvirt no longer fails to start the aforementioned guests.\n(BZ#1100575)\nAll qemu-kvm users are advised to upgrade to these updated packages, which\ncontain backported patches to correct these issues. After installing this\nupdate, shut down all running virtual machines. Once all virtual machines\nhave shut down, start them again for this update to take effect.",
  "Platform": [
    "Red Hat Enterprise Linux 6"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2014:0743",
      "ID": "RHSA-2014:0743"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-4148",
      "ID": "CVE-2013-4148"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-4151",
      "ID": "CVE-2013-4151"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-4535",
      "ID": "CVE-2013-4535"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-4536",
      "ID": "CVE-2013-4536"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-4541",
      "ID": "CVE-2013-4541"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-4542",
      "ID": "CVE-2013-4542"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-6399",
      "ID": "CVE-2013-6399"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-0182",
      "ID": "CVE-2014-0182"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-2894",
      "ID": "CVE-2014-2894"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-3461",
      "ID": "CVE-2014-3461"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "qemu-guest-agent is earlier than 2:0.12.1.2-2.415.el6_5.10"
                  },
                  {
                    "Comment": "qemu-guest-agent is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "qemu-img is earlier than 2:0.12.1.2-2.415.el6_5.10"
                  },
                  {
                    "Comment": "qemu-img is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "qemu-kvm is earlier than 2:0.12.1.2-2.415.el6_5.10"
                  },
                  {
                    "Comment": "qemu-kvm is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "qemu-kvm-tools is earlier than 2:0.12.1.2-2.415.el6_5.10"
                  },
                  {
                    "Comment": "qemu-kvm-tools is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-4148"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-4151"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-4535"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-4536"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-4541"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-4542"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-6399"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-0182"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-2894"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-3461"
    }
  ],
  "Issued": {
    "Date": "2014-06-10"
  }
}