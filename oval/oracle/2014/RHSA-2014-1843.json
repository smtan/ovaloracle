{
  "Title": "RHSA-2014:1843: kernel security and bug fix update (Important)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux\noperating system.\n* A race condition flaw was found in the way the Linux kernel's KVM\nsubsystem handled PIT (Programmable Interval Timer) emulation. A guest user\nwho has access to the PIT I/O ports could use this flaw to crash the host.\n(CVE-2014-3611, Important)\n* A memory corruption flaw was found in the way the USB ConnectTech\nWhiteHEAT serial driver processed completion commands sent via USB Request\nBlocks buffers. An attacker with physical access to the system could use\nthis flaw to crash the system or, potentially, escalate their privileges on\nthe system. (CVE-2014-3185, Moderate)\n* It was found that the Linux kernel's KVM subsystem did not handle the VM\nexits gracefully for the invept (Invalidate Translations Derived from EPT)\nand invvpid (Invalidate Translations Based on VPID) instructions. On hosts\nwith an Intel processor and invept/invppid VM exit support, an unprivileged\nguest user could use these instructions to crash the guest. (CVE-2014-3645,\nCVE-2014-3646, Moderate)\nRed Hat would like to thank Lars Bull of Google for reporting\nCVE-2014-3611, and the Advanced Threat Research team at Intel Security for\nreporting CVE-2014-3645 and CVE-2014-3646.\nThis update also fixes the following bugs:\n* This update fixes several race conditions between PCI error recovery\ncallbacks and potential calls of the ifup and ifdown commands in the tg3\ndriver. When triggered, these race conditions could cause a kernel crash.\n(BZ#1142570)\n* Previously, GFS2 failed to unmount a sub-mounted GFS2 file system if its\nparent was also a GFS2 file system. This problem has been fixed by adding\nthe appropriate d_op->d_hash() routine call for the last component of the\nmount point path in the path name lookup mechanism code (namei).\n(BZ#1145193)\n* Due to previous changes in the virtio-net driver, a Red Hat Enterprise\nLinux 6.6 guest was unable to boot with the \"mgr_rxbuf=off\" option\nspecified. This was caused by providing the page_to_skb() function with an\nincorrect packet length in the driver's Rx path. This problem has been\nfixed and the guest in the described scenario can now boot successfully.\n(BZ#1148693)\n* When using one of the newer IPSec Authentication Header (AH) algorithms\nwith Openswan, a kernel panic could occur. This happened because the\nmaximum truncated ICV length was too small. To fix this problem, the\nMAX_AH_AUTH_LEN parameter has been set to 64. (BZ#1149083)\n* A bug in the IPMI driver caused the kernel to panic when an IPMI\ninterface was removed using the hotmod script. The IPMI driver has been\nfixed to properly clean the relevant data when removing an IPMI interface.\n(BZ#1149578)\n* Due to a bug in the IPMI driver, the kernel could panic when adding an\nIPMI interface that was previously removed using the hotmod script.\nThis update fixes this bug by ensuring that the relevant shadow structure\nis initialized at the right time. (BZ#1149580)\nAll kernel users are advised to upgrade to these updated packages, which\ncontain backported patches to correct these issues. The system must be\nrebooted for this update to take effect.",
  "Platform": [
    "Red Hat Enterprise Linux 6"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2014:1843",
      "ID": "RHSA-2014:1843"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-3185",
      "ID": "CVE-2014-3185"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-3611",
      "ID": "CVE-2014-3611"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-3645",
      "ID": "CVE-2014-3645"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-3646",
      "ID": "CVE-2014-3646"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.32-504.1.3.el6 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.32-504.1.3.el6 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.32-504.1.3.el6"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-abi-whitelists is earlier than 0:2.6.32-504.1.3.el6"
                  },
                  {
                    "Comment": "kernel-abi-whitelists is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-bootwrapper is earlier than 0:2.6.32-504.1.3.el6"
                  },
                  {
                    "Comment": "kernel-bootwrapper is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:2.6.32-504.1.3.el6"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:2.6.32-504.1.3.el6"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.32-504.1.3.el6"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.32-504.1.3.el6"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-firmware is earlier than 0:2.6.32-504.1.3.el6"
                  },
                  {
                    "Comment": "kernel-firmware is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:2.6.32-504.1.3.el6"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:2.6.32-504.1.3.el6"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:2.6.32-504.1.3.el6"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perf is earlier than 0:2.6.32-504.1.3.el6"
                  },
                  {
                    "Comment": "perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "python-perf is earlier than 0:2.6.32-504.1.3.el6"
                  },
                  {
                    "Comment": "python-perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-3185"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-3611"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-3645"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-3646"
    }
  ],
  "Issued": {
    "Date": "2014-11-11"
  }
}