{
  "Title": "RHSA-2014:1997: kernel security and bug fix update (Important)",
  "Description": "* A flaw was found in the way the Linux kernel handled GS segment register\nbase switching when recovering from a #SS (stack segment) fault on an\nerroneous return to user space. A local, unprivileged user could use this\nflaw to escalate their privileges on the system. (CVE-2014-9322, Important)\n* A flaw was found in the way the Linux kernel's SCTP implementation\nhandled malformed or duplicate Address Configuration Change Chunks\n(ASCONF). A remote attacker could use either of these flaws to crash the\nsystem. (CVE-2014-3673, CVE-2014-3687, Important)\n* A flaw was found in the way the Linux kernel's SCTP implementation\nhandled the association's output queue. A remote attacker could send\nspecially crafted packets that would cause the system to use an excessive\namount of memory, leading to a denial of service. (CVE-2014-3688,\nImportant)\n* A stack overflow flaw caused by infinite recursion was found in the way\nthe Linux kernel's UDF file system implementation processed indirect ICBs.\nAn attacker with physical access to the system could use a specially\ncrafted UDF image to crash the system. (CVE-2014-6410, Low)\n* It was found that the Linux kernel's networking implementation did not\ncorrectly handle the setting of the keepalive socket option on raw sockets.\nA local user able to create a raw socket could use this flaw to crash the\nsystem. (CVE-2012-6657, Low)\n* It was found that the parse_rock_ridge_inode_internal() function of the\nLinux kernel's ISOFS implementation did not correctly check relocated\ndirectories when processing Rock Ridge child link (CL) tags. An attacker\nwith physical access to the system could use a specially crafted ISO image\nto crash the system or, potentially, escalate their privileges on the\nsystem. (CVE-2014-5471, CVE-2014-5472, Low)\nRed Hat would like to thank Andy Lutomirski for reporting CVE-2014-9322.\nThe CVE-2014-3673 issue was discovered by Liu Wei of Red Hat.\nBug fixes:\n* This update fixes a race condition issue between the sock_queue_err_skb\nfunction and sk_forward_alloc handling in the socket error queue\n(MSG_ERRQUEUE), which could occasionally cause the kernel, for example when\nusing PTP, to incorrectly track allocated memory for the error queue, in\nwhich case a traceback would occur in the system log. (BZ#1155427)\n* The zcrypt device driver did not detect certain crypto cards and the\nrelated domains for crypto adapters on System z and s390x architectures.\nConsequently, it was not possible to run the system on new crypto hardware.\nThis update enables toleration mode for such devices so that the system\ncan make use of newer crypto hardware. (BZ#1158311)\n* After mounting and unmounting an XFS file system several times\nconsecutively, the umount command occasionally became unresponsive.\nThis was caused by the xlog_cil_force_lsn() function that was not waiting\nfor completion as expected. With this update, xlog_cil_force_lsn() has been\nmodified to correctly wait for completion, thus fixing this bug.\n(BZ#1158325)\n* When using the ixgbe adapter with disabled LRO and the tx-usec or rs-usec\nvariables set to 0, transmit interrupts could not be set lower than the\ndefault of 8 buffered tx frames. Consequently, a delay of TCP transfer\noccurred. The restriction of a minimum of 8 buffered frames has been\nremoved, and the TCP delay no longer occurs. (BZ#1158326)\n* The offb driver has been updated for the QEMU standard VGA adapter,\nfixing an incorrect displaying of colors issue. (BZ#1158328)\n* Under certain circumstances, when a discovered MTU expired, the IPv6\nconnection became unavailable for a short period of time. This bug has been\nfixed, and the connection now works as expected. (BZ#1161418)\n* A low throughput occurred when using the dm-thin driver to write to\nunprovisioned or shared chunks for a thin pool with the chunk size bigger\nthan the max_sectors_kb variable. (BZ#1161420)\n* Large write workloads on thin LVs could cause the iozone and smallfile\nutilities to terminate unexpectedly. (BZ#1161421)",
  "Platform": [
    "Red Hat Enterprise Linux 6"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2014:1997",
      "ID": "RHSA-2014:1997"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-6657",
      "ID": "CVE-2012-6657"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-3673",
      "ID": "CVE-2014-3673"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-3687",
      "ID": "CVE-2014-3687"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-3688",
      "ID": "CVE-2014-3688"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-5471",
      "ID": "CVE-2014-5471"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-5472",
      "ID": "CVE-2014-5472"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-6410",
      "ID": "CVE-2014-6410"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-9322",
      "ID": "CVE-2014-9322"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.32-504.3.3.el6 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.32-504.3.3.el6 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.32-504.3.3.el6"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-abi-whitelists is earlier than 0:2.6.32-504.3.3.el6"
                  },
                  {
                    "Comment": "kernel-abi-whitelists is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-bootwrapper is earlier than 0:2.6.32-504.3.3.el6"
                  },
                  {
                    "Comment": "kernel-bootwrapper is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:2.6.32-504.3.3.el6"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:2.6.32-504.3.3.el6"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.32-504.3.3.el6"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.32-504.3.3.el6"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-firmware is earlier than 0:2.6.32-504.3.3.el6"
                  },
                  {
                    "Comment": "kernel-firmware is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:2.6.32-504.3.3.el6"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:2.6.32-504.3.3.el6"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:2.6.32-504.3.3.el6"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perf is earlier than 0:2.6.32-504.3.3.el6"
                  },
                  {
                    "Comment": "perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "python-perf is earlier than 0:2.6.32-504.3.3.el6"
                  },
                  {
                    "Comment": "python-perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-6657"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-3673"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-3687"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-3688"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-5471"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-5472"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-6410"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-9322"
    }
  ],
  "Issued": {
    "Date": "2014-12-16"
  }
}