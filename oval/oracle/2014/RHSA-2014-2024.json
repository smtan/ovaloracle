{
  "Title": "RHSA-2014:2024: ntp security update (Important)",
  "Description": "The Network Time Protocol (NTP) is used to synchronize a computer's time\nwith a referenced time source.\nMultiple buffer overflow flaws were discovered in ntpd's crypto_recv(),\nctl_putdata(), and configure() functions. A remote attacker could use\neither of these flaws to send a specially crafted request packet that could\ncrash ntpd or, potentially, execute arbitrary code with the privileges of\nthe ntp user. Note: the crypto_recv() flaw requires non-default\nconfigurations to be active, while the ctl_putdata() flaw, by default, can\nonly be exploited via local attackers, and the configure() flaw requires\nadditional authentication to exploit. (CVE-2014-9295)\nIt was found that ntpd automatically generated weak keys for its internal\nuse if no ntpdc request authentication key was specified in the ntp.conf\nconfiguration file. A remote attacker able to match the configured IP\nrestrictions could guess the generated key, and possibly use it to send\nntpdc query or configuration requests. (CVE-2014-9293)\nIt was found that ntp-keygen used a weak method for generating MD5 keys.\nThis could possibly allow an attacker to guess generated MD5 keys that\ncould then be used to spoof an NTP client or server. Note: it is\nrecommended to regenerate any MD5 keys that had explicitly been generated\nwith ntp-keygen; the default installation does not contain such keys).\n(CVE-2014-9294)\nA missing return statement in the receive() function could potentially\nallow a remote attacker to bypass NTP's authentication mechanism.\n(CVE-2014-9296)\nAll ntp users are advised to upgrade to this updated package, which\ncontains backported patches to resolve these issues. After installing the\nupdate, the ntpd daemon will restart automatically.",
  "Platform": [
    "Red Hat Enterprise Linux 6",
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2014:2024",
      "ID": "RHSA-2014:2024"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-9293",
      "ID": "CVE-2014-9293"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-9294",
      "ID": "CVE-2014-9294"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-9295",
      "ID": "CVE-2014-9295"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-9296",
      "ID": "CVE-2014-9296"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ntp is earlier than 0:4.2.6p5-19.el7_0"
                  },
                  {
                    "Comment": "ntp is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ntp-doc is earlier than 0:4.2.6p5-19.el7_0"
                  },
                  {
                    "Comment": "ntp-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ntp-perl is earlier than 0:4.2.6p5-19.el7_0"
                  },
                  {
                    "Comment": "ntp-perl is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ntpdate is earlier than 0:4.2.6p5-19.el7_0"
                  },
                  {
                    "Comment": "ntpdate is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "sntp is earlier than 0:4.2.6p5-19.el7_0"
                  },
                  {
                    "Comment": "sntp is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      },
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ntp is earlier than 0:4.2.6p5-2.el6_6"
                  },
                  {
                    "Comment": "ntp is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ntp-doc is earlier than 0:4.2.6p5-2.el6_6"
                  },
                  {
                    "Comment": "ntp-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ntp-perl is earlier than 0:4.2.6p5-2.el6_6"
                  },
                  {
                    "Comment": "ntp-perl is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ntpdate is earlier than 0:4.2.6p5-2.el6_6"
                  },
                  {
                    "Comment": "ntpdate is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-9293"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-9294"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-9295"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-9296"
    }
  ],
  "Issued": {
    "Date": "2014-12-20"
  }
}