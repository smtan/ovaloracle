{
  "Title": "RHSA-2014:0285: kernel security, bug fix, and enhancement update (Important)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux\noperating system.\n* A buffer overflow flaw was found in the way the qeth_snmp_command()\nfunction in the Linux kernel's QETH network device driver implementation\nhandled SNMP IOCTL requests with an out-of-bounds length. A local,\nunprivileged user could use this flaw to crash the system or, potentially,\nescalate their privileges on the system. (CVE-2013-6381, Important)\n* A flaw was found in the way the ipc_rcu_putref() function in the Linux\nkernel's IPC implementation handled reference counter decrementing.\nA local, unprivileged user could use this flaw to trigger an Out of Memory\n(OOM) condition and, potentially, crash the system. (CVE-2013-4483,\nModerate)\n* It was found that the Xen hypervisor implementation did not correctly\ncheck privileges of hypercall attempts made by HVM guests, allowing\nhypercalls to be invoked from protection rings 1 and 2 in addition to ring\n0. A local attacker in an HVM guest able to execute code on privilege\nlevels 1 and 2 could potentially use this flaw to further escalate their\nprivileges in that guest. Note: Xen HVM guests running unmodified versions\nof Red Hat Enterprise Linux and Microsoft Windows are not affected by this\nissue because they are known to only use protection rings 0 (kernel) and 3\n(userspace). (CVE-2013-4554, Moderate)\n* A flaw was found in the way the Linux kernel's Adaptec RAID controller\n(aacraid) checked permissions of compat IOCTLs. A local attacker could use\nthis flaw to bypass intended security restrictions. (CVE-2013-6383,\nModerate)\n* It was found that, under specific circumstances, a combination of write\noperations to write-combined memory and locked CPU instructions may cause a\ncore hang on certain AMD CPUs (for more information, refer to AMD CPU\nerratum 793 linked in the References section). A privileged user in a guest\nrunning under the Xen hypervisor could use this flaw to cause a denial of\nservice on the host system. This update adds a workaround to the Xen\nhypervisor implementation, which mitigates the AMD CPU issue. Note: this\nissue only affects AMD Family 16h Models 00h-0Fh Processors. Non-AMD CPUs\nare not vulnerable. (CVE-2013-6885, Moderate)\n* It was found that certain protocol handlers in the Linux kernel's\nnetworking implementation could set the addr_len value without initializing\nthe associated data structure. A local, unprivileged user could use this\nflaw to leak kernel stack memory to user space using the recvmsg, recvfrom,\nand recvmmsg system calls. (CVE-2013-7263, Low)\n* A flaw was found in the way the get_dumpable() function return value was\ninterpreted in the ptrace subsystem of the Linux kernel. When\n'fs.suid_dumpable' was set to 2, a local, unprivileged local user could\nuse this flaw to bypass intended ptrace restrictions and obtain\npotentially sensitive information. (CVE-2013-2929, Low)\nRed Hat would like to thank Vladimir Davydov of Parallels for reporting\nCVE-2013-4483 and the Xen project for reporting CVE-2013-4554 and\nCVE-2013-6885. Upstream acknowledges Jan Beulich as the original reporter\nof CVE-2013-4554 and CVE-2013-6885.\nThis update also fixes several bugs and adds one enhancement.\nDocumentation for these changes will be available shortly from the\nTechnical Notes document linked to in the References section.\nAll kernel users are advised to upgrade to these updated packages, which\ncontain backported patches to correct these issues and add this\nenhancement. The system must be rebooted for this update to take effect.",
  "Platform": [
    "Red Hat Enterprise Linux 5"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2014:0285",
      "ID": "RHSA-2014:0285"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-2929",
      "ID": "CVE-2013-2929"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-4483",
      "ID": "CVE-2013-4483"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-4554",
      "ID": "CVE-2013-4554"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-6381",
      "ID": "CVE-2013-6381"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-6383",
      "ID": "CVE-2013-6383"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-6885",
      "ID": "CVE-2013-6885"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-7263",
      "ID": "CVE-2013-7263"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.18-371.6.1.el5 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.18-371.6.1.el5 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.18-371.6.1.el5"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-PAE is earlier than 0:2.6.18-371.6.1.el5"
                  },
                  {
                    "Comment": "kernel-PAE is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-PAE-devel is earlier than 0:2.6.18-371.6.1.el5"
                  },
                  {
                    "Comment": "kernel-PAE-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:2.6.18-371.6.1.el5"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:2.6.18-371.6.1.el5"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.18-371.6.1.el5"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.18-371.6.1.el5"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:2.6.18-371.6.1.el5"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:2.6.18-371.6.1.el5"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:2.6.18-371.6.1.el5"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xen is earlier than 0:2.6.18-371.6.1.el5"
                  },
                  {
                    "Comment": "kernel-xen is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xen-devel is earlier than 0:2.6.18-371.6.1.el5"
                  },
                  {
                    "Comment": "kernel-xen-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 5 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 5 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-2929"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-4483"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-4554"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-6381"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-6383"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-6885"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-7263"
    }
  ],
  "Issued": {
    "Date": "2014-03-12"
  }
}