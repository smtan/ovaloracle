{
  "Title": "RHSA-2014:1606: file security and bug fix update (Moderate)",
  "Description": "The \"file\" command is used to identify a particular file according to the\ntype of data contained in the file. The command can identify various file\ntypes, including ELF binaries, system libraries, RPM packages, and\ndifferent graphics formats.\nMultiple denial of service flaws were found in the way file parsed certain\nComposite Document Format (CDF) files. A remote attacker could use either\nof these flaws to crash file, or an application using file, via a specially\ncrafted CDF file. (CVE-2014-0237, CVE-2014-0238, CVE-2014-3479,\nCVE-2014-3480, CVE-2012-1571)\nTwo denial of service flaws were found in the way file handled indirect and\nsearch rules. A remote attacker could use either of these flaws to cause\nfile, or an application using file, to crash or consume an excessive amount\nof CPU. (CVE-2014-1943, CVE-2014-2270)\nThis update also fixes the following bugs:\n* Previously, the output of the \"file\" command contained redundant white\nspaces. With this update, the new STRING_TRIM flag has been introduced to\nremove the unnecessary white spaces. (BZ#664513)\n* Due to a bug, the \"file\" command could incorrectly identify an XML\ndocument as a LaTex document. The underlying source code has been modified\nto fix this bug and the command now works as expected. (BZ#849621)\n* Previously, the \"file\" command could not recognize .JPG files and\nincorrectly labeled them as \"Minix filesystem\". This bug has been fixed and\nthe command now properly detects .JPG files. (BZ#873997)\n* Under certain circumstances, the \"file\" command incorrectly detected\nNETpbm files as \"x86 boot sector\". This update applies a patch to fix this\nbug and the command now detects NETpbm files as expected. (BZ#884396)\n* Previously, the \"file\" command incorrectly identified ASCII text files as\na .PIC image file. With this update, a patch has been provided to address\nthis bug and the command now correctly recognizes ASCII text files.\n(BZ#980941)\n* On 32-bit PowerPC systems, the \"from\" field was missing from the output\nof the \"file\" command. The underlying source code has been modified to fix\nthis bug and \"file\" output now contains the \"from\" field as expected.\n(BZ#1037279)\n* The \"file\" command incorrectly detected text files as \"RRDTool DB version\nool - Round Robin Database Tool\". This update applies a patch to fix this\nbug and the command now correctly detects text files. (BZ#1064463)\n* Previously, the \"file\" command supported only version 1 and 2 of the QCOW\nformat. As a consequence, file was unable to detect a \"qcow2 compat=1.1\"\nfile created on Red Hat Enterprise Linux 7. With this update, support for\nQCOW version 3 has been added so that the command now detects such files as\nexpected. (BZ#1067771)\nAll file users are advised to upgrade to these updated packages, which\ncontain backported patches to correct these issues.",
  "Platform": [
    "Red Hat Enterprise Linux 6"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2014:1606",
      "ID": "RHSA-2014:1606"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-1571",
      "ID": "CVE-2012-1571"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-0237",
      "ID": "CVE-2014-0237"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-0238",
      "ID": "CVE-2014-0238"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-1943",
      "ID": "CVE-2014-1943"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-2270",
      "ID": "CVE-2014-2270"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-3479",
      "ID": "CVE-2014-3479"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-3480",
      "ID": "CVE-2014-3480"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "file is earlier than 0:5.04-21.el6"
                  },
                  {
                    "Comment": "file is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "file-devel is earlier than 0:5.04-21.el6"
                  },
                  {
                    "Comment": "file-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "file-libs is earlier than 0:5.04-21.el6"
                  },
                  {
                    "Comment": "file-libs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "file-static is earlier than 0:5.04-21.el6"
                  },
                  {
                    "Comment": "file-static is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "python-magic is earlier than 0:5.04-21.el6"
                  },
                  {
                    "Comment": "python-magic is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-1571"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-0237"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-0238"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-1943"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-2270"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-3479"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-3480"
    }
  ],
  "Issued": {
    "Date": "2014-10-13"
  }
}