{
  "Title": "RHSA-2014:0408: java-1.6.0-openjdk security and bug fix update (Important)",
  "Description": "The java-1.6.0-openjdk packages provide the OpenJDK 6 Java Runtime\nEnvironment and the OpenJDK 6 Java Software Development Kit.\nAn input validation flaw was discovered in the medialib library in the 2D\ncomponent. A specially crafted image could trigger Java Virtual Machine\nmemory corruption when processed. A remote attacker, or an untrusted Java\napplication or applet, could possibly use this flaw to execute arbitrary\ncode with the privileges of the user running the Java Virtual Machine.\n(CVE-2014-0429)\nMultiple flaws were discovered in the Hotspot and 2D components in OpenJDK.\nAn untrusted Java application or applet could use these flaws to trigger\nJava Virtual Machine memory corruption and possibly bypass Java sandbox\nrestrictions. (CVE-2014-0456, CVE-2014-2397, CVE-2014-2421)\nMultiple improper permission check issues were discovered in the Libraries\ncomponent in OpenJDK. An untrusted Java application or applet could use\nthese flaws to bypass Java sandbox restrictions. (CVE-2014-0457,\nCVE-2014-0461)\nMultiple improper permission check issues were discovered in the AWT,\nJAX-WS, JAXB, Libraries, and Sound components in OpenJDK. An untrusted Java\napplication or applet could use these flaws to bypass certain Java sandbox\nrestrictions. (CVE-2014-2412, CVE-2014-0451, CVE-2014-0458, CVE-2014-2423,\nCVE-2014-0452, CVE-2014-2414, CVE-2014-0446, CVE-2014-2427)\nMultiple flaws were identified in the Java Naming and Directory Interface\n(JNDI) DNS client. These flaws could make it easier for a remote attacker\nto perform DNS spoofing attacks. (CVE-2014-0460)\nIt was discovered that the JAXP component did not properly prevent access\nto arbitrary files when a SecurityManager was present. This flaw could\ncause a Java application using JAXP to leak sensitive information, or\naffect application availability. (CVE-2014-2403)\nIt was discovered that the Security component in OpenJDK could leak some\ntiming information when performing PKCS#1 unpadding. This could possibly\nlead to the disclosure of some information that was meant to be protected\nby encryption. (CVE-2014-0453)\nIt was discovered that the fix for CVE-2013-5797 did not properly resolve\ninput sanitization flaws in javadoc. When javadoc documentation was\ngenerated from an untrusted Java source code and hosted on a domain not\ncontrolled by the code author, these issues could make it easier to perform\ncross-site scripting (XSS) attacks. (CVE-2014-2398)\nAn insecure temporary file use flaw was found in the way the unpack200\nutility created log files. A local attacker could possibly use this flaw to\nperform a symbolic link attack and overwrite arbitrary files with the\nprivileges of the user running unpack200. (CVE-2014-1876)\nThis update also fixes the following bug:\n* The OpenJDK update to IcedTea version 1.13 introduced a regression\nrelated to the handling of the jdk_version_info variable. This variable was\nnot properly zeroed out before being passed to the Java Virtual Machine,\nresulting in a memory leak in the java.lang.ref.Finalizer class.\nThis update fixes this issue, and memory leaks no longer occur.\n(BZ#1085373)\nAll users of java-1.6.0-openjdk are advised to upgrade to these updated\npackages, which resolve these issues. All running instances of OpenJDK Java\nmust be restarted for the update to take effect.",
  "Platform": [
    "Red Hat Enterprise Linux 5",
    "Red Hat Enterprise Linux 6"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2014:0408",
      "ID": "RHSA-2014:0408"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-0429",
      "ID": "CVE-2014-0429"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-0446",
      "ID": "CVE-2014-0446"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-0451",
      "ID": "CVE-2014-0451"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-0452",
      "ID": "CVE-2014-0452"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-0453",
      "ID": "CVE-2014-0453"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-0456",
      "ID": "CVE-2014-0456"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-0457",
      "ID": "CVE-2014-0457"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-0458",
      "ID": "CVE-2014-0458"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-0460",
      "ID": "CVE-2014-0460"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-0461",
      "ID": "CVE-2014-0461"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-1876",
      "ID": "CVE-2014-1876"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-2397",
      "ID": "CVE-2014-2397"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-2398",
      "ID": "CVE-2014-2398"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-2403",
      "ID": "CVE-2014-2403"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-2412",
      "ID": "CVE-2014-2412"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-2414",
      "ID": "CVE-2014-2414"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-2421",
      "ID": "CVE-2014-2421"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-2423",
      "ID": "CVE-2014-2423"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-2427",
      "ID": "CVE-2014-2427"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk is earlier than 1:1.6.0.0-5.1.13.3.el5_10"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk-demo is earlier than 1:1.6.0.0-5.1.13.3.el5_10"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk-demo is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk-devel is earlier than 1:1.6.0.0-5.1.13.3.el5_10"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk-javadoc is earlier than 1:1.6.0.0-5.1.13.3.el5_10"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk-javadoc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk-src is earlier than 1:1.6.0.0-5.1.13.3.el5_10"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk-src is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 5 is installed"
          }
        ]
      },
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk is earlier than 1:1.6.0.0-5.1.13.3.el6_5"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk-demo is earlier than 1:1.6.0.0-5.1.13.3.el6_5"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk-demo is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk-devel is earlier than 1:1.6.0.0-5.1.13.3.el6_5"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk-javadoc is earlier than 1:1.6.0.0-5.1.13.3.el6_5"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk-javadoc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk-src is earlier than 1:1.6.0.0-5.1.13.3.el6_5"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk-src is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 5 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-0429"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-0446"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-0451"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-0452"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-0453"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-0456"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-0457"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-0458"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-0460"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-0461"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-1876"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-2397"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-2398"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-2403"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-2412"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-2414"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-2421"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-2423"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-2427"
    }
  ],
  "Issued": {
    "Date": "2014-04-16"
  }
}