{
  "Title": "RHSA-2014:0255: subversion security update (Moderate)",
  "Description": "Subversion (SVN) is a concurrent version control system which enables one\nor more users to collaborate in developing and maintaining a hierarchy of\nfiles and directories while keeping a history of all changes. The\nmod_dav_svn module is used with the Apache HTTP Server to allow access to\nSubversion repositories via HTTP.\nA flaw was found in the way the mod_dav_svn module handled OPTIONS\nrequests. A remote attacker with read access to an SVN repository served\nvia HTTP could use this flaw to cause the httpd process that handled such a\nrequest to crash. (CVE-2014-0032)\nA flaw was found in the way Subversion handled file names with newline\ncharacters when the FSFS repository format was used. An attacker with\ncommit access to an SVN repository could corrupt a revision by committing a\nspecially crafted file. (CVE-2013-1968)\nA flaw was found in the way the svnserve tool of Subversion handled remote\nclient network connections. An attacker with read access to an SVN\nrepository served via svnserve could use this flaw to cause the svnserve\ndaemon to exit, leading to a denial of service. (CVE-2013-2112)\nAll subversion users should upgrade to these updated packages, which\ncontain backported patches to correct these issues. After installing the\nupdated packages, for the update to take effect, you must restart the httpd\ndaemon, if you are using mod_dav_svn, and the svnserve daemon, if you are\nserving Subversion repositories via the svn:// protocol.",
  "Platform": [
    "Red Hat Enterprise Linux 5",
    "Red Hat Enterprise Linux 6"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2014:0255",
      "ID": "RHSA-2014:0255"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-1968",
      "ID": "CVE-2013-1968"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-2112",
      "ID": "CVE-2013-2112"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-0032",
      "ID": "CVE-2014-0032"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mod_dav_svn is earlier than 0:1.6.11-12.el5_10"
                  },
                  {
                    "Comment": "mod_dav_svn is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "subversion is earlier than 0:1.6.11-12.el5_10"
                  },
                  {
                    "Comment": "subversion is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "subversion-devel is earlier than 0:1.6.11-12.el5_10"
                  },
                  {
                    "Comment": "subversion-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "subversion-javahl is earlier than 0:1.6.11-12.el5_10"
                  },
                  {
                    "Comment": "subversion-javahl is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "subversion-perl is earlier than 0:1.6.11-12.el5_10"
                  },
                  {
                    "Comment": "subversion-perl is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "subversion-ruby is earlier than 0:1.6.11-12.el5_10"
                  },
                  {
                    "Comment": "subversion-ruby is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 5 is installed"
          }
        ]
      },
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mod_dav_svn is earlier than 0:1.6.11-10.el6_5"
                  },
                  {
                    "Comment": "mod_dav_svn is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "subversion is earlier than 0:1.6.11-10.el6_5"
                  },
                  {
                    "Comment": "subversion is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "subversion-devel is earlier than 0:1.6.11-10.el6_5"
                  },
                  {
                    "Comment": "subversion-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "subversion-gnome is earlier than 0:1.6.11-10.el6_5"
                  },
                  {
                    "Comment": "subversion-gnome is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "subversion-javahl is earlier than 0:1.6.11-10.el6_5"
                  },
                  {
                    "Comment": "subversion-javahl is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "subversion-kde is earlier than 0:1.6.11-10.el6_5"
                  },
                  {
                    "Comment": "subversion-kde is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "subversion-perl is earlier than 0:1.6.11-10.el6_5"
                  },
                  {
                    "Comment": "subversion-perl is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "subversion-ruby is earlier than 0:1.6.11-10.el6_5"
                  },
                  {
                    "Comment": "subversion-ruby is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "subversion-svn2cl is earlier than 0:1.6.11-10.el6_5"
                  },
                  {
                    "Comment": "subversion-svn2cl is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 5 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-1968"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-2112"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-0032"
    }
  ],
  "Issued": {
    "Date": "2014-03-05"
  }
}