{
  "Title": "RHSA-2014:0679: openssl security update (Important)",
  "Description": "OpenSSL is a toolkit that implements the Secure Sockets Layer (SSL v2/v3)\nand Transport Layer Security (TLS v1) protocols, as well as a\nfull-strength, general purpose cryptography library.\nIt was found that OpenSSL clients and servers could be forced, via a\nspecially crafted handshake packet, to use weak keying material for\ncommunication. A man-in-the-middle attacker could use this flaw to decrypt\nand modify traffic between a client and a server. (CVE-2014-0224)\nNote: In order to exploit this flaw, both the server and the client must be\nusing a vulnerable version of OpenSSL; the server must be using OpenSSL\nversion 1.0.1 and above, and the client must be using any version of\nOpenSSL. For more information about this flaw, refer to:\nhttps://access.redhat.com/site/articles/904433\nA buffer overflow flaw was found in the way OpenSSL handled invalid DTLS\npacket fragments. A remote attacker could possibly use this flaw to execute\narbitrary code on a DTLS client or server. (CVE-2014-0195)\nMultiple flaws were found in the way OpenSSL handled read and write buffers\nwhen the SSL_MODE_RELEASE_BUFFERS mode was enabled. A TLS/SSL client or\nserver using OpenSSL could crash or unexpectedly drop connections when\nprocessing certain SSL traffic. (CVE-2010-5298, CVE-2014-0198)\nA denial of service flaw was found in the way OpenSSL handled certain DTLS\nServerHello requests. A specially crafted DTLS handshake packet could cause\na DTLS client using OpenSSL to crash. (CVE-2014-0221)\nA NULL pointer dereference flaw was found in the way OpenSSL performed\nanonymous Elliptic Curve Diffie Hellman (ECDH) key exchange. A specially\ncrafted handshake packet could cause a TLS/SSL client that has the\nanonymous ECDH cipher suite enabled to crash. (CVE-2014-3470)\nRed Hat would like to thank the OpenSSL project for reporting these issues.\nUpstream acknowledges KIKUCHI Masashi of Lepidum as the original reporter\nof CVE-2014-0224, Jüri Aedla as the original reporter of CVE-2014-0195,\nImre Rad of Search-Lab as the original reporter of CVE-2014-0221, and Felix\nGröbert and Ivan Fratrić of Google as the original reporters of\nCVE-2014-3470.\nAll OpenSSL users are advised to upgrade to these updated packages, which\ncontain backported patches to correct these issues. For the update to take\neffect, all services linked to the OpenSSL library (such as httpd and other\nSSL-enabled services) must be restarted or the system rebooted.",
  "Platform": [
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2014:0679",
      "ID": "RHSA-2014:0679"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2010-5298",
      "ID": "CVE-2010-5298"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-0195",
      "ID": "CVE-2014-0195"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-0198",
      "ID": "CVE-2014-0198"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-0221",
      "ID": "CVE-2014-0221"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-0224",
      "ID": "CVE-2014-0224"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-3470",
      "ID": "CVE-2014-3470"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "openssl is earlier than 1:1.0.1e-34.el7_0.3"
                  },
                  {
                    "Comment": "openssl is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "openssl-devel is earlier than 1:1.0.1e-34.el7_0.3"
                  },
                  {
                    "Comment": "openssl-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "openssl-libs is earlier than 1:1.0.1e-34.el7_0.3"
                  },
                  {
                    "Comment": "openssl-libs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "openssl-perl is earlier than 1:1.0.1e-34.el7_0.3"
                  },
                  {
                    "Comment": "openssl-perl is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "openssl-static is earlier than 1:1.0.1e-34.el7_0.3"
                  },
                  {
                    "Comment": "openssl-static is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 7 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2010-5298"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-0195"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-0198"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-0221"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-0224"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-3470"
    }
  ],
  "Issued": {
    "Date": "2014-06-10"
  }
}