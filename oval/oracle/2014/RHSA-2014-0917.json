{
  "Title": "RHSA-2014:0917: nss and nspr security, bug fix, and enhancement update (Critical)",
  "Description": "Network Security Services (NSS) is a set of libraries designed to support\nthe cross-platform development of security-enabled client and server\napplications. Netscape Portable Runtime (NSPR) provides platform\nindependence for non-GUI operating system facilities.\nA race condition was found in the way NSS verified certain certificates.\nA remote attacker could use this flaw to crash an application using NSS or,\npossibly, execute arbitrary code with the privileges of the user running\nthat application. (CVE-2014-1544)\nA flaw was found in the way TLS False Start was implemented in NSS.\nAn attacker could use this flaw to potentially return unencrypted\ninformation from the server. (CVE-2013-1740)\nA race condition was found in the way NSS implemented session ticket\nhandling as specified by RFC 5077. An attacker could use this flaw to crash\nan application using NSS or, in rare cases, execute arbitrary code with the\nprivileges of the user running that application. (CVE-2014-1490)\nIt was found that NSS accepted weak Diffie-Hellman Key exchange (DHKE)\nparameters. This could possibly lead to weak encryption being used in\ncommunication between the client and the server. (CVE-2014-1491)\nAn out-of-bounds write flaw was found in NSPR. A remote attacker could\npotentially use this flaw to crash an application using NSPR or, possibly,\nexecute arbitrary code with the privileges of the user running that\napplication. This NSPR flaw was not exposed to web content in any shipped\nversion of Firefox. (CVE-2014-1545)\nIt was found that the implementation of Internationalizing Domain Names in\nApplications (IDNA) hostname matching in NSS did not follow the RFC 6125\nrecommendations. This could lead to certain invalid certificates with\ninternational characters to be accepted as valid. (CVE-2014-1492)\nRed Hat would like to thank the Mozilla project for reporting the\nCVE-2014-1544, CVE-2014-1490, CVE-2014-1491, and CVE-2014-1545 issues.\nUpstream acknowledges Tyson Smith and Jesse Schwartzentruber as the\noriginal reporters of CVE-2014-1544, Brian Smith as the original reporter\nof CVE-2014-1490, Antoine Delignat-Lavaud and Karthikeyan Bhargavan as the\noriginal reporters of CVE-2014-1491, and Abhishek Arya as the original\nreporter of CVE-2014-1545.\nIn addition, the nss package has been upgraded to upstream version 3.16.1,\nand the nspr package has been upgraded to upstream version 4.10.6. These\nupdated packages provide a number of bug fixes and enhancements over the\nprevious versions. (BZ#1112136, BZ#1112135)\nUsers of NSS and NSPR are advised to upgrade to these updated packages,\nwhich correct these issues and add these enhancements. After installing\nthis update, applications using NSS or NSPR must be restarted for this\nupdate to take effect.",
  "Platform": [
    "Red Hat Enterprise Linux 6"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2014:0917",
      "ID": "RHSA-2014:0917"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-1740",
      "ID": "CVE-2013-1740"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-1490",
      "ID": "CVE-2014-1490"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-1491",
      "ID": "CVE-2014-1491"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-1492",
      "ID": "CVE-2014-1492"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-1544",
      "ID": "CVE-2014-1544"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-1545",
      "ID": "CVE-2014-1545"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "nspr is earlier than 0:4.10.6-1.el6_5"
                  },
                  {
                    "Comment": "nspr is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "nspr-devel is earlier than 0:4.10.6-1.el6_5"
                  },
                  {
                    "Comment": "nspr-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "nss-util is earlier than 0:3.16.1-1.el6_5"
                  },
                  {
                    "Comment": "nss-util is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "nss-util-devel is earlier than 0:3.16.1-1.el6_5"
                  },
                  {
                    "Comment": "nss-util-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "nss is earlier than 0:3.16.1-4.el6_5"
                  },
                  {
                    "Comment": "nss is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "nss-devel is earlier than 0:3.16.1-4.el6_5"
                  },
                  {
                    "Comment": "nss-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "nss-pkcs11-devel is earlier than 0:3.16.1-4.el6_5"
                  },
                  {
                    "Comment": "nss-pkcs11-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "nss-sysinit is earlier than 0:3.16.1-4.el6_5"
                  },
                  {
                    "Comment": "nss-sysinit is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "nss-tools is earlier than 0:3.16.1-4.el6_5"
                  },
                  {
                    "Comment": "nss-tools is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Critical",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-1740"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-1490"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-1491"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-1492"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-1544"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-1545"
    }
  ],
  "Issued": {
    "Date": "2014-07-22"
  }
}