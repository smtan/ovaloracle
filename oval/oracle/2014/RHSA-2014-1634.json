{
  "Title": "RHSA-2014:1634: java-1.6.0-openjdk security and bug fix update (Important)",
  "Description": "The java-1.6.0-openjdk packages provide the OpenJDK 6 Java Runtime\nEnvironment and the OpenJDK 6 Java Software Development Kit.\nMultiple flaws were discovered in the Libraries, 2D, and Hotspot components\nin OpenJDK. An untrusted Java application or applet could use these flaws\nto bypass certain Java sandbox restrictions. (CVE-2014-6506, CVE-2014-6531,\nCVE-2014-6502, CVE-2014-6511, CVE-2014-6504, CVE-2014-6519)\nIt was discovered that the StAX XML parser in the JAXP component in OpenJDK\nperformed expansion of external parameter entities even when external\nentity substitution was disabled. A remote attacker could use this flaw to\nperform XML eXternal Entity (XXE) attack against applications using the\nStAX parser to parse untrusted XML documents. (CVE-2014-6517)\nIt was discovered that the DatagramSocket implementation in OpenJDK failed\nto perform source address checks for packets received on a connected\nsocket. A remote attacker could use this flaw to have their packets\nprocessed as if they were received from the expected source.\n(CVE-2014-6512)\nIt was discovered that the TLS/SSL implementation in the JSSE component in\nOpenJDK failed to properly verify the server identity during the\nrenegotiation following session resumption, making it possible for\nmalicious TLS/SSL servers to perform a Triple Handshake attack against\nclients using JSSE and client certificate authentication. (CVE-2014-6457)\nIt was discovered that the CipherInputStream class implementation in\nOpenJDK did not properly handle certain exceptions. This could possibly\nallow an attacker to affect the integrity of an encrypted stream handled by\nthis class. (CVE-2014-6558)\nThe CVE-2014-6512 was discovered by Florian Weimer of Red Hat Product\nSecurity.\nThis update also fixes the following bug:\n* The TLS/SSL implementation in OpenJDK previously failed to handle\nDiffie-Hellman (DH) keys with more than 1024 bits. This caused client\napplications using JSSE to fail to establish TLS/SSL connections to servers\nusing larger DH keys during the connection handshake. This update adds\nsupport for DH keys with size up to 2048 bits. (BZ#1148309)\nAll users of java-1.6.0-openjdk are advised to upgrade to these updated\npackages, which resolve these issues. All running instances of OpenJDK Java\nmust be restarted for the update to take effect.",
  "Platform": [
    "Red Hat Enterprise Linux 5",
    "Red Hat Enterprise Linux 6",
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2014:1634",
      "ID": "RHSA-2014:1634"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-6457",
      "ID": "CVE-2014-6457"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-6502",
      "ID": "CVE-2014-6502"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-6504",
      "ID": "CVE-2014-6504"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-6506",
      "ID": "CVE-2014-6506"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-6511",
      "ID": "CVE-2014-6511"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-6512",
      "ID": "CVE-2014-6512"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-6517",
      "ID": "CVE-2014-6517"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-6519",
      "ID": "CVE-2014-6519"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-6531",
      "ID": "CVE-2014-6531"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-6558",
      "ID": "CVE-2014-6558"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk is earlier than 1:1.6.0.33-1.13.5.0.el5_11"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk-demo is earlier than 1:1.6.0.33-1.13.5.0.el5_11"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk-demo is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk-devel is earlier than 1:1.6.0.33-1.13.5.0.el5_11"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk-javadoc is earlier than 1:1.6.0.33-1.13.5.0.el5_11"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk-javadoc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk-src is earlier than 1:1.6.0.33-1.13.5.0.el5_11"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk-src is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 5 is installed"
          }
        ]
      },
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk is earlier than 1:1.6.0.33-1.13.5.0.el7_0"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk-demo is earlier than 1:1.6.0.33-1.13.5.0.el7_0"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk-demo is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk-devel is earlier than 1:1.6.0.33-1.13.5.0.el7_0"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk-javadoc is earlier than 1:1.6.0.33-1.13.5.0.el7_0"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk-javadoc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk-src is earlier than 1:1.6.0.33-1.13.5.0.el7_0"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk-src is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      },
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk is earlier than 1:1.6.0.33-1.13.5.0.el6_6"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk-demo is earlier than 1:1.6.0.33-1.13.5.0.el6_6"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk-demo is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk-devel is earlier than 1:1.6.0.33-1.13.5.0.el6_6"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk-javadoc is earlier than 1:1.6.0.33-1.13.5.0.el6_6"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk-javadoc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk-src is earlier than 1:1.6.0.33-1.13.5.0.el6_6"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk-src is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 5 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-6457"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-6502"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-6504"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-6506"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-6511"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-6512"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-6517"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-6519"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-6531"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-6558"
    }
  ],
  "Issued": {
    "Date": "2014-10-15"
  }
}