{
  "Title": "RHSA-2021:3892: java-11-openjdk security and bug fix update (Important)",
  "Description": "The java-11-openjdk packages provide the OpenJDK 11 Java Runtime Environment and the OpenJDK 11 Java Software Development Kit.\nSecurity Fix(es):\n* OpenJDK: Loop in HttpsServer triggered during TLS session close (JSSE, 8254967) (CVE-2021-35565)\n* OpenJDK: Incorrect principal selection when using Kerberos Constrained Delegation (Libraries, 8266689) (CVE-2021-35567)\n* OpenJDK: Weak ciphers preferred over stronger ones for TLS (JSSE, 8264210) (CVE-2021-35550)\n* OpenJDK: Excessive memory allocation in RTFParser (Swing, 8265167) (CVE-2021-35556)\n* OpenJDK: Excessive memory allocation in RTFReader (Swing, 8265580) (CVE-2021-35559)\n* OpenJDK: Excessive memory allocation in HashMap and HashSet (Utility, 8266097) (CVE-2021-35561)\n* OpenJDK: Certificates with end dates too far in the future can corrupt keystore (Keytool, 8266137) (CVE-2021-35564)\n* OpenJDK: Unexpected exception raised during TLS handshake (JSSE, 8267729) (CVE-2021-35578)\n* OpenJDK: Excessive memory allocation in BMPImageReader (ImageIO, 8267735) (CVE-2021-35586)\n* OpenJDK: Non-constant comparison during TLS handshakes (JSSE, 8269618) (CVE-2021-35603)\nFor more details about the security issue(s), including the impact, a CVSS score, acknowledgments, and other related information, refer to the CVE page(s) listed in the References section.\nBug Fix(es):\n* Previously, uninstalling the OpenJDK RPMs attempted to remove a client directory that did not exist. This directory is no longer used in java-11-openjdk and all references to it have now been removed. (RHBZ#1698873)",
  "Platform": [
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2021:3892",
      "ID": "RHSA-2021:3892"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2021-35550",
      "ID": "CVE-2021-35550"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2021-35556",
      "ID": "CVE-2021-35556"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2021-35559",
      "ID": "CVE-2021-35559"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2021-35561",
      "ID": "CVE-2021-35561"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2021-35564",
      "ID": "CVE-2021-35564"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2021-35565",
      "ID": "CVE-2021-35565"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2021-35567",
      "ID": "CVE-2021-35567"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2021-35578",
      "ID": "CVE-2021-35578"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2021-35586",
      "ID": "CVE-2021-35586"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2021-35603",
      "ID": "CVE-2021-35603"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-11-openjdk is earlier than 1:11.0.13.0.8-1.el7_9"
                  },
                  {
                    "Comment": "java-11-openjdk is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-11-openjdk-demo is earlier than 1:11.0.13.0.8-1.el7_9"
                  },
                  {
                    "Comment": "java-11-openjdk-demo is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-11-openjdk-devel is earlier than 1:11.0.13.0.8-1.el7_9"
                  },
                  {
                    "Comment": "java-11-openjdk-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-11-openjdk-headless is earlier than 1:11.0.13.0.8-1.el7_9"
                  },
                  {
                    "Comment": "java-11-openjdk-headless is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-11-openjdk-javadoc is earlier than 1:11.0.13.0.8-1.el7_9"
                  },
                  {
                    "Comment": "java-11-openjdk-javadoc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-11-openjdk-javadoc-zip is earlier than 1:11.0.13.0.8-1.el7_9"
                  },
                  {
                    "Comment": "java-11-openjdk-javadoc-zip is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-11-openjdk-jmods is earlier than 1:11.0.13.0.8-1.el7_9"
                  },
                  {
                    "Comment": "java-11-openjdk-jmods is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-11-openjdk-src is earlier than 1:11.0.13.0.8-1.el7_9"
                  },
                  {
                    "Comment": "java-11-openjdk-src is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-11-openjdk-static-libs is earlier than 1:11.0.13.0.8-1.el7_9"
                  },
                  {
                    "Comment": "java-11-openjdk-static-libs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 7 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2021-35550"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2021-35556"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2021-35559"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2021-35561"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2021-35564"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2021-35565"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2021-35567"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2021-35578"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2021-35586"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2021-35603"
    }
  ],
  "Issued": {
    "Date": "2021-10-20"
  }
}