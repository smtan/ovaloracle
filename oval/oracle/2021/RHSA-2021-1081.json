{
  "Title": "RHSA-2021:1081: kernel-rt security and bug fix update (Important)",
  "Description": "The kernel-rt packages provide the Real Time Linux Kernel, which enables fine-tuning for systems with extremely high determinism requirements.\nSecurity Fix(es):\n* kernel: use after free in eventpoll.c may lead to escalation of privilege (CVE-2020-0466)\n* kernel: SCSI target (LIO) write to any block on ILO backstore (CVE-2020-28374)\n* kernel: Use after free via PI futex state (CVE-2021-3347)\n* kernel: race conditions caused by wrong locking in net/vmw_vsock/af_vsock.c (CVE-2021-26708)\n* kernel: out-of-bounds read in libiscsi module (CVE-2021-27364)\n* kernel: heap buffer overflow in the iSCSI subsystem (CVE-2021-27365)\n* Kernel: KVM: host stack overflow due to lazy update IOAPIC (CVE-2020-27152)\n* kernel: iscsi: unrestricted access to sessions and handles (CVE-2021-27363)\nFor more details about the security issue(s), including the impact, a CVSS score, acknowledgments, and other related information, refer to the CVE page(s) listed in the References section.\nBug Fix(es):\n* kernel-rt possible livelock: WARNING: CPU: 28 PID: 3109 at kernel/ptrace.c:242 ptrace_check_attach+0xdd/0x1a0 (BZ#1925308)\n* kernel-rt: update RT source tree to the RHEL-8.3.z3 source tree (BZ#1926369)",
  "Platform": [
    "Red Hat Enterprise Linux 8"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2021:1081",
      "ID": "RHSA-2021:1081"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-0466",
      "ID": "CVE-2020-0466"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-27152",
      "ID": "CVE-2020-27152"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-28374",
      "ID": "CVE-2020-28374"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2021-26708",
      "ID": "CVE-2021-26708"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2021-27363",
      "ID": "CVE-2021-27363"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2021-27364",
      "ID": "CVE-2021-27364"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2021-27365",
      "ID": "CVE-2021-27365"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2021-3347",
      "ID": "CVE-2021-3347"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "Oracle Linux 8 is installed"
              },
              {
                "Comment": "Red Hat CoreOS 4 is installed"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel-rt earlier than 0:4.18.0-240.22.1.rt7.77.el8_3 is currently running"
              },
              {
                "Comment": "kernel-rt earlier than 0:4.18.0-240.22.1.rt7.77.el8_3 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt is earlier than 0:4.18.0-240.22.1.rt7.77.el8_3"
                  },
                  {
                    "Comment": "kernel-rt is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-core is earlier than 0:4.18.0-240.22.1.rt7.77.el8_3"
                  },
                  {
                    "Comment": "kernel-rt-core is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug is earlier than 0:4.18.0-240.22.1.rt7.77.el8_3"
                  },
                  {
                    "Comment": "kernel-rt-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug-core is earlier than 0:4.18.0-240.22.1.rt7.77.el8_3"
                  },
                  {
                    "Comment": "kernel-rt-debug-core is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug-devel is earlier than 0:4.18.0-240.22.1.rt7.77.el8_3"
                  },
                  {
                    "Comment": "kernel-rt-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug-kvm is earlier than 0:4.18.0-240.22.1.rt7.77.el8_3"
                  },
                  {
                    "Comment": "kernel-rt-debug-kvm is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug-modules is earlier than 0:4.18.0-240.22.1.rt7.77.el8_3"
                  },
                  {
                    "Comment": "kernel-rt-debug-modules is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug-modules-extra is earlier than 0:4.18.0-240.22.1.rt7.77.el8_3"
                  },
                  {
                    "Comment": "kernel-rt-debug-modules-extra is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-devel is earlier than 0:4.18.0-240.22.1.rt7.77.el8_3"
                  },
                  {
                    "Comment": "kernel-rt-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-kvm is earlier than 0:4.18.0-240.22.1.rt7.77.el8_3"
                  },
                  {
                    "Comment": "kernel-rt-kvm is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-modules is earlier than 0:4.18.0-240.22.1.rt7.77.el8_3"
                  },
                  {
                    "Comment": "kernel-rt-modules is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-modules-extra is earlier than 0:4.18.0-240.22.1.rt7.77.el8_3"
                  },
                  {
                    "Comment": "kernel-rt-modules-extra is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [

        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 8 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-0466"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-27152"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-28374"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2021-26708"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2021-27363"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2021-27364"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2021-27365"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2021-3347"
    }
  ],
  "Issued": {
    "Date": "2021-04-06"
  }
}