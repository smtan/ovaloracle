{
  "Title": "RHSA-2021:2314: kernel security and bug fix update (Important)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux operating system.\nSecurity Fix(es):\n* kernel: Integer overflow in Intel(R) Graphics Drivers (CVE-2020-12362)\n* kernel: Use after free via PI futex state (CVE-2021-3347)\n* kernel: use-after-free in n_tty_receive_buf_common function in drivers/tty/n_tty.c (CVE-2020-8648)\n* kernel: Improper input validation in some Intel(R) Graphics Drivers (CVE-2020-12363)\n* kernel: Null pointer dereference in some Intel(R) Graphics Drivers (CVE-2020-12364)\n* kernel: Speculation on pointer arithmetic against bpf_context pointer (CVE-2020-27170)\nFor more details about the security issue(s), including the impact, a CVSS score, acknowledgments, and other related information, refer to the CVE page(s) listed in the References section.\nBug Fix(es):\n* kernel crash when call the timer function (sctp_generate_proto_unreach_event) of sctp module (BZ#1707184)\n* SCSI error handling process on HP P440ar controller gets stuck indefinitely in device reset operation (BZ#1830268)\n* netfilter: reproducible deadlock on nft_log module autoload (BZ#1858329)\n* netfilter: NULL pointer dereference in nf_tables_set_lookup() (BZ#1873171)\n* [DELL EMC 7.9 Bug]: No acpi_pad threads on top command for \"power cap policy equal to 0 watts\" (BZ#1883174)\n* A race between i40e_ndo_set_vf_mac() and i40e_vsi_clear() in the i40e driver causes a use after free condition of the kmalloc-4096 slab cache. (BZ#1886003)\n* netxen driver performs poorly with RT kernel (BZ#1894274)\n* gendisk->disk_part_tbl->last_lookup retains pointer after partition deletion (BZ#1898596)\n* Kernel experiences panic in update_group_power() due to division error even with Bug 1701115 fix (BZ#1910763)\n* RHEL7.9 - zfcp: fix handling of FCP_RESID_OVER bit in fcp ingress path (BZ#1917839)\n* RHEL7.9 - mm/THP: do not access vma->vm_mm after calling handle_userfault (BZ#1917840)\n* raid: wrong raid io account (BZ#1927106)\n* qla2x00_status_cont_entry() missing upstream patch that prevents unnecessary ABRT/warnings (BZ#1933784)\n* RHEL 7.9.z - System hang caused by workqueue stall in qla2xxx driver (BZ#1937945)\n* selinux: setsebool can trigger a deadlock (BZ#1939091)\n* [Hyper-V][RHEL-7] Cannot boot kernel 3.10.0-1160.21.1.el7.x86_64 on Hyper-V (BZ#1941841)",
  "Platform": [
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2021:2314",
      "ID": "RHSA-2021:2314"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-12362",
      "ID": "CVE-2020-12362"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-12363",
      "ID": "CVE-2020-12363"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-12364",
      "ID": "CVE-2020-12364"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-27170",
      "ID": "CVE-2020-27170"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-8648",
      "ID": "CVE-2020-8648"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2021-3347",
      "ID": "CVE-2021-3347"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:3.10.0-1160.31.1.el7 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:3.10.0-1160.31.1.el7 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "bpftool is earlier than 0:3.10.0-1160.31.1.el7"
                  },
                  {
                    "Comment": "bpftool is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:3.10.0-1160.31.1.el7"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-abi-whitelists is earlier than 0:3.10.0-1160.31.1.el7"
                  },
                  {
                    "Comment": "kernel-abi-whitelists is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-bootwrapper is earlier than 0:3.10.0-1160.31.1.el7"
                  },
                  {
                    "Comment": "kernel-bootwrapper is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:3.10.0-1160.31.1.el7"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:3.10.0-1160.31.1.el7"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:3.10.0-1160.31.1.el7"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:3.10.0-1160.31.1.el7"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:3.10.0-1160.31.1.el7"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:3.10.0-1160.31.1.el7"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:3.10.0-1160.31.1.el7"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-tools is earlier than 0:3.10.0-1160.31.1.el7"
                  },
                  {
                    "Comment": "kernel-tools is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-tools-libs is earlier than 0:3.10.0-1160.31.1.el7"
                  },
                  {
                    "Comment": "kernel-tools-libs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-tools-libs-devel is earlier than 0:3.10.0-1160.31.1.el7"
                  },
                  {
                    "Comment": "kernel-tools-libs-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perf is earlier than 0:3.10.0-1160.31.1.el7"
                  },
                  {
                    "Comment": "perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "python-perf is earlier than 0:3.10.0-1160.31.1.el7"
                  },
                  {
                    "Comment": "python-perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 7 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-12362"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-12363"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-12364"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-27170"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-8648"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2021-3347"
    }
  ],
  "Issued": {
    "Date": "2021-06-08"
  }
}