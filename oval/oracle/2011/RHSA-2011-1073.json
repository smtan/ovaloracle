{
  "Title": "RHSA-2011:1073: bash security, bug fix, and enhancement update (Low)",
  "Description": "Bash is the default shell for Red Hat Enterprise Linux.\nIt was found that certain scripts bundled with the Bash documentation\ncreated temporary files in an insecure way. A malicious, local user could\nuse this flaw to conduct a symbolic link attack, allowing them to overwrite\nthe contents of arbitrary files accessible to the victim running the\nscripts. (CVE-2008-5374)\nThis update fixes the following bugs:\n* When using the source builtin at location \".\", occasionally, bash\nopted to preserve internal consistency and abort scripts. This caused\nbash to abort scripts that assigned values to read-only variables.\nThis is now fixed to ensure that such scripts are now executed as\nwritten and not aborted. (BZ#448508)\n* When the tab key was pressed for auto-completion options for the typed\ntext, the cursor moved to an unexpected position on a previous line if\nthe prompt contained characters that cannot be viewed and a \"\\]\". This\nis now fixed to retain the cursor at the expected position at the end of\nthe target line after autocomplete options correctly display. (BZ#463880)\n* Bash attempted to interpret the NOBITS .dynamic section of the ELF\nheader. This resulted in a \"^D: bad ELF interpreter: No such\nfile or directory\" message. This is fixed to ensure that the invalid\n\"^D\" does not appear in the error message. (BZ#484809)\n* The $RANDOM variable in Bash carried over values from a previous\nexecution for later jobs. This is fixed and the $RANDOM variable\ngenerates a new random number for each use. (BZ#492908)\n* When Bash ran a shell script with an embedded null character, bash's\nsource builtin parsed the script incorrectly. This is fixed and\nbash's source builtin correctly parses shell script null characters.\n(BZ#503701)\n* The bash manual page for \"trap\" did not mention that signals ignored upon\nentry cannot be listed later. The manual page was updated for this update\nand now specifically notes that \"Signals ignored upon entry to the shell\ncannot be trapped, reset or listed\". (BZ#504904)\n* Bash's readline incorrectly displayed additional text when resizing\nthe terminal window when text spanned more than one line, which caused\nincorrect display output. This is now fixed to ensure that text in more\nthan one line in a resized window displays as expected. (BZ#525474)\n* Previously, bash incorrectly displayed \"Broken pipe\" messages for\nbuiltins like \"echo\" and \"printf\" when output did not succeed due to\nEPIPE. This is fixed to ensure that the unnecessary \"Broken pipe\"\nmessages no longer display. (BZ#546529)\n* Inserts with the repeat function were not possible after a deletion in\nvi-mode. This has been corrected and, with this update, the repeat function\nworks as expected after a deletion. (BZ#575076)\n* In some situations, bash incorrectly appended \"/\" to files instead of\njust directories during tab-completion, causing incorrect\nauto-completions. This is fixed and auto-complete appends \"/\" only to\ndirectories. (BZ#583919)\n* Bash had a memory leak in the \"read\" builtin when the number of fields\nbeing read was not equal to the number of variables passed as arguments,\ncausing a shell script crash. This is fixed to prevent a memory leak and\nshell script crash. (BZ#618393)\n* /usr/share/doc/bash-3.2/loadables in the bash package contained source\nfiles which would not build due to missing C header files. With this\nupdate, the unusable (and unbuildable) source files were removed from the\npackage. (BZ#663656)\nThis update also adds the following enhancement:\n* The system-wide \"/etc/bash.bash_logout\" bash logout file is now enabled.\nThis allows administrators to write system-wide logout actions for all\nusers. (BZ#592979)\nUsers of bash are advised to upgrade to this updated package, which\ncontains backported patches to resolve these issues and add this\nenhancement.",
  "Platform": [
    "Red Hat Enterprise Linux 5"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2011:1073",
      "ID": "RHSA-2011:1073"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-5374",
      "ID": "CVE-2008-5374"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [

        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 5 is installed"
          },
          {
            "Comment": "bash is earlier than 0:3.2-32.el5"
          },
          {
            "Comment": "bash is signed with Red Hat redhatrelease2 key"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 5 is installed"
      }
    ]
  },
  "Severity": "Low",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-5374"
    }
  ],
  "Issued": {
    "Date": "2011-07-21"
  }
}