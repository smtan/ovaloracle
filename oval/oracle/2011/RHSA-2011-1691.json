{
  "Title": "RHSA-2011:1691: util-linux-ng security, bug fix, and enhancement update (Low)",
  "Description": "The util-linux-ng packages contain a large variety of low-level system\nutilities that are necessary for a Linux operating system to function.\nMultiple flaws were found in the way the mount and umount commands\nperformed mtab (mounted file systems table) file updates. A local,\nunprivileged user allowed to mount or unmount file systems could use these\nflaws to corrupt the mtab file and create a stale lock file, preventing\nother users from mounting and unmounting file systems. (CVE-2011-1675,\nCVE-2011-1677)\nThis update also fixes the following bugs:\n* Due to a hard coded limit of 128 devices, an attempt to run the\n\"blkid -c\" command on more than 128 devices caused blkid to terminate\nunexpectedly. This update increases the maximum number of devices to 8192\nso that blkid no longer crashes in this scenario. (BZ#675999)\n* Previously, the \"swapon -a\" command did not detect device-mapper\ndevices that were already in use. This update corrects the swapon utility\nto detect such devices as expected. (BZ#679741)\n* Prior to this update, the presence of an invalid line in the /etc/fstab\nfile could cause the umount utility to terminate unexpectedly with\na segmentation fault. This update applies a patch that corrects this error\nso that umount now correctly reports invalid lines and no longer crashes.\n(BZ#684203)\n* Previously, an attempt to use the wipefs utility on a partitioned\ndevice caused the utility to terminate unexpectedly with an error. This\nupdate adapts wipefs to only display a warning message in this situation.\n(BZ#696959)\n* When providing information on interprocess communication (IPC)\nfacilities, the ipcs utility could previously display a process owner as\na negative number if the user's UID was too large. This update adapts the\nunderlying source code to make sure the UID values are now displayed\ncorrectly. (BZ#712158)\n* In the installation scriptlets, the uuidd package uses the chkconfig\nutility to enable and disable the uuidd service. Previously, this package\ndid not depend on the chkconfig package, which could lead to errors during\ninstallation if chkconfig was not installed. This update adds chkconfig\nto the list of dependencies so that such errors no longer occur.\n(BZ#712808)\n* The previous version of the /etc/udev/rules.d/60-raw.rules file\ncontained a statement that both this file and raw devices are deprecated.\nThis is no longer true and the Red Hat Enterprise Linux kernel supports\nthis functionality. With this update, the aforementioned file no longer\ncontains this incorrect statement. (BZ#716995)\n* Previously, an attempt to use the cfdisk utility to read the default\nRed Hat Enterprise Linux 6 partition layout failed with an error. This\nupdate corrects this error and the cfdisk utility can now read the default\npartition layout as expected. (BZ#723352)\n* The previous version of the tailf(1) manual page incorrectly stated that\nusers can use the \"--lines=NUMBER\" command line option to limit the number\nof displayed lines. However, the tailf utility does not allow the use of\nthe equals sign (=) between the option and its argument. This update\ncorrects this error. (BZ#679831)\n* The fstab(5) manual page has been updated to clarify that empty lines in\nthe /etc/fstab configuration file are ignored. (BZ#694648)\nAs well, this update adds the following enhancements:\n* A new fstrim utility has been added to the package. This utility allows\nthe root user to discard unused blocks on a mounted file system.\n(BZ#692119)\n* The login utility has been updated to provide support for failed login\nattempts that are reported by PAM. (BZ#696731)\n* The lsblk utility has been updated to provide additional information\nabout the topology and status of block devices. (BZ#723638)\n* The agetty utility has been updated to pass the hostname to the login\nutility. (BZ#726092)\nAll users of util-linux-ng are advised to upgrade to these updated\npackages, which contain backported patches to correct these issues and add\nthese enhancements.",
  "Platform": [
    "Red Hat Enterprise Linux 6"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2011:1691",
      "ID": "RHSA-2011:1691"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-1675",
      "ID": "CVE-2011-1675"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-1677",
      "ID": "CVE-2011-1677"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libblkid is earlier than 0:2.17.2-12.4.el6"
                  },
                  {
                    "Comment": "libblkid is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libblkid-devel is earlier than 0:2.17.2-12.4.el6"
                  },
                  {
                    "Comment": "libblkid-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libuuid is earlier than 0:2.17.2-12.4.el6"
                  },
                  {
                    "Comment": "libuuid is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libuuid-devel is earlier than 0:2.17.2-12.4.el6"
                  },
                  {
                    "Comment": "libuuid-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "util-linux-ng is earlier than 0:2.17.2-12.4.el6"
                  },
                  {
                    "Comment": "util-linux-ng is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "uuidd is earlier than 0:2.17.2-12.4.el6"
                  },
                  {
                    "Comment": "uuidd is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Low",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-1675"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-1677"
    }
  ],
  "Issued": {
    "Date": "2011-12-05"
  }
}