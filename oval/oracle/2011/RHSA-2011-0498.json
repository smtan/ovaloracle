{
  "Title": "RHSA-2011:0498: kernel security, bug fix, and enhancement update (Important)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux\noperating system.\nSecurity fixes:\n* An integer overflow flaw in ib_uverbs_poll_cq() could allow a local,\nunprivileged user to cause a denial of service or escalate their\nprivileges. (CVE-2010-4649, Important)\n* An integer signedness flaw in drm_modeset_ctl() could allow a local,\nunprivileged user to cause a denial of service or escalate their\nprivileges. (CVE-2011-1013, Important)\n* The Radeon GPU drivers in the Linux kernel were missing sanity checks for\nthe Anti Aliasing (AA) resolve register values which could allow a local,\nunprivileged user to cause a denial of service or escalate their privileges\non systems using a graphics card from the ATI Radeon R300, R400, or R500\nfamily of cards. (CVE-2011-1016, Important)\n* A flaw in dccp_rcv_state_process() could allow a remote attacker to\ncause a denial of service, even when the socket was already closed.\n(CVE-2011-1093, Important)\n* A flaw in the Linux kernel's Stream Control Transmission Protocol (SCTP)\nimplementation could allow a remote attacker to cause a denial of service\nif the sysctl \"net.sctp.addip_enable\" and \"auth_enable\" variables were\nturned on (they are off by default). (CVE-2011-1573, Important)\n* A memory leak in the inotify_init() system call. In some cases, it could\nleak a group, which could allow a local, unprivileged user to eventually\ncause a denial of service. (CVE-2010-4250, Moderate)\n* A missing validation of a null-terminated string data structure element\nin bnep_sock_ioctl() could allow a local user to cause an information leak\nor a denial of service. (CVE-2011-1079, Moderate)\n* An information leak in bcm_connect() in the Controller Area Network (CAN)\nBroadcast Manager implementation could allow a local, unprivileged user to\nleak kernel mode addresses in \"/proc/net/can-bcm\". (CVE-2010-4565, Low)\n* A flaw was found in the Linux kernel's Integrity Measurement Architecture\n(IMA) implementation. When SELinux was disabled, adding an IMA rule which\nwas supposed to be processed by SELinux would cause ima_match_rules() to\nalways succeed, ignoring any remaining rules. (CVE-2011-0006, Low)\n* A missing initialization flaw in the XFS file system implementation could\nlead to an information leak. (CVE-2011-0711, Low)\n* Buffer overflow flaws in snd_usb_caiaq_audio_init() and\nsnd_usb_caiaq_midi_init() could allow a local, unprivileged user with\naccess to a Native Instruments USB audio device to cause a denial of\nservice or escalate their privileges. (CVE-2011-0712, Low)\n* The start_code and end_code values in \"/proc/[pid]/stat\" were not\nprotected. In certain scenarios, this flaw could be used to defeat Address\nSpace Layout Randomization (ASLR). (CVE-2011-0726, Low)\n* A flaw in dev_load() could allow a local user who has the CAP_NET_ADMIN\ncapability to load arbitrary modules from \"/lib/modules/\", instead of only\nnetdev modules. (CVE-2011-1019, Low)\n* A flaw in ib_uverbs_poll_cq() could allow a local, unprivileged user to\ncause an information leak. (CVE-2011-1044, Low)\n* A missing validation of a null-terminated string data structure element\nin do_replace() could allow a local user who has the CAP_NET_ADMIN\ncapability to cause an information leak. (CVE-2011-1080, Low)\nRed Hat would like to thank Vegard Nossum for reporting CVE-2010-4250;\nVasiliy Kulikov for reporting CVE-2011-1079, CVE-2011-1019, and\nCVE-2011-1080; Dan Rosenberg for reporting CVE-2010-4565 and CVE-2011-0711;\nRafael Dominguez Vega for reporting CVE-2011-0712; and Kees Cook for\nreporting CVE-2011-0726.\nThis update also fixes various bugs and adds an enhancement. Documentation\nfor these changes will be available shortly from the Technical Notes\ndocument linked to in the References section.\nUsers should upgrade to these updated packages, which contain backported\npatches to resolve these issues, and fix the bugs and add the enhancement\nnoted in the Technical Notes. The system must be rebooted for this update\nto take effect.",
  "Platform": [
    "Red Hat Enterprise Linux 6"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2011:0498",
      "ID": "RHSA-2011:0498"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2010-4250",
      "ID": "CVE-2010-4250"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2010-4565",
      "ID": "CVE-2010-4565"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2010-4649",
      "ID": "CVE-2010-4649"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-0006",
      "ID": "CVE-2011-0006"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-0711",
      "ID": "CVE-2011-0711"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-0712",
      "ID": "CVE-2011-0712"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-0726",
      "ID": "CVE-2011-0726"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-1013",
      "ID": "CVE-2011-1013"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-1016",
      "ID": "CVE-2011-1016"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-1019",
      "ID": "CVE-2011-1019"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-1044",
      "ID": "CVE-2011-1044"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-1079",
      "ID": "CVE-2011-1079"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-1080",
      "ID": "CVE-2011-1080"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-1093",
      "ID": "CVE-2011-1093"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-1573",
      "ID": "CVE-2011-1573"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.32-71.29.1.el6 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.32-71.29.1.el6 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.32-71.29.1.el6"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-bootwrapper is earlier than 0:2.6.32-71.29.1.el6"
                  },
                  {
                    "Comment": "kernel-bootwrapper is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:2.6.32-71.29.1.el6"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:2.6.32-71.29.1.el6"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.32-71.29.1.el6"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.32-71.29.1.el6"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-firmware is earlier than 0:2.6.32-71.29.1.el6"
                  },
                  {
                    "Comment": "kernel-firmware is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:2.6.32-71.29.1.el6"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:2.6.32-71.29.1.el6"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:2.6.32-71.29.1.el6"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perf is earlier than 0:2.6.32-71.29.1.el6"
                  },
                  {
                    "Comment": "perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2010-4250"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2010-4565"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2010-4649"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-0006"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-0711"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-0712"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-0726"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-1013"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-1016"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-1019"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-1044"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-1079"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-1080"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-1093"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-1573"
    }
  ],
  "Issued": {
    "Date": "2011-05-10"
  }
}