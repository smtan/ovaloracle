{
  "Title": "RHSA-2011:1849: kernel security and bug fix update (Important)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux\noperating system.\nSecurity fix:\n* Using the SG_IO IOCTL to issue SCSI requests to partitions or LVM volumes\nresulted in the requests being passed to the underlying block device. If a\nprivileged user only had access to a single partition or LVM volume, they\ncould use this flaw to bypass those restrictions and gain read and write\naccess (and be able to issue other SCSI commands) to the entire block\ndevice.\nIn KVM (Kernel-based Virtual Machine) environments using raw format virtio\ndisks backed by a partition or LVM volume, a privileged guest user could\nbypass intended restrictions and issue read and write requests (and other\nSCSI commands) on the host, and possibly access the data of other guests\nthat reside on the same underlying block device. Partition-based and\nLVM-based storage pools are not used by default. Refer to Red Hat Bugzilla\nbug 752375 for further details and a mitigation script for users who cannot\napply this update immediately. (CVE-2011-4127, Important)\nBug fixes:\n* Previously, idle load balancer kick requests from other CPUs could be\nserviced without first receiving an inter-processor interrupt (IPI). This\ncould have led to a deadlock. (BZ#750459)\n* This update fixes a performance regression that may have caused processes\n(including KVM guests) to hang for a number of seconds. (BZ#751403)\n* When md_raid1_unplug_device() was called while holding a spinlock, under\ncertain device failure conditions, it was possible for the lock to be\nrequested again, deeper in the call chain, causing a deadlock. Now,\nmd_raid1_unplug_device() is no longer called while holding a spinlock.\n(BZ#755545)\n* In hpet_next_event(), an interrupt could have occurred between the read\nand write of the HPET (High Performance Event Timer) and the value of\nHPET_COUNTER was then beyond that being written to the comparator\n(HPET_Tn_CMP). Consequently, the timers were overdue for up to several\nminutes. Now, a comparison is performed between the value of the counter\nand the comparator in the HPET code. If the counter is beyond the\ncomparator, the \"-ETIME\" error code is returned. (BZ#756426)\n* Index allocation in the virtio-blk module was based on a monotonically\nincreasing variable \"index\". Consequently, released indexes were not reused\nand after a period of time, no new were available. Now, virtio-blk uses the\nida API to allocate indexes. (BZ#756427)\n* A bug related to Context Caching existed in the Intel IOMMU support\nmodule. On some newer Intel systems, the Context Cache mode has changed\nfrom previous hardware versions, potentially exposing a Context coherency\nrace. The bug was exposed when performing a series of hot plug and unplug\noperations of a Virtual Function network device which was immediately\nconfigured into the network stack, i.e., successfully performed dynamic\nhost configuration protocol (DHCP). When the coherency race occurred, the\nassigned device would not work properly in the guest virtual machine. With\nthis update, the Context coherency is corrected and the race and\npotentially resulting device assignment failure no longer occurs.\n(BZ#757671)\n* The align_va_addr kernel parameter was ignored if secondary CPUs were\ninitialized. This happened because the parameter settings were overridden\nduring the initialization of secondary CPUs. Also, the align_va_addr\nparameter documentation contained incorrect parameter arguments. With this\nupdate, the underlying code has been modified to prevent the overriding and\nthe documentation has been updated. This update also removes the unused\ncode introduced by the patch for BZ#739456. (BZ#758028)\n* Dell systems based on a future Intel processor with graphics acceleration\nrequired the selection of the install system with basic video driver\ninstallation option. This update removes this requirement. (BZ#758513)",
  "Platform": [
    "Red Hat Enterprise Linux 6"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2011:1849",
      "ID": "RHSA-2011:1849"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-4127",
      "ID": "CVE-2011-4127"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-4621",
      "ID": "CVE-2011-4621"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.32-220.2.1.el6 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.32-220.2.1.el6 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.32-220.2.1.el6"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-bootwrapper is earlier than 0:2.6.32-220.2.1.el6"
                  },
                  {
                    "Comment": "kernel-bootwrapper is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:2.6.32-220.2.1.el6"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:2.6.32-220.2.1.el6"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.32-220.2.1.el6"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.32-220.2.1.el6"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-firmware is earlier than 0:2.6.32-220.2.1.el6"
                  },
                  {
                    "Comment": "kernel-firmware is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:2.6.32-220.2.1.el6"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:2.6.32-220.2.1.el6"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:2.6.32-220.2.1.el6"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perf is earlier than 0:2.6.32-220.2.1.el6"
                  },
                  {
                    "Comment": "perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "python-perf is earlier than 0:2.6.32-220.2.1.el6"
                  },
                  {
                    "Comment": "python-perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-4127"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-4621"
    }
  ],
  "Issued": {
    "Date": "2011-12-22"
  }
}