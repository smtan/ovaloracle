{
  "Title": "RHSA-2017:0906: httpd security and bug fix update (Moderate)",
  "Description": "The httpd packages provide the Apache HTTP Server, a powerful, efficient, and extensible web server.\nSecurity Fix(es):\n* It was discovered that the mod_session_crypto module of httpd did not use any mechanisms to verify integrity of the encrypted session data stored in the user's browser. A remote attacker could use this flaw to decrypt and modify session data using a padding oracle attack. (CVE-2016-0736)\n* It was discovered that the mod_auth_digest module of httpd did not properly check for memory allocation failures. A remote attacker could use this flaw to cause httpd child processes to repeatedly crash if the server used HTTP digest authentication. (CVE-2016-2161)\n* It was discovered that the HTTP parser in httpd incorrectly allowed certain characters not permitted by the HTTP protocol specification to appear unencoded in HTTP request headers. If httpd was used in conjunction with a proxy or backend server that interpreted those characters differently, a remote attacker could possibly use this flaw to inject data into HTTP responses, resulting in proxy cache poisoning. (CVE-2016-8743)\nNote: The fix for the CVE-2016-8743 issue causes httpd to return \"400 Bad Request\" error to HTTP clients which do not strictly follow HTTP protocol specification. A newly introduced configuration directive \"HttpProtocolOptions Unsafe\" can be used to re-enable the old less strict parsing. However, such setting also re-introduces the CVE-2016-8743 issue.\nBug Fix(es):\n* When waking up child processes during a graceful restart, the httpd parent process could attempt to open more connections than necessary if a large number of child processes had been active prior to the restart. Consequently, a graceful restart could take a long time to complete. With this update, httpd has been fixed to limit the number of connections opened during a graceful restart to the number of active children, and the described problem no longer occurs. (BZ#1420002)\n* Previously, httpd running in a container returned the 500 HTTP status code (Internal Server Error) when a connection to a WebSocket server was closed. As a consequence, the httpd server failed to deliver the correct HTTP status and data to a client. With this update, httpd correctly handles all proxied requests to the WebSocket server, and the described problem no longer occurs. (BZ#1429947)\n* In a configuration using LDAP authentication with the mod_authnz_ldap module, the name set using the AuthLDAPBindDN directive was not correctly used to bind to the LDAP server for all queries. Consequently, authorization attempts failed. The LDAP modules have been fixed to ensure the configured name is correctly bound for LDAP queries, and authorization using LDAP no longer fails. (BZ#1420047)",
  "Platform": [
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2017:0906",
      "ID": "RHSA-2017:0906"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-0736",
      "ID": "CVE-2016-0736"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-2161",
      "ID": "CVE-2016-2161"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-4975",
      "ID": "CVE-2016-4975"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-8743",
      "ID": "CVE-2016-8743"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "httpd is earlier than 0:2.4.6-45.el7_3.4"
                  },
                  {
                    "Comment": "httpd is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "httpd-devel is earlier than 0:2.4.6-45.el7_3.4"
                  },
                  {
                    "Comment": "httpd-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "httpd-manual is earlier than 0:2.4.6-45.el7_3.4"
                  },
                  {
                    "Comment": "httpd-manual is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "httpd-tools is earlier than 0:2.4.6-45.el7_3.4"
                  },
                  {
                    "Comment": "httpd-tools is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mod_ldap is earlier than 0:2.4.6-45.el7_3.4"
                  },
                  {
                    "Comment": "mod_ldap is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mod_proxy_html is earlier than 1:2.4.6-45.el7_3.4"
                  },
                  {
                    "Comment": "mod_proxy_html is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mod_session is earlier than 0:2.4.6-45.el7_3.4"
                  },
                  {
                    "Comment": "mod_session is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mod_ssl is earlier than 1:2.4.6-45.el7_3.4"
                  },
                  {
                    "Comment": "mod_ssl is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 7 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-0736"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-2161"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-4975"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-8743"
    }
  ],
  "Issued": {
    "Date": "2017-04-12"
  }
}