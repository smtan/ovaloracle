{
  "Title": "RHSA-2017:0252: ntp security update (Moderate)",
  "Description": "The Network Time Protocol (NTP) is used to synchronize a computer's time with another referenced time source. These packages include the ntpd service which continuously adjusts system time and utilities used to query and configure the ntpd service.\nSecurity Fix(es):\n* It was found that when ntp is configured with rate limiting for all associations the limits are also applied to responses received from its configured sources. A remote attacker who knows the sources can cause a denial of service by preventing ntpd from accepting valid responses from its sources. (CVE-2016-7426)\n* A flaw was found in the control mode functionality of ntpd. A remote attacker could send a crafted control mode packet which could lead to information disclosure or result in DDoS amplification attacks. (CVE-2016-9310)\n* A flaw was found in the way ntpd implemented the trap service. A remote attacker could send a specially crafted packet to cause a null pointer dereference that will crash ntpd, resulting in a denial of service. (CVE-2016-9311)\n* A flaw was found in the way ntpd running on a host with multiple network interfaces handled certain server responses. A remote attacker could use this flaw which would cause ntpd to not synchronize with the source. (CVE-2016-7429)\n* A flaw was found in the way ntpd calculated the root delay. A remote attacker could send a specially-crafted spoofed packet to cause denial of service or in some special cases even crash. (CVE-2016-7433)",
  "Platform": [
    "Red Hat Enterprise Linux 6",
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2017:0252",
      "ID": "RHSA-2017:0252"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-7426",
      "ID": "CVE-2016-7426"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-7429",
      "ID": "CVE-2016-7429"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-7433",
      "ID": "CVE-2016-7433"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-9310",
      "ID": "CVE-2016-9310"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-9311",
      "ID": "CVE-2016-9311"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ntp is earlier than 0:4.2.6p5-10.el6_8.2"
                  },
                  {
                    "Comment": "ntp is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ntp-doc is earlier than 0:4.2.6p5-10.el6_8.2"
                  },
                  {
                    "Comment": "ntp-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ntp-perl is earlier than 0:4.2.6p5-10.el6_8.2"
                  },
                  {
                    "Comment": "ntp-perl is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ntpdate is earlier than 0:4.2.6p5-10.el6_8.2"
                  },
                  {
                    "Comment": "ntpdate is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      },
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ntp is earlier than 0:4.2.6p5-25.el7_3.1"
                  },
                  {
                    "Comment": "ntp is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ntp-doc is earlier than 0:4.2.6p5-25.el7_3.1"
                  },
                  {
                    "Comment": "ntp-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ntp-perl is earlier than 0:4.2.6p5-25.el7_3.1"
                  },
                  {
                    "Comment": "ntp-perl is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ntpdate is earlier than 0:4.2.6p5-25.el7_3.1"
                  },
                  {
                    "Comment": "ntpdate is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "sntp is earlier than 0:4.2.6p5-25.el7_3.1"
                  },
                  {
                    "Comment": "sntp is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-7426"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-7429"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-7433"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-9310"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-9311"
    }
  ],
  "Issued": {
    "Date": "2017-02-06"
  }
}