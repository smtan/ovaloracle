{
  "Title": "RHSA-2017:0817: kernel security, bug fix, and enhancement update (Moderate)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux operating system.\nSecurity Fix(es):\n* It was discovered that a remote attacker could leverage the generation of IPv6 atomic fragments to trigger the use of fragmentation in an arbitrary IPv6 flow (in scenarios in which actual fragmentation of packets is not needed) and could subsequently perform any type of a fragmentation-based attack against legacy IPv6 nodes that do not implement RFC6946. (CVE-2016-10142, Moderate)\n* A flaw was discovered in the way the Linux kernel dealt with paging structures. When the kernel invalidated a paging structure that was not in use locally, it could, in principle, race against another CPU that is switching to a process that uses the paging structure in question. A local user could use a thread running with a stale cached virtual->physical translation to potentially escalate their privileges if the translation in question were writable and the physical page got reused for something critical (for example, a page table). (CVE-2016-2069, Moderate)\n* A race condition flaw was found in the ioctl_send_fib() function in the Linux kernel's aacraid implementation. A local attacker could use this flaw to cause a denial of service (out-of-bounds access or system crash) by changing a certain size value. (CVE-2016-6480, Moderate)\n* It was found that when the gcc stack protector was enabled, reading the /proc/keys file could cause a panic in the Linux kernel due to stack corruption. This happened because an incorrect buffer size was used to hold a 64-bit timeout value rendered as weeks. (CVE-2016-7042, Moderate)\n* It was found that when file permissions were modified via chmod and the user modifying them was not in the owning group or capable of CAP_FSETID, the setgid bit would be cleared. Setting a POSIX ACL via setxattr sets the file permissions as well as the new ACL, but doesn't clear the setgid bit in a similar way. This could allow a local user to gain group privileges via certain setgid applications. (CVE-2016-7097, Moderate)\n* A flaw was found in the Linux networking subsystem where a local attacker with CAP_NET_ADMIN capabilities could cause an out-of-bounds memory access by creating a smaller-than-expected ICMP header and sending to its destination via sendto(). (CVE-2016-8399, Moderate)\n* It was found that the blk_rq_map_user_iov() function in the Linux kernel's block device implementation did not properly restrict the type of iterator, which could allow a local attacker to read or write to arbitrary kernel memory locations or cause a denial of service (use-after-free) by leveraging write access to a /dev/sg device. (CVE-2016-9576, CVE-2016-10088, Moderate)\n* A flaw was found in the USB-MIDI Linux kernel driver: a double-free error could be triggered for the 'umidi' object. An attacker with physical access to the system could use this flaw to escalate their privileges. (CVE-2016-2384, Low)\nThe CVE-2016-7042 issue was discovered by Ondrej Kozina (Red Hat) and the CVE-2016-7097 issue was discovered by Andreas Gruenbacher (Red Hat) and Jan Kara (SUSE).\nAdditional Changes:\nFor detailed information on changes in this release, see the Red Hat Enterprise Linux 6.9 Release Notes and Red Hat Enterprise Linux 6.9 Technical Notes linked from the References section.",
  "Platform": [
    "Red Hat Enterprise Linux 6"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2017:0817",
      "ID": "RHSA-2017:0817"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-10088",
      "ID": "CVE-2016-10088"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-10142",
      "ID": "CVE-2016-10142"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-2069",
      "ID": "CVE-2016-2069"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-2384",
      "ID": "CVE-2016-2384"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-6480",
      "ID": "CVE-2016-6480"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-7042",
      "ID": "CVE-2016-7042"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-7097",
      "ID": "CVE-2016-7097"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-8399",
      "ID": "CVE-2016-8399"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-9576",
      "ID": "CVE-2016-9576"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-5551",
      "ID": "CVE-2017-5551"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.32-696.el6 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.32-696.el6 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.32-696.el6"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-abi-whitelists is earlier than 0:2.6.32-696.el6"
                  },
                  {
                    "Comment": "kernel-abi-whitelists is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-bootwrapper is earlier than 0:2.6.32-696.el6"
                  },
                  {
                    "Comment": "kernel-bootwrapper is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:2.6.32-696.el6"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:2.6.32-696.el6"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.32-696.el6"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.32-696.el6"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-firmware is earlier than 0:2.6.32-696.el6"
                  },
                  {
                    "Comment": "kernel-firmware is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:2.6.32-696.el6"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:2.6.32-696.el6"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:2.6.32-696.el6"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perf is earlier than 0:2.6.32-696.el6"
                  },
                  {
                    "Comment": "perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "python-perf is earlier than 0:2.6.32-696.el6"
                  },
                  {
                    "Comment": "python-perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-10088"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-10142"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-2069"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-2384"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-6480"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-7042"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-7097"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-8399"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-9576"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-5551"
    }
  ],
  "Issued": {
    "Date": "2017-03-21"
  }
}