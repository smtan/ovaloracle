{
  "Title": "RHSA-2017:3200: kernel security and bug fix update (Important)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux operating system.\nSecurity Fix(es):\n* A race condition issue leading to a use-after-free flaw was found in the way the raw packet sockets are implemented in the Linux kernel networking subsystem handling synchronization. A local user able to open a raw packet socket (requires the CAP_NET_RAW capability) could use this flaw to elevate their privileges on the system. (CVE-2017-1000111, Important)\n* An exploitable memory corruption flaw was found in the Linux kernel. The append path can be erroneously switched from UFO to non-UFO in ip_ufo_append_data() when building an UFO packet with MSG_MORE option. If unprivileged user namespaces are available, this flaw can be exploited to gain root privileges. (CVE-2017-1000112, Important)\n* A divide-by-zero vulnerability was found in the __tcp_select_window function in the Linux kernel. This can result in a kernel panic causing a local denial of service. (CVE-2017-14106, Moderate)\nRed Hat would like to thank Willem de Bruijn for reporting CVE-2017-1000111 and Andrey Konovalov for reporting CVE-2017-1000112.\nBug Fix(es):\n* When the operating system was booted with Red Hat Enterprise Virtualization, and the eh_deadline sysfs parameter was set to 10s, the Storage Area Network (SAN) issues caused eh_deadline to trigger with no handler. Consequently, a kernel panic occurred. This update fixes the lpfc driver, thus preventing the kernel panic under described circumstances. (BZ#1487220)\n* When an NFS server returned the NFS4ERR_BAD_SEQID error to an OPEN request, the open-owner was removed from the state_owners rbtree. Consequently, NFS4 client infinite loop that required a reboot to recover occurred. This update changes NFS4ERR_BAD_SEQID handling to leave the open-owner in the state_owners rbtree by updating the create_time parameter so that it looks like a new open-owner. As a result, an NFS4 client is now able to recover without falling into the infinite recovery loop after receiving NFS4ERR_BAD_SEQID. (BZ#1491123)\n* If an NFS client attempted to mount NFSv3 shares from an NFS server exported directly to the client's IP address, and this NFS client had already mounted other shares that originated from the same server but were exported to the subnetwork which this client was part of, the auth.unix.ip cache expiration was not handled correctly. Consequently, the client received the 'stale file handle' errors when trying to mount the share. This update fixes handling of the cache expiration, and the NFSv3 shares now mount as expected without producing the 'stale file handle' errors. (BZ#1497976)\n* When running a script that raised the tx ring count to its maximum value supported by the Solarflare Network Interface Controller (NIC) driver, the EF10 family NICs allowed the settings exceeding the hardware's capability. Consequently, the Solarflare hardware became unusable with Red Hat Entepripse Linux 6. This update fixes the sfc driver, so that the tx ring can have maximum 2048 entries for all EF10 NICs. As a result, the Solarflare hardware no longer becomes unusable with Red Hat Entepripse Linux 6 due to this bug. (BZ#1498019)",
  "Platform": [
    "Red Hat Enterprise Linux 6"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2017:3200",
      "ID": "RHSA-2017:3200"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-1000111",
      "ID": "CVE-2017-1000111"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-1000112",
      "ID": "CVE-2017-1000112"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-14106",
      "ID": "CVE-2017-14106"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.32-696.16.1.el6 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.32-696.16.1.el6 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.32-696.16.1.el6"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-abi-whitelists is earlier than 0:2.6.32-696.16.1.el6"
                  },
                  {
                    "Comment": "kernel-abi-whitelists is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-bootwrapper is earlier than 0:2.6.32-696.16.1.el6"
                  },
                  {
                    "Comment": "kernel-bootwrapper is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:2.6.32-696.16.1.el6"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:2.6.32-696.16.1.el6"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.32-696.16.1.el6"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.32-696.16.1.el6"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-firmware is earlier than 0:2.6.32-696.16.1.el6"
                  },
                  {
                    "Comment": "kernel-firmware is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:2.6.32-696.16.1.el6"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:2.6.32-696.16.1.el6"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:2.6.32-696.16.1.el6"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perf is earlier than 0:2.6.32-696.16.1.el6"
                  },
                  {
                    "Comment": "perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "python-perf is earlier than 0:2.6.32-696.16.1.el6"
                  },
                  {
                    "Comment": "python-perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-1000111"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-1000112"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-14106"
    }
  ],
  "Issued": {
    "Date": "2017-11-14"
  }
}