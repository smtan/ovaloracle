{
  "Title": "RHSA-2017:0307: kernel security and bug fix update (Moderate)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux operating system.\nSecurity Fix(es):\n* When creating audit records for parameters to executed children processes, an attacker can convince the Linux kernel audit subsystem can create corrupt records which may allow an attacker to misrepresent or evade logging of executing commands. (CVE-2016-6136, Moderate)\n* A flaw was found in the Linux kernel's implementation of the SCTP protocol. A remote attacker could trigger an out-of-bounds read with an offset of up to 64kB potentially causing the system to crash. (CVE-2016-9555, Moderate)\nBug Fix(es):\n* The qlnic driver previously attempted to fetch pending transmission descriptors before all writes were complete, which lead to firmware hangs. With this update, the qlcnic driver has been fixed to complete all writes before the hardware fetches any pending transmission descriptors. As a result, the firmware no longer hangs with the qlcnic driver. (BZ#1403143)\n* Previously, when a NFS share was mounted, the file-system (FS) cache was incorrectly enabled even when the \"-o fsc\" option was not used in the mount command. Consequently, the cachefilesd service stored files in the NFS share even when not instructed to by the user. With this update, NFS does not use the FS cache if not instructed by the \"-o fsc\" option. As a result, NFS no longer enables caching if the \"-o fsc\" option is not used. (BZ#1399172)\n* Previously, an NFS client and NFS server got into a NFS4 protocol loop involving a WRITE action and a NFS4ERR_EXPIRED response when the current_fileid counter got to the wraparound point by overflowing the value of 32 bits. This update fixes the NFS server to handle the current_fileid wraparound. As a result, the described NFS4 protocol loop no longer occurs. (BZ#1399174)\n* Previously, certain configurations of the Hewlett Packard Smart Array (HPSA) devices caused hardware to be set offline incorrectly when the HPSA driver was expected to wait for existing I/O operations to complete. Consequently, a kernel panic occurred. This update prevents the described problem. As a result, the kernel panic no longer occurs. (BZ#1399175)\n* Previously, memory corruption by copying data into the wrong memory locations sometimes occurred, because the __copy_tofrom_user() function was returning incorrect values. This update fixes the __copy_tofrom_user() function so that it no longer returns larger values than the number of bytes it was asked to copy. As a result, memory corruption no longer occurs in he described scenario. (BZ#1398185)\n* Previously, guest virtual machines (VMs) on a Hyper-V server cluster got in some cases rebooted during the graceful node failover test, because the host kept sending heartbeat packets independently of guests responding to them. This update fixes the bug by properly responding to all the heartbeat messages in the queue, even if they are pending. As a result, guest VMs no longer get rebooted under the described circumstances. (BZ#1397739)\n* When the \"punching hole\" feature of the fallocate utility was used on an ext4 file system inode with extent depth of 1, the extent tree of the inode sometimes became corrupted. With this update, the underlying source code has been fixed, and extent tree corruption no longer occurs in the described situation. (BZ#1397808)",
  "Platform": [
    "Red Hat Enterprise Linux 6"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2017:0307",
      "ID": "RHSA-2017:0307"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-6136",
      "ID": "CVE-2016-6136"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-9555",
      "ID": "CVE-2016-9555"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.32-642.15.1.el6 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.32-642.15.1.el6 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.32-642.15.1.el6"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-abi-whitelists is earlier than 0:2.6.32-642.15.1.el6"
                  },
                  {
                    "Comment": "kernel-abi-whitelists is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-bootwrapper is earlier than 0:2.6.32-642.15.1.el6"
                  },
                  {
                    "Comment": "kernel-bootwrapper is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:2.6.32-642.15.1.el6"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:2.6.32-642.15.1.el6"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.32-642.15.1.el6"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.32-642.15.1.el6"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-firmware is earlier than 0:2.6.32-642.15.1.el6"
                  },
                  {
                    "Comment": "kernel-firmware is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:2.6.32-642.15.1.el6"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:2.6.32-642.15.1.el6"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:2.6.32-642.15.1.el6"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perf is earlier than 0:2.6.32-642.15.1.el6"
                  },
                  {
                    "Comment": "perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "python-perf is earlier than 0:2.6.32-642.15.1.el6"
                  },
                  {
                    "Comment": "python-perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-6136"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-9555"
    }
  ],
  "Issued": {
    "Date": "2017-02-23"
  }
}