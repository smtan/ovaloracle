{
  "Title": "RHSA-2017:1204: java-1.7.0-openjdk security update (Moderate)",
  "Description": "The java-1.7.0-openjdk packages provide the OpenJDK 7 Java Runtime Environment and the OpenJDK 7 Java Software Development Kit.\nSecurity Fix(es):\n* An untrusted library search path flaw was found in the JCE component of OpenJDK. A local attacker could possibly use this flaw to cause a Java application using JCE to load an attacker-controlled library and hence escalate their privileges. (CVE-2017-3511)\n* It was found that the JAXP component of OpenJDK failed to correctly enforce parse tree size limits when parsing XML document. An attacker able to make a Java application parse a specially crafted XML document could use this flaw to make it consume an excessive amount of CPU and memory. (CVE-2017-3526)\n* It was discovered that the HTTP client implementation in the Networking component of OpenJDK could cache and re-use an NTLM authenticated connection in a different security context. A remote attacker could possibly use this flaw to make a Java application perform HTTP requests authenticated with credentials of a different user. (CVE-2017-3509)\nNote: This update adds support for the \"jdk.ntlm.cache\" system property which, when set to false, prevents caching of NTLM connections and authentications and hence prevents this issue. However, caching remains enabled by default.\n* It was discovered that the Security component of OpenJDK did not allow users to restrict the set of algorithms allowed for Jar integrity verification. This flaw could allow an attacker to modify content of the Jar file that used weak signing key or hash algorithm. (CVE-2017-3539)\nNote: This updates extends the fix for CVE-2016-5542 released as part of the RHSA-2016:2658 erratum to no longer allow the MD5 hash algorithm during the Jar integrity verification by adding it to the jdk.jar.disabledAlgorithms security property.\n* Newline injection flaws were discovered in FTP and SMTP client implementations in the Networking component in OpenJDK. A remote attacker could possibly use these flaws to manipulate FTP or SMTP connections established by a Java application. (CVE-2017-3533, CVE-2017-3544)",
  "Platform": [
    "Red Hat Enterprise Linux 6",
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2017:1204",
      "ID": "RHSA-2017:1204"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-3509",
      "ID": "CVE-2017-3509"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-3511",
      "ID": "CVE-2017-3511"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-3526",
      "ID": "CVE-2017-3526"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-3533",
      "ID": "CVE-2017-3533"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-3539",
      "ID": "CVE-2017-3539"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-3544",
      "ID": "CVE-2017-3544"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk is earlier than 1:1.7.0.141-2.6.10.1.el7_3"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk-accessibility is earlier than 1:1.7.0.141-2.6.10.1.el7_3"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk-accessibility is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk-demo is earlier than 1:1.7.0.141-2.6.10.1.el7_3"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk-demo is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk-devel is earlier than 1:1.7.0.141-2.6.10.1.el7_3"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk-headless is earlier than 1:1.7.0.141-2.6.10.1.el7_3"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk-headless is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk-javadoc is earlier than 1:1.7.0.141-2.6.10.1.el7_3"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk-javadoc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk-src is earlier than 1:1.7.0.141-2.6.10.1.el7_3"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk-src is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      },
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk is earlier than 1:1.7.0.141-2.6.10.1.el6_9"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk-demo is earlier than 1:1.7.0.141-2.6.10.1.el6_9"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk-demo is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk-devel is earlier than 1:1.7.0.141-2.6.10.1.el6_9"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk-javadoc is earlier than 1:1.7.0.141-2.6.10.1.el6_9"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk-javadoc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk-src is earlier than 1:1.7.0.141-2.6.10.1.el6_9"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk-src is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-3509"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-3511"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-3526"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-3533"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-3539"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-3544"
    }
  ],
  "Issued": {
    "Date": "2017-05-09"
  }
}