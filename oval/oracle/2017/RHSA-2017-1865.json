{
  "Title": "RHSA-2017:1865: X.org X11 libraries security, bug fix and enhancement update (Moderate)",
  "Description": "The X11 (Xorg) libraries provide library routines that are used within all X Window applications.\nThe following packages have been upgraded to a later upstream version: libX11 (1.6.5), libXaw (1.0.13), libXdmcp (1.1.2), libXfixes (5.0.3), libXfont (1.5.2), libXi (1.7.9), libXpm (3.5.12), libXrandr (1.5.1), libXrender (0.9.10), libXt (1.1.5), libXtst (1.2.3), libXv (1.0.11), libXvMC (1.0.10), libXxf86vm (1.1.4), libdrm (2.4.74), libepoxy (1.3.1), libevdev (1.5.6), libfontenc (1.1.3), libvdpau (1.1.1), libwacom (0.24), libxcb (1.12), libxkbfile (1.0.9), mesa (17.0.1), mesa-private-llvm (3.9.1), xcb-proto (1.12), xkeyboard-config (2.20), xorg-x11-proto-devel (7.7). (BZ#1401667, BZ#1401668, BZ#1401669, BZ#1401670, BZ#1401671, BZ#1401672, BZ#1401673, BZ#1401675, BZ#1401676, BZ#1401677, BZ#1401678, BZ#1401679, BZ#1401680, BZ#1401681, BZ#1401682, BZ#1401683, BZ#1401685, BZ#1401690, BZ#1401752, BZ#1401753, BZ#1401754, BZ#1402560, BZ#1410477, BZ#1411390, BZ#1411392, BZ#1411393, BZ#1411452, BZ#1420224)\nSecurity Fix(es):\n* An integer overflow flaw leading to a heap-based buffer overflow was found in libXpm. An attacker could use this flaw to crash an application using libXpm via a specially crafted XPM file. (CVE-2016-10164)\n* It was discovered that libXdmcp used weak entropy to generate session keys. On a multi-user system using xdmcp, a local attacker could potentially use information available from the process list to brute force the key, allowing them to hijack other users' sessions. (CVE-2017-2625)\n* It was discovered that libICE used a weak entropy to generate keys. A local attacker could potentially use this flaw for session hijacking using the information available from the process list. (CVE-2017-2626)\nRed Hat would like to thank Eric Sesterhenn (X41 D-Sec GmbH) for reporting CVE-2017-2625 and CVE-2017-2626.\nAdditional Changes:\nFor detailed information on changes in this release, see the Red Hat Enterprise Linux 7.4 Release Notes linked from the References section.",
  "Platform": [
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2017:1865",
      "ID": "RHSA-2017:1865"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-10164",
      "ID": "CVE-2016-10164"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-2625",
      "ID": "CVE-2017-2625"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-2626",
      "ID": "CVE-2017-2626"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "xorg-x11-proto-devel is earlier than 0:7.7-20.el7"
                  },
                  {
                    "Comment": "xorg-x11-proto-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXfont2 is earlier than 0:2.0.1-2.el7"
                  },
                  {
                    "Comment": "libXfont2 is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXfont2-devel is earlier than 0:2.0.1-2.el7"
                  },
                  {
                    "Comment": "libXfont2-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libxkbcommon is earlier than 0:0.7.1-1.el7"
                  },
                  {
                    "Comment": "libxkbcommon is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libxkbcommon-devel is earlier than 0:0.7.1-1.el7"
                  },
                  {
                    "Comment": "libxkbcommon-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libxkbcommon-x11 is earlier than 0:0.7.1-1.el7"
                  },
                  {
                    "Comment": "libxkbcommon-x11 is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libxkbcommon-x11-devel is earlier than 0:0.7.1-1.el7"
                  },
                  {
                    "Comment": "libxkbcommon-x11-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "drm-utils is earlier than 0:2.4.74-1.el7"
                  },
                  {
                    "Comment": "drm-utils is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libdrm is earlier than 0:2.4.74-1.el7"
                  },
                  {
                    "Comment": "libdrm is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libdrm-devel is earlier than 0:2.4.74-1.el7"
                  },
                  {
                    "Comment": "libdrm-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libepoxy is earlier than 0:1.3.1-1.el7"
                  },
                  {
                    "Comment": "libepoxy is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libepoxy-devel is earlier than 0:1.3.1-1.el7"
                  },
                  {
                    "Comment": "libepoxy-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libevdev is earlier than 0:1.5.6-1.el7"
                  },
                  {
                    "Comment": "libevdev is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libevdev-devel is earlier than 0:1.5.6-1.el7"
                  },
                  {
                    "Comment": "libevdev-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libevdev-utils is earlier than 0:1.5.6-1.el7"
                  },
                  {
                    "Comment": "libevdev-utils is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "xcb-proto is earlier than 0:1.12-2.el7"
                  },
                  {
                    "Comment": "xcb-proto is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libxcb is earlier than 0:1.12-1.el7"
                  },
                  {
                    "Comment": "libxcb is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libxcb-devel is earlier than 0:1.12-1.el7"
                  },
                  {
                    "Comment": "libxcb-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libxcb-doc is earlier than 0:1.12-1.el7"
                  },
                  {
                    "Comment": "libxcb-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXrandr is earlier than 0:1.5.1-2.el7"
                  },
                  {
                    "Comment": "libXrandr is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXrandr-devel is earlier than 0:1.5.1-2.el7"
                  },
                  {
                    "Comment": "libXrandr-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXfixes is earlier than 0:5.0.3-1.el7"
                  },
                  {
                    "Comment": "libXfixes is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXfixes-devel is earlier than 0:5.0.3-1.el7"
                  },
                  {
                    "Comment": "libXfixes-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXi is earlier than 0:1.7.9-1.el7"
                  },
                  {
                    "Comment": "libXi is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXi-devel is earlier than 0:1.7.9-1.el7"
                  },
                  {
                    "Comment": "libXi-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXtst is earlier than 0:1.2.3-1.el7"
                  },
                  {
                    "Comment": "libXtst is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXtst-devel is earlier than 0:1.2.3-1.el7"
                  },
                  {
                    "Comment": "libXtst-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXrender is earlier than 0:0.9.10-1.el7"
                  },
                  {
                    "Comment": "libXrender is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXrender-devel is earlier than 0:0.9.10-1.el7"
                  },
                  {
                    "Comment": "libXrender-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXt is earlier than 0:1.1.5-3.el7"
                  },
                  {
                    "Comment": "libXt is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXt-devel is earlier than 0:1.1.5-3.el7"
                  },
                  {
                    "Comment": "libXt-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXpm is earlier than 0:3.5.12-1.el7"
                  },
                  {
                    "Comment": "libXpm is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXpm-devel is earlier than 0:3.5.12-1.el7"
                  },
                  {
                    "Comment": "libXpm-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXaw is earlier than 0:1.0.13-4.el7"
                  },
                  {
                    "Comment": "libXaw is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXaw-devel is earlier than 0:1.0.13-4.el7"
                  },
                  {
                    "Comment": "libXaw-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXv is earlier than 0:1.0.11-1.el7"
                  },
                  {
                    "Comment": "libXv is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXv-devel is earlier than 0:1.0.11-1.el7"
                  },
                  {
                    "Comment": "libXv-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXvMC is earlier than 0:1.0.10-1.el7"
                  },
                  {
                    "Comment": "libXvMC is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXvMC-devel is earlier than 0:1.0.10-1.el7"
                  },
                  {
                    "Comment": "libXvMC-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXxf86vm is earlier than 0:1.1.4-1.el7"
                  },
                  {
                    "Comment": "libXxf86vm is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXxf86vm-devel is earlier than 0:1.1.4-1.el7"
                  },
                  {
                    "Comment": "libXxf86vm-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libxkbfile is earlier than 0:1.0.9-3.el7"
                  },
                  {
                    "Comment": "libxkbfile is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libxkbfile-devel is earlier than 0:1.0.9-3.el7"
                  },
                  {
                    "Comment": "libxkbfile-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libfontenc is earlier than 0:1.1.3-3.el7"
                  },
                  {
                    "Comment": "libfontenc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libfontenc-devel is earlier than 0:1.1.3-3.el7"
                  },
                  {
                    "Comment": "libfontenc-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXfont is earlier than 0:1.5.2-1.el7"
                  },
                  {
                    "Comment": "libXfont is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXfont-devel is earlier than 0:1.5.2-1.el7"
                  },
                  {
                    "Comment": "libXfont-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "xkeyboard-config is earlier than 0:2.20-1.el7"
                  },
                  {
                    "Comment": "xkeyboard-config is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "xkeyboard-config-devel is earlier than 0:2.20-1.el7"
                  },
                  {
                    "Comment": "xkeyboard-config-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libvdpau is earlier than 0:1.1.1-3.el7"
                  },
                  {
                    "Comment": "libvdpau is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libvdpau-devel is earlier than 0:1.1.1-3.el7"
                  },
                  {
                    "Comment": "libvdpau-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libvdpau-docs is earlier than 0:1.1.1-3.el7"
                  },
                  {
                    "Comment": "libvdpau-docs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXcursor is earlier than 0:1.1.14-8.el7"
                  },
                  {
                    "Comment": "libXcursor is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXcursor-devel is earlier than 0:1.1.14-8.el7"
                  },
                  {
                    "Comment": "libXcursor-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libwacom is earlier than 0:0.24-1.el7"
                  },
                  {
                    "Comment": "libwacom is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libwacom-data is earlier than 0:0.24-1.el7"
                  },
                  {
                    "Comment": "libwacom-data is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libwacom-devel is earlier than 0:0.24-1.el7"
                  },
                  {
                    "Comment": "libwacom-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libinput is earlier than 0:1.6.3-2.el7"
                  },
                  {
                    "Comment": "libinput is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libinput-devel is earlier than 0:1.6.3-2.el7"
                  },
                  {
                    "Comment": "libinput-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "vulkan is earlier than 0:1.0.39.1-2.el7"
                  },
                  {
                    "Comment": "vulkan is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "vulkan-devel is earlier than 0:1.0.39.1-2.el7"
                  },
                  {
                    "Comment": "vulkan-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "vulkan-filesystem is earlier than 0:1.0.39.1-2.el7"
                  },
                  {
                    "Comment": "vulkan-filesystem is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mesa-private-llvm is earlier than 0:3.9.1-3.el7"
                  },
                  {
                    "Comment": "mesa-private-llvm is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mesa-private-llvm-devel is earlier than 0:3.9.1-3.el7"
                  },
                  {
                    "Comment": "mesa-private-llvm-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libX11 is earlier than 0:1.6.5-1.el7"
                  },
                  {
                    "Comment": "libX11 is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libX11-common is earlier than 0:1.6.5-1.el7"
                  },
                  {
                    "Comment": "libX11-common is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libX11-devel is earlier than 0:1.6.5-1.el7"
                  },
                  {
                    "Comment": "libX11-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXdmcp is earlier than 0:1.1.2-6.el7"
                  },
                  {
                    "Comment": "libXdmcp is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libXdmcp-devel is earlier than 0:1.1.2-6.el7"
                  },
                  {
                    "Comment": "libXdmcp-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libICE is earlier than 0:1.0.9-9.el7"
                  },
                  {
                    "Comment": "libICE is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libICE-devel is earlier than 0:1.0.9-9.el7"
                  },
                  {
                    "Comment": "libICE-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mesa-dri-drivers is earlier than 0:17.0.1-6.20170307.el7"
                  },
                  {
                    "Comment": "mesa-dri-drivers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mesa-filesystem is earlier than 0:17.0.1-6.20170307.el7"
                  },
                  {
                    "Comment": "mesa-filesystem is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mesa-libEGL is earlier than 0:17.0.1-6.20170307.el7"
                  },
                  {
                    "Comment": "mesa-libEGL is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mesa-libEGL-devel is earlier than 0:17.0.1-6.20170307.el7"
                  },
                  {
                    "Comment": "mesa-libEGL-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mesa-libGL is earlier than 0:17.0.1-6.20170307.el7"
                  },
                  {
                    "Comment": "mesa-libGL is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mesa-libGL-devel is earlier than 0:17.0.1-6.20170307.el7"
                  },
                  {
                    "Comment": "mesa-libGL-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mesa-libGLES is earlier than 0:17.0.1-6.20170307.el7"
                  },
                  {
                    "Comment": "mesa-libGLES is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mesa-libGLES-devel is earlier than 0:17.0.1-6.20170307.el7"
                  },
                  {
                    "Comment": "mesa-libGLES-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mesa-libOSMesa is earlier than 0:17.0.1-6.20170307.el7"
                  },
                  {
                    "Comment": "mesa-libOSMesa is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mesa-libOSMesa-devel is earlier than 0:17.0.1-6.20170307.el7"
                  },
                  {
                    "Comment": "mesa-libOSMesa-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mesa-libgbm is earlier than 0:17.0.1-6.20170307.el7"
                  },
                  {
                    "Comment": "mesa-libgbm is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mesa-libgbm-devel is earlier than 0:17.0.1-6.20170307.el7"
                  },
                  {
                    "Comment": "mesa-libgbm-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mesa-libglapi is earlier than 0:17.0.1-6.20170307.el7"
                  },
                  {
                    "Comment": "mesa-libglapi is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mesa-libxatracker is earlier than 0:17.0.1-6.20170307.el7"
                  },
                  {
                    "Comment": "mesa-libxatracker is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mesa-libxatracker-devel is earlier than 0:17.0.1-6.20170307.el7"
                  },
                  {
                    "Comment": "mesa-libxatracker-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mesa-vulkan-drivers is earlier than 0:17.0.1-6.20170307.el7"
                  },
                  {
                    "Comment": "mesa-vulkan-drivers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 7 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-10164"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-2625"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-2626"
    }
  ],
  "Issued": {
    "Date": "2017-08-01"
  }
}