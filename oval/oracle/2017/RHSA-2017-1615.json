{
  "Title": "RHSA-2017:1615: kernel security and bug fix update (Important)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux operating system.\nSecurity Fix(es):\n* A flaw was found in the way Linux kernel allocates heap memory to build the scattergather list from a fragment list(skb_shinfo(skb)->frag_list) in\nthe socket buffer(skb_buff). The heap overflow occurred if 'MAX_SKB_FRAGS + 1' parameter and 'NETIF_F_FRAGLIST' feature were used together. A\nremote user or process could use this flaw to potentially escalate their privilege on a system. (CVE-2017-7477, Important)\n* The NFS2/3 RPC client could send long arguments to the NFS server. These encoded arguments are stored in an array of memory pages, and accessed using pointer variables. Arbitrarily long arguments could make these pointers point outside the array and cause an out-of-bounds memory access. A remote user or program could use this flaw to crash the kernel (denial of service). (CVE-2017-7645, Important)\n* The NFSv2 and NFSv3 server implementations in the Linux kernel through 4.10.13 lacked certain checks for the end of a buffer. A remote attacker could trigger a pointer-arithmetic error or possibly cause other unspecified impacts using crafted requests related to fs/nfsd/nfs3xdr.c and fs/nfsd/nfsxdr.c. (CVE-2017-7895, Important)\n* The Linux kernel built with the Kernel-based Virtual Machine (CONFIG_KVM) support was vulnerable to an incorrect segment selector(SS) value error. The error could occur while loading values into the SS register in long mode. A user or process inside a guest could use this flaw to crash the guest, resulting in DoS or potentially escalate their privileges inside the guest. (CVE-2017-2583, Moderate)\n* A flaw was found in the Linux kernel's handling of packets with the URG flag. Applications using the splice() and tcp_splice_read() functionality could allow a remote attacker to force the kernel to enter a condition in which it could loop indefinitely. (CVE-2017-6214, Moderate)\nRed Hat would like to thank Ari Kauppi for reporting CVE-2017-7895 and Xiaohan Zhang (Huawei Inc.) for reporting CVE-2017-2583.\nBug Fix(es):\n* Previously, the reserved-pages counter (HugePages_Rsvd) was bigger than the total-pages counter (HugePages_Total) in the /proc/meminfo file, and HugePages_Rsvd underflowed. With this update, the HugeTLB feature of the Linux kernel has been fixed, and HugePages_Rsvd underflow no longer occurs. (BZ#1445184)\n* If a directory on a NFS client was modified while being listed, the NFS client could restart the directory listing multiple times. Consequently, the performance of listing the directory was sub-optimal. With this update, the restarting of the directory listing happens less frequently. As a result, the performance of listing the directory while it is being modified has improved. (BZ#1450851)\n* The Fibre Channel over Ethernet (FCoE) adapter in some cases failed to reboot. This update fixes the qla2xxx driver, and FCoE adapter now reboots as expected. (BZ#1446246)\n* When a VM with Virtual Function I/O (VFIO) device was rebooted, the QEMU process occasionally terminated unexpectedly due to a failed VFIO Direct Memory Access (DMA) map request. This update fixes the vfio driver and QEMU no longer crashes in the described situation. (BZ#1450855)\n* When the operating system was booted with the in-box lpfc driver, a kernel panic occurred on the little-endian variant of IBM Power Systems. This update fixes lpfc, and the kernel no longer panics in the described situation. (BZ#1452044)\n* When creating or destroying a VM with Virtual Function I/O (VFIO) devices with \"Hugepages\" feature enabled, errors in Direct Memory Access (DMA) page table entry (PTE) mappings occurred, and QEMU memory usage behaved unpredictably. This update fixes range computation when making room for large pages in Input/Output Memory Management Unit (IOMMU). As a result, errors in DMA PTE mappings no longer occur, and QEMU has a predictable memory usage in the described situation. (BZ#1450856)",
  "Platform": [
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2017:1615",
      "ID": "RHSA-2017:1615"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-2583",
      "ID": "CVE-2017-2583"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-6214",
      "ID": "CVE-2017-6214"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-7477",
      "ID": "CVE-2017-7477"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-7645",
      "ID": "CVE-2017-7645"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2017-7895",
      "ID": "CVE-2017-7895"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:3.10.0-514.26.1.el7 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:3.10.0-514.26.1.el7 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:3.10.0-514.26.1.el7"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-abi-whitelists is earlier than 0:3.10.0-514.26.1.el7"
                  },
                  {
                    "Comment": "kernel-abi-whitelists is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-bootwrapper is earlier than 0:3.10.0-514.26.1.el7"
                  },
                  {
                    "Comment": "kernel-bootwrapper is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:3.10.0-514.26.1.el7"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:3.10.0-514.26.1.el7"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:3.10.0-514.26.1.el7"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:3.10.0-514.26.1.el7"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:3.10.0-514.26.1.el7"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:3.10.0-514.26.1.el7"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:3.10.0-514.26.1.el7"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-tools is earlier than 0:3.10.0-514.26.1.el7"
                  },
                  {
                    "Comment": "kernel-tools is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-tools-libs is earlier than 0:3.10.0-514.26.1.el7"
                  },
                  {
                    "Comment": "kernel-tools-libs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-tools-libs-devel is earlier than 0:3.10.0-514.26.1.el7"
                  },
                  {
                    "Comment": "kernel-tools-libs-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perf is earlier than 0:3.10.0-514.26.1.el7"
                  },
                  {
                    "Comment": "perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "python-perf is earlier than 0:3.10.0-514.26.1.el7"
                  },
                  {
                    "Comment": "python-perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 7 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-2583"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-6214"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-7477"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-7645"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2017-7895"
    }
  ],
  "Issued": {
    "Date": "2017-06-28"
  }
}