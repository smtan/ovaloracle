{
  "Title": "RHSA-2010:0842: kernel security and bug fix update (Important)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux\noperating system.\nThis update fixes the following security issues:\n* Missing sanity checks in the Intel i915 driver in the Linux kernel could\nallow a local, unprivileged user to escalate their privileges.\n(CVE-2010-2962, Important)\n* compat_alloc_user_space() in the Linux kernel 32/64-bit compatibility\nlayer implementation was missing sanity checks. This function could be\nabused in other areas of the Linux kernel if its length argument can be\ncontrolled from user-space. On 64-bit systems, a local, unprivileged user\ncould use this flaw to escalate their privileges. (CVE-2010-3081,\nImportant)\n* A buffer overflow flaw in niu_get_ethtool_tcam_all() in the niu Ethernet\ndriver in the Linux kernel, could allow a local user to cause a denial of\nservice or escalate their privileges. (CVE-2010-3084, Important)\n* A flaw in the IA32 system call emulation provided in 64-bit Linux kernels\ncould allow a local user to escalate their privileges. (CVE-2010-3301,\nImportant)\n* A flaw in sctp_packet_config() in the Linux kernel's Stream Control\nTransmission Protocol (SCTP) implementation could allow a remote attacker\nto cause a denial of service. (CVE-2010-3432, Important)\n* A missing integer overflow check in snd_ctl_new() in the Linux kernel's\nsound subsystem could allow a local, unprivileged user on a 32-bit system\nto cause a denial of service or escalate their privileges. (CVE-2010-3442,\nImportant)\n* A flaw was found in sctp_auth_asoc_get_hmac() in the Linux kernel's SCTP\nimplementation. When iterating through the hmac_ids array, it did not reset\nthe last id element if it was out of range. This could allow a remote\nattacker to cause a denial of service. (CVE-2010-3705, Important)\n* A function in the Linux kernel's Reliable Datagram Sockets (RDS) protocol\nimplementation was missing sanity checks, which could allow a local,\nunprivileged user to escalate their privileges. (CVE-2010-3904, Important)\n* A flaw in drm_ioctl() in the Linux kernel's Direct Rendering Manager\n(DRM) implementation could allow a local, unprivileged user to cause an\ninformation leak. (CVE-2010-2803, Moderate)\n* It was found that wireless drivers might not always clear allocated\nbuffers when handling a driver-specific IOCTL information request. A local\nuser could trigger this flaw to cause an information leak. (CVE-2010-2955,\nModerate)\n* A NULL pointer dereference flaw in ftrace_regex_lseek() in the Linux\nkernel's ftrace implementation could allow a local, unprivileged user to\ncause a denial of service. Note: The debugfs file system must be mounted\nlocally to exploit this issue. It is not mounted by default.\n(CVE-2010-3079, Moderate)\n* A flaw in the Linux kernel's packet writing driver could be triggered\nvia the PKT_CTRL_CMD_STATUS IOCTL request, possibly allowing a local,\nunprivileged user with access to \"/dev/pktcdvd/control\" to cause an\ninformation leak. Note: By default, only users in the cdrom group have\naccess to \"/dev/pktcdvd/control\". (CVE-2010-3437, Moderate)\n* A flaw was found in the way KVM (Kernel-based Virtual Machine) handled\nthe reloading of fs and gs segment registers when they had invalid\nselectors. A privileged host user with access to \"/dev/kvm\" could use this\nflaw to crash the host. (CVE-2010-3698, Moderate)\nRed Hat would like to thank Kees Cook for reporting CVE-2010-2962 and\nCVE-2010-2803; Ben Hawkes for reporting CVE-2010-3081 and CVE-2010-3301;\nDan Rosenberg for reporting CVE-2010-3442, CVE-2010-3705, CVE-2010-3904,\nand CVE-2010-3437; and Robert Swiecki for reporting CVE-2010-3079.\nThis update also fixes several bugs. Documentation for these bug fixes will\nbe available shortly from the Technical Notes document linked to in the\nReferences section.\nUsers should upgrade to these updated packages, which contain backported\npatches to correct these issues. The system must be rebooted for this\nupdate to take effect.",
  "Platform": [
    "Red Hat Enterprise Linux 6"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2010:0842",
      "ID": "RHSA-2010:0842"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2010-2803",
      "ID": "CVE-2010-2803"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2010-2955",
      "ID": "CVE-2010-2955"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2010-2962",
      "ID": "CVE-2010-2962"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2010-3079",
      "ID": "CVE-2010-3079"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2010-3081",
      "ID": "CVE-2010-3081"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2010-3084",
      "ID": "CVE-2010-3084"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2010-3301",
      "ID": "CVE-2010-3301"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2010-3432",
      "ID": "CVE-2010-3432"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2010-3437",
      "ID": "CVE-2010-3437"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2010-3442",
      "ID": "CVE-2010-3442"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2010-3698",
      "ID": "CVE-2010-3698"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2010-3705",
      "ID": "CVE-2010-3705"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2010-3904",
      "ID": "CVE-2010-3904"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.32-71.7.1.el6 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.32-71.7.1.el6 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.32-71.7.1.el6"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-bootwrapper is earlier than 0:2.6.32-71.7.1.el6"
                  },
                  {
                    "Comment": "kernel-bootwrapper is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:2.6.32-71.7.1.el6"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:2.6.32-71.7.1.el6"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.32-71.7.1.el6"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.32-71.7.1.el6"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-firmware is earlier than 0:2.6.32-71.7.1.el6"
                  },
                  {
                    "Comment": "kernel-firmware is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:2.6.32-71.7.1.el6"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:2.6.32-71.7.1.el6"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:2.6.32-71.7.1.el6"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perf is earlier than 0:2.6.32-71.7.1.el6"
                  },
                  {
                    "Comment": "perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2010-2803"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2010-2955"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2010-2962"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2010-3079"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2010-3081"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2010-3084"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2010-3301"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2010-3432"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2010-3437"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2010-3442"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2010-3698"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2010-3705"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2010-3904"
    }
  ],
  "Issued": {
    "Date": "2010-11-10"
  }
}