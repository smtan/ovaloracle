{
  "Title": "RHSA-2015:1135: php security and bug fix update (Important)",
  "Description": "PHP is an HTML-embedded scripting language commonly used with the Apache\nHTTP Server.\nA flaw was found in the way the PHP module for the Apache httpd web server\nhandled pipelined requests. A remote attacker could use this flaw to\ntrigger the execution of a PHP script in a deinitialized interpreter,\ncausing it to crash or, possibly, execute arbitrary code. (CVE-2015-3330)\nA flaw was found in the way PHP parsed multipart HTTP POST requests. A\nspecially crafted request could cause PHP to use an excessive amount of CPU\ntime. (CVE-2015-4024)\nAn uninitialized pointer use flaw was found in PHP's Exif extension. A\nspecially crafted JPEG or TIFF file could cause a PHP application using the\nexif_read_data() function to crash or, possibly, execute arbitrary code\nwith the privileges of the user running that PHP application.\n(CVE-2015-0232)\nAn integer overflow flaw leading to a heap-based buffer overflow was found\nin the way PHP's FTP extension parsed file listing FTP server responses. A\nmalicious FTP server could use this flaw to cause a PHP application to\ncrash or, possibly, execute arbitrary code. (CVE-2015-4022)\nMultiple flaws were discovered in the way PHP performed object\nunserialization. Specially crafted input processed by the unserialize()\nfunction could cause a PHP application to crash or, possibly, execute\narbitrary code. (CVE-2014-8142, CVE-2015-0231, CVE-2015-0273,\nCVE-2015-2787, CVE-2015-4147, CVE-2015-4148, CVE-2015-4599, CVE-2015-4600,\nCVE-2015-4601, CVE-2015-4602, CVE-2015-4603)\nIt was found that certain PHP functions did not properly handle file names\ncontaining a NULL character. A remote attacker could possibly use this flaw\nto make a PHP script access unexpected files and bypass intended file\nsystem access restrictions. (CVE-2015-2348, CVE-2015-4025, CVE-2015-4026,\nCVE-2015-3411, CVE-2015-3412, CVE-2015-4598)\nMultiple flaws were found in the way the way PHP's Phar extension parsed\nPhar archives. A specially crafted archive could cause PHP to crash or,\npossibly, execute arbitrary code when opened. (CVE-2015-2301,\nCVE-2015-2783, CVE-2015-3307, CVE-2015-3329, CVE-2015-4021)\nMultiple flaws were found in PHP's File Information (fileinfo) extension.\nA remote attacker could cause a PHP application to crash if it used\nfileinfo to identify type of attacker supplied files. (CVE-2014-9652,\nCVE-2015-4604, CVE-2015-4605)\nA heap buffer overflow flaw was found in the enchant_broker_request_dict()\nfunction of PHP's enchant extension. An attacker able to make a PHP\napplication enchant dictionaries could possibly cause it to crash.\n(CVE-2014-9705)\nA buffer over-read flaw was found in the GD library used by the PHP gd\nextension. A specially crafted GIF file could cause a PHP application using\nthe imagecreatefromgif() function to crash. (CVE-2014-9709)\nThis update also fixes the following bugs:\n* The libgmp library in some cases terminated unexpectedly with a\nsegmentation fault when being used with other libraries that use the GMP\nmemory management. With this update, PHP no longer changes libgmp memory\nallocators, which prevents the described crash from occurring. (BZ#1212305)\n* When using the Open Database Connectivity (ODBC) API, the PHP process\nin some cases terminated unexpectedly with a segmentation fault. The\nunderlying code has been adjusted to prevent this crash. (BZ#1212299)\n* Previously, running PHP on a big-endian system sometimes led to memory\ncorruption in the fileinfo module. This update adjusts the behavior of\nthe PHP pointer so that it can be freed without causing memory corruption.\n(BZ#1212298)\nAll php users are advised to upgrade to these updated packages, which\ncontain backported patches to correct these issues. After installing the\nupdated packages, the httpd daemon must be restarted for the update to\ntake effect.",
  "Platform": [
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2015:1135",
      "ID": "RHSA-2015:1135"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-8142",
      "ID": "CVE-2014-8142"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-9652",
      "ID": "CVE-2014-9652"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-9705",
      "ID": "CVE-2014-9705"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-9709",
      "ID": "CVE-2014-9709"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-0231",
      "ID": "CVE-2015-0231"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-0232",
      "ID": "CVE-2015-0232"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-0273",
      "ID": "CVE-2015-0273"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-2301",
      "ID": "CVE-2015-2301"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-2348",
      "ID": "CVE-2015-2348"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-2783",
      "ID": "CVE-2015-2783"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-2787",
      "ID": "CVE-2015-2787"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-3307",
      "ID": "CVE-2015-3307"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-3329",
      "ID": "CVE-2015-3329"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-3330",
      "ID": "CVE-2015-3330"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-3411",
      "ID": "CVE-2015-3411"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-3412",
      "ID": "CVE-2015-3412"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-4021",
      "ID": "CVE-2015-4021"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-4022",
      "ID": "CVE-2015-4022"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-4024",
      "ID": "CVE-2015-4024"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-4025",
      "ID": "CVE-2015-4025"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-4026",
      "ID": "CVE-2015-4026"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-4147",
      "ID": "CVE-2015-4147"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-4148",
      "ID": "CVE-2015-4148"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-4598",
      "ID": "CVE-2015-4598"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-4599",
      "ID": "CVE-2015-4599"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-4600",
      "ID": "CVE-2015-4600"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-4601",
      "ID": "CVE-2015-4601"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-4602",
      "ID": "CVE-2015-4602"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-4603",
      "ID": "CVE-2015-4603"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-4604",
      "ID": "CVE-2015-4604"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-4605",
      "ID": "CVE-2015-4605"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-4643",
      "ID": "CVE-2015-4643"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php is earlier than 0:5.4.16-36.el7_1"
                  },
                  {
                    "Comment": "php is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php-bcmath is earlier than 0:5.4.16-36.el7_1"
                  },
                  {
                    "Comment": "php-bcmath is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php-cli is earlier than 0:5.4.16-36.el7_1"
                  },
                  {
                    "Comment": "php-cli is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php-common is earlier than 0:5.4.16-36.el7_1"
                  },
                  {
                    "Comment": "php-common is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php-dba is earlier than 0:5.4.16-36.el7_1"
                  },
                  {
                    "Comment": "php-dba is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php-devel is earlier than 0:5.4.16-36.el7_1"
                  },
                  {
                    "Comment": "php-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php-embedded is earlier than 0:5.4.16-36.el7_1"
                  },
                  {
                    "Comment": "php-embedded is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php-enchant is earlier than 0:5.4.16-36.el7_1"
                  },
                  {
                    "Comment": "php-enchant is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php-fpm is earlier than 0:5.4.16-36.el7_1"
                  },
                  {
                    "Comment": "php-fpm is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php-gd is earlier than 0:5.4.16-36.el7_1"
                  },
                  {
                    "Comment": "php-gd is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php-intl is earlier than 0:5.4.16-36.el7_1"
                  },
                  {
                    "Comment": "php-intl is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php-ldap is earlier than 0:5.4.16-36.el7_1"
                  },
                  {
                    "Comment": "php-ldap is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php-mbstring is earlier than 0:5.4.16-36.el7_1"
                  },
                  {
                    "Comment": "php-mbstring is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php-mysql is earlier than 0:5.4.16-36.el7_1"
                  },
                  {
                    "Comment": "php-mysql is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php-mysqlnd is earlier than 0:5.4.16-36.el7_1"
                  },
                  {
                    "Comment": "php-mysqlnd is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php-odbc is earlier than 0:5.4.16-36.el7_1"
                  },
                  {
                    "Comment": "php-odbc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php-pdo is earlier than 0:5.4.16-36.el7_1"
                  },
                  {
                    "Comment": "php-pdo is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php-pgsql is earlier than 0:5.4.16-36.el7_1"
                  },
                  {
                    "Comment": "php-pgsql is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php-process is earlier than 0:5.4.16-36.el7_1"
                  },
                  {
                    "Comment": "php-process is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php-pspell is earlier than 0:5.4.16-36.el7_1"
                  },
                  {
                    "Comment": "php-pspell is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php-recode is earlier than 0:5.4.16-36.el7_1"
                  },
                  {
                    "Comment": "php-recode is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php-snmp is earlier than 0:5.4.16-36.el7_1"
                  },
                  {
                    "Comment": "php-snmp is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php-soap is earlier than 0:5.4.16-36.el7_1"
                  },
                  {
                    "Comment": "php-soap is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php-xml is earlier than 0:5.4.16-36.el7_1"
                  },
                  {
                    "Comment": "php-xml is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php-xmlrpc is earlier than 0:5.4.16-36.el7_1"
                  },
                  {
                    "Comment": "php-xmlrpc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 7 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-8142"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-9652"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-9705"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-9709"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-0231"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-0232"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-0273"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-2301"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-2348"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-2783"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-2787"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-3307"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-3329"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-3330"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-3411"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-3412"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-4021"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-4022"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-4024"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-4025"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-4026"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-4147"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-4148"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-4598"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-4599"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-4600"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-4601"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-4602"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-4603"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-4604"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-4605"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-4643"
    }
  ],
  "Issued": {
    "Date": "2015-06-23"
  }
}