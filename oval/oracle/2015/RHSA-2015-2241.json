{
  "Title": "RHSA-2015:2241: chrony security, bug fix, and enhancement update (Moderate)",
  "Description": "The chrony suite, chronyd and chronyc, is an advanced implementation of the\nNetwork Time Protocol (NTP), specially designed to support systems with\nintermittent connections. It can synchronize the system clock with NTP\nservers, hardware reference clocks, and manual input. It can also operate\nas an NTPv4 (RFC 5905) server or peer to provide a time service to other\ncomputers in the network.\nAn out-of-bounds write flaw was found in the way chrony stored certain\naddresses when configuring NTP or cmdmon access. An attacker that has the\ncommand key and is allowed to access cmdmon (only localhost is allowed by\ndefault) could use this flaw to crash chronyd or, possibly, execute\narbitrary code with the privileges of the chronyd process. (CVE-2015-1821)\nAn uninitialized pointer use flaw was found when allocating memory to save\nunacknowledged replies to authenticated command requests. An attacker that\nhas the command key and is allowed to access cmdmon (only localhost is\nallowed by default) could use this flaw to crash chronyd or, possibly,\nexecute arbitrary code with the privileges of the chronyd process.\n(CVE-2015-1822)\nA denial of service flaw was found in the way chrony hosts that were\npeering with each other authenticated themselves before updating their\ninternal state variables. An attacker could send packets to one peer host,\nwhich could cascade to other peers, and stop the synchronization process\namong the reached peers. (CVE-2015-1853)\nThese issues were discovered by Miroslav Lichvár of Red Hat.\nThe chrony packages have been upgraded to upstream version 2.1.1, which\nprovides a number of bug fixes and enhancements over the previous version.\nNotable enhancements include:\n* Updated to NTP version 4 (RFC 5905)\n* Added pool directive to specify pool of NTP servers\n* Added leapsecmode directive to select how to correct clock for leap\nsecond\n* Added smoothtime directive to smooth served time and enable leap smear\n* Added asynchronous name resolving with POSIX threads\n* Ready for year 2036 (next NTP era)\n* Improved clock control\n* Networking code reworked to open separate client sockets for each NTP\nserver\n(BZ#1117882)\nThis update also fixes the following bug:\n* The chronyd service previously assumed that network interfaces specified\nwith the \"bindaddress\" directive were ready when the service was started.\nThis could cause chronyd to fail to bind an NTP server socket to the\ninterface if the interface was not ready. With this update, chronyd uses\nthe IP_FREEBIND socket option, enabling it to bind to an interface later,\nnot only when the service starts. (BZ#1169353)\nIn addition, this update adds the following enhancement:\n* The chronyd service now supports four modes of handling leap seconds,\nconfigured using the \"leapsecmode\" option. The clock can be either stepped\nby the kernel (the default \"system\" mode), stepped by chronyd (\"step\"\nmode), slowly adjusted by slewing (\"slew\" mode), or the leap second can be\nignored and corrected later in normal operation (\"ignore\" mode). If you\nselect slewing, the correction will always start at 00:00:00 UTC and will\nbe applied at a rate specified in the \"maxslewrate\" option. (BZ#1206504)\nAll chrony users are advised to upgrade to these updated packages, which\ncorrect these issues and add these enhancements.",
  "Platform": [
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2015:2241",
      "ID": "RHSA-2015:2241"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-1821",
      "ID": "CVE-2015-1821"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-1822",
      "ID": "CVE-2015-1822"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-1853",
      "ID": "CVE-2015-1853"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [

        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          },
          {
            "Comment": "chrony is earlier than 0:2.1.1-1.el7"
          },
          {
            "Comment": "chrony is signed with Red Hat redhatrelease2 key"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 7 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-1821"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-1822"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-1853"
    }
  ],
  "Issued": {
    "Date": "2015-11-19"
  }
}