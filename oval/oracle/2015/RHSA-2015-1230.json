{
  "Title": "RHSA-2015:1230: java-1.7.0-openjdk security update (Important)",
  "Description": "The java-1.7.0-openjdk packages provide the OpenJDK 7 Java Runtime\nEnvironment and the OpenJDK 7 Java Software Development Kit.\nMultiple flaws were discovered in the 2D, CORBA, JMX, Libraries and RMI\ncomponents in OpenJDK. An untrusted Java application or applet could use\nthese flaws to bypass certain Java sandbox restrictions. (CVE-2015-4760,\nCVE-2015-2628, CVE-2015-4731, CVE-2015-2590, CVE-2015-4732, CVE-2015-4733)\nA flaw was found in the way the Libraries component of OpenJDK verified\nOnline Certificate Status Protocol (OCSP) responses. An OCSP response with\nno nextUpdate date specified was incorrectly handled as having unlimited\nvalidity, possibly causing a revoked X.509 certificate to be interpreted as\nvalid. (CVE-2015-4748)\nIt was discovered that the JCE component in OpenJDK failed to use constant\ntime comparisons in multiple cases. An attacker could possibly use these\nflaws to disclose sensitive information by measuring the time used to\nperform operations using these non-constant time comparisons.\n(CVE-2015-2601)\nA flaw was found in the RC4 encryption algorithm. When using certain keys\nfor RC4 encryption, an attacker could obtain portions of the plain text\nfrom the cipher text without the knowledge of the encryption key.\n(CVE-2015-2808)\nNote: With this update, OpenJDK now disables RC4 SSL/TLS cipher suites by\ndefault to address the CVE-2015-2808 issue. Refer to Red Hat Bugzilla bug\n1207101, linked to in the References section, for additional details about\nthis change.\nA flaw was found in the way the TLS protocol composed the Diffie-Hellman\n(DH) key exchange. A man-in-the-middle attacker could use this flaw to\nforce the use of weak 512 bit export-grade keys during the key exchange,\nallowing them do decrypt all traffic. (CVE-2015-4000)\nNote: This update forces the TLS/SSL client implementation in OpenJDK to\nreject DH key sizes below 768 bits, which prevents sessions to be\ndowngraded to export-grade keys. Refer to Red Hat Bugzilla bug 1223211,\nlinked to in the References section, for additional details about this\nchange.\nIt was discovered that the JNDI component in OpenJDK did not handle DNS\nresolutions correctly. An attacker able to trigger such DNS errors could\ncause a Java application using JNDI to consume memory and CPU time, and\npossibly block further DNS resolution. (CVE-2015-4749)\nMultiple information leak flaws were found in the JMX and 2D components in\nOpenJDK. An untrusted Java application or applet could use this flaw to\nbypass certain Java sandbox restrictions. (CVE-2015-2621, CVE-2015-2632)\nA flaw was found in the way the JSSE component in OpenJDK performed X.509\ncertificate identity verification when establishing a TLS/SSL connection to\na host identified by an IP address. In certain cases, the certificate was\naccepted as valid if it was issued for a host name to which the IP address\nresolves rather than for the IP address. (CVE-2015-2625)\nAll users of java-1.7.0-openjdk are advised to upgrade to these updated\npackages, which resolve these issues. All running instances of OpenJDK Java\nmust be restarted for the update to take effect.",
  "Platform": [
    "Red Hat Enterprise Linux 5"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2015:1230",
      "ID": "RHSA-2015:1230"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-2590",
      "ID": "CVE-2015-2590"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-2601",
      "ID": "CVE-2015-2601"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-2621",
      "ID": "CVE-2015-2621"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-2625",
      "ID": "CVE-2015-2625"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-2628",
      "ID": "CVE-2015-2628"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-2632",
      "ID": "CVE-2015-2632"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-2808",
      "ID": "CVE-2015-2808"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-4000",
      "ID": "CVE-2015-4000"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-4731",
      "ID": "CVE-2015-4731"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-4732",
      "ID": "CVE-2015-4732"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-4733",
      "ID": "CVE-2015-4733"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-4748",
      "ID": "CVE-2015-4748"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-4749",
      "ID": "CVE-2015-4749"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-4760",
      "ID": "CVE-2015-4760"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk is earlier than 1:1.7.0.85-2.6.1.3.el5_11"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk-demo is earlier than 1:1.7.0.85-2.6.1.3.el5_11"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk-demo is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk-devel is earlier than 1:1.7.0.85-2.6.1.3.el5_11"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk-javadoc is earlier than 1:1.7.0.85-2.6.1.3.el5_11"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk-javadoc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.7.0-openjdk-src is earlier than 1:1.7.0.85-2.6.1.3.el5_11"
                  },
                  {
                    "Comment": "java-1.7.0-openjdk-src is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 5 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 5 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-2590"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-2601"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-2621"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-2625"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-2628"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-2632"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-2808"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-4000"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-4731"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-4732"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-4733"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-4748"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-4749"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-4760"
    }
  ],
  "Issued": {
    "Date": "2015-07-15"
  }
}