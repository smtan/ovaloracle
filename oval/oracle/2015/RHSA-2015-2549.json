{
  "Title": "RHSA-2015:2549: libxml2 security update (Moderate)",
  "Description": "The libxml2 library is a development toolbox providing the implementation\nof various XML standards.\nSeveral denial of service flaws were found in libxml2, a library providing\nsupport for reading, modifying, and writing XML and HTML files. A remote\nattacker could provide a specially crafted XML or HTML file that, when\nprocessed by an application using libxml2, would cause that application to\nuse an excessive amount of CPU, leak potentially sensitive information, or\nin certain cases crash the application. (CVE-2015-5312, CVE-2015-7497,\nCVE-2015-7498, CVE-2015-7499, CVE-2015-7500 CVE-2015-7941, CVE-2015-7942,\nCVE-2015-8241, CVE-2015-8242, CVE-2015-8317, BZ#1213957, BZ#1281955)\nRed Hat would like to thank the GNOME project for reporting CVE-2015-7497,\nCVE-2015-7498, CVE-2015-7499, CVE-2015-7500, CVE-2015-8241, CVE-2015-8242,\nand CVE-2015-8317. Upstream acknowledges Kostya Serebryany of Google as the\noriginal reporter of CVE-2015-7497, CVE-2015-7498, CVE-2015-7499, and\nCVE-2015-7500; Hugh Davenport as the original reporter of CVE-2015-8241 and\nCVE-2015-8242; and Hanno Boeck as the original reporter of CVE-2015-8317.\nAll libxml2 users are advised to upgrade to these updated packages, which\ncontain a backported patch to correct these issues. The desktop must be\nrestarted (log out, then log back in) for this update to take effect.",
  "Platform": [
    "Red Hat Enterprise Linux 6"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2015:2549",
      "ID": "RHSA-2015:2549"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-5312",
      "ID": "CVE-2015-5312"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-7497",
      "ID": "CVE-2015-7497"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-7498",
      "ID": "CVE-2015-7498"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-7499",
      "ID": "CVE-2015-7499"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-7500",
      "ID": "CVE-2015-7500"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-7941",
      "ID": "CVE-2015-7941"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-7942",
      "ID": "CVE-2015-7942"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-8241",
      "ID": "CVE-2015-8241"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-8242",
      "ID": "CVE-2015-8242"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-8317",
      "ID": "CVE-2015-8317"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-8710",
      "ID": "CVE-2015-8710"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libxml2 is earlier than 0:2.7.6-20.el6_7.1"
                  },
                  {
                    "Comment": "libxml2 is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libxml2-devel is earlier than 0:2.7.6-20.el6_7.1"
                  },
                  {
                    "Comment": "libxml2-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libxml2-python is earlier than 0:2.7.6-20.el6_7.1"
                  },
                  {
                    "Comment": "libxml2-python is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libxml2-static is earlier than 0:2.7.6-20.el6_7.1"
                  },
                  {
                    "Comment": "libxml2-static is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-5312"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-7497"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-7498"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-7499"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-7500"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-7941"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-7942"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-8241"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-8242"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-8317"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-8710"
    }
  ],
  "Issued": {
    "Date": "2015-12-07"
  }
}