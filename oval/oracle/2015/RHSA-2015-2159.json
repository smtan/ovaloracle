{
  "Title": "RHSA-2015:2159: curl security, bug fix, and enhancement update (Moderate)",
  "Description": "The curl packages provide the libcurl library and the curl utility for\ndownloading files from servers using various protocols, including HTTP,\nFTP, and LDAP.\nIt was found that the libcurl library did not correctly handle partial\nliteral IP addresses when parsing received HTTP cookies. An attacker able\nto trick a user into connecting to a malicious server could use this flaw\nto set the user's cookie to a crafted domain, making other cookie-related\nissues easier to exploit. (CVE-2014-3613)\nA flaw was found in the way the libcurl library performed the duplication\nof connection handles. If an application set the CURLOPT_COPYPOSTFIELDS\noption for a handle, using the handle's duplicate could cause the\napplication to crash or disclose a portion of its memory. (CVE-2014-3707)\nIt was discovered that the libcurl library failed to properly handle URLs\nwith embedded end-of-line characters. An attacker able to make an\napplication using libcurl access a specially crafted URL via an HTTP proxy\ncould use this flaw to inject additional headers to the request or\nconstruct additional requests. (CVE-2014-8150)\nIt was discovered that libcurl implemented aspects of the NTLM and\nNegotatiate authentication incorrectly. If an application uses libcurl\nand the affected mechanisms in a specifc way, certain requests to a\npreviously NTLM-authenticated server could appears as sent by the wrong\nauthenticated user. Additionally, the initial set of credentials for HTTP\nNegotiate-authenticated requests could be reused in subsequent requests,\nalthough a different set of credentials was specified. (CVE-2015-3143,\nCVE-2015-3148)\nRed Hat would like to thank the cURL project for reporting these issues.\nBug fixes:\n* An out-of-protocol fallback to SSL 3.0 was available with libcurl.\nAttackers could abuse the fallback to force downgrade of the SSL version.\nThe fallback has been removed from libcurl. Users requiring this\nfunctionality can explicitly enable SSL 3.0 through the libcurl API.\n(BZ#1154060)\n* TLS 1.1 and TLS 1.2 are no longer disabled by default in libcurl. You can\nexplicitly disable them through the libcurl API. (BZ#1170339)\n* FTP operations such as downloading files took a significantly long time\nto complete. Now, the FTP implementation in libcurl correctly sets blocking\ndirection and estimated timeout for connections, resulting in faster FTP\ntransfers. (BZ#1218272)\nEnhancements:\n* With the updated packages, it is possible to explicitly enable or disable\nnew Advanced Encryption Standard (AES) cipher suites to be used for the TLS\nprotocol. (BZ#1066065)\n* The libcurl library did not implement a non-blocking SSL handshake, which\nnegatively affected performance of applications based on the libcurl multi\nAPI. The non-blocking SSL handshake has been implemented in libcurl, and\nthe libcurl multi API now immediately returns the control back to the\napplication whenever it cannot read or write data from or to the underlying\nnetwork socket. (BZ#1091429)\n* The libcurl library used an unnecessarily long blocking delay for actions\nwith no active file descriptors, even for short operations. Some actions,\nsuch as resolving a host name using /etc/hosts, took a long time to\ncomplete. The blocking code in libcurl has been modified so that the\ninitial delay is short and gradually increases until an event occurs.\n(BZ#1130239)\nAll curl users are advised to upgrade to these updated packages, which\ncontain backported patches to correct these issues and add these\nenhancements.",
  "Platform": [
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2015:2159",
      "ID": "RHSA-2015:2159"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-3613",
      "ID": "CVE-2014-3613"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-3707",
      "ID": "CVE-2014-3707"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-8150",
      "ID": "CVE-2014-8150"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-3143",
      "ID": "CVE-2015-3143"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-3148",
      "ID": "CVE-2015-3148"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "curl is earlier than 0:7.29.0-25.el7"
                  },
                  {
                    "Comment": "curl is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libcurl is earlier than 0:7.29.0-25.el7"
                  },
                  {
                    "Comment": "libcurl is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libcurl-devel is earlier than 0:7.29.0-25.el7"
                  },
                  {
                    "Comment": "libcurl-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 7 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-3613"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-3707"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-8150"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-3143"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-3148"
    }
  ],
  "Issued": {
    "Date": "2015-11-19"
  }
}