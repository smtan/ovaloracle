{
  "Title": "RHSA-2015:2088: openssh security, bug fix, and enhancement update (Moderate)",
  "Description": "OpenSSH is OpenBSD's SSH (Secure Shell) protocol implementation. These\npackages include the core files necessary for both the OpenSSH client and\nserver.\nA flaw was found in the way OpenSSH handled PAM authentication when using\nprivilege separation. An attacker with valid credentials on the system and\nable to fully compromise a non-privileged pre-authentication process using\na different flaw could use this flaw to authenticate as other users.\n(CVE-2015-6563)\nA use-after-free flaw was found in OpenSSH. An attacker able to fully\ncompromise a non-privileged pre-authentication process using a different\nflaw could possibly cause sshd to crash or execute arbitrary code with\nroot privileges. (CVE-2015-6564)\nIt was discovered that the OpenSSH sshd daemon did not check the list of\nkeyboard-interactive authentication methods for duplicates. A remote\nattacker could use this flaw to bypass the MaxAuthTries limit, making it\neasier to perform password guessing attacks. (CVE-2015-5600)\nIt was found that the OpenSSH ssh-agent, a program to hold private keys\nused for public key authentication, was vulnerable to password guessing\nattacks. An attacker able to connect to the agent could use this flaw to\nconduct a brute-force attack to unlock keys in the ssh-agent. (BZ#1238238)\nThis update fixes the following bugs:\n* Previously, the sshd_config(5) man page was misleading and could thus\nconfuse the user. This update improves the man page text to clearly\ndescribe the AllowGroups feature. (BZ#1150007)\n* The limit for the function for restricting the number of files listed using the wildcard character (*) that prevents the Denial of Service (DoS) for both server and client was previously set too low. Consequently, the user reaching the limit was prevented from listing a directory with a large number of files over Secure File Transfer Protocol (SFTP). This update increases the aforementioned limit, thus fixing this bug. (BZ#1160377)\n* When the ForceCommand option with a pseudoterminal was used and the\nMaxSession option was set to \"2\", multiplexed SSH connections did not work\nas expected. After the user attempted to open a second multiplexed\nconnection, the attempt failed if the first connection was still open. This\nupdate modifies OpenSSH to issue only one audit message per session, and\nthe user is thus able to open two multiplexed connections in this\nsituation. (BZ#1199112)\n* The ssh-copy-id utility failed if the account on the remote server did\nnot use an sh-like shell. Remote commands have been modified to run in an\nsh-like shell, and ssh-copy-id now works also with non-sh-like shells.\n(BZ#1201758)\n* Due to a race condition between auditing messages and answers when using\nControlMaster multiplexing, one session in the shared connection randomly\nand unexpectedly exited the connection. This update fixes the race\ncondition in the auditing code, and multiplexing connections now work as\nexpected even with a number of sessions created at once. (BZ#1240613)\nIn addition, this update adds the following enhancements:\n* As not all Lightweight Directory Access Protocol (LDAP) servers possess\na default schema, as expected by the ssh-ldap-helper program, this update\nprovides the user with an ability to adjust the LDAP query to get public\nkeys from servers with a different schema, while the default functionality\nstays untouched. (BZ#1201753)\n* With this enhancement update, the administrator is able to set\npermissions for files uploaded using Secure File Transfer Protocol (SFTP).\n(BZ#1197989)\n* This update provides the LDAP schema in LDAP Data Interchange Format (LDIF) format as a complement to the old schema previously accepted\nby OpenLDAP. (BZ#1184938)\n* With this update, the user can selectively disable the Generic Security\nServices API (GSSAPI) key exchange algorithms as any normal key exchange.\n(BZ#1253062)\nUsers of openssh are advised to upgrade to these updated packages, which\ncorrect these issues and add these enhancements.",
  "Platform": [
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2015:2088",
      "ID": "RHSA-2015:2088"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-5600",
      "ID": "CVE-2015-5600"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-6563",
      "ID": "CVE-2015-6563"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-6564",
      "ID": "CVE-2015-6564"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "openssh is earlier than 0:6.6.1p1-22.el7"
                  },
                  {
                    "Comment": "openssh is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "openssh-askpass is earlier than 0:6.6.1p1-22.el7"
                  },
                  {
                    "Comment": "openssh-askpass is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "openssh-clients is earlier than 0:6.6.1p1-22.el7"
                  },
                  {
                    "Comment": "openssh-clients is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "openssh-keycat is earlier than 0:6.6.1p1-22.el7"
                  },
                  {
                    "Comment": "openssh-keycat is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "openssh-ldap is earlier than 0:6.6.1p1-22.el7"
                  },
                  {
                    "Comment": "openssh-ldap is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "openssh-server is earlier than 0:6.6.1p1-22.el7"
                  },
                  {
                    "Comment": "openssh-server is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "openssh-server-sysvinit is earlier than 0:6.6.1p1-22.el7"
                  },
                  {
                    "Comment": "openssh-server-sysvinit is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "pam_ssh_agent_auth is earlier than 0:0.9.3-9.22.el7"
                  },
                  {
                    "Comment": "pam_ssh_agent_auth is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 7 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-5600"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-6563"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-6564"
    }
  ],
  "Issued": {
    "Date": "2015-11-19"
  }
}