{
  "Title": "RHSA-2015:2152: kernel security, bug fix, and enhancement update (Important)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux\noperating system.\n* A flaw was found in the way the Linux kernel's file system implementation\nhandled rename operations in which the source was inside and the\ndestination was outside of a bind mount. A privileged user inside a\ncontainer could use this flaw to escape the bind mount and, potentially,\nescalate their privileges on the system. (CVE-2015-2925, Important)\n* A race condition flaw was found in the way the Linux kernel's IPC\nsubsystem initialized certain fields in an IPC object structure that were\nlater used for permission checking before inserting the object into a\nglobally visible list. A local, unprivileged user could potentially use\nthis flaw to elevate their privileges on the system. (CVE-2015-7613,\nImportant)\n* It was found that reporting emulation failures to user space could lead\nto either a local (CVE-2014-7842) or a L2->L1 (CVE-2010-5313) denial of\nservice. In the case of a local denial of service, an attacker must have\naccess to the MMIO area or be able to access an I/O port. (CVE-2010-5313,\nCVE-2014-7842, Moderate)\n* A flaw was found in the way the Linux kernel's KVM subsystem handled\nnon-canonical addresses when emulating instructions that change the RIP\n(for example, branches or calls). A guest user with access to an I/O or\nMMIO region could use this flaw to crash the guest. (CVE-2014-3647,\nModerate)\n* It was found that the Linux kernel memory resource controller's (memcg)\nhandling of OOM (out of memory) conditions could lead to deadlocks.\nAn attacker could use this flaw to lock up the system. (CVE-2014-8171,\nModerate)\n* A race condition flaw was found between the chown and execve system\ncalls. A local, unprivileged user could potentially use this flaw to\nescalate their privileges on the system. (CVE-2015-3339, Moderate)\n* A flaw was discovered in the way the Linux kernel's TTY subsystem handled\nthe tty shutdown phase. A local, unprivileged user could use this flaw to\ncause a denial of service on the system. (CVE-2015-4170, Moderate)\n* A NULL pointer dereference flaw was found in the SCTP implementation.\nA local user could use this flaw to cause a denial of service on the system\nby triggering a kernel panic when creating multiple sockets in parallel\nwhile the system did not have the SCTP module loaded. (CVE-2015-5283,\nModerate)\n* A flaw was found in the way the Linux kernel's perf subsystem retrieved\nuserlevel stack traces on PowerPC systems. A local, unprivileged user could\nuse this flaw to cause a denial of service on the system. (CVE-2015-6526,\nModerate)\n* A flaw was found in the way the Linux kernel's Crypto subsystem handled\nautomatic loading of kernel modules. A local user could use this flaw to\nload any installed kernel module, and thus increase the attack surface of\nthe running kernel. (CVE-2013-7421, CVE-2014-9644, Low)\n* An information leak flaw was found in the way the Linux kernel changed\ncertain segment registers and thread-local storage (TLS) during a context\nswitch. A local, unprivileged user could use this flaw to leak the user\nspace TLS base address of an arbitrary process. (CVE-2014-9419, Low)\n* It was found that the Linux kernel KVM subsystem's sysenter instruction\nemulation was not sufficient. An unprivileged guest user could use this\nflaw to escalate their privileges by tricking the hypervisor to emulate a\nSYSENTER instruction in 16-bit mode, if the guest OS did not initialize the\nSYSENTER model-specific registers (MSRs). Note: Certified guest operating\nsystems for Red Hat Enterprise Linux with KVM do initialize the SYSENTER\nMSRs and are thus not vulnerable to this issue when running on a KVM\nhypervisor. (CVE-2015-0239, Low)\n* A flaw was found in the way the Linux kernel handled the securelevel\nfunctionality after performing a kexec operation. A local attacker could\nuse this flaw to bypass the security mechanism of the\nsecurelevel/secureboot combination. (CVE-2015-7837, Low)",
  "Platform": [
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2015:2152",
      "ID": "RHSA-2015:2152"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2010-5313",
      "ID": "CVE-2010-5313"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-7421",
      "ID": "CVE-2013-7421"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-3647",
      "ID": "CVE-2014-3647"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-7842",
      "ID": "CVE-2014-7842"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-8171",
      "ID": "CVE-2014-8171"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-9419",
      "ID": "CVE-2014-9419"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-9644",
      "ID": "CVE-2014-9644"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-0239",
      "ID": "CVE-2015-0239"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-2925",
      "ID": "CVE-2015-2925"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-3288",
      "ID": "CVE-2015-3288"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-3339",
      "ID": "CVE-2015-3339"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-4170",
      "ID": "CVE-2015-4170"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-5283",
      "ID": "CVE-2015-5283"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-6526",
      "ID": "CVE-2015-6526"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-7553",
      "ID": "CVE-2015-7553"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-7613",
      "ID": "CVE-2015-7613"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-7837",
      "ID": "CVE-2015-7837"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-8215",
      "ID": "CVE-2015-8215"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-0774",
      "ID": "CVE-2016-0774"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:3.10.0-327.el7 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:3.10.0-327.el7 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:3.10.0-327.el7"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-abi-whitelists is earlier than 0:3.10.0-327.el7"
                  },
                  {
                    "Comment": "kernel-abi-whitelists is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-bootwrapper is earlier than 0:3.10.0-327.el7"
                  },
                  {
                    "Comment": "kernel-bootwrapper is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:3.10.0-327.el7"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:3.10.0-327.el7"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:3.10.0-327.el7"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:3.10.0-327.el7"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:3.10.0-327.el7"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:3.10.0-327.el7"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:3.10.0-327.el7"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-tools is earlier than 0:3.10.0-327.el7"
                  },
                  {
                    "Comment": "kernel-tools is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-tools-libs is earlier than 0:3.10.0-327.el7"
                  },
                  {
                    "Comment": "kernel-tools-libs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-tools-libs-devel is earlier than 0:3.10.0-327.el7"
                  },
                  {
                    "Comment": "kernel-tools-libs-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perf is earlier than 0:3.10.0-327.el7"
                  },
                  {
                    "Comment": "perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "python-perf is earlier than 0:3.10.0-327.el7"
                  },
                  {
                    "Comment": "python-perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 7 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2010-5313"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-7421"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-3647"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-7842"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-8171"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-9419"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-9644"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-0239"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-2925"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-3288"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-3339"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-4170"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-5283"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-6526"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-7553"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-7613"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-7837"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-8215"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-0774"
    }
  ],
  "Issued": {
    "Date": "2015-11-19"
  }
}