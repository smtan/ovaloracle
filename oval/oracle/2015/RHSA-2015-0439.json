{
  "Title": "RHSA-2015:0439: krb5 security, bug fix and enhancement update (Moderate)",
  "Description": "A NULL pointer dereference flaw was found in the MIT Kerberos SPNEGO acceptor for continuation tokens. A remote, unauthenticated attacker could use this flaw to crash a GSSAPI-enabled server application. (CVE-2014-4344)\nA buffer overflow was found in the KADM5 administration server (kadmind) when it was used with an LDAP back end for the KDC database. A remote, authenticated attacker could potentially use this flaw to execute arbitrary code on the system running kadmind. (CVE-2014-4345)\nA use-after-free flaw was found in the way the MIT Kerberos libgssapi_krb5 library processed valid context deletion tokens. An attacker able to make an application using the GSS-API library (libgssapi) call the gss_process_context_token() function could use this flaw to crash that application. (CVE-2014-5352)\nIf kadmind were used with an LDAP back end for the KDC database, a remote, authenticated attacker with the permissions to set the password policy could crash kadmind by attempting to use a named ticket policy object as a password policy for a principal. (CVE-2014-5353)\nA double-free flaw was found in the way MIT Kerberos handled invalid External Data Representation (XDR) data. An authenticated user could use this flaw to crash the MIT Kerberos administration server (kadmind), or other applications using Kerberos libraries, using specially crafted XDR packets. (CVE-2014-9421)\nIt was found that the MIT Kerberos administration server (kadmind) incorrectly accepted certain authentication requests for two-component server principal names. A remote attacker able to acquire a key with a particularly named principal (such as \"kad/x\") could use this flaw to impersonate any user to kadmind, and perform administrative actions as that user. (CVE-2014-9422)\nAn information disclosure flaw was found in the way MIT Kerberos RPCSEC_GSS implementation (libgssrpc) handled certain requests. An attacker could send a specially crafted request to an application using libgssrpc to disclose a limited portion of uninitialized memory used by that application. (CVE-2014-9423)\nTwo buffer over-read flaws were found in the way MIT Kerberos handled certain requests. A remote, unauthenticated attacker able to inject packets into a client or server application's GSSAPI session could use either of these flaws to crash the application. (CVE-2014-4341, CVE-2014-4342)\nA double-free flaw was found in the MIT Kerberos SPNEGO initiators. An attacker able to spoof packets to appear as though they are from an GSSAPI acceptor could use this flaw to crash a client application that uses MIT Kerberos. (CVE-2014-4343)\nRed Hat would like to thank the MIT Kerberos project for reporting the CVE-2014-5352, CVE-2014-9421, CVE-2014-9422, and CVE-2014-9423 issues. MIT Kerberos project acknowledges Nico Williams for helping with the analysis of CVE-2014-5352.\nThe krb5 packages have been upgraded to upstream version 1.12, which provides a number of bug fixes and enhancements, including:\n* Added plug-in interfaces for principal-to-username mapping and verifying authorization to user accounts.\n* When communicating with a KDC over a connected TCP or HTTPS socket, the client gives the KDC more time to reply before it transmits the request to another server. (BZ#1049709, BZ#1127995)\nThis update also fixes multiple bugs, for example:\n* The Kerberos client library did not recognize certain exit statuses that the resolver libraries could return when looking up the addresses of servers configured in the /etc/krb5.conf file or locating Kerberos servers using DNS service location. The library could treat non-fatal return codes as fatal errors. Now, the library interprets the specific return codes correctly. (BZ#1084068, BZ#1109102)\nIn addition, this update adds various enhancements. Among others:\n* Added support for contacting KDCs and kpasswd servers through HTTPS proxies implementing the Kerberos KDC Proxy (KKDCP) protocol. (BZ#1109919)",
  "Platform": [
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2015:0439",
      "ID": "RHSA-2015:0439"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-4341",
      "ID": "CVE-2014-4341"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-4342",
      "ID": "CVE-2014-4342"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-4343",
      "ID": "CVE-2014-4343"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-4344",
      "ID": "CVE-2014-4344"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-4345",
      "ID": "CVE-2014-4345"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-5352",
      "ID": "CVE-2014-5352"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-5353",
      "ID": "CVE-2014-5353"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-9421",
      "ID": "CVE-2014-9421"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-9422",
      "ID": "CVE-2014-9422"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-9423",
      "ID": "CVE-2014-9423"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "krb5-devel is earlier than 0:1.12.2-14.el7"
                  },
                  {
                    "Comment": "krb5-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "krb5-libs is earlier than 0:1.12.2-14.el7"
                  },
                  {
                    "Comment": "krb5-libs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "krb5-pkinit is earlier than 0:1.12.2-14.el7"
                  },
                  {
                    "Comment": "krb5-pkinit is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "krb5-server is earlier than 0:1.12.2-14.el7"
                  },
                  {
                    "Comment": "krb5-server is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "krb5-server-ldap is earlier than 0:1.12.2-14.el7"
                  },
                  {
                    "Comment": "krb5-server-ldap is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "krb5-workstation is earlier than 0:1.12.2-14.el7"
                  },
                  {
                    "Comment": "krb5-workstation is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 7 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-4341"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-4342"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-4343"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-4344"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-4345"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-5352"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-5353"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-9421"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-9422"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-9423"
    }
  ],
  "Issued": {
    "Date": "2015-03-05"
  }
}