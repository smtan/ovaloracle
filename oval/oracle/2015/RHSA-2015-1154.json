{
  "Title": "RHSA-2015:1154: libreswan security, bug fix and enhancement update (Moderate)",
  "Description": "Libreswan is an implementation of IPsec & IKE for Linux. IPsec is the\nInternet Protocol Security and uses strong cryptography to provide both\nauthentication and encryption services. These services allow you to build\nsecure tunnels through untrusted networks such as virtual private network\n(VPN).\nA flaw was discovered in the way Libreswan's IKE daemon processed certain\nIKEv1 payloads. A remote attacker could send specially crafted IKEv1\npayloads that, when processed, would lead to a denial of service (daemon\ncrash). (CVE-2015-3204)\nRed Hat would like to thank Javantea for reporting this issue.\nThis update fixes the following bugs:\n* Previously, the programs/pluto/state.h and\nprograms/pluto/kernel_netlink.c files had a maximum SELinux context size\nof 257 and 1024 respectively. These restrictions set by libreswan limited\nthe size of the context that can be exchanged by pluto (the IPSec daemon)\nwhen using a Labeled Internet Protocol Security (IPsec). The SElinux\nlabels for Labeled IPsec have been extended to 4096 bytes and the\nmentioned restrictions no longer exist. (BZ#1198650)\n* On some architectures, the kernel AES_GCM IPsec algorithm did not work\nproperly with acceleration drivers. On those kernels, some acceleration\nmodules are added to the modprobe blacklist. However, Libreswan was\nignoring this blacklist, leading to AES_GCM failures. This update adds\nsupport for the module blacklist to the libreswan packages and thus\nprevents the AES_GCM failures from occurring. (BZ#1208022)\n* An IPv6 issue has been resolved that prevented ipv6-icmp Neighbour\nDiscovery from working properly once an IPsec tunnel is established (and\none endpoint reboots). When upgrading, ensure that /etc/ipsec.conf is\nloading all /etc/ipsec.d/*conf files using the /etc/ipsec.conf \"include\"\nstatement, or explicitly include this new configuration file in\n/etc/ipsec.conf. (BZ#1208023)\n* A FIPS self-test prevented libreswan from properly starting in FIPS mode.\nThis bug has been fixed and libreswan now works in FIPS mode as expected.\n(BZ#1211146)\nIn addition, this update adds the following enhancements:\n* A new option \"seedbits=\" has been added to pre-seed the Network Security\nServices (NSS) pseudo random number generator (PRNG) function with entropy\nfrom the /dev/random file on startup. This option is disabled by default.\nIt can be enabled by setting the \"seedbits=\" option in the \"config setup\"\nsection in the /etc/ipsec.conf file. (BZ#1198649)\n* The build process now runs a Cryptographic Algorithm Validation Program\n(CAVP) certification test on the Internet Key Exchange version 1 and 2\n(IKEv1 and IKEv2) PRF/PRF+ functions. (BZ#1213652)\nAll libreswan users are advised to upgrade to these updated packages,\nwhich contain backported patches to correct these issues and add these\nenhancements.",
  "Platform": [
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2015:1154",
      "ID": "RHSA-2015:1154"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-3204",
      "ID": "CVE-2015-3204"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [

        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          },
          {
            "Comment": "libreswan is earlier than 0:3.12-10.1.el7_1"
          },
          {
            "Comment": "libreswan is signed with Red Hat redhatrelease2 key"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 7 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-3204"
    }
  ],
  "Issued": {
    "Date": "2015-06-23"
  }
}