{
  "Title": "RHSA-2015:1665: mariadb security update (Moderate)",
  "Description": "MariaDB is a multi-user, multi-threaded SQL database server that is binary\ncompatible with MySQL.\nIt was found that the MySQL client library permitted but did not require a\nclient to use SSL/TLS when establishing a secure connection to a MySQL\nserver using the \"--ssl\" option. A man-in-the-middle attacker could use\nthis flaw to strip the SSL/TLS protection from a connection between a\nclient and a server. (CVE-2015-3152)\nThis update fixes several vulnerabilities in the MariaDB database server.\nInformation about these flaws can be found on the Oracle Critical Patch\nUpdate Advisory page, listed in the References section. (CVE-2015-0501,\nCVE-2015-2568, CVE-2015-0499, CVE-2015-2571, CVE-2015-0433, CVE-2015-0441,\nCVE-2015-0505, CVE-2015-2573, CVE-2015-2582, CVE-2015-2620, CVE-2015-2643,\nCVE-2015-2648, CVE-2015-4737, CVE-2015-4752, CVE-2015-4757)\nThese updated packages upgrade MariaDB to version 5.5.44. Refer to the\nMariaDB Release Notes listed in the References section for a complete list\nof changes.\nAll MariaDB users should upgrade to these updated packages, which correct\nthese issues. After installing this update, the MariaDB server daemon\n(mysqld) will be restarted automatically.",
  "Platform": [
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2015:1665",
      "ID": "RHSA-2015:1665"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-0433",
      "ID": "CVE-2015-0433"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-0441",
      "ID": "CVE-2015-0441"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-0499",
      "ID": "CVE-2015-0499"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-0501",
      "ID": "CVE-2015-0501"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-0505",
      "ID": "CVE-2015-0505"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-2568",
      "ID": "CVE-2015-2568"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-2571",
      "ID": "CVE-2015-2571"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-2573",
      "ID": "CVE-2015-2573"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-2582",
      "ID": "CVE-2015-2582"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-2620",
      "ID": "CVE-2015-2620"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-2643",
      "ID": "CVE-2015-2643"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-2648",
      "ID": "CVE-2015-2648"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-3152",
      "ID": "CVE-2015-3152"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-4737",
      "ID": "CVE-2015-4737"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-4752",
      "ID": "CVE-2015-4752"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-4757",
      "ID": "CVE-2015-4757"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-4864",
      "ID": "CVE-2015-4864"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mariadb is earlier than 1:5.5.44-1.el7_1"
                  },
                  {
                    "Comment": "mariadb is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mariadb-bench is earlier than 1:5.5.44-1.el7_1"
                  },
                  {
                    "Comment": "mariadb-bench is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mariadb-devel is earlier than 1:5.5.44-1.el7_1"
                  },
                  {
                    "Comment": "mariadb-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mariadb-embedded is earlier than 1:5.5.44-1.el7_1"
                  },
                  {
                    "Comment": "mariadb-embedded is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mariadb-embedded-devel is earlier than 1:5.5.44-1.el7_1"
                  },
                  {
                    "Comment": "mariadb-embedded-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mariadb-libs is earlier than 1:5.5.44-1.el7_1"
                  },
                  {
                    "Comment": "mariadb-libs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mariadb-server is earlier than 1:5.5.44-1.el7_1"
                  },
                  {
                    "Comment": "mariadb-server is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mariadb-test is earlier than 1:5.5.44-1.el7_1"
                  },
                  {
                    "Comment": "mariadb-test is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 7 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-0433"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-0441"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-0499"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-0501"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-0505"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-2568"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-2571"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-2573"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-2582"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-2620"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-2643"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-2648"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-3152"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-4737"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-4752"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-4757"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-4864"
    }
  ],
  "Issued": {
    "Date": "2015-08-24"
  }
}