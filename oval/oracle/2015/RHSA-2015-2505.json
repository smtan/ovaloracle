{
  "Title": "RHSA-2015:2505: abrt and libreport security update (Moderate)",
  "Description": "ABRT (Automatic Bug Reporting Tool) is a tool to help users to detect\ndefects in applications and to create a bug report with all the information\nneeded by a maintainer to fix it. It uses a plug-in system to extend its\nfunctionality. libreport provides an API for reporting different problems\nin applications to different bug targets, such as Bugzilla, FTP, and Trac.\nIt was found that the ABRT debug information installer\n(abrt-action-install-debuginfo-to-abrt-cache) did not use temporary\ndirectories in a secure way. A local attacker could use the flaw to create\nsymbolic links and files at arbitrary locations as the abrt user.\n(CVE-2015-5273)\nIt was discovered that the kernel-invoked coredump processor provided by\nABRT did not handle symbolic links correctly when writing core dumps of\nABRT programs to the ABRT dump directory (/var/spool/abrt). A local\nattacker with write access to an ABRT problem directory could use this flaw\nto escalate their privileges. (CVE-2015-5287)\nIt was found that ABRT may have exposed unintended information to Red Hat\nBugzilla during crash reporting. A bug in the libreport library caused\nchanges made by a user in files included in a crash report to be discarded.\nAs a result, Red Hat Bugzilla attachments may contain data that was not\nintended to be made public, including host names, IP addresses, or command\nline options. (CVE-2015-5302)\nThis flaw did not affect default installations of ABRT on Red Hat\nEnterprise Linux as they do not post data to Red Hat Bugzilla. This feature\ncan however be enabled, potentially impacting modified ABRT instances.\nAs a precaution, Red Hat has identified bugs filed by such non-default Red\nHat Enterprise Linux users of ABRT and marked them private.\nRed Hat would like to thank Philip Pettersson of Samsung for reporting the\nCVE-2015-5273 and CVE-2015-5287 issues. The CVE-2015-5302 issue was\ndiscovered by Bastien Nocera of Red Hat.\nAll users of abrt and libreport are advised to upgrade to these updated\npackages, which contain backported patches to correct these issues.",
  "Platform": [
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2015:2505",
      "ID": "RHSA-2015:2505"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-5273",
      "ID": "CVE-2015-5273"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-5287",
      "ID": "CVE-2015-5287"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-5302",
      "ID": "CVE-2015-5302"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "abrt is earlier than 0:2.1.11-35.el7"
                  },
                  {
                    "Comment": "abrt is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "abrt-addon-ccpp is earlier than 0:2.1.11-35.el7"
                  },
                  {
                    "Comment": "abrt-addon-ccpp is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "abrt-addon-kerneloops is earlier than 0:2.1.11-35.el7"
                  },
                  {
                    "Comment": "abrt-addon-kerneloops is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "abrt-addon-pstoreoops is earlier than 0:2.1.11-35.el7"
                  },
                  {
                    "Comment": "abrt-addon-pstoreoops is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "abrt-addon-python is earlier than 0:2.1.11-35.el7"
                  },
                  {
                    "Comment": "abrt-addon-python is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "abrt-addon-upload-watch is earlier than 0:2.1.11-35.el7"
                  },
                  {
                    "Comment": "abrt-addon-upload-watch is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "abrt-addon-vmcore is earlier than 0:2.1.11-35.el7"
                  },
                  {
                    "Comment": "abrt-addon-vmcore is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "abrt-addon-xorg is earlier than 0:2.1.11-35.el7"
                  },
                  {
                    "Comment": "abrt-addon-xorg is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "abrt-cli is earlier than 0:2.1.11-35.el7"
                  },
                  {
                    "Comment": "abrt-cli is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "abrt-console-notification is earlier than 0:2.1.11-35.el7"
                  },
                  {
                    "Comment": "abrt-console-notification is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "abrt-dbus is earlier than 0:2.1.11-35.el7"
                  },
                  {
                    "Comment": "abrt-dbus is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "abrt-desktop is earlier than 0:2.1.11-35.el7"
                  },
                  {
                    "Comment": "abrt-desktop is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "abrt-devel is earlier than 0:2.1.11-35.el7"
                  },
                  {
                    "Comment": "abrt-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "abrt-gui is earlier than 0:2.1.11-35.el7"
                  },
                  {
                    "Comment": "abrt-gui is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "abrt-gui-devel is earlier than 0:2.1.11-35.el7"
                  },
                  {
                    "Comment": "abrt-gui-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "abrt-gui-libs is earlier than 0:2.1.11-35.el7"
                  },
                  {
                    "Comment": "abrt-gui-libs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "abrt-libs is earlier than 0:2.1.11-35.el7"
                  },
                  {
                    "Comment": "abrt-libs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "abrt-python is earlier than 0:2.1.11-35.el7"
                  },
                  {
                    "Comment": "abrt-python is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "abrt-python-doc is earlier than 0:2.1.11-35.el7"
                  },
                  {
                    "Comment": "abrt-python-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "abrt-retrace-client is earlier than 0:2.1.11-35.el7"
                  },
                  {
                    "Comment": "abrt-retrace-client is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "abrt-tui is earlier than 0:2.1.11-35.el7"
                  },
                  {
                    "Comment": "abrt-tui is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libreport is earlier than 0:2.1.11-31.el7"
                  },
                  {
                    "Comment": "libreport is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libreport-anaconda is earlier than 0:2.1.11-31.el7"
                  },
                  {
                    "Comment": "libreport-anaconda is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libreport-cli is earlier than 0:2.1.11-31.el7"
                  },
                  {
                    "Comment": "libreport-cli is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libreport-compat is earlier than 0:2.1.11-31.el7"
                  },
                  {
                    "Comment": "libreport-compat is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libreport-devel is earlier than 0:2.1.11-31.el7"
                  },
                  {
                    "Comment": "libreport-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libreport-filesystem is earlier than 0:2.1.11-31.el7"
                  },
                  {
                    "Comment": "libreport-filesystem is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libreport-gtk is earlier than 0:2.1.11-31.el7"
                  },
                  {
                    "Comment": "libreport-gtk is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libreport-gtk-devel is earlier than 0:2.1.11-31.el7"
                  },
                  {
                    "Comment": "libreport-gtk-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libreport-newt is earlier than 0:2.1.11-31.el7"
                  },
                  {
                    "Comment": "libreport-newt is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libreport-plugin-bugzilla is earlier than 0:2.1.11-31.el7"
                  },
                  {
                    "Comment": "libreport-plugin-bugzilla is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libreport-plugin-kerneloops is earlier than 0:2.1.11-31.el7"
                  },
                  {
                    "Comment": "libreport-plugin-kerneloops is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libreport-plugin-logger is earlier than 0:2.1.11-31.el7"
                  },
                  {
                    "Comment": "libreport-plugin-logger is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libreport-plugin-mailx is earlier than 0:2.1.11-31.el7"
                  },
                  {
                    "Comment": "libreport-plugin-mailx is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libreport-plugin-reportuploader is earlier than 0:2.1.11-31.el7"
                  },
                  {
                    "Comment": "libreport-plugin-reportuploader is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libreport-plugin-rhtsupport is earlier than 0:2.1.11-31.el7"
                  },
                  {
                    "Comment": "libreport-plugin-rhtsupport is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libreport-plugin-ureport is earlier than 0:2.1.11-31.el7"
                  },
                  {
                    "Comment": "libreport-plugin-ureport is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libreport-python is earlier than 0:2.1.11-31.el7"
                  },
                  {
                    "Comment": "libreport-python is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libreport-rhel is earlier than 0:2.1.11-31.el7"
                  },
                  {
                    "Comment": "libreport-rhel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libreport-rhel-anaconda-bugzilla is earlier than 0:2.1.11-31.el7"
                  },
                  {
                    "Comment": "libreport-rhel-anaconda-bugzilla is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libreport-rhel-bugzilla is earlier than 0:2.1.11-31.el7"
                  },
                  {
                    "Comment": "libreport-rhel-bugzilla is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libreport-web is earlier than 0:2.1.11-31.el7"
                  },
                  {
                    "Comment": "libreport-web is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libreport-web-devel is earlier than 0:2.1.11-31.el7"
                  },
                  {
                    "Comment": "libreport-web-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 7 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-5273"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-5287"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-5302"
    }
  ],
  "Issued": {
    "Date": "2015-11-23"
  }
}