{
  "Title": "RHBA-2015:2116: GTK+ bug fix and enhancement update (Moderate)",
  "Description": "The GTK+ packages contain the GIMP ToolKit (GTK+), a library for creating graphical user interfaces for the X Window System. \nThis update contains a number of rebases to the latest upstream stable versions, which provides a number of bug fixes and enhancements over the previous versions. For more information on the changes, see the GNOME release notes and Red Hat Enterprise Linux 7.2 Release Notes.\nThe orc packages have been upgraded to version 0.4.22. (BZ#1174391)\nThe atk packages have been upgraded to version 2.14.0. (BZ#1174433)\nThe cairo packages have been upgraded to version 1.14.2. (BZ#1174435)\nThe pango packages have been upgraded to version 1.36.8. (BZ#1174436)\nThe gdk-pixbuf2 packages have been upgraded to version 2.31.1. (BZ#1174438)\nThe gobject-introspection packages have been upgraded to version 1.42.0. (BZ#1174439)\nThe glib-networking packages have been upgraded to version 2.42.2. (BZ#1174447)\nThe dconf packages have been upgraded to version 0.22.0. (BZ#1174448)\nThe gtksourceview3 packages have been upgraded to version 3.14.2. (BZ#1174500)\nThe json-glib packages have been upgraded to version 1.0.2. (BZ#1174501)\nThe webkitgtk3 packages have been upgraded to version 2.4.9. (BZ#1174556)\nThe glibmm24 packages have been upgraded to version 2.42.0. (BZ#1174565)\nThe harfbuzz packages have been upgraded to version 0.9.36. (BZ#1201148)\nThe libxklavier packages have been upgraded to version 5.4. (BZ#1202874)\nThe glib2 packages have been upgraded to version 2.42.2. (BZ#1203755)\nThe gtk2 packages have been upgraded to version 2.24.28. (BZ#1221171)\nThis update also fixes the following bugs:\n* Previously, GTK+ was treating frame times from _NET_WM_FRAME_DRAWN and\n_NET_WM_FRAME_TIMINGS as local monotonic times, but they are actually\nextended-precision versions of the server time. This was causing rendering stalls when using GTK+ applications remotely. With this update, frame times are converted to monotonic times when the X server and client are not running on the same system, and GTK+ applications can be used remotely without rendering stalls. (BZ#1243646) \n* Previously, the glib2 packages were rebased to a version that deprecated the g_memmove() function. As a consequence, libgsf failed to build from source. This update replaces g_memmove() with memmove(), thus fixing this bug. (BZ#1132679)\n* Prior to this update, the Python plug-in for GDB did not work with the version of GDB in Red Hat Enterprise Linux 7.1. As a consequence, GDB returned error messages when debugging glib2 applications. This update applies an upstream fix to use newer GDB APIs, and the Python GDB debugging aid for glib2 applications now works as expected. (BZ#1055733)\n* The glib2 utility previously returned confusing warning messages when programs added GObject properties after the class was initialized. The functionality of adding a property after the class was initialized has been added back due to backward compatibility concerns, and error messages on properties thus no longer appear. (BZ#1168600)\n* When selecting a file in the \"Add attachment\" window, Evolution previously terminated unexpectedly with a segmentation fault. This update fixes the gtk_tree_row_ref_deleted() function causing this bug, and attaching a file no longer leads to a crash. (BZ#1175941)\n* Previously, the CUPS back end checked an incorrect port to connect to remote printers. Consequently, fetching printer information failed and the \"Print\" button became insensitive. This update makes sure CUPS checks the correct port, thus fixing this bug. (BZ#1221157, BZ#1154038)\nUsers of GTK+ are advised to upgrade to these updated packages, which fix these bugs and add these enhancements.",
  "Platform": [
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHBA-2015:2116",
      "ID": "RHBA-2015:2116"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-3190",
      "ID": "CVE-2016-3190"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "dconf is earlier than 0:0.22.0-2.el7"
                  },
                  {
                    "Comment": "dconf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "dconf-devel is earlier than 0:0.22.0-2.el7"
                  },
                  {
                    "Comment": "dconf-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "dconf-editor is earlier than 0:0.22.0-2.el7"
                  },
                  {
                    "Comment": "dconf-editor is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "pango is earlier than 0:1.36.8-2.el7"
                  },
                  {
                    "Comment": "pango is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "pango-devel is earlier than 0:1.36.8-2.el7"
                  },
                  {
                    "Comment": "pango-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "pango-tests is earlier than 0:1.36.8-2.el7"
                  },
                  {
                    "Comment": "pango-tests is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "atk is earlier than 0:2.14.0-1.el7"
                  },
                  {
                    "Comment": "atk is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "atk-devel is earlier than 0:2.14.0-1.el7"
                  },
                  {
                    "Comment": "atk-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "harfbuzz is earlier than 0:0.9.36-1.el7"
                  },
                  {
                    "Comment": "harfbuzz is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "harfbuzz-devel is earlier than 0:0.9.36-1.el7"
                  },
                  {
                    "Comment": "harfbuzz-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "harfbuzz-icu is earlier than 0:0.9.36-1.el7"
                  },
                  {
                    "Comment": "harfbuzz-icu is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "glibmm24 is earlier than 0:2.42.0-1.el7"
                  },
                  {
                    "Comment": "glibmm24 is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "glibmm24-devel is earlier than 0:2.42.0-1.el7"
                  },
                  {
                    "Comment": "glibmm24-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "glibmm24-doc is earlier than 0:2.42.0-1.el7"
                  },
                  {
                    "Comment": "glibmm24-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "gobject-introspection is earlier than 0:1.42.0-1.el7"
                  },
                  {
                    "Comment": "gobject-introspection is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "gobject-introspection-devel is earlier than 0:1.42.0-1.el7"
                  },
                  {
                    "Comment": "gobject-introspection-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "cairo is earlier than 0:1.14.2-1.el7"
                  },
                  {
                    "Comment": "cairo is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "cairo-devel is earlier than 0:1.14.2-1.el7"
                  },
                  {
                    "Comment": "cairo-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "cairo-gobject is earlier than 0:1.14.2-1.el7"
                  },
                  {
                    "Comment": "cairo-gobject is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "cairo-gobject-devel is earlier than 0:1.14.2-1.el7"
                  },
                  {
                    "Comment": "cairo-gobject-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "cairo-tools is earlier than 0:1.14.2-1.el7"
                  },
                  {
                    "Comment": "cairo-tools is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "json-glib is earlier than 0:1.0.2-1.el7"
                  },
                  {
                    "Comment": "json-glib is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "json-glib-devel is earlier than 0:1.0.2-1.el7"
                  },
                  {
                    "Comment": "json-glib-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "gtksourceview3 is earlier than 0:3.14.3-1.el7"
                  },
                  {
                    "Comment": "gtksourceview3 is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "gtksourceview3-devel is earlier than 0:3.14.3-1.el7"
                  },
                  {
                    "Comment": "gtksourceview3-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "glib-networking is earlier than 0:2.42.0-1.el7"
                  },
                  {
                    "Comment": "glib-networking is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mozjs24 is earlier than 0:24.2.0-6.el7"
                  },
                  {
                    "Comment": "mozjs24 is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "mozjs24-devel is earlier than 0:24.2.0-6.el7"
                  },
                  {
                    "Comment": "mozjs24-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libgsf is earlier than 0:1.14.26-7.el7"
                  },
                  {
                    "Comment": "libgsf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libgsf-devel is earlier than 0:1.14.26-7.el7"
                  },
                  {
                    "Comment": "libgsf-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "shared-mime-info is earlier than 0:1.1-9.el7"
                  },
                  {
                    "Comment": "shared-mime-info is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "gtk2 is earlier than 0:2.24.28-8.el7"
                  },
                  {
                    "Comment": "gtk2 is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "gtk2-devel is earlier than 0:2.24.28-8.el7"
                  },
                  {
                    "Comment": "gtk2-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "gtk2-devel-docs is earlier than 0:2.24.28-8.el7"
                  },
                  {
                    "Comment": "gtk2-devel-docs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "gtk2-immodule-xim is earlier than 0:2.24.28-8.el7"
                  },
                  {
                    "Comment": "gtk2-immodule-xim is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "gtk2-immodules is earlier than 0:2.24.28-8.el7"
                  },
                  {
                    "Comment": "gtk2-immodules is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libxklavier is earlier than 0:5.4-7.el7"
                  },
                  {
                    "Comment": "libxklavier is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libxklavier-devel is earlier than 0:5.4-7.el7"
                  },
                  {
                    "Comment": "libxklavier-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libnotify is earlier than 0:0.7.5-8.el7"
                  },
                  {
                    "Comment": "libnotify is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libnotify-devel is earlier than 0:0.7.5-8.el7"
                  },
                  {
                    "Comment": "libnotify-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "orc is earlier than 0:0.4.22-5.el7"
                  },
                  {
                    "Comment": "orc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "orc-compiler is earlier than 0:0.4.22-5.el7"
                  },
                  {
                    "Comment": "orc-compiler is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "orc-devel is earlier than 0:0.4.22-5.el7"
                  },
                  {
                    "Comment": "orc-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "orc-doc is earlier than 0:0.4.22-5.el7"
                  },
                  {
                    "Comment": "orc-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libwebkit2gtk is earlier than 0:2.4.9-5.el7"
                  },
                  {
                    "Comment": "libwebkit2gtk is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "webkitgtk3 is earlier than 0:2.4.9-5.el7"
                  },
                  {
                    "Comment": "webkitgtk3 is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "webkitgtk3-devel is earlier than 0:2.4.9-5.el7"
                  },
                  {
                    "Comment": "webkitgtk3-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "webkitgtk3-doc is earlier than 0:2.4.9-5.el7"
                  },
                  {
                    "Comment": "webkitgtk3-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "gdk-pixbuf2 is earlier than 0:2.31.6-3.el7"
                  },
                  {
                    "Comment": "gdk-pixbuf2 is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "gdk-pixbuf2-devel is earlier than 0:2.31.6-3.el7"
                  },
                  {
                    "Comment": "gdk-pixbuf2-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "gdk-pixbuf2-tests is earlier than 0:2.31.6-3.el7"
                  },
                  {
                    "Comment": "gdk-pixbuf2-tests is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "gtk3 is earlier than 0:3.14.13-16.el7"
                  },
                  {
                    "Comment": "gtk3 is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "gtk3-devel is earlier than 0:3.14.13-16.el7"
                  },
                  {
                    "Comment": "gtk3-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "gtk3-devel-docs is earlier than 0:3.14.13-16.el7"
                  },
                  {
                    "Comment": "gtk3-devel-docs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "gtk3-immodule-xim is earlier than 0:3.14.13-16.el7"
                  },
                  {
                    "Comment": "gtk3-immodule-xim is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "gtk3-immodules is earlier than 0:3.14.13-16.el7"
                  },
                  {
                    "Comment": "gtk3-immodules is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "glib2 is earlier than 0:2.42.2-5.el7"
                  },
                  {
                    "Comment": "glib2 is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "glib2-devel is earlier than 0:2.42.2-5.el7"
                  },
                  {
                    "Comment": "glib2-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "glib2-doc is earlier than 0:2.42.2-5.el7"
                  },
                  {
                    "Comment": "glib2-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "glib2-fam is earlier than 0:2.42.2-5.el7"
                  },
                  {
                    "Comment": "glib2-fam is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 7 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-3190"
    }
  ],
  "Issued": {
    "Date": "2015-11-19"
  }
}