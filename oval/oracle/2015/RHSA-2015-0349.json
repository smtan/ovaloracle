{
  "Title": "RHSA-2015:0349: qemu-kvm security, bug fix, and enhancement update (Important)",
  "Description": "KVM (Kernel-based Virtual Machine) is a full virtualization solution for Linux on AMD64 and Intel 64 systems. The qemu-kvm packages provide the user-space component for running virtual machines using KVM.\nIt was found that the Cirrus blit region checks were insufficient. A privileged guest user could use this flaw to write outside of VRAM-allocated buffer boundaries in the host's QEMU process address space with attacker-provided data. (CVE-2014-8106)\nAn uninitialized data structure use flaw was found in the way the set_pixel_format() function sanitized the value of bits_per_pixel. An attacker able to access a guest's VNC console could use this flaw to crash the guest. (CVE-2014-7815)\nIt was found that certain values that were read when loading RAM during migration were not validated. A user able to alter the savevm data (either on the disk or over the wire during migration) could use either of these flaws to corrupt QEMU process memory on the (destination) host, which could potentially result in arbitrary code execution on the host with the privileges of the QEMU process. (CVE-2014-7840)\nA NULL pointer dereference flaw was found in the way QEMU handled UDP packets with a source port and address of 0 when QEMU's user networking was in use. A local guest user could use this flaw to crash the guest. (CVE-2014-3640)\nRed Hat would like to thank James Spadaro of Cisco for reporting CVE-2014-7815, and Xavier Mehrenberger and Stephane Duverger of Airbus for reporting CVE-2014-3640. The CVE-2014-8106 issue was found by Paolo Bonzini of Red Hat, and the CVE-2014-7840 issue was discovered by Michael S. Tsirkin of Red Hat.\nBug fixes:\n* The KVM utility executed demanding routing update system calls every time it performed an MSI vector mask/unmask operation. Consequently, guests running legacy systems such as Red Hat Enterprise Linux 5 could, under certain circumstances, experience significant slowdown. Now, the routing system calls during mask/unmask operations are skipped, and the performance of legacy guests is now more consistent. (BZ#1098976)\n* Due to a bug in the Internet Small Computer System Interface (iSCSI) driver, a qemu-kvm process terminated unexpectedly with a segmentation fault when the \"write same\" command was executed in guest mode under the iSCSI protocol. This update fixes the bug, and the \"write same\" command now functions in guest mode under iSCSI as intended. (BZ#1083413)\n* The QEMU command interface did not properly handle resizing of cache memory during guest migration, causing QEMU to terminate unexpectedly with a segmentation fault. This update fixes the related code, and QEMU no longer crashes in the described situation. (BZ#1066338)\nEnhancements:\n* The maximum number of supported virtual CPUs (vCPUs) in a KVM guest has been increased to 240. This increases the number of virtual processing units that the user can assign to the guest, and therefore improves its performance potential. (BZ#1134408)\n* Support for the 5th Generation Intel Core processors has been added to the QEMU hypervisor, the KVM kernel code, and the libvirt API. This allows KVM guests to use the following instructions and features: ADCX, ADOX, RDSFEED, PREFETCHW, and supervisor mode access prevention (SMAP). (BZ#1116117)\n* The \"dump-guest-memory\" command now supports crash dump compression. This makes it possible for users who cannot use the \"virsh dump\" command to require less hard disk space for guest crash dumps. In addition, saving a compressed guest crash dump frequently takes less time than saving a non-compressed one. (BZ#1157798)\n* This update introduces support for flight recorder tracing, which uses SystemTap to automatically capture qemu-kvm data while the guest machine is running. For detailed instructions on how to configure and use flight recorder tracing, see the Virtualization Deployment and Administration Guide, linked to in the References section below. (BZ#1088112)",
  "Platform": [
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2015:0349",
      "ID": "RHSA-2015:0349"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-3640",
      "ID": "CVE-2014-3640"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-7815",
      "ID": "CVE-2014-7815"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-7840",
      "ID": "CVE-2014-7840"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-8106",
      "ID": "CVE-2014-8106"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libcacard is earlier than 10:1.5.3-86.el7"
                  },
                  {
                    "Comment": "libcacard is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libcacard-devel is earlier than 10:1.5.3-86.el7"
                  },
                  {
                    "Comment": "libcacard-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libcacard-tools is earlier than 10:1.5.3-86.el7"
                  },
                  {
                    "Comment": "libcacard-tools is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "qemu-img is earlier than 10:1.5.3-86.el7"
                  },
                  {
                    "Comment": "qemu-img is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "qemu-kvm is earlier than 10:1.5.3-86.el7"
                  },
                  {
                    "Comment": "qemu-kvm is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "qemu-kvm-common is earlier than 10:1.5.3-86.el7"
                  },
                  {
                    "Comment": "qemu-kvm-common is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "qemu-kvm-tools is earlier than 10:1.5.3-86.el7"
                  },
                  {
                    "Comment": "qemu-kvm-tools is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 7 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-3640"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-7815"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-7840"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-8106"
    }
  ],
  "Issued": {
    "Date": "2015-03-05"
  }
}