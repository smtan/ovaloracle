{
  "Title": "RHSA-2015:1272: kernel security, bug fix, and enhancement update (Moderate)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux\noperating system.\n* A flaw was found in the way Linux kernel's Transparent Huge Pages (THP)\nimplementation handled non-huge page migration. A local, unprivileged user\ncould use this flaw to crash the kernel by migrating transparent hugepages.\n(CVE-2014-3940, Moderate)\n* A buffer overflow flaw was found in the way the Linux kernel's eCryptfs\nimplementation decoded encrypted file names. A local, unprivileged user\ncould use this flaw to crash the system or, potentially, escalate their\nprivileges on the system. (CVE-2014-9683, Moderate)\n* A race condition flaw was found between the chown and execve system\ncalls. When changing the owner of a setuid user binary to root, the race\ncondition could momentarily make the binary setuid root. A local,\nunprivileged user could potentially use this flaw to escalate their\nprivileges on the system. (CVE-2015-3339, Moderate)\n* Multiple out-of-bounds write flaws were found in the way the Cherry\nCymotion keyboard driver, KYE/Genius device drivers, Logitech device\ndrivers, Monterey Genius KB29E keyboard driver, Petalynx Maxter remote\ncontrol driver, and Sunplus wireless desktop driver handled HID reports\nwith an invalid report descriptor size. An attacker with physical access to\nthe system could use either of these flaws to write data past an allocated\nmemory buffer. (CVE-2014-3184, Low)\n* An information leak flaw was found in the way the Linux kernel's Advanced\nLinux Sound Architecture (ALSA) implementation handled access of the user\ncontrol's state. A local, privileged user could use this flaw to leak\nkernel memory to user space. (CVE-2014-4652, Low)\n* It was found that the espfix functionality could be bypassed by\ninstalling a 16-bit RW data segment into GDT instead of LDT (which espfix\nchecks), and using that segment on the stack. A local, unprivileged user\ncould potentially use this flaw to leak kernel stack addresses.\n(CVE-2014-8133, Low)\n* An information leak flaw was found in the Linux kernel's IEEE 802.11\nwireless networking implementation. When software encryption was used, a\nremote attacker could use this flaw to leak up to 8 bytes of plaintext.\n(CVE-2014-8709, Low)\n* It was found that the Linux kernel KVM subsystem's sysenter instruction\nemulation was not sufficient. An unprivileged guest user could use this\nflaw to escalate their privileges by tricking the hypervisor to emulate a\nSYSENTER instruction in 16-bit mode, if the guest OS did not initialize the\nSYSENTER model-specific registers (MSRs). Note: Certified guest operating\nsystems for Red Hat Enterprise Linux with KVM do initialize the SYSENTER\nMSRs and are thus not vulnerable to this issue when running on a KVM\nhypervisor. (CVE-2015-0239, Low)\nRed Hat would like to thank Andy Lutomirski for reporting the CVE-2014-8133\nissue, and Nadav Amit for reporting the CVE-2015-0239 issue.\nThis update fixes several hundred bugs and adds numerous enhancements.\nRefer to the Red Hat Enterprise Linux 6.7 Release Notes for information on\nthe most significant of these changes, and the following Knowledgebase\narticle for further information:\nhttps://access.redhat.com/articles/1466073\nAll kernel users are advised to upgrade to these updated packages, which\ncontain backported patches to correct these issues and add these\nenhancements. The system must be rebooted for this update to take effect.",
  "Platform": [
    "Red Hat Enterprise Linux 6"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2015:1272",
      "ID": "RHSA-2015:1272"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-3184",
      "ID": "CVE-2014-3184"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-3940",
      "ID": "CVE-2014-3940"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-4652",
      "ID": "CVE-2014-4652"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-8133",
      "ID": "CVE-2014-8133"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-8709",
      "ID": "CVE-2014-8709"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2014-9683",
      "ID": "CVE-2014-9683"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-0239",
      "ID": "CVE-2015-0239"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-3339",
      "ID": "CVE-2015-3339"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.32-573.el6 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.32-573.el6 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.32-573.el6"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-abi-whitelists is earlier than 0:2.6.32-573.el6"
                  },
                  {
                    "Comment": "kernel-abi-whitelists is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-bootwrapper is earlier than 0:2.6.32-573.el6"
                  },
                  {
                    "Comment": "kernel-bootwrapper is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:2.6.32-573.el6"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:2.6.32-573.el6"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.32-573.el6"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.32-573.el6"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-firmware is earlier than 0:2.6.32-573.el6"
                  },
                  {
                    "Comment": "kernel-firmware is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:2.6.32-573.el6"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:2.6.32-573.el6"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:2.6.32-573.el6"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perf is earlier than 0:2.6.32-573.el6"
                  },
                  {
                    "Comment": "perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "python-perf is earlier than 0:2.6.32-573.el6"
                  },
                  {
                    "Comment": "python-perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-3184"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-3940"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-4652"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-8133"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-8709"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2014-9683"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-0239"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-3339"
    }
  ],
  "Issued": {
    "Date": "2015-07-20"
  }
}