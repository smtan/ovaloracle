{
  "Title": "RHSA-2013:0594: kernel security and bug fix update (Low)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux\noperating system.\nThis update fixes the following security issues:\n* Buffer overflow flaws were found in the udf_load_logicalvol() function in\nthe Universal Disk Format (UDF) file system implementation in the Linux\nkernel. An attacker with physical access to a system could use these flaws\nto cause a denial of service or escalate their privileges. (CVE-2012-3400,\nLow)\nThis update also fixes the following bugs:\n* Previously, race conditions could sometimes occur in interrupt handling \non the Emulex BladeEngine 2 (BE2) controllers, causing the network adapter\nto become unresponsive. This update provides a series of patches for the \nbe2net driver, which prevents the race from occurring. The network cards \nusing BE2 chipsets no longer hang due to incorrectly handled interrupt \nevents. (BZ#884704)\n* A boot-time memory allocation pool (the DMI heap) is used to keep the\nlist of Desktop Management Interface (DMI) devices during the system boot.\nPreviously, the size of the DMI heap was only 2048 bytes on the AMD64 and\nIntel 64 architectures and the DMI heap space could become easily depleted\non some systems, such as the IBM System x3500 M2. A subsequent OOM failure\ncould, under certain circumstances, lead to a NULL pointer entry being\nstored in the DMI device list. Consequently, scanning of such a corrupted\nDMI device list resulted in a kernel panic. The boot-time memory allocation\npool for the AMD64 and Intel 64 architectures has been enlarged to 4096\nbytes and the routines responsible for populating the DMI device list have\nbeen modified to skip entries if their name string is NULL. The kernel no\nlonger panics in this scenario. (BZ#902683)\n* The size of the buffer used to print the kernel taint output on kernel\npanic was too small, which resulted in the kernel taint output not being\nprinted completely sometimes. With this update, the size of the buffer has\nbeen adjusted and the kernel taint output is now displayed properly.\n(BZ#905829)\n* The code to print the kernel taint output contained a typographical\nerror. Consequently, the kernel taint output, which is displayed on kernel\npanic, could not provide taint error messages for unsupported hardware.\nThis update fixes the typo and the kernel taint output is now displayed\ncorrectly. (BZ#885063)\nUsers should upgrade to these updated packages, which contain backported\npatches to correct these issues. The system must be rebooted for this\nupdate to take effect.",
  "Platform": [
    "Red Hat Enterprise Linux 5"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2013:0594",
      "ID": "RHSA-2013:0594"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-3400",
      "ID": "CVE-2012-3400"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.18-348.2.1.el5 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.18-348.2.1.el5 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.18-348.2.1.el5"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-PAE is earlier than 0:2.6.18-348.2.1.el5"
                  },
                  {
                    "Comment": "kernel-PAE is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-PAE-devel is earlier than 0:2.6.18-348.2.1.el5"
                  },
                  {
                    "Comment": "kernel-PAE-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:2.6.18-348.2.1.el5"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:2.6.18-348.2.1.el5"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.18-348.2.1.el5"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.18-348.2.1.el5"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:2.6.18-348.2.1.el5"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:2.6.18-348.2.1.el5"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:2.6.18-348.2.1.el5"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xen is earlier than 0:2.6.18-348.2.1.el5"
                  },
                  {
                    "Comment": "kernel-xen is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xen-devel is earlier than 0:2.6.18-348.2.1.el5"
                  },
                  {
                    "Comment": "kernel-xen-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 5 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 5 is installed"
      }
    ]
  },
  "Severity": "Low",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-3400"
    }
  ],
  "Issued": {
    "Date": "2013-03-05"
  }
}