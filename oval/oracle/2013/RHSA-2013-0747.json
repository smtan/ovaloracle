{
  "Title": "RHSA-2013:0747: kernel security and bug fix update (Moderate)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux\noperating system.\nThis update fixes the following security issues:\n* A flaw was found in the Xen netback driver implementation in the Linux\nkernel. A privileged guest user with access to a para-virtualized network\ndevice could use this flaw to cause a long loop in netback, leading to a\ndenial of service that could potentially affect the entire system.\n(CVE-2013-0216, Moderate)\n* A flaw was found in the Xen PCI device back-end driver implementation in\nthe Linux kernel. A privileged guest user in a guest that has a PCI\npassthrough device could use this flaw to cause a denial of service that\ncould potentially affect the entire system. (CVE-2013-0231, Moderate)\n* A NULL pointer dereference flaw was found in the IP packet transformation\nframework (XFRM) implementation in the Linux kernel. A local user who has\nthe CAP_NET_ADMIN capability could use this flaw to cause a denial of\nservice. (CVE-2013-1826, Moderate)\n* Information leak flaws were found in the XFRM implementation in the\nLinux kernel. A local user who has the CAP_NET_ADMIN capability could use\nthese flaws to leak kernel stack memory to user-space. (CVE-2012-6537, Low)\n* An information leak flaw was found in the logical link control (LLC)\nimplementation in the Linux kernel. A local, unprivileged user could use\nthis flaw to leak kernel stack memory to user-space. (CVE-2012-6542, Low)\n* Two information leak flaws were found in the Linux kernel's Asynchronous\nTransfer Mode (ATM) subsystem. A local, unprivileged user could use these\nflaws to leak kernel stack memory to user-space. (CVE-2012-6546, Low)\n* An information leak flaw was found in the TUN/TAP device driver in the\nLinux kernel's networking implementation. A local user with access to a\nTUN/TAP virtual interface could use this flaw to leak kernel stack memory\nto user-space. (CVE-2012-6547, Low)\nRed Hat would like to thank the Xen project for reporting the CVE-2013-0216\nand CVE-2013-0231 issues.\nThis update also fixes the following bugs:\n* The IPv4 code did not correctly update the Maximum Transfer Unit (MTU) of\nthe designed interface when receiving ICMP Fragmentation Needed packets.\nConsequently, a remote host did not respond correctly to ping attempts.\nWith this update, the IPv4 code has been modified so the MTU of the\ndesigned interface is adjusted as expected in this situation. The ping\ncommand now provides the expected output. (BZ#923353)\n* Previously, the be2net code expected the last word of an MCC completion\nmessage from the firmware to be transferred by direct memory access (DMA)\nat once. However, this is not always true, and could therefore cause the\nBUG_ON() macro to be triggered in the be_mcc_compl_is_new() function,\nconsequently leading to a kernel panic. The BUG_ON() macro has been\nremoved from be_mcc_compl_is_new(), and the kernel panic no longer occurs\nin this scenario. (BZ#923910)\n* Previously, the NFSv3 server incorrectly converted 64-bit cookies to\n32-bit. Consequently, the cookies became invalid, which affected all file\nsystem operations depending on these cookies, such as the READDIR operation\nthat is used to read entries from a directory. This led to various\nproblems, such as exported directories being empty or displayed\nincorrectly, or an endless loop of the READDIRPLUS procedure which could\npotentially cause a buffer overflow. This update modifies knfsd code so\nthat 64-bit cookies are now handled correctly and all file system\noperations work as expected. (BZ#924087)\nUsers should upgrade to these updated packages, which contain backported\npatches to correct these issues. The system must be rebooted for this\nupdate to take effect.",
  "Platform": [
    "Red Hat Enterprise Linux 5"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2013:0747",
      "ID": "RHSA-2013:0747"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-6537",
      "ID": "CVE-2012-6537"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-6542",
      "ID": "CVE-2012-6542"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-6546",
      "ID": "CVE-2012-6546"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-6547",
      "ID": "CVE-2012-6547"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-0216",
      "ID": "CVE-2013-0216"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-0231",
      "ID": "CVE-2013-0231"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-1826",
      "ID": "CVE-2013-1826"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.18-348.4.1.el5 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.18-348.4.1.el5 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.18-348.4.1.el5"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-PAE is earlier than 0:2.6.18-348.4.1.el5"
                  },
                  {
                    "Comment": "kernel-PAE is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-PAE-devel is earlier than 0:2.6.18-348.4.1.el5"
                  },
                  {
                    "Comment": "kernel-PAE-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:2.6.18-348.4.1.el5"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:2.6.18-348.4.1.el5"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.18-348.4.1.el5"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.18-348.4.1.el5"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:2.6.18-348.4.1.el5"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:2.6.18-348.4.1.el5"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:2.6.18-348.4.1.el5"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xen is earlier than 0:2.6.18-348.4.1.el5"
                  },
                  {
                    "Comment": "kernel-xen is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xen-devel is earlier than 0:2.6.18-348.4.1.el5"
                  },
                  {
                    "Comment": "kernel-xen-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 5 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 5 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-6537"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-6542"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-6546"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-6547"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-0216"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-0231"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-1826"
    }
  ],
  "Issued": {
    "Date": "2013-04-16"
  }
}