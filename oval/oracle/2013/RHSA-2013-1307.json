{
  "Title": "RHSA-2013:1307: php53 security, bug fix and enhancement update (Moderate)",
  "Description": "PHP is an HTML-embedded scripting language commonly used with the Apache\nHTTP Server.\nIt was found that PHP did not properly handle file names with a NULL\ncharacter. A remote attacker could possibly use this flaw to make a PHP\nscript access unexpected files and bypass intended file system access\nrestrictions. (CVE-2006-7243)\nIt was found that PHP did not check for carriage returns in HTTP headers,\nallowing intended HTTP response splitting protections to be bypassed.\nDepending on the web browser the victim is using, a remote attacker could\nuse this flaw to perform HTTP response splitting attacks. (CVE-2011-1398)\nA flaw was found in PHP's SSL client's hostname identity check when\nhandling certificates that contain hostnames with NULL bytes. If an\nattacker was able to get a carefully crafted certificate signed by a\ntrusted Certificate Authority, the attacker could use the certificate to\nconduct man-in-the-middle attacks to spoof SSL servers. (CVE-2013-4248)\nAn integer signedness issue, leading to a heap-based buffer underflow, was\nfound in the PHP scandir() function. If a remote attacker could upload an\nexcessively large number of files to a directory the scandir() function\nruns on, it could cause the PHP interpreter to crash or, possibly, execute\narbitrary code. (CVE-2012-2688)\nIt was found that PHP did not correctly handle the magic_quotes_gpc\nconfiguration directive. This could result in magic_quotes_gpc input\nescaping not being applied in all cases, possibly making it easier for a\nremote attacker to perform SQL injection attacks. (CVE-2012-0831)\nIt was found that the PHP SOAP parser allowed the expansion of external XML\nentities during SOAP message parsing. A remote attacker could possibly use\nthis flaw to read arbitrary files that are accessible to a PHP application\nusing a SOAP extension. (CVE-2013-1643)\nThese updated php53 packages also include numerous bug fixes and\nenhancements. Space precludes documenting all of these changes in this\nadvisory. Users are directed to the Red Hat Enterprise Linux 5.10 Technical\nNotes, linked to in the References, for information on the most significant\nof these changes.\nAll PHP users are advised to upgrade to these updated packages, which\ncontain backported patches to correct these issues and add this\nenhancement. After installing the updated packages, the httpd daemon must\nbe restarted for the update to take effect.",
  "Platform": [
    "Red Hat Enterprise Linux 5"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2013:1307",
      "ID": "RHSA-2013:1307"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2006-7243",
      "ID": "CVE-2006-7243"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-1398",
      "ID": "CVE-2011-1398"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-0831",
      "ID": "CVE-2012-0831"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-2688",
      "ID": "CVE-2012-2688"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-1643",
      "ID": "CVE-2013-1643"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-4248",
      "ID": "CVE-2013-4248"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53 is earlier than 0:5.3.3-21.el5"
                  },
                  {
                    "Comment": "php53 is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-bcmath is earlier than 0:5.3.3-21.el5"
                  },
                  {
                    "Comment": "php53-bcmath is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-cli is earlier than 0:5.3.3-21.el5"
                  },
                  {
                    "Comment": "php53-cli is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-common is earlier than 0:5.3.3-21.el5"
                  },
                  {
                    "Comment": "php53-common is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-dba is earlier than 0:5.3.3-21.el5"
                  },
                  {
                    "Comment": "php53-dba is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-devel is earlier than 0:5.3.3-21.el5"
                  },
                  {
                    "Comment": "php53-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-gd is earlier than 0:5.3.3-21.el5"
                  },
                  {
                    "Comment": "php53-gd is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-imap is earlier than 0:5.3.3-21.el5"
                  },
                  {
                    "Comment": "php53-imap is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-intl is earlier than 0:5.3.3-21.el5"
                  },
                  {
                    "Comment": "php53-intl is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-ldap is earlier than 0:5.3.3-21.el5"
                  },
                  {
                    "Comment": "php53-ldap is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-mbstring is earlier than 0:5.3.3-21.el5"
                  },
                  {
                    "Comment": "php53-mbstring is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-mysql is earlier than 0:5.3.3-21.el5"
                  },
                  {
                    "Comment": "php53-mysql is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-odbc is earlier than 0:5.3.3-21.el5"
                  },
                  {
                    "Comment": "php53-odbc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-pdo is earlier than 0:5.3.3-21.el5"
                  },
                  {
                    "Comment": "php53-pdo is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-pgsql is earlier than 0:5.3.3-21.el5"
                  },
                  {
                    "Comment": "php53-pgsql is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-process is earlier than 0:5.3.3-21.el5"
                  },
                  {
                    "Comment": "php53-process is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-pspell is earlier than 0:5.3.3-21.el5"
                  },
                  {
                    "Comment": "php53-pspell is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-snmp is earlier than 0:5.3.3-21.el5"
                  },
                  {
                    "Comment": "php53-snmp is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-soap is earlier than 0:5.3.3-21.el5"
                  },
                  {
                    "Comment": "php53-soap is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-xml is earlier than 0:5.3.3-21.el5"
                  },
                  {
                    "Comment": "php53-xml is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-xmlrpc is earlier than 0:5.3.3-21.el5"
                  },
                  {
                    "Comment": "php53-xmlrpc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 5 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 5 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2006-7243"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-1398"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-0831"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-2688"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-1643"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-4248"
    }
  ],
  "Issued": {
    "Date": "2013-09-30"
  }
}