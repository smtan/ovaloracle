{
  "Title": "RHSA-2013:1635: pacemaker security, bug fix, and enhancement update (Low)",
  "Description": "Pacemaker is a high-availability cluster resource manager with a powerful\npolicy engine.\nA denial of service flaw was found in the way Pacemaker performed\nauthentication and processing of remote connections in certain\ncircumstances. When Pacemaker was configured to allow remote Cluster\nInformation Base (CIB) configuration or resource management, a remote\nattacker could use this flaw to cause Pacemaker to block indefinitely\n(preventing it from serving other requests). (CVE-2013-0281)\nNote: The default Pacemaker configuration in Red Hat Enterprise Linux 6 has\nthe remote CIB management functionality disabled.\nThe pacemaker package has been upgraded to upstream version 1.1.10, which\nprovides a number of bug fixes and enhancements over the previous version:\n* Pacemaker no longer assumes unknown cman nodes are safely stopped.\n* The core dump file now converts all exit codes into positive 'errno'\nvalues.\n* Pacemaker ensures a return to a stable state after too many fencing\nfailures, and initiates a shutdown if a node claimed to be fenced is still\nactive.\n* The crm_error tool adds the ability to list and print error symbols.\n* The crm_resource command allows individual resources to be reprobed, and\nimplements the \"--ban\" option for moving resources away from nodes.\nThe \"--clear\" option has replaced the \"--unmove\" option. Also, crm_resource\nnow supports OCF tracing when using the \"--force\" option.\n* The IPC mechanism restores the ability for members of the haclient group\nto connect to the cluster.\n* The Policy Engine daemon allows active nodes in the current membership to\nbe fenced without quorum.\n* Policy Engine now suppresses meaningless IDs when displaying anonymous\nclone status, supports maintenance mode for a single node, and correctly\nhandles the recovered resources before they are operated on.\n* XML configuration files are now checked for non-printing characters and\nreplaced with their octal equivalent when exporting XML text. Also, a more\nreliable buffer allocation strategy has been implemented to prevent\nlockups.\n(BZ#987355)\nAdditional bug fixes:\n* The \"crm_resource --move\" command was designed for atomic resources and\ncould not handle resources on clones, masters, or slaves present on\nmultiple nodes. Consequently, crm_resource could not obtain enough\ninformation to move a resource and did not perform any action. The \"--ban\"\nand \"--clear\" options have been added to allow the administrator to\ninstruct the cluster unambiguously. Clone, master, and slave resources can\nnow be navigated within the cluster as expected. (BZ#902407)\n* The hacluster user account did not have a user identification (UID) or\ngroup identification (GID) number reserved on the system. Thus, UID and GID\nvalues were picked randomly during the installation process. The UID and\nGID number 189 was reserved for hacluster and is now used consistently for\nall installations. (BZ#908450)\n* Certain clusters used node host names that did not match the output of\nthe \"uname -n\" command. Thus, the default node name used by the crm_standby\nand crm_failcount commands was incorrect and caused the cluster to ignore\nthe update by the administrator. The crm_node command is now used instead\nof the uname utility in helper scripts. As a result, the cluster behaves as\nexpected. (BZ#913093)\n* Due to incorrect return code handling, internal recovery logic of the\ncrm_mon utility was not executed when a configuration updated failed to\napply, leading to an assertion failure. Return codes are now checked\ncorrectly, and the recovery of an expected error state is now handled\ntransparently. (BZ#951371)\n* cman's automatic unfencing feature failed when combined with Pacemaker.\nSupport for automated unfencing in Pacemaker has been added, and the\nunwanted behavior no longer occurs. (BZ#996850)\nAll pacemaker users are advised to upgrade to these updated packages, which\ncontain backported patches to correct these issues and add these\nenhancements.",
  "Platform": [
    "Red Hat Enterprise Linux 6"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2013:1635",
      "ID": "RHSA-2013:1635"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-0281",
      "ID": "CVE-2013-0281"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "pacemaker is earlier than 0:1.1.10-14.el6"
                  },
                  {
                    "Comment": "pacemaker is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "pacemaker-cli is earlier than 0:1.1.10-14.el6"
                  },
                  {
                    "Comment": "pacemaker-cli is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "pacemaker-cluster-libs is earlier than 0:1.1.10-14.el6"
                  },
                  {
                    "Comment": "pacemaker-cluster-libs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "pacemaker-cts is earlier than 0:1.1.10-14.el6"
                  },
                  {
                    "Comment": "pacemaker-cts is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "pacemaker-doc is earlier than 0:1.1.10-14.el6"
                  },
                  {
                    "Comment": "pacemaker-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "pacemaker-libs is earlier than 0:1.1.10-14.el6"
                  },
                  {
                    "Comment": "pacemaker-libs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "pacemaker-libs-devel is earlier than 0:1.1.10-14.el6"
                  },
                  {
                    "Comment": "pacemaker-libs-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "pacemaker-remote is earlier than 0:1.1.10-14.el6"
                  },
                  {
                    "Comment": "pacemaker-remote is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Low",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-0281"
    }
  ],
  "Issued": {
    "Date": "2013-11-20"
  }
}