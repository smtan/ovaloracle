{
  "Title": "RHSA-2013:1173: kernel security and bug fix update (Important)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux\noperating system.\nThis update fixes the following security issues:\n* A flaw was found in the way the Linux kernel's Stream Control\nTransmission Protocol (SCTP) implementation handled duplicate cookies. If a\nlocal user queried SCTP connection information at the same time a remote\nattacker has initialized a crafted SCTP connection to the system, it could\ntrigger a NULL pointer dereference, causing the system to crash.\n(CVE-2013-2206, Important)\n* It was found that the fix for CVE-2012-3552 released via RHSA-2012:1304\nintroduced an invalid free flaw in the Linux kernel's TCP/IP protocol suite\nimplementation. A local, unprivileged user could use this flaw to corrupt\nkernel memory via crafted sendmsg() calls, allowing them to cause a denial\nof service or, potentially, escalate their privileges on the system.\n(CVE-2013-2224, Important)\n* A flaw was found in the Linux kernel's Performance Events implementation.\nOn systems with certain Intel processors, a local, unprivileged user could\nuse this flaw to cause a denial of service by leveraging the perf subsystem\nto write into the reserved bits of the OFFCORE_RSP_0 and OFFCORE_RSP_1\nmodel-specific registers. (CVE-2013-2146, Moderate)\n* An invalid pointer dereference flaw was found in the Linux kernel's\nTCP/IP protocol suite implementation. A local, unprivileged user could use\nthis flaw to crash the system or, potentially, escalate their privileges on\nthe system by using sendmsg() with an IPv6 socket connected to an IPv4\ndestination. (CVE-2013-2232, Moderate)\n* Information leak flaws in the Linux kernel's Bluetooth implementation\ncould allow a local, unprivileged user to leak kernel memory to user-space.\n(CVE-2012-6544, Low)\n* An information leak flaw in the Linux kernel could allow a privileged,\nlocal user to leak kernel memory to user-space. (CVE-2013-2237, Low)\nThis update also fixes several bugs. Documentation for these changes will\nbe available shortly from the Technical Notes document linked to in the\nReferences section.\nUsers should upgrade to these updated packages, which contain backported\npatches to correct these issues. The system must be rebooted for this\nupdate to take effect.",
  "Platform": [
    "Red Hat Enterprise Linux 6"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2013:1173",
      "ID": "RHSA-2013:1173"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-6544",
      "ID": "CVE-2012-6544"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-2146",
      "ID": "CVE-2013-2146"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-2206",
      "ID": "CVE-2013-2206"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-2224",
      "ID": "CVE-2013-2224"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-2232",
      "ID": "CVE-2013-2232"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-2237",
      "ID": "CVE-2013-2237"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.32-358.18.1.el6 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.32-358.18.1.el6 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.32-358.18.1.el6"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-bootwrapper is earlier than 0:2.6.32-358.18.1.el6"
                  },
                  {
                    "Comment": "kernel-bootwrapper is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:2.6.32-358.18.1.el6"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:2.6.32-358.18.1.el6"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.32-358.18.1.el6"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.32-358.18.1.el6"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-firmware is earlier than 0:2.6.32-358.18.1.el6"
                  },
                  {
                    "Comment": "kernel-firmware is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:2.6.32-358.18.1.el6"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:2.6.32-358.18.1.el6"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:2.6.32-358.18.1.el6"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perf is earlier than 0:2.6.32-358.18.1.el6"
                  },
                  {
                    "Comment": "perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "python-perf is earlier than 0:2.6.32-358.18.1.el6"
                  },
                  {
                    "Comment": "python-perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-6544"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-2146"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-2206"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-2224"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-2232"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-2237"
    }
  ],
  "Issued": {
    "Date": "2013-08-27"
  }
}