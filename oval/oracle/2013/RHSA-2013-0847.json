{
  "Title": "RHSA-2013:0847: kernel security and bug fix update (Moderate)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux\noperating system.\nThis update fixes the following security issue:\n* A flaw was found in the way the Xen hypervisor AMD IOMMU driver handled\ninterrupt remapping entries. By default, a single interrupt remapping\ntable is used, and old interrupt remapping entries are not cleared,\npotentially allowing a privileged guest user in a guest that has a\npassed-through, bus-mastering capable PCI device to inject interrupt\nentries into others guests, including the privileged management domain\n(Dom0), leading to a denial of service. (CVE-2013-0153, Moderate)\nRed Hat would like to thank the Xen project for reporting the CVE-2013-0153\nissue.\nThis update also fixes the following bugs:\n* When a process is opening a file over NFSv4, sometimes an OPEN call can\nsucceed while the following GETATTR operation fails with an NFS4ERR_DELAY\nerror. The NFSv4 code did not handle such a situation correctly and allowed\nan NFSv4 client to attempt to use the buffer that should contain the\nGETATTR information. However, the buffer did not contain the valid GETATTR\ninformation, which caused the client to return a \"-ENOTDIR\" error.\nConsequently, the process failed to open the requested file. This update\nbackports a patch that adds a test condition verifying validity of the\nGETATTR information. If the GETATTR information is invalid, it is obtained\nlater and the process opens the requested file as expected. (BZ#947736)\n* Previously, the xdr routines in NFS version 2 and 3 conditionally updated\nthe res->count variable. Read retry attempts after a short NFS read() call\ncould fail to update the res->count variable, resulting in truncated read\ndata being returned. With this update, the res->count variable is updated\nunconditionally so this bug can no longer occur. (BZ#952098)\n* When handling requests from Intelligent Platform Management Interface\n(IPMI) clients, the IPMI driver previously used two different locks for an\nIPMI request. If two IPMI clients sent their requests at the same time,\neach request could receive one of the locks and then wait for the second\nlock to become available. This resulted in a deadlock situation and the\nsystem became unresponsive. The problem could occur more likely in\nenvironments with many IPMI clients. This update modifies the IPMI driver\nto handle the received messages using tasklets so the driver now uses a\nsafe locking technique when handling IPMI requests and the mentioned\ndeadlock can no longer occur. (BZ#953435)\n* Incorrect locking around the cl_state_owners list could cause the NFSv4\nstate reclaimer thread to enter an infinite loop while holding the Big\nKernel Lock (BLK). As a consequence, the NFSv4 client became unresponsive.\nWith this update, safe list iteration is used, which prevents the NFSv4\nclient from hanging in this scenario. (BZ#954296)\nUsers should upgrade to these updated packages, which contain backported\npatches to correct these issues. The system must be rebooted for this\nupdate to take effect.",
  "Platform": [
    "Red Hat Enterprise Linux 5"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2013:0847",
      "ID": "RHSA-2013:0847"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-0153",
      "ID": "CVE-2013-0153"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.18-348.6.1.el5 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.18-348.6.1.el5 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.18-348.6.1.el5"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-PAE is earlier than 0:2.6.18-348.6.1.el5"
                  },
                  {
                    "Comment": "kernel-PAE is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-PAE-devel is earlier than 0:2.6.18-348.6.1.el5"
                  },
                  {
                    "Comment": "kernel-PAE-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:2.6.18-348.6.1.el5"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:2.6.18-348.6.1.el5"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.18-348.6.1.el5"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.18-348.6.1.el5"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:2.6.18-348.6.1.el5"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:2.6.18-348.6.1.el5"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:2.6.18-348.6.1.el5"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xen is earlier than 0:2.6.18-348.6.1.el5"
                  },
                  {
                    "Comment": "kernel-xen is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xen-devel is earlier than 0:2.6.18-348.6.1.el5"
                  },
                  {
                    "Comment": "kernel-xen-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 5 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 5 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-0153"
    }
  ],
  "Issued": {
    "Date": "2013-05-21"
  }
}