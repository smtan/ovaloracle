{
  "Title": "RHSA-2013:0685: perl security update (Moderate)",
  "Description": "Perl is a high-level programming language commonly used for system\nadministration utilities and web programming.\nA heap overflow flaw was found in Perl. If a Perl application allowed\nuser input to control the count argument of the string repeat operator, an\nattacker could cause the application to crash or, potentially, execute\narbitrary code with the privileges of the user running the application.\n(CVE-2012-5195)\nA denial of service flaw was found in the way Perl's rehashing code\nimplementation, responsible for recalculation of hash keys and\nredistribution of hash content, handled certain input. If an attacker\nsupplied specially-crafted input to be used as hash keys by a Perl\napplication, it could cause excessive memory consumption. (CVE-2013-1667)\nIt was found that the Perl CGI module, used to handle Common Gateway\nInterface requests and responses, incorrectly sanitized the values for\nSet-Cookie and P3P headers. If a Perl application using the CGI module\nreused cookies values and accepted untrusted input from web browsers, a\nremote attacker could use this flaw to alter member items of the cookie or\nadd new items. (CVE-2012-5526)\nIt was found that the Perl Locale::Maketext module, used to localize Perl\napplications, did not properly handle backslashes or fully-qualified method\nnames. An attacker could possibly use this flaw to execute arbitrary Perl\ncode with the privileges of a Perl application that uses untrusted\nLocale::Maketext templates. (CVE-2012-6329)\nRed Hat would like to thank the Perl project for reporting CVE-2012-5195\nand CVE-2013-1667. Upstream acknowledges Tim Brown as the original\nreporter of CVE-2012-5195 and Yves Orton as the original reporter of\nCVE-2013-1667.\nAll Perl users should upgrade to these updated packages, which contain\nbackported patches to correct these issues. All running Perl programs\nmust be restarted for this update to take effect.",
  "Platform": [
    "Red Hat Enterprise Linux 5",
    "Red Hat Enterprise Linux 6"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2013:0685",
      "ID": "RHSA-2013:0685"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-5195",
      "ID": "CVE-2012-5195"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-5526",
      "ID": "CVE-2012-5526"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-6329",
      "ID": "CVE-2012-6329"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-1667",
      "ID": "CVE-2013-1667"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl is earlier than 4:5.8.8-40.el5_9"
                  },
                  {
                    "Comment": "perl is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-suidperl is earlier than 4:5.8.8-40.el5_9"
                  },
                  {
                    "Comment": "perl-suidperl is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 5 is installed"
          }
        ]
      },
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl is earlier than 4:5.10.1-130.el6_4"
                  },
                  {
                    "Comment": "perl is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-Archive-Extract is earlier than 1:0.38-130.el6_4"
                  },
                  {
                    "Comment": "perl-Archive-Extract is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-Archive-Tar is earlier than 0:1.58-130.el6_4"
                  },
                  {
                    "Comment": "perl-Archive-Tar is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-CGI is earlier than 0:3.51-130.el6_4"
                  },
                  {
                    "Comment": "perl-CGI is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-CPAN is earlier than 0:1.9402-130.el6_4"
                  },
                  {
                    "Comment": "perl-CPAN is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-CPANPLUS is earlier than 0:0.88-130.el6_4"
                  },
                  {
                    "Comment": "perl-CPANPLUS is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-Compress-Raw-Bzip2 is earlier than 0:2.020-130.el6_4"
                  },
                  {
                    "Comment": "perl-Compress-Raw-Bzip2 is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-Compress-Raw-Zlib is earlier than 1:2.020-130.el6_4"
                  },
                  {
                    "Comment": "perl-Compress-Raw-Zlib is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-Compress-Zlib is earlier than 0:2.020-130.el6_4"
                  },
                  {
                    "Comment": "perl-Compress-Zlib is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-Digest-SHA is earlier than 1:5.47-130.el6_4"
                  },
                  {
                    "Comment": "perl-Digest-SHA is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-ExtUtils-CBuilder is earlier than 1:0.27-130.el6_4"
                  },
                  {
                    "Comment": "perl-ExtUtils-CBuilder is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-ExtUtils-Embed is earlier than 0:1.28-130.el6_4"
                  },
                  {
                    "Comment": "perl-ExtUtils-Embed is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-ExtUtils-MakeMaker is earlier than 0:6.55-130.el6_4"
                  },
                  {
                    "Comment": "perl-ExtUtils-MakeMaker is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-ExtUtils-ParseXS is earlier than 1:2.2003.0-130.el6_4"
                  },
                  {
                    "Comment": "perl-ExtUtils-ParseXS is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-File-Fetch is earlier than 0:0.26-130.el6_4"
                  },
                  {
                    "Comment": "perl-File-Fetch is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-IO-Compress-Base is earlier than 0:2.020-130.el6_4"
                  },
                  {
                    "Comment": "perl-IO-Compress-Base is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-IO-Compress-Bzip2 is earlier than 0:2.020-130.el6_4"
                  },
                  {
                    "Comment": "perl-IO-Compress-Bzip2 is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-IO-Compress-Zlib is earlier than 0:2.020-130.el6_4"
                  },
                  {
                    "Comment": "perl-IO-Compress-Zlib is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-IO-Zlib is earlier than 1:1.09-130.el6_4"
                  },
                  {
                    "Comment": "perl-IO-Zlib is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-IPC-Cmd is earlier than 1:0.56-130.el6_4"
                  },
                  {
                    "Comment": "perl-IPC-Cmd is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-Locale-Maketext-Simple is earlier than 1:0.18-130.el6_4"
                  },
                  {
                    "Comment": "perl-Locale-Maketext-Simple is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-Log-Message is earlier than 1:0.02-130.el6_4"
                  },
                  {
                    "Comment": "perl-Log-Message is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-Log-Message-Simple is earlier than 0:0.04-130.el6_4"
                  },
                  {
                    "Comment": "perl-Log-Message-Simple is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-Module-Build is earlier than 1:0.3500-130.el6_4"
                  },
                  {
                    "Comment": "perl-Module-Build is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-Module-CoreList is earlier than 0:2.18-130.el6_4"
                  },
                  {
                    "Comment": "perl-Module-CoreList is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-Module-Load is earlier than 1:0.16-130.el6_4"
                  },
                  {
                    "Comment": "perl-Module-Load is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-Module-Load-Conditional is earlier than 0:0.30-130.el6_4"
                  },
                  {
                    "Comment": "perl-Module-Load-Conditional is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-Module-Loaded is earlier than 1:0.02-130.el6_4"
                  },
                  {
                    "Comment": "perl-Module-Loaded is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-Module-Pluggable is earlier than 1:3.90-130.el6_4"
                  },
                  {
                    "Comment": "perl-Module-Pluggable is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-Object-Accessor is earlier than 1:0.34-130.el6_4"
                  },
                  {
                    "Comment": "perl-Object-Accessor is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-Package-Constants is earlier than 1:0.02-130.el6_4"
                  },
                  {
                    "Comment": "perl-Package-Constants is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-Params-Check is earlier than 1:0.26-130.el6_4"
                  },
                  {
                    "Comment": "perl-Params-Check is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-Parse-CPAN-Meta is earlier than 1:1.40-130.el6_4"
                  },
                  {
                    "Comment": "perl-Parse-CPAN-Meta is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-Pod-Escapes is earlier than 1:1.04-130.el6_4"
                  },
                  {
                    "Comment": "perl-Pod-Escapes is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-Pod-Simple is earlier than 1:3.13-130.el6_4"
                  },
                  {
                    "Comment": "perl-Pod-Simple is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-Term-UI is earlier than 0:0.20-130.el6_4"
                  },
                  {
                    "Comment": "perl-Term-UI is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-Test-Harness is earlier than 0:3.17-130.el6_4"
                  },
                  {
                    "Comment": "perl-Test-Harness is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-Test-Simple is earlier than 0:0.92-130.el6_4"
                  },
                  {
                    "Comment": "perl-Test-Simple is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-Time-HiRes is earlier than 4:1.9721-130.el6_4"
                  },
                  {
                    "Comment": "perl-Time-HiRes is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-Time-Piece is earlier than 0:1.15-130.el6_4"
                  },
                  {
                    "Comment": "perl-Time-Piece is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-core is earlier than 0:5.10.1-130.el6_4"
                  },
                  {
                    "Comment": "perl-core is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-devel is earlier than 4:5.10.1-130.el6_4"
                  },
                  {
                    "Comment": "perl-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-libs is earlier than 4:5.10.1-130.el6_4"
                  },
                  {
                    "Comment": "perl-libs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-parent is earlier than 1:0.221-130.el6_4"
                  },
                  {
                    "Comment": "perl-parent is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-suidperl is earlier than 4:5.10.1-130.el6_4"
                  },
                  {
                    "Comment": "perl-suidperl is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perl-version is earlier than 3:0.77-130.el6_4"
                  },
                  {
                    "Comment": "perl-version is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 5 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-5195"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-5526"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-6329"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-1667"
    }
  ],
  "Issued": {
    "Date": "2013-03-26"
  }
}