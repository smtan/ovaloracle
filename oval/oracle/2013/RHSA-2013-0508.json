{
  "Title": "RHSA-2013:0508: sssd security, bug fix and enhancement update (Low)",
  "Description": "The System Security Services Daemon (SSSD) provides a set of daemons to\nmanage access to remote directories and authentication mechanisms. It\nprovides an NSS and PAM interface toward the system and a pluggable\nback-end system to connect to multiple different account sources. It is\nalso the basis to provide client auditing and policy services for projects\nsuch as FreeIPA.\nA race condition was found in the way SSSD copied and removed user home\ndirectories. A local attacker who is able to write into the home directory\nof a different user who is being removed could use this flaw to perform\nsymbolic link attacks, possibly allowing them to modify and delete\narbitrary files with the privileges of the root user. (CVE-2013-0219)\nMultiple out-of-bounds memory read flaws were found in the way the autofs\nand SSH service responders parsed certain SSSD packets. An attacker could\nspend a specially-crafted packet that, when processed by the autofs or SSH\nservice responders, would cause SSSD to crash. This issue only caused a\ntemporary denial of service, as SSSD was automatically restarted by the\nmonitor process after the crash. (CVE-2013-0220)\nThe CVE-2013-0219 and CVE-2013-0220 issues were discovered by Florian\nWeimer of the Red Hat Product Security Team.\nThese updated sssd packages also include numerous bug fixes and\nenhancements. Space precludes documenting all of these changes in this\nadvisory. Users are directed to the Red Hat Enterprise Linux 6.4 Technical\nNotes, linked to in the References, for information on the most significant\nof these changes.\nAll SSSD users are advised to upgrade to these updated packages, which\nupgrade SSSD to upstream version 1.9 to correct these issues, fix these\nbugs and add these enhancements.",
  "Platform": [
    "Red Hat Enterprise Linux 6"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2013:0508",
      "ID": "RHSA-2013:0508"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-0219",
      "ID": "CVE-2013-0219"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-0220",
      "ID": "CVE-2013-0220"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libipa_hbac is earlier than 0:1.9.2-82.el6"
                  },
                  {
                    "Comment": "libipa_hbac is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libipa_hbac-devel is earlier than 0:1.9.2-82.el6"
                  },
                  {
                    "Comment": "libipa_hbac-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libipa_hbac-python is earlier than 0:1.9.2-82.el6"
                  },
                  {
                    "Comment": "libipa_hbac-python is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libsss_autofs is earlier than 0:1.9.2-82.el6"
                  },
                  {
                    "Comment": "libsss_autofs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libsss_idmap is earlier than 0:1.9.2-82.el6"
                  },
                  {
                    "Comment": "libsss_idmap is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libsss_idmap-devel is earlier than 0:1.9.2-82.el6"
                  },
                  {
                    "Comment": "libsss_idmap-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libsss_sudo is earlier than 0:1.9.2-82.el6"
                  },
                  {
                    "Comment": "libsss_sudo is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libsss_sudo-devel is earlier than 0:1.9.2-82.el6"
                  },
                  {
                    "Comment": "libsss_sudo-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "sssd is earlier than 0:1.9.2-82.el6"
                  },
                  {
                    "Comment": "sssd is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "sssd-client is earlier than 0:1.9.2-82.el6"
                  },
                  {
                    "Comment": "sssd-client is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "sssd-tools is earlier than 0:1.9.2-82.el6"
                  },
                  {
                    "Comment": "sssd-tools is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Low",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-0219"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-0220"
    }
  ],
  "Issued": {
    "Date": "2013-02-20"
  }
}