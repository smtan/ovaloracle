{
  "Title": "RHSA-2013:1449: kernel security and bug fix update (Moderate)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux\noperating system.\n* A flaw was found in the way the Linux kernel handled the creation of\ntemporary IPv6 addresses. If the IPv6 privacy extension was enabled\n(/proc/sys/net/ipv6/conf/eth0/use_tempaddr is set to '2'), an attacker on\nthe local network could disable IPv6 temporary address generation, leading\nto a potential information disclosure. (CVE-2013-0343, Moderate)\n* An information leak flaw was found in the way Linux kernel's device\nmapper subsystem, under certain conditions, interpreted data written to\nsnapshot block devices. An attacker could use this flaw to read data from\ndisk blocks in free space, which are normally inaccessible. (CVE-2013-4299,\nModerate)\n* An off-by-one flaw was found in the way the ANSI CPRNG implementation in\nthe Linux kernel processed non-block size aligned requests. This could lead\nto random numbers being generated with less bits of entropy than expected\nwhen ANSI CPRNG was used. (CVE-2013-4345, Moderate)\n* An information leak flaw was found in the way Xen hypervisor emulated the\nOUTS instruction for 64-bit paravirtualized guests. A privileged guest user\ncould use this flaw to leak hypervisor stack memory to the guest.\n(CVE-2013-4368, Moderate)\nRed Hat would like to thank Fujitsu for reporting CVE-2013-4299, Stephan\nMueller for reporting CVE-2013-4345, and the Xen project for reporting\nCVE-2013-4368.\nThis update also fixes the following bug:\n* A bug in the GFS2 code prevented glock work queues from freeing\nglock-related memory while the glock memory shrinker repeatedly queued a\nlarge number of demote requests, for example when performing a simultaneous\nbackup of several live GFS2 volumes with a large file count. As a\nconsequence, the glock work queues became overloaded which resulted in a\nhigh CPU usage and the GFS2 file systems being unresponsive for a\nsignificant amount of time. A patch has been applied to alleviate this\nproblem by calling the yield() function after scheduling a certain amount\nof tasks on the glock work queues. The problem can now occur only with\nextremely high work loads. (BZ#1014714)\nAll kernel users are advised to upgrade to these updated packages, which\ncontain backported patches to correct these issues. The system must be\nrebooted for this update to take effect.",
  "Platform": [
    "Red Hat Enterprise Linux 5"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2013:1449",
      "ID": "RHSA-2013:1449"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-0343",
      "ID": "CVE-2013-0343"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-4299",
      "ID": "CVE-2013-4299"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-4345",
      "ID": "CVE-2013-4345"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2013-4368",
      "ID": "CVE-2013-4368"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.18-371.1.2.el5 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.18-371.1.2.el5 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.18-371.1.2.el5"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-PAE is earlier than 0:2.6.18-371.1.2.el5"
                  },
                  {
                    "Comment": "kernel-PAE is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-PAE-devel is earlier than 0:2.6.18-371.1.2.el5"
                  },
                  {
                    "Comment": "kernel-PAE-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:2.6.18-371.1.2.el5"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:2.6.18-371.1.2.el5"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.18-371.1.2.el5"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.18-371.1.2.el5"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:2.6.18-371.1.2.el5"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:2.6.18-371.1.2.el5"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:2.6.18-371.1.2.el5"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xen is earlier than 0:2.6.18-371.1.2.el5"
                  },
                  {
                    "Comment": "kernel-xen is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xen-devel is earlier than 0:2.6.18-371.1.2.el5"
                  },
                  {
                    "Comment": "kernel-xen-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 5 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 5 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-0343"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-4299"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-4345"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2013-4368"
    }
  ],
  "Issued": {
    "Date": "2013-10-22"
  }
}