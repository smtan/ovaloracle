{
  "Title": "RHSA-2008:0237: kernel security and bug fix update (Important)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux\noperating system.\nThese updated packages fix the following security issues:\n* the absence of a protection mechanism when attempting to access a\ncritical section of code has been found in the Linux kernel open file\ndescriptors control mechanism, fcntl. This could allow a local unprivileged\nuser to simultaneously execute code, which would otherwise be protected\nagainst parallel execution. As well, a race condition when handling locks\nin the Linux kernel fcntl functionality, may have allowed a process\nbelonging to a local unprivileged user to gain re-ordered access to the\ndescriptor table. (CVE-2008-1669, Important)\n* on AMD64 architectures, the possibility of a kernel crash was discovered\nby testing the Linux kernel process-trace ability. This could allow a local\nunprivileged user to cause a denial of service (kernel crash).\n(CVE-2008-1615, Important)\n* the absence of a protection mechanism when attempting to access a\ncritical section of code, as well as a race condition, have been found\nin the Linux kernel file system event notifier, dnotify. This could allow a\nlocal unprivileged user to get inconsistent data, or to send arbitrary\nsignals to arbitrary system processes. (CVE-2008-1375, Important)\nRed Hat would like to thank Nick Piggin for responsibly disclosing the\nfollowing issue:\n* when accessing kernel memory locations, certain Linux kernel drivers\nregistering a fault handler did not perform required range checks. A local\nunprivileged user could use this flaw to gain read or write access to\narbitrary kernel memory, or possibly cause a kernel crash.\n(CVE-2008-0007, Important)\n* the possibility of a kernel crash was found in the Linux kernel IPsec\nprotocol implementation, due to improper handling of fragmented ESP\npackets. When an attacker controlling an intermediate router fragmented\nthese packets into very small pieces, it would cause a kernel crash on the\nreceiving node during packet reassembly. (CVE-2007-6282, Important)\n* a flaw in the MOXA serial driver could allow a local unprivileged user\nto perform privileged operations, such as replacing firmware.\n(CVE-2005-0504, Important)\nAs well, these updated packages fix the following bugs:\n* multiple buffer overflows in the neofb driver have been resolved. It was\nnot possible for an unprivileged user to exploit these issues, and as such,\nthey have not been handled as security issues.\n* a kernel panic, due to inconsistent detection of AGP aperture size, has\nbeen resolved.\n* a race condition in UNIX domain sockets may have caused \"recv()\" to\nreturn zero. In clustered configurations, this may have caused unexpected\nfailovers.\n* to prevent link storms, network link carrier events were delayed by up to\none second, causing unnecessary packet loss. Now, link carrier events are\nscheduled immediately.\n* a client-side race on blocking locks caused large time delays on NFS file\nsystems.\n* in certain situations, the libATA sata_nv driver may have sent commands\nwith duplicate tags, which were rejected by SATA devices. This may have\ncaused infinite reboots.\n* running the \"service network restart\" command may have caused networking\nto fail.\n* a bug in NFS caused cached information about directories to be stored\nfor too long, causing wrong attributes to be read.\n* on systems with a large highmem/lowmem ratio, NFS write performance may\nhave been very slow when using small files.\n* a bug, which caused network hangs when the system clock was wrapped\naround zero, has been resolved.\nRed Hat Enterprise Linux 4 users are advised to upgrade to these updated\npackages, which contain backported patches to resolve these issues.",
  "Platform": [
    "Red Hat Enterprise Linux 4"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2008:0237",
      "ID": "RHSA-2008:0237"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2005-0504",
      "ID": "CVE-2005-0504"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2007-6282",
      "ID": "CVE-2007-6282"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-0007",
      "ID": "CVE-2008-0007"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-1375",
      "ID": "CVE-2008-1375"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-1615",
      "ID": "CVE-2008-1615"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-1669",
      "ID": "CVE-2008-1669"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.9-67.0.15.EL is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.9-67.0.15.EL is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.9-67.0.15.EL"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat master key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.9-67.0.15.EL"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat master key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.9-67.0.15.EL"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat master key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-hugemem is earlier than 0:2.6.9-67.0.15.EL"
                  },
                  {
                    "Comment": "kernel-hugemem is signed with Red Hat master key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-hugemem-devel is earlier than 0:2.6.9-67.0.15.EL"
                  },
                  {
                    "Comment": "kernel-hugemem-devel is signed with Red Hat master key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-largesmp is earlier than 0:2.6.9-67.0.15.EL"
                  },
                  {
                    "Comment": "kernel-largesmp is signed with Red Hat master key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-largesmp-devel is earlier than 0:2.6.9-67.0.15.EL"
                  },
                  {
                    "Comment": "kernel-largesmp-devel is signed with Red Hat master key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-smp is earlier than 0:2.6.9-67.0.15.EL"
                  },
                  {
                    "Comment": "kernel-smp is signed with Red Hat master key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-smp-devel is earlier than 0:2.6.9-67.0.15.EL"
                  },
                  {
                    "Comment": "kernel-smp-devel is signed with Red Hat master key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xenU is earlier than 0:2.6.9-67.0.15.EL"
                  },
                  {
                    "Comment": "kernel-xenU is signed with Red Hat master key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xenU-devel is earlier than 0:2.6.9-67.0.15.EL"
                  },
                  {
                    "Comment": "kernel-xenU-devel is signed with Red Hat master key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 4 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 4 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2005-0504"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2007-6282"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-0007"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-1375"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-1615"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-1669"
    }
  ],
  "Issued": {
    "Date": "2008-05-07"
  }
}