{
  "Title": "RHSA-2008:0665: Updated kernel packages for Red Hat Enterprise Linux 4.7 (Moderate)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux\noperating system.\nKernel Feature Support: \n* iostat displays I/O performance for partitions\n* I/O task accounting added to getrusage(), allowing comprehensive core\nstatistics\n* page cache pages count added to show_mem() output\n* tux O_ATOMICLOOKUP flag removed from the open() system call: replaced\nwith O_CLOEXEC\n* the kernel now exports process limit information to /proc/[PID]/limits\n* implement udp_poll() to reduce likelihood of false positives returned\nfrom select()\n* the TCP_RTO_MIN parameter can now be configured to a maximum of 3000\nmilliseconds. This is configured using \"ip route\"\n* update CIFS to version 1.50\nAdded Features:\n* nfs.enable_ino64 boot command line parameter: enable and disable 32-bit\ninode numbers when using NFS\n* tick \"divider\" kernel boot parameter: reduce CPU overhead, and increase\nefficiency at the cost of lowering timing accuracy\n* /proc/sys/vm/nfs-writeback-lowmem-only tunable parameter: resolve NFS\nread performance\n* /proc/sys/vm/write-mapped tunable option, allowing the option of faster\nNFS reads\n* support for Large Receive Offload as a networking module\n* core dump masking, allowing a core dump process to skip the shared memory\nsegments of a process\nVirtualization:\n* para-virtualized network and block device drivers, to increase\nfully-virtualized guest performance\n* support for more than three VNIF numbers per guest domain\nPlatform Support:\n* AMD ATI SB800 SATA controller, AMD ATI SB600 and SB700 40-pin IDE cable\n* 64-bit DMA support on AMD ATI SB700\n* PCI device IDs to support Intel ICH10\n* /dev/msr[0-n] device files\n* powernow-k8 as a module\n* SLB shadow buffer support for IBM POWER6 systems\n* support for CPU frequencies greater than 32-bit on IBM POWER5, IBM POWER6\n* floating point load and store handler for IBM POWER6\nAdded Drivers and Updates:\n* ixgbe 1.1.18, for the Intel 82598 10GB ethernet controller\n* bnx2x 1.40.22, for network adapters on the Broadcom 5710 chipset\n* dm-hp-sw 1.0.0, for HP Active/Standby\n* zfcp version and bug fixes\n* qdio to fix FCP/SCSI write I/O expiring on LPARs\n* cio bug fixes\n* eHEA latest upstream, and netdump and netconsole support\n* ipr driver support for dual SAS RAID controllers\n* correct CPU cache info and SATA support for Intel Tolapai\n* i5000_edac support for Intel 5000 chipsets\n* i3000_edac support for Intel 3000 and 3010 chipsets\n* add i2c_piix4 module on 64-bit systems to support AMD ATI SB600, 700\nand 800\n* i2c-i801 support for Intel Tolapai\n* qla4xxx: 5.01.01-d2 to 5.01.02-d4-rhel4.7-00\n* qla2xxx: 8.01.07-d4 to 8.01.07-d4-rhel4.7-02\n* cciss: 2.6.16 to 2.6.20\n* mptfusion: 3.02.99.00rh to 3.12.19.00rh\n* lpfc:0: 8.0.16.34 to 8.0.16.40\n* megaraid_sas: 00.00.03.13 to 00.00.03.18-rh1\n* stex: 3.0.0.1 to  3.6.0101.2\n* arcmsr: 1.20.00.13 to 1.20.00.15.rh4u7\n* aacraid: 1.1-5[2441] to 1.1.5[2455]\nMiscellaneous Updates:\n* OFED 1.3 support\n* wacom driver to add support for Cintiq 20WSX, Wacom Intuos3 12x19, 12x12\nand 4x6 tablets\n* sata_svw driver to support Broadcom HT-1100 chipsets\n* libata to un-blacklist Hitachi drives to enable NCQ\n* ide driver allows command line option to disable ide drivers\n* psmouse support for cortps protocol\nThese updated packages fix the following security issues:\n* NULL pointer access due to missing checks for terminal validity.\n(CVE-2008-2812, Moderate)\n* a security flaw was found in the Linux kernel Universal Disk Format file\nsystem. (CVE-2006-4145, Low)\nFor further details, refer to the latest Red Hat Enterprise Linux 4.7\nrelease notes: redhat.com/docs/manuals/enterprise",
  "Platform": [
    "Red Hat Enterprise Linux 4"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2008:0665",
      "ID": "RHSA-2008:0665"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2006-4145",
      "ID": "CVE-2006-4145"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-2812",
      "ID": "CVE-2008-2812"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.9-78.EL is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.9-78.EL is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.9-78.EL"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.9-78.EL"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.9-78.EL"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-hugemem is earlier than 0:2.6.9-78.EL"
                  },
                  {
                    "Comment": "kernel-hugemem is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-hugemem-devel is earlier than 0:2.6.9-78.EL"
                  },
                  {
                    "Comment": "kernel-hugemem-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-largesmp is earlier than 0:2.6.9-78.EL"
                  },
                  {
                    "Comment": "kernel-largesmp is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-largesmp-devel is earlier than 0:2.6.9-78.EL"
                  },
                  {
                    "Comment": "kernel-largesmp-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-smp is earlier than 0:2.6.9-78.EL"
                  },
                  {
                    "Comment": "kernel-smp is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-smp-devel is earlier than 0:2.6.9-78.EL"
                  },
                  {
                    "Comment": "kernel-smp-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xenU is earlier than 0:2.6.9-78.EL"
                  },
                  {
                    "Comment": "kernel-xenU is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xenU-devel is earlier than 0:2.6.9-78.EL"
                  },
                  {
                    "Comment": "kernel-xenU-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 4 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 4 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2006-4145"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-2812"
    }
  ],
  "Issued": {
    "Date": "2008-07-24"
  }
}