{
  "Title": "RHSA-2008:0957: kernel security and bug fix update (Important)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux\noperating system.\n* the Xen implementation did not prevent applications running in a\npara-virtualized guest from modifying CR4 TSC. This could cause a local\ndenial of service. (CVE-2007-5907, Important)\n* Tavis Ormandy reported missing boundary checks in the Virtual Dynamic\nShared Objects (vDSO) implementation. This could allow a local unprivileged\nuser to cause a denial of service or escalate privileges. (CVE-2008-3527,\nImportant)\n* the do_truncate() and generic_file_splice_write() functions did not clear\nthe setuid and setgid bits. This could allow a local unprivileged user to\nobtain access to privileged information. (CVE-2008-4210, CVE-2008-3833,\nImportant)\n* a flaw was found in the Linux kernel splice implementation. This could\ncause a local denial of service when there is a certain failure in the\nadd_to_page_cache_lru() function. (CVE-2008-4302, Important)\n* a flaw was found in the Linux kernel when running on AMD64 systems.\nDuring a context switch, EFLAGS were being neither saved nor restored. This\ncould allow a local unprivileged user to cause a denial of service.\n(CVE-2006-5755, Low)\n* a flaw was found in the Linux kernel virtual memory implementation. This\ncould allow a local unprivileged user to cause a denial of service.\n(CVE-2008-2372, Low)\n* an integer overflow was discovered in the Linux kernel Datagram\nCongestion Control Protocol (DCCP) implementation. This could allow a\nremote attacker to cause a denial of service. By default, remote DCCP is\nblocked by SELinux. (CVE-2008-3276, Low)\nIn addition, these updated packages fix the following bugs:\n* random32() seeding has been improved. \n* in a multi-core environment, a race between the QP async event-handler\nand the destro_qp() function could occur. This led to unpredictable results\nduring invalid memory access, which could lead to a kernel crash.\n* a format string was omitted in the call to the request_module() function.\n* a stack overflow caused by an infinite recursion bug in the binfmt_misc\nkernel module was corrected.\n* the ata_scsi_rbuf_get() and ata_scsi_rbuf_put() functions now check for\nscatterlist usage before calling kmap_atomic().\n* a sentinel NUL byte was added to the device_write() function to ensure\nthat lspace.name is NUL-terminated.\n* in the character device driver, a range_is_allowed() check was added to\nthe read_mem() and write_mem() functions. It was possible for an\nillegitimate application to bypass these checks, and access /dev/mem beyond\nthe 1M limit by calling mmap_mem() instead. Also, the parameters of\nrange_is_allowed() were changed to cleanly handle greater than 32-bits of\nphysical address on 32-bit architectures.\n* some of the newer Nehalem-based systems declare their CPU DSDT entries as\ntype \"Alias\". During boot, this caused an \"Error attaching device data\"\nmessage to be logged.\n* the evtchn event channel device lacked locks and memory barriers. This\nhas led to xenstore becoming unresponsive on the Itanium® architecture.\n* sending of gratuitous ARP packets in the Xen frontend network driver is\nnow delayed until the backend signals that its carrier status has been\nprocessed by the stack.\n* on forcedeth devices, whenever setting ethtool parameters for link speed,\nthe device could stop receiving interrupts.\n* the CIFS 'forcedirectio' option did not allow text to be appended to files.\n* the gettimeofday() function returned a backwards time on Intel® 64.\n* residual-count corrections during UNDERRUN handling were added to the\nqla2xxx driver.                                                   \n* the fix for a small quirk was removed for certain Adaptec controllers for\nwhich it caused problems.\n* the \"xm trigger init\" command caused a domain panic if a userland\napplication was running on a guest on the Intel® 64 architecture.\nUsers of kernel should upgrade to these updated packages, which contain\nbackported patches to correct these issues.",
  "Platform": [
    "Red Hat Enterprise Linux 5"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2008:0957",
      "ID": "RHSA-2008:0957"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2006-5755",
      "ID": "CVE-2006-5755"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2007-5907",
      "ID": "CVE-2007-5907"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-2372",
      "ID": "CVE-2008-2372"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-3276",
      "ID": "CVE-2008-3276"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-3527",
      "ID": "CVE-2008-3527"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-3833",
      "ID": "CVE-2008-3833"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-4210",
      "ID": "CVE-2008-4210"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-4302",
      "ID": "CVE-2008-4302"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.18-92.1.18.el5 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.18-92.1.18.el5 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.18-92.1.18.el5"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-PAE is earlier than 0:2.6.18-92.1.18.el5"
                  },
                  {
                    "Comment": "kernel-PAE is signed with Red Hat redhatrelease key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-PAE-devel is earlier than 0:2.6.18-92.1.18.el5"
                  },
                  {
                    "Comment": "kernel-PAE-devel is signed with Red Hat redhatrelease key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:2.6.18-92.1.18.el5"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:2.6.18-92.1.18.el5"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.18-92.1.18.el5"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.18-92.1.18.el5"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:2.6.18-92.1.18.el5"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:2.6.18-92.1.18.el5"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:2.6.18-92.1.18.el5"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xen is earlier than 0:2.6.18-92.1.18.el5"
                  },
                  {
                    "Comment": "kernel-xen is signed with Red Hat redhatrelease key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xen-devel is earlier than 0:2.6.18-92.1.18.el5"
                  },
                  {
                    "Comment": "kernel-xen-devel is signed with Red Hat redhatrelease key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 5 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 5 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2006-5755"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2007-5907"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-2372"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-3276"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-3527"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-3833"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-4210"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-4302"
    }
  ],
  "Issued": {
    "Date": "2008-11-04"
  }
}