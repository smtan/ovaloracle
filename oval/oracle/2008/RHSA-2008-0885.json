{
  "Title": "RHSA-2008:0885: kernel security and bug fix update (Important)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux\noperating system.\nSecurity fixes:\n* a missing capability check was found in the Linux kernel do_change_type\nroutine. This could allow a local unprivileged user to gain privileged\naccess or cause a denial of service. (CVE-2008-2931, Important)\n* a flaw was found in the Linux kernel Direct-IO implementation. This could\nallow a local unprivileged user to cause a denial of service.\n(CVE-2007-6716, Important)\n* Tobias Klein reported a missing check in the Linux kernel Open Sound\nSystem (OSS) implementation. This deficiency could lead to a possible\ninformation leak. (CVE-2008-3272, Moderate)\n* a deficiency was found in the Linux kernel virtual filesystem (VFS)\nimplementation. This could allow a local unprivileged user to attempt file\ncreation within deleted directories, possibly causing a denial of service.\n(CVE-2008-3275, Moderate)\n* a flaw was found in the Linux kernel tmpfs implementation. This could\nallow a local unprivileged user to read sensitive information from the\nkernel. (CVE-2007-6417, Moderate)\nBug fixes:\n* when copying a small IPoIB packet from the original skb it was received\nin to a new, smaller skb, all fields in the new skb were not initialized.\nThis may have caused a kernel oops.\n* previously, data may have been written beyond the end of an array,\ncausing memory corruption on certain systems, resulting in hypervisor\ncrashes during context switching.\n* a kernel crash may have occurred on heavily-used Samba servers after 24\nto 48 hours of use.\n* under heavy memory pressure, pages may have been swapped out from under\nthe SGI Altix XPMEM driver, causing silent data corruption in the kernel.\n* the ixgbe driver is untested, but support was advertised for the Intel\n82598 network card. If this card was present when the ixgbe driver was\nloaded, a NULL pointer dereference and a panic occurred.\n* on certain systems, if multiple InfiniBand queue pairs simultaneously\nfell into an error state, an overrun may have occurred, stopping traffic.\n* with bridging, when forward delay was set to zero, setting an interface\nto the forwarding state was delayed by one or possibly two timers,\ndepending on whether STP was enabled. This may have caused long delays in\nmoving an interface to the forwarding state. This issue caused packet loss\nwhen migrating virtual machines, preventing them from being migrated\nwithout interrupting applications.\n* on certain multinode systems, IPMI device nodes were created in reverse\norder of where they physically resided.\n* process hangs may have occurred while accessing application data files\nvia asynchronous direct I/O system calls.\n* on systems with heavy lock traffic, a possible deadlock may have caused\nanything requiring locks over NFS to stop, or be very slow. Errors such as\n\"lockd: server [IP] not responding, timed out\" were logged on client\nsystems.\n* unexpected removals of USB devices may have caused a NULL pointer\ndereference in kobject_get_path.\n* on Itanium-based systems, repeatedly creating and destroying Windows\nguests may have caused Dom0 to crash, due to the \"XENMEM_add_to_physmap\"\nhypercall, used by para-virtualized drivers on HVM, being SMP-unsafe.\n* when using an MD software RAID, crashes may have occurred when devices\nwere removed or changed while being iterated through. Correct locking is\nnow used.\n* break requests had no effect when using \"Serial Over Lan\" with the Intel\n82571 network card. This issue may have caused log in problems.\n* on Itanium-based systems, module_free() referred the first parameter\nbefore checking it was valid. This may have caused a kernel panic when\nexiting SystemTap.\nRed Hat Enterprise Linux 5 users are advised to upgrade to these updated\npackages, which contain backported patches to resolve these issues.",
  "Platform": [
    "Red Hat Enterprise Linux 5"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2008:0885",
      "ID": "RHSA-2008:0885"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2007-6417",
      "ID": "CVE-2007-6417"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2007-6716",
      "ID": "CVE-2007-6716"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-2931",
      "ID": "CVE-2008-2931"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-3272",
      "ID": "CVE-2008-3272"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-3275",
      "ID": "CVE-2008-3275"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.18-92.1.13.el5 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.18-92.1.13.el5 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.18-92.1.13.el5"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-PAE is earlier than 0:2.6.18-92.1.13.el5"
                  },
                  {
                    "Comment": "kernel-PAE is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-PAE-devel is earlier than 0:2.6.18-92.1.13.el5"
                  },
                  {
                    "Comment": "kernel-PAE-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:2.6.18-92.1.13.el5"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:2.6.18-92.1.13.el5"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.18-92.1.13.el5"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.18-92.1.13.el5"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:2.6.18-92.1.13.el5"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:2.6.18-92.1.13.el5"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:2.6.18-92.1.13.el5"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xen is earlier than 0:2.6.18-92.1.13.el5"
                  },
                  {
                    "Comment": "kernel-xen is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xen-devel is earlier than 0:2.6.18-92.1.13.el5"
                  },
                  {
                    "Comment": "kernel-xen-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 5 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 5 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2007-6417"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2007-6716"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-2931"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-3272"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-3275"
    }
  ],
  "Issued": {
    "Date": "2008-09-24"
  }
}