{
  "Title": "RHSA-2008:1017: kernel security and bug fix update (Important)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux\noperating system.\n* Olaf Kirch reported a flaw in the i915 kernel driver. This flaw could,\npotentially, lead to local privilege escalation. Note: the flaw only\naffects systems based on the Intel G33 Express Chipset and newer.\n(CVE-2008-3831, Important)\n* Miklos Szeredi reported a missing check for files opened with O_APPEND in\nthe sys_splice(). This could allow a local, unprivileged user to bypass the\nappend-only file restrictions. (CVE-2008-4554, Important)\n* a deficiency was found in the Linux kernel Stream Control Transmission\nProtocol (SCTP) implementation. This could lead to a possible denial of\nservice if one end of a SCTP connection did not support the AUTH extension.\n(CVE-2008-4576, Important)\nIn addition, these updated packages fix the following bugs:\n* on Itanium® systems, when a multithreaded program was traced using the\ncommand \"strace -f\", messages such as\nPANIC: attached pid 10740 exited \nPANIC: handle_group_exit: 10740 leader 10721\n...\nwill be displayed, and after which the trace would stop.  With these\nupdated packages, \"strace -f\" command no longer results in these error\nmessages, and strace terminates normally after tracing all threads.\n* on big-endian systems such as PowerPC, the getsockopt() function\nincorrectly returned 0 depending on the parameters passed to it when the\ntime to live (TTL) value equaled 255.\n* when using an NFSv4 file system, accessing the same file with two\nseparate processes simultaneously resulted in the NFS client process\nbecoming unresponsive.\n* on AMD64 and Intel® 64 hypervisor-enabled systems, when a syscall\ncorrectly returned '-1' in code compiled on Red Hat Enterprise Linux 5, the\nsame code, when run with the strace utility, would incorrectly return an\ninvalid return value. This has been fixed: on AMD64 and Intel® 64\nhypervisor-enabled systems, syscalls in compiled code return the same,\ncorrect values as syscalls run with strace.\n* on the Itanium® architecture, fully-virtualized guest domains created\nusing more than 64 GB of memory caused other guest domains not to receive\ninterrupts. This caused soft lockups on other guests. All guest domains are\nnow able to receive interrupts regardless of their allotted memory.\n* when user-space used SIGIO notification, which was not disabled before\nclosing a file descriptor and was then re-enabled in a different process,\nan attempt by the kernel to dereference a stale pointer led to a kernel\ncrash. With this fix, such a situation no longer causes a kernel crash.\n* modifications to certain pages made through a memory-mapped region could\nhave been lost in cases when the NFS client needed to invalidate the page\ncache for that particular memory-mapped file.\n* fully-virtualized Windows® guests became unresponsive due to the vIOSAPIC\ncomponent being multiprocessor-unsafe. With this fix, vIOSAPIC is\nmultiprocessor-safe and Windows guests do not become unresponsive.\n* on certain systems, keyboard controllers could not withstand continuous\nrequests to switch keyboard LEDs on or off. This resulted in some or all\nkey presses not being registered by the system.\n* on the Itanium® architecture, setting the \"vm.nr_hugepages\" sysctl\nparameter caused a kernel stack overflow resulting in a kernel panic, and\npossibly stack corruption. With this fix, setting vm.nr_hugepages works\ncorrectly.\n* hugepages allow the Linux kernel to utilize the multiple page size\ncapabilities of modern hardware architectures. In certain configurations,\nsystems with large amounts of memory could fail to allocate most of this\nmemory for hugepages even if it was free. This could result, for example,\nin database restart failures.\nUsers should upgrade to these updated packages, which contain backported\npatches to correct these issues.",
  "Platform": [
    "Red Hat Enterprise Linux 5"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2008:1017",
      "ID": "RHSA-2008:1017"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-3831",
      "ID": "CVE-2008-3831"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-4554",
      "ID": "CVE-2008-4554"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-4576",
      "ID": "CVE-2008-4576"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.18-92.1.22.el5 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.18-92.1.22.el5 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.18-92.1.22.el5"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-PAE is earlier than 0:2.6.18-92.1.22.el5"
                  },
                  {
                    "Comment": "kernel-PAE is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-PAE-devel is earlier than 0:2.6.18-92.1.22.el5"
                  },
                  {
                    "Comment": "kernel-PAE-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:2.6.18-92.1.22.el5"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:2.6.18-92.1.22.el5"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.18-92.1.22.el5"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.18-92.1.22.el5"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:2.6.18-92.1.22.el5"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:2.6.18-92.1.22.el5"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:2.6.18-92.1.22.el5"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xen is earlier than 0:2.6.18-92.1.22.el5"
                  },
                  {
                    "Comment": "kernel-xen is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xen-devel is earlier than 0:2.6.18-92.1.22.el5"
                  },
                  {
                    "Comment": "kernel-xen-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 5 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 5 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-3831"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-4554"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-4576"
    }
  ],
  "Issued": {
    "Date": "2008-12-16"
  }
}