{
  "Title": "RHSA-2008:0233: kernel security and bug fix update (Important)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux\noperating system.\nThese updated packages fix the following security issues:\n* the absence of a protection mechanism when attempting to access a\ncritical section of code has been found in the Linux kernel open file\ndescriptors control mechanism, fcntl. This could allow a local unprivileged\nuser to simultaneously execute code, which would otherwise be protected\nagainst parallel execution. As well, a race condition when handling locks\nin the Linux kernel fcntl functionality, may have allowed a process\nbelonging to a local unprivileged user to gain re-ordered access to the\ndescriptor table. (CVE-2008-1669, Important)\n* a possible hypervisor panic was found in the Linux kernel. A privileged\nuser of a fully virtualized guest could initiate a stress-test File\nTransfer Protocol (FTP) transfer between the guest and the hypervisor,\npossibly leading to hypervisor panic. (CVE-2008-1619, Important)\n* the absence of a protection mechanism when attempting to access a\ncritical section of code, as well as a race condition, have been found\nin the Linux kernel file system event notifier, dnotify. This could allow a\nlocal unprivileged user to get inconsistent data, or to send arbitrary\nsignals to arbitrary system processes. (CVE-2008-1375, Important)\nRed Hat would like to thank Nick Piggin for responsibly disclosing the\nfollowing issue:\n* when accessing kernel memory locations, certain Linux kernel drivers\nregistering a fault handler did not perform required range checks. A local\nunprivileged user could use this flaw to gain read or write access to\narbitrary kernel memory, or possibly cause a kernel crash.\n(CVE-2008-0007, Important)\n* the absence of sanity-checks was found in the hypervisor block backend\ndriver, when running 32-bit paravirtualized guests on a 64-bit host. The\nnumber of blocks to be processed per one request from guest to host, or\nvice-versa, was not checked for its maximum value, which could have allowed\na local privileged user of the guest operating system to cause a denial of\nservice. (CVE-2007-5498, Important)\n* it was discovered that the Linux kernel handled string operations in the\nopposite way to the GNU Compiler Collection (GCC). This could allow a local\nunprivileged user to cause memory corruption. (CVE-2008-1367, Low)\nAs well, these updated packages fix the following bugs:\n* on IBM System z architectures, when running QIOASSIST enabled QDIO\ndevices in an IBM z/VM environment, the output queue stalled under heavy\nload. This caused network performance to degrade, possibly causing network\nhangs and outages.\n* multiple buffer overflows were discovered in the neofb video driver. It\nwas not possible for an unprivileged user to exploit these issues, and as\nsuch, they have not been handled as security issues.\n* when running Microsoft Windows in a HVM, a bug in vmalloc/vfree caused\nnetwork performance to degrade.\n* on certain architectures, a bug in the libATA sata_nv driver may have\ncaused infinite reboots, and an \"ata1: CPB flags CMD err flags 0x11\" error.\n* repeatedly hot-plugging a PCI Express card may have caused \"Bad DLLP\"\nerrors.\n* a NULL pointer dereference in NFS, which may have caused applications to\ncrash, has been resolved.\n* when attempting to kexec reboot, either manually or via a panic-triggered\nkdump, the Unisys ES7000/one hanged after rebooting in the new kernel,\nafter printing the \"Memory: 32839688k/33685504k available\" line.\nRed Hat Enterprise Linux 5 users are advised to upgrade to these updated\npackages, which contain backported patches to resolve these issues.",
  "Platform": [
    "Red Hat Enterprise Linux 5"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2008:0233",
      "ID": "RHSA-2008:0233"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2007-5498",
      "ID": "CVE-2007-5498"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-0007",
      "ID": "CVE-2008-0007"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-1367",
      "ID": "CVE-2008-1367"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-1375",
      "ID": "CVE-2008-1375"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-1619",
      "ID": "CVE-2008-1619"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2008-1669",
      "ID": "CVE-2008-1669"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.18-53.1.19.el5 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.18-53.1.19.el5 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.18-53.1.19.el5"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-PAE is earlier than 0:2.6.18-53.1.19.el5"
                  },
                  {
                    "Comment": "kernel-PAE is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-PAE-devel is earlier than 0:2.6.18-53.1.19.el5"
                  },
                  {
                    "Comment": "kernel-PAE-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:2.6.18-53.1.19.el5"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:2.6.18-53.1.19.el5"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.18-53.1.19.el5"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.18-53.1.19.el5"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:2.6.18-53.1.19.el5"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:2.6.18-53.1.19.el5"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:2.6.18-53.1.19.el5"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xen is earlier than 0:2.6.18-53.1.19.el5"
                  },
                  {
                    "Comment": "kernel-xen is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xen-devel is earlier than 0:2.6.18-53.1.19.el5"
                  },
                  {
                    "Comment": "kernel-xen-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 5 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 5 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2007-5498"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-0007"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-1367"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-1375"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-1619"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2008-1669"
    }
  ],
  "Issued": {
    "Date": "2008-05-07"
  }
}