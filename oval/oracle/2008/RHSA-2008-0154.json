{
  "Title": "RHSA-2008:0154: kernel security and bug fix update (Important)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux\noperating system.\nThese updated packages fix the following security issues:\n* a flaw in the hypervisor for hosts running on Itanium architectures\nallowed an Intel VTi domain to read arbitrary physical memory from other\nIntel VTi domains, which could make information available to unauthorized\nusers. (CVE-2007-6207, Important)\n* two buffer overflow flaws were found in ISDN subsystem. A local\nunprivileged user could use these flaws to cause a denial of service.\n(CVE-2007-5938: Important, CVE-2007-6063: Moderate)\n* a possible NULL pointer dereference was found in the subsystem used for\nshowing CPU information, as used by CHRP systems on PowerPC architectures.\nThis may have allowed a local unprivileged user to cause a denial of\nservice (crash). (CVE-2007-6694, Moderate)\n* a flaw was found in the handling of zombie processes. A local user could\ncreate processes that would not be properly reaped, possibly causing a\ndenial of service. (CVE-2006-6921, Moderate)\nAs well, these updated packages fix the following bugs:\n* a bug was found in the Linux kernel audit subsystem. When the audit\ndaemon was setup to log the execve system call with a large number of\narguments, the kernel could run out of memory, causing a kernel panic.\n* on IBM System z architectures, using the IBM Hardware Management Console\nto toggle IBM FICON channel path ids (CHPID) caused a file ID miscompare,\npossibly causing data corruption.\n* when running the IA-32 Execution Layer (IA-32EL) or a Java VM on Itanium\narchitectures, a bug in the address translation in the hypervisor caused\nthe wrong address to be registered, causing Dom0 to hang.\n* on Itanium architectures, frequent Corrected Platform Error errors may\nhave caused the hypervisor to hang.\n* when enabling a CPU without hot plug support, routines for checking the\npresence of the CPU were missing. The CPU tried to access its own\nresources, causing a kernel panic.\n* after updating to kernel-2.6.18-53.el5, a bug in the CCISS driver caused\nthe HP Array Configuration Utility CLI to become unstable, possibly causing\na system hang, or a kernel panic.\n* a bug in NFS directory caching could have caused different hosts to have\ndifferent views of NFS directories.\n* on Itanium architectures, the Corrected Machine Check Interrupt masked\nhot-added CPUs as disabled.\n* when running Oracle database software on the Intel 64 and AMD64\narchitectures, if an SGA larger than 4GB was created, and had hugepages\nallocated to it, the hugepages were not freed after database shutdown.\n* in a clustered environment, when two or more NFS clients had the same\nlogical volume mounted, and one of them modified a file on the volume, NULL\ncharacters may have been inserted, possibly causing data corruption.\nThese updated packages resolve several severe issues in the lpfc driver:\n* a system hang after LUN discovery.\n* a general fault protection, a NULL pointer dereference, or slab\ncorruption could occur while running a debug on the kernel.\n* the inability to handle kernel paging requests in \"lpfc_get_scsi_buf\".\n* erroneous structure references caused certain FC discovery routines to\nreference and change \"lpfc_nodelist\" structures, even after they were\nfreed.\n* the lpfc driver failed to interpret certain fields correctly, causing\ntape backup software to fail. Tape drives reported \"Illegal Request\".\n* the lpfc driver did not clear structures correctly, resulting in SCSI\nI/Os being rejected by targets, and causing errors.\nRed Hat Enterprise Linux 5 users are advised to upgrade to these updated\npackages, which contain backported patches to resolve these issues.",
  "Platform": [
    "Red Hat Enterprise Linux 5"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2008:0154",
      "ID": "RHSA-2008:0154"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2006-6921",
      "ID": "CVE-2006-6921"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2007-5938",
      "ID": "CVE-2007-5938"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2007-6063",
      "ID": "CVE-2007-6063"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2007-6207",
      "ID": "CVE-2007-6207"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2007-6694",
      "ID": "CVE-2007-6694"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.18-53.1.14.el5 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.18-53.1.14.el5 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.18-53.1.14.el5"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-PAE is earlier than 0:2.6.18-53.1.14.el5"
                  },
                  {
                    "Comment": "kernel-PAE is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-PAE-devel is earlier than 0:2.6.18-53.1.14.el5"
                  },
                  {
                    "Comment": "kernel-PAE-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:2.6.18-53.1.14.el5"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:2.6.18-53.1.14.el5"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.18-53.1.14.el5"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.18-53.1.14.el5"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:2.6.18-53.1.14.el5"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:2.6.18-53.1.14.el5"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:2.6.18-53.1.14.el5"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xen is earlier than 0:2.6.18-53.1.14.el5"
                  },
                  {
                    "Comment": "kernel-xen is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xen-devel is earlier than 0:2.6.18-53.1.14.el5"
                  },
                  {
                    "Comment": "kernel-xen-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 5 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 5 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2006-6921"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2007-5938"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2007-6063"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2007-6207"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2007-6694"
    }
  ],
  "Issued": {
    "Date": "2008-03-05"
  }
}