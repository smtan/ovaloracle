{
  "Title": "RHSA-2012:0710: firefox security update (Critical)",
  "Description": "Mozilla Firefox is an open source web browser. XULRunner provides the XUL\nRuntime environment for Mozilla Firefox.\nSeveral flaws were found in the processing of malformed web content. A web\npage containing malicious content could cause Firefox to crash or,\npotentially, execute arbitrary code with the privileges of the user running\nFirefox. (CVE-2011-3101, CVE-2012-1937, CVE-2012-1938, CVE-2012-1939,\nCVE-2012-1940, CVE-2012-1941, CVE-2012-1946, CVE-2012-1947)\nNote: CVE-2011-3101 only affected users of certain NVIDIA display drivers\nwith graphics cards that have hardware acceleration enabled.\nIt was found that the Content Security Policy (CSP) implementation in\nFirefox no longer blocked Firefox inline event handlers. A remote attacker\ncould use this flaw to possibly bypass a web application's intended\nrestrictions, if that application relied on CSP to protect against flaws\nsuch as cross-site scripting (XSS). (CVE-2012-1944)\nIf a web server hosted HTML files that are stored on a Microsoft Windows\nshare, or a Samba share, loading such files with Firefox could result in\nWindows shortcut files (.lnk) in the same share also being loaded. An\nattacker could use this flaw to view the contents of local files and\ndirectories on the victim's system. This issue also affected users opening\nHTML files from Microsoft Windows shares, or Samba shares, that are mounted\non their systems. (CVE-2012-1945)\nFor technical details regarding these flaws, refer to the Mozilla security\nadvisories for Firefox 10.0.5 ESR. You can find a link to the Mozilla\nadvisories in the References section of this erratum.\nRed Hat would like to thank the Mozilla project for reporting these issues.\nUpstream acknowledges Ken Russell of Google as the original reporter of\nCVE-2011-3101; Igor Bukanov, Olli Pettay, Boris Zbarsky, and Jesse Ruderman\nas the original reporters of CVE-2012-1937; Jesse Ruderman, Igor Bukanov,\nBill McCloskey, Christian Holler, Andrew McCreight, and Brian Bondy as the\noriginal reporters of CVE-2012-1938; Christian Holler as the original\nreporter of CVE-2012-1939; security researcher Abhishek Arya of Google as\nthe original reporter of CVE-2012-1940, CVE-2012-1941, and CVE-2012-1947;\nsecurity researcher Arthur Gerkis as the original reporter of\nCVE-2012-1946; security researcher Adam Barth as the original reporter of\nCVE-2012-1944; and security researcher Paul Stone as the original reporter\nof CVE-2012-1945.\nAll Firefox users should upgrade to these updated packages, which contain\nFirefox version 10.0.5 ESR, which corrects these issues. After installing\nthe update, Firefox must be restarted for the changes to take effect.",
  "Platform": [
    "Red Hat Enterprise Linux 5",
    "Red Hat Enterprise Linux 6"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2012:0710",
      "ID": "RHSA-2012:0710"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-3101",
      "ID": "CVE-2011-3101"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-1937",
      "ID": "CVE-2012-1937"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-1938",
      "ID": "CVE-2012-1938"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-1939",
      "ID": "CVE-2012-1939"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-1940",
      "ID": "CVE-2012-1940"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-1941",
      "ID": "CVE-2012-1941"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-1944",
      "ID": "CVE-2012-1944"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-1945",
      "ID": "CVE-2012-1945"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-1946",
      "ID": "CVE-2012-1946"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-1947",
      "ID": "CVE-2012-1947"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-3105",
      "ID": "CVE-2012-3105"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "xulrunner is earlier than 0:10.0.5-1.el6_2"
                  },
                  {
                    "Comment": "xulrunner is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "xulrunner-devel is earlier than 0:10.0.5-1.el6_2"
                  },
                  {
                    "Comment": "xulrunner-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "firefox is earlier than 0:10.0.5-1.el6_2"
                  },
                  {
                    "Comment": "firefox is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      },
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "xulrunner is earlier than 0:10.0.5-1.el5_8"
                  },
                  {
                    "Comment": "xulrunner is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "xulrunner-devel is earlier than 0:10.0.5-1.el5_8"
                  },
                  {
                    "Comment": "xulrunner-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "firefox is earlier than 0:10.0.5-1.el5_8"
                  },
                  {
                    "Comment": "firefox is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 5 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 5 is installed"
      }
    ]
  },
  "Severity": "Critical",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-3101"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-1937"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-1938"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-1939"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-1940"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-1941"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-1944"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-1945"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-1946"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-1947"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-3105"
    }
  ],
  "Issued": {
    "Date": "2012-06-05"
  }
}