{
  "Title": "RHSA-2012:1061: kernel security and bug fix update (Moderate)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux\noperating system.\nSecurity fix:\n* The fix for CVE-2011-1083 (RHSA-2012:0150) introduced a flaw in the way\nthe Linux kernel's Event Poll (epoll) subsystem handled resource clean up\nwhen an ELOOP error code was returned. A local, unprivileged user could use\nthis flaw to cause a denial of service. (CVE-2012-3375, Moderate)\nBug fixes:\n* The qla2xxx driver handled interrupts for QLogic Fibre Channel adapters\nincorrectly due to a bug in a test condition for MSI-X support. This update\ncorrects the bug and qla2xxx now handles interrupts as expected.\n(BZ#816373)\n* A process scheduler did not handle RPC priority wait queues correctly.\nConsequently, the process scheduler failed to wake up all scheduled tasks\nas expected after RPC timeout, which caused the system to become\nunresponsive and could significantly decrease system performance. This\nupdate modifies the process scheduler to handle RPC priority wait queues as\nexpected. All scheduled tasks are now properly woken up after RPC timeout\nand the system behaves as expected. (BZ#817571)\n* The kernel version 2.6.18-308.4.1.el5 contained several bugs which led to\nan overrun of the NFS server page array. Consequently, any attempt to\nconnect an NFS client running on Red Hat Enterprise Linux 5.8 to the NFS\nserver running on the system with this kernel caused the NFS server to\nterminate unexpectedly and the kernel to panic. This update corrects the\nbugs causing NFS page array overruns and the kernel no longer crashes in\nthis scenario. (BZ#820358)\n* An insufficiently designed calculation in the CPU accelerator in the\nprevious kernel caused an arithmetic overflow in the sched_clock() function\nwhen system uptime exceeded 208.5 days. This overflow led to a kernel panic\non the systems using the Time Stamp Counter (TSC) or Virtual Machine\nInterface (VMI) clock source. This update corrects the calculation so that\nthis arithmetic overflow and kernel panic can no longer occur under these\ncircumstances.\nNote: This advisory does not include a fix for this bug for the 32-bit\narchitecture. (BZ#824654)\n* Under memory pressure, memory pages that are still a part of a\ncheckpointing transaction can be invalidated. However, when the pages were\ninvalidated, the journal head was re-filed onto the transactions' \"forget\"\nlist, which caused the current running transaction's block to be modified.\nAs a result, block accounting was not properly performed on that modified\nblock because it appeared to have already been modified due to the journal\nhead being re-filed. This could trigger an assertion failure in the\n\"journal_commit_transaction()\" function on the system. The \"b_modified\"\nflag is now cleared before the journal head is filed onto any transaction;\nassertion failures no longer occur. (BZ#827205)\n* When running more than 30 instances of the cclengine utility concurrently\non IBM System z with IBM Communications Controller for Linux, the system\ncould become unresponsive. This was caused by a missing wake_up() function\ncall in the qeth_release_buffer() function in the QETH network device\ndriver. This update adds the missing wake_up() function call and the system\nnow responds as expected in this scenario. (BZ#829059)\n* Recent changes removing support for the Flow Director from the ixgbe\ndriver introduced bugs that caused the RSS (Receive Side Scaling)\nfunctionality to stop working correctly on Intel 82599EB 10 Gigabit\nEthernet network devices. This update corrects the return code in the\nixgbe_cache_ring_fdir function and setting of the registers that control\nthe RSS redirection table. Also, obsolete code related to Flow Director\nsupport has been removed. The RSS functionality now works as expected on\nthese devices. (BZ#832169)\nUsers should upgrade to these updated packages, which contain backported\npatches to correct these issues. The system must be rebooted for this\nupdate to take effect.",
  "Platform": [
    "Red Hat Enterprise Linux 5"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2012:1061",
      "ID": "RHSA-2012:1061"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-3375",
      "ID": "CVE-2012-3375"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.18-308.11.1.el5 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.18-308.11.1.el5 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.18-308.11.1.el5"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-PAE is earlier than 0:2.6.18-308.11.1.el5"
                  },
                  {
                    "Comment": "kernel-PAE is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-PAE-devel is earlier than 0:2.6.18-308.11.1.el5"
                  },
                  {
                    "Comment": "kernel-PAE-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:2.6.18-308.11.1.el5"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:2.6.18-308.11.1.el5"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.18-308.11.1.el5"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.18-308.11.1.el5"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:2.6.18-308.11.1.el5"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:2.6.18-308.11.1.el5"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:2.6.18-308.11.1.el5"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xen is earlier than 0:2.6.18-308.11.1.el5"
                  },
                  {
                    "Comment": "kernel-xen is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xen-devel is earlier than 0:2.6.18-308.11.1.el5"
                  },
                  {
                    "Comment": "kernel-xen-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 5 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 5 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-3375"
    }
  ],
  "Issued": {
    "Date": "2012-07-10"
  }
}