{
  "Title": "RHSA-2012:0322: java-1.6.0-openjdk security update (Important)",
  "Description": "These packages provide the OpenJDK 6 Java Runtime Environment and the\nOpenJDK 6 Software Development Kit.\nIt was discovered that Java2D did not properly check graphics rendering\nobjects before passing them to the native renderer. Malicious input, or an\nuntrusted Java application or applet could use this flaw to crash the Java\nVirtual Machine (JVM), or bypass Java sandbox restrictions. (CVE-2012-0497)\nIt was discovered that the exception thrown on deserialization failure did\nnot always contain a proper identification of the cause of the failure. An\nuntrusted Java application or applet could use this flaw to bypass Java\nsandbox restrictions. (CVE-2012-0505)\nThe AtomicReferenceArray class implementation did not properly check if\nthe array was of the expected Object[] type. A malicious Java application\nor applet could use this flaw to bypass Java sandbox restrictions.\n(CVE-2011-3571)\nIt was discovered that the use of TimeZone.setDefault() was not restricted\nby the SecurityManager, allowing an untrusted Java application or applet to\nset a new default time zone, and hence bypass Java sandbox restrictions.\n(CVE-2012-0503)\nThe HttpServer class did not limit the number of headers read from HTTP\nrequests. A remote attacker could use this flaw to make an application\nusing HttpServer use an excessive amount of CPU time via a\nspecially-crafted request. This update introduces a header count limit\ncontrolled using the sun.net.httpserver.maxReqHeaders property. The default\nvalue is 200. (CVE-2011-5035)\nThe Java Sound component did not properly check buffer boundaries.\nMalicious input, or an untrusted Java application or applet could use this\nflaw to cause the Java Virtual Machine (JVM) to crash or disclose a portion\nof its memory. (CVE-2011-3563)\nA flaw was found in the AWT KeyboardFocusManager that could allow an\nuntrusted Java application or applet to acquire keyboard focus and possibly\nsteal sensitive information. (CVE-2012-0502)\nIt was discovered that the CORBA (Common Object Request Broker\nArchitecture) implementation in Java did not properly protect repository\nidentifiers on certain CORBA objects. This could have been used to modify\nimmutable object data. (CVE-2012-0506)\nAn off-by-one flaw, causing a stack overflow, was found in the unpacker for\nZIP files. A specially-crafted ZIP archive could cause the Java Virtual\nMachine (JVM) to crash when opened. (CVE-2012-0501)\nThis erratum also upgrades the OpenJDK package to IcedTea6 1.10.6. Refer to\nthe NEWS file, linked to in the References, for further information.\nAll users of java-1.6.0-openjdk are advised to upgrade to these updated\npackages, which resolve these issues. All running instances of OpenJDK Java\nmust be restarted for the update to take effect.",
  "Platform": [
    "Red Hat Enterprise Linux 5"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2012:0322",
      "ID": "RHSA-2012:0322"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-3563",
      "ID": "CVE-2011-3563"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-3571",
      "ID": "CVE-2011-3571"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-5035",
      "ID": "CVE-2011-5035"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-0497",
      "ID": "CVE-2012-0497"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-0501",
      "ID": "CVE-2012-0501"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-0502",
      "ID": "CVE-2012-0502"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-0503",
      "ID": "CVE-2012-0503"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-0505",
      "ID": "CVE-2012-0505"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-0506",
      "ID": "CVE-2012-0506"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-0507",
      "ID": "CVE-2012-0507"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk is earlier than 1:1.6.0.0-1.25.1.10.6.el5_8"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk-demo is earlier than 1:1.6.0.0-1.25.1.10.6.el5_8"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk-demo is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk-devel is earlier than 1:1.6.0.0-1.25.1.10.6.el5_8"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk-javadoc is earlier than 1:1.6.0.0-1.25.1.10.6.el5_8"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk-javadoc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.6.0-openjdk-src is earlier than 1:1.6.0.0-1.25.1.10.6.el5_8"
                  },
                  {
                    "Comment": "java-1.6.0-openjdk-src is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 5 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 5 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-3563"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-3571"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-5035"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-0497"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-0501"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-0502"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-0503"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-0505"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-0506"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-0507"
    }
  ],
  "Issued": {
    "Date": "2012-02-21"
  }
}