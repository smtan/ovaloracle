{
  "Title": "RHBA-2012:0881: freeradius bug fix and enhancement update (Low)",
  "Description": "FreeRADIUS is an open-source Remote Authentication Dial In User Service (RADIUS) server which allows RADIUS clients to perform authentication against the RADIUS server. The RADIUS server may optionally perform accounting of its operations using the RADIUS protocol.\nThe freeradius packages have been upgraded to upstream version 2.1.12, which provides a number of bug fixes and enhancements over the previous version. (BZ#736878)\nThis update fixes the following bugs:\n* The radtest command-line argument to request the PPP hint option was not parsed correctly. Consequently, radclient did not add the PPP hint to the request packet and the test failed. This update corrects the problem and radtest now functions as expected. (BZ#787116)\n* After log rotation, the freeradius logrotate script failed to reload the radiusd daemon after a log rotation and log messages were lost. This update has added a command to the freeradius logrotate script to reload the radiusd daemon and the radiusd daemon reinitializes and reopens its log files after log rotation as expected. (BZ#705723)\n* The radtest argument with the eap-md5 option failed because it passed the IP family argument when invoking the radeapclient utility and the radeapclient utility did not recognize the IP family. The radeapclient now recognizes the IP family argument and radtest now works with eap-md5 as expected. (BZ#712803)\n* Previously, freeradius was compiled without the \"--with-udpfromto\" option. Consequently, with a multihomed server and explicitly specifying the IP address, freeradius sent the reply from the wrong IP address. With this update, freeradius has been built with the --with-udpfromto configuration option and the RADIUS reply is always sourced from the IP the request was sent to. (BZ#700870)\n* The password expiration field for local passwords was not checked by the unix module and the debug information was erroneous. Consequently, a user with an expired password in the local password file was authenticated despite having an expired password. With this update, check of the password expiration has been modified. A user with an expired local password is denied access and correct debugging information is written to the log file. (BZ#753764)\n* Due to invalid syntax in the PostgreSQL admin schema file, the FreeRADIUS PostgreSQL tables failed to be created. With this update, the syntax has been adjusted and the tables are created as expected. (BZ#690756)\n* When FreeRADIUS received a request, it sometimes failed with the following message:\nWARNING: Internal sanity check failed in event handler for request 6\nThis bug was fixed by upgrading to upstream version 2.1.12. (BZ#782905)\n* FreeRADIUS has a thread pool that will dynamically grow based on load.  If multiple threads using the rlm_perl() function are spawned in quick succession, freeradius sometimes terminated unexpectedly with a segmentation fault due to parallel calls to the rlm_perl_clone() function. With this update, mutex for the threads has been added and the problem no longer occurs. (BZ#810605)\nAll users of freeradius are advised to upgrade to these updated packages, which fix these bugs and add these enhancements.",
  "Platform": [
    "Red Hat Enterprise Linux 6"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHBA-2012:0881",
      "ID": "RHBA-2012:0881"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-4966",
      "ID": "CVE-2011-4966"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "freeradius is earlier than 0:2.1.12-3.el6"
                  },
                  {
                    "Comment": "freeradius is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "freeradius-krb5 is earlier than 0:2.1.12-3.el6"
                  },
                  {
                    "Comment": "freeradius-krb5 is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "freeradius-ldap is earlier than 0:2.1.12-3.el6"
                  },
                  {
                    "Comment": "freeradius-ldap is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "freeradius-mysql is earlier than 0:2.1.12-3.el6"
                  },
                  {
                    "Comment": "freeradius-mysql is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "freeradius-perl is earlier than 0:2.1.12-3.el6"
                  },
                  {
                    "Comment": "freeradius-perl is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "freeradius-postgresql is earlier than 0:2.1.12-3.el6"
                  },
                  {
                    "Comment": "freeradius-postgresql is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "freeradius-python is earlier than 0:2.1.12-3.el6"
                  },
                  {
                    "Comment": "freeradius-python is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "freeradius-unixODBC is earlier than 0:2.1.12-3.el6"
                  },
                  {
                    "Comment": "freeradius-unixODBC is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "freeradius-utils is earlier than 0:2.1.12-3.el6"
                  },
                  {
                    "Comment": "freeradius-utils is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Low",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-4966"
    }
  ],
  "Issued": {
    "Date": "2012-06-20"
  }
}