{
  "Title": "RHSA-2012:0902: cifs-utils security, bug fix, and enhancement update (Low)",
  "Description": "The cifs-utils package contains tools for mounting and managing shares on\nLinux using the SMB/CIFS protocol. The CIFS shares can be used as standard\nLinux file systems.\nA file existence disclosure flaw was found in mount.cifs. If the tool was\ninstalled with the setuid bit set, a local attacker could use this flaw to\ndetermine the existence of files or directories in directories not\naccessible to the attacker. (CVE-2012-1586)\nNote: mount.cifs from the cifs-utils package distributed by Red Hat does\nnot have the setuid bit set. We recommend that administrators do not\nmanually set the setuid bit for mount.cifs.\nThis update also fixes the following bugs:\n* The cifs.mount(8) manual page was previously missing documentation for\nseveral mount options. With this update, the missing entries have been\nadded to the manual page. (BZ#769923)\n* Previously, the mount.cifs utility did not properly update the\n\"/etc/mtab\" system information file when remounting an existing CIFS\nmount. Consequently, mount.cifs created a duplicate entry of the existing\nmount entry. This update adds the del_mtab() function to cifs.mount, which\nensures that the old mount entry is removed from \"/etc/mtab\" before adding\nthe updated mount entry. (BZ#770004)\n* The mount.cifs utility did not properly convert user and group names to\nnumeric UIDs and GIDs. Therefore, when the \"uid\", \"gid\" or \"cruid\" mount\noptions were specified with user or group names, CIFS shares were mounted\nwith default values. This caused shares to be inaccessible to the intended\nusers because UID and GID is set to \"0\" by default. With this update, user\nand group names are properly converted so that CIFS shares are now mounted\nwith specified user and group ownership as expected. (BZ#796463)\n* The cifs.upcall utility did not respect the \"domain_realm\" section in\nthe \"krb5.conf\" file and worked only with the default domain.\nConsequently, an attempt to mount a CIFS share from a different than the\ndefault domain failed with the following error message:\nmount error(126): Required key not available\nThis update modifies the underlying code so that cifs.upcall handles\nmultiple Kerberos domains correctly and CIFS shares can now be mounted as\nexpected in a multi-domain environment. (BZ#805490)\nIn addition, this update adds the following enhancements:\n* The cifs.upcall utility previously always used the \"/etc/krb5.conf\" file\nregardless of whether the user had specified a custom Kerberos\nconfiguration file. This update adds the \"--krb5conf\" option to\ncifs.upcall allowing the administrator to specify an alternate\nkrb5.conf file. For more information on this option, refer to the\ncifs.upcall(8) manual page. (BZ#748756)\n* The cifs.upcall utility did not optimally determine the correct service\nprincipal name (SPN) used for Kerberos authentication, which occasionally\ncaused krb5 authentication to fail when mounting a server's unqualified\ndomain name. This update improves cifs.upcall so that the method used to\ndetermine the SPN is now more versatile. (BZ#748757)\n* This update adds the \"backupuid\" and \"backupgid\" mount options to the\nmount.cifs utility. When specified, these options grant a user or a group\nthe right to access files with the backup intent. For more information on\nthese options, refer to the mount.cifs(8) manual page. (BZ#806337)\nAll users of cifs-utils are advised to upgrade to this updated package,\nwhich contains backported patches to fix these issues and add these\nenhancements.",
  "Platform": [
    "Red Hat Enterprise Linux 6"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2012:0902",
      "ID": "RHSA-2012:0902"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-1586",
      "ID": "CVE-2012-1586"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [

        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          },
          {
            "Comment": "cifs-utils is earlier than 0:4.8.1-10.el6"
          },
          {
            "Comment": "cifs-utils is signed with Red Hat redhatrelease2 key"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Low",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-1586"
    }
  ],
  "Issued": {
    "Date": "2012-06-19"
  }
}