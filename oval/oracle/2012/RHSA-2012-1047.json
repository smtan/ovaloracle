{
  "Title": "RHSA-2012:1047: php53 security update (Moderate)",
  "Description": "PHP is an HTML-embedded scripting language commonly used with the Apache\nHTTP Server.\nIt was discovered that the PHP XSL extension did not restrict the file\nwriting capability of libxslt. A remote attacker could use this flaw to\ncreate or overwrite an arbitrary file that is writable by the user running\nPHP, if a PHP script processed untrusted eXtensible Style Sheet Language\nTransformations (XSLT) content. (CVE-2012-0057)\nNote: This update disables file writing by default. A new PHP configuration\ndirective, \"xsl.security_prefs\", can be used to enable file writing in\nXSLT.\nA flaw was found in the way PHP validated file names in file upload\nrequests. A remote attacker could possibly use this flaw to bypass the\nsanitization of the uploaded file names, and cause a PHP script to store\nthe uploaded file in an unexpected directory, by using a directory\ntraversal attack. (CVE-2012-1172)\nMultiple integer overflow flaws, leading to heap-based buffer overflows,\nwere found in the way the PHP phar extension processed certain fields of\ntar archive files. A remote attacker could provide a specially-crafted tar\narchive file that, when processed by a PHP application using the phar\nextension, could cause the application to crash or, potentially, execute\narbitrary code with the privileges of the user running PHP. (CVE-2012-2386)\nA format string flaw was found in the way the PHP phar extension processed\ncertain PHAR files. A remote attacker could provide a specially-crafted\nPHAR file, which once processed in a PHP application using the phar\nextension, could lead to information disclosure and possibly arbitrary code\nexecution via a crafted phar:// URI. (CVE-2010-2950)\nA flaw was found in the DES algorithm implementation in the crypt()\npassword hashing function in PHP. If the password string to be hashed\ncontained certain characters, the remainder of the string was ignored when\ncalculating the hash, significantly reducing the password strength.\n(CVE-2012-2143)\nNote: With this update, passwords are no longer truncated when performing\nDES hashing. Therefore, new hashes of the affected passwords will not match\nstored hashes generated using vulnerable PHP versions, and will need to be\nupdated.\nIt was discovered that the fix for CVE-2012-1823, released via\nRHSA-2012:0547, did not properly filter all php-cgi command line arguments.\nA specially-crafted request to a PHP script could cause the PHP interpreter\nto execute the script in a loop, or output usage information that triggers\nan Internal Server Error. (CVE-2012-2336)\nA memory leak flaw was found in the PHP strtotime() function call. A remote\nattacker could possibly use this flaw to cause excessive memory consumption\nby triggering many strtotime() function calls. (CVE-2012-0789)\nIt was found that PHP did not check the zend_strndup() function's return\nvalue in certain cases. A remote attacker could possibly use this flaw to\ncrash a PHP application. (CVE-2011-4153)\nUpstream acknowledges Rubin Xu and Joseph Bonneau as the original reporters\nof CVE-2012-2143.\nAll php53 users should upgrade to these updated packages, which contain\nbackported patches to resolve these issues. After installing the updated\npackages, the httpd daemon must be restarted for the update to take effect.",
  "Platform": [
    "Red Hat Enterprise Linux 5"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2012:1047",
      "ID": "RHSA-2012:1047"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2010-2950",
      "ID": "CVE-2010-2950"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-4153",
      "ID": "CVE-2011-4153"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-0057",
      "ID": "CVE-2012-0057"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-0789",
      "ID": "CVE-2012-0789"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-1172",
      "ID": "CVE-2012-1172"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-2143",
      "ID": "CVE-2012-2143"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-2336",
      "ID": "CVE-2012-2336"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-2386",
      "ID": "CVE-2012-2386"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53 is earlier than 0:5.3.3-13.el5_8"
                  },
                  {
                    "Comment": "php53 is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-bcmath is earlier than 0:5.3.3-13.el5_8"
                  },
                  {
                    "Comment": "php53-bcmath is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-cli is earlier than 0:5.3.3-13.el5_8"
                  },
                  {
                    "Comment": "php53-cli is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-common is earlier than 0:5.3.3-13.el5_8"
                  },
                  {
                    "Comment": "php53-common is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-dba is earlier than 0:5.3.3-13.el5_8"
                  },
                  {
                    "Comment": "php53-dba is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-devel is earlier than 0:5.3.3-13.el5_8"
                  },
                  {
                    "Comment": "php53-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-gd is earlier than 0:5.3.3-13.el5_8"
                  },
                  {
                    "Comment": "php53-gd is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-imap is earlier than 0:5.3.3-13.el5_8"
                  },
                  {
                    "Comment": "php53-imap is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-intl is earlier than 0:5.3.3-13.el5_8"
                  },
                  {
                    "Comment": "php53-intl is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-ldap is earlier than 0:5.3.3-13.el5_8"
                  },
                  {
                    "Comment": "php53-ldap is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-mbstring is earlier than 0:5.3.3-13.el5_8"
                  },
                  {
                    "Comment": "php53-mbstring is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-mysql is earlier than 0:5.3.3-13.el5_8"
                  },
                  {
                    "Comment": "php53-mysql is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-odbc is earlier than 0:5.3.3-13.el5_8"
                  },
                  {
                    "Comment": "php53-odbc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-pdo is earlier than 0:5.3.3-13.el5_8"
                  },
                  {
                    "Comment": "php53-pdo is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-pgsql is earlier than 0:5.3.3-13.el5_8"
                  },
                  {
                    "Comment": "php53-pgsql is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-process is earlier than 0:5.3.3-13.el5_8"
                  },
                  {
                    "Comment": "php53-process is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-pspell is earlier than 0:5.3.3-13.el5_8"
                  },
                  {
                    "Comment": "php53-pspell is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-snmp is earlier than 0:5.3.3-13.el5_8"
                  },
                  {
                    "Comment": "php53-snmp is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-soap is earlier than 0:5.3.3-13.el5_8"
                  },
                  {
                    "Comment": "php53-soap is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-xml is earlier than 0:5.3.3-13.el5_8"
                  },
                  {
                    "Comment": "php53-xml is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "php53-xmlrpc is earlier than 0:5.3.3-13.el5_8"
                  },
                  {
                    "Comment": "php53-xmlrpc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 5 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 5 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2010-2950"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-4153"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-0057"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-0789"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-1172"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-2143"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-2336"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-2386"
    }
  ],
  "Issued": {
    "Date": "2012-06-27"
  }
}