{
  "Title": "RHSA-2012:0743: kernel security and bug fix update (Important)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux\noperating system.\nThis update fixes the following security issues:\n* A local, unprivileged user could use an integer overflow flaw in\ndrm_mode_dirtyfb_ioctl() to cause a denial of service or escalate their\nprivileges. (CVE-2012-0044, Important)\n* A buffer overflow flaw was found in the macvtap device driver, used for\ncreating a bridged network between the guest and the host in KVM\n(Kernel-based Virtual Machine) environments. A privileged guest user in a\nKVM guest could use this flaw to crash the host. Note: This issue only\naffected hosts that have the vhost_net module loaded with the\nexperimental_zcopytx module option enabled (it is not enabled by default),\nand that also have macvtap configured for at least one guest.\n(CVE-2012-2119, Important)\n* When a set user ID (setuid) application is executed, certain personality\nflags for controlling the application's behavior are cleared (that is, a\nprivileged application will not be affected by those flags). It was found\nthat those flags were not cleared if the application was made privileged\nvia file system capabilities. A local, unprivileged user could use this\nflaw to change the behavior of such applications, allowing them to bypass\nintended restrictions. Note that for default installations, no application\nshipped by Red Hat for Red Hat Enterprise Linux is made privileged via file\nsystem capabilities. (CVE-2012-2123, Important)\n* It was found that the data_len parameter of the sock_alloc_send_pskb()\nfunction in the Linux kernel's networking implementation was not validated\nbefore use. A privileged guest user in a KVM guest could use this flaw to\ncrash the host or, possibly, escalate their privileges on the host.\n(CVE-2012-2136, Important)\n* A buffer overflow flaw was found in the setup_routing_entry() function in\nthe KVM subsystem of the Linux kernel in the way the Message Signaled\nInterrupts (MSI) routing entry was handled. A local, unprivileged user\ncould use this flaw to cause a denial of service or, possibly, escalate\ntheir privileges. (CVE-2012-2137, Important)\n* A race condition was found in the Linux kernel's memory management\nsubsystem in the way pmd_none_or_clear_bad(), when called with mmap_sem in\nread mode, and Transparent Huge Pages (THP) page faults interacted. A\nprivileged user in a KVM guest with the ballooning functionality enabled\ncould potentially use this flaw to crash the host. A local, unprivileged\nuser could use this flaw to crash the system. (CVE-2012-1179, Moderate)\n* A flaw was found in the way device memory was handled during guest device\nremoval. Upon successful device removal, memory used by the device was not\nproperly unmapped from the corresponding IOMMU or properly released from\nthe kernel, leading to a memory leak. A malicious user on a KVM host who\nhas the ability to assign a device to a guest could use this flaw to crash\nthe host. (CVE-2012-2121, Moderate)\n* A flaw was found in the Linux kernel's Reliable Datagram Sockets (RDS)\nprotocol implementation. A local, unprivileged user could use this flaw to\ncause a denial of service. (CVE-2012-2372, Moderate)\n* A race condition was found in the Linux kernel's memory management\nsubsystem in the way pmd_populate() and pte_offset_map_lock() interacted on\n32-bit x86 systems with more than 4GB of RAM. A local, unprivileged user\ncould use this flaw to cause a denial of service. (CVE-2012-2373, Moderate)\nRed Hat would like to thank Chen Haogang for reporting CVE-2012-0044.\nThis update also fixes several bugs. Documentation for these changes will\nbe available shortly from the Technical Notes document linked to in the\nReferences section.\nUsers should upgrade to these updated packages, which contain backported\npatches to correct these issues, and fix the bugs noted in the Technical\nNotes. The system must be rebooted for this update to take effect.",
  "Platform": [
    "Red Hat Enterprise Linux 6"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2012:0743",
      "ID": "RHSA-2012:0743"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-0044",
      "ID": "CVE-2012-0044"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-1179",
      "ID": "CVE-2012-1179"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-2119",
      "ID": "CVE-2012-2119"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-2121",
      "ID": "CVE-2012-2121"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-2123",
      "ID": "CVE-2012-2123"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-2136",
      "ID": "CVE-2012-2136"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-2137",
      "ID": "CVE-2012-2137"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-2372",
      "ID": "CVE-2012-2372"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-2373",
      "ID": "CVE-2012-2373"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.32-220.23.1.el6 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.32-220.23.1.el6 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.32-220.23.1.el6"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-bootwrapper is earlier than 0:2.6.32-220.23.1.el6"
                  },
                  {
                    "Comment": "kernel-bootwrapper is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:2.6.32-220.23.1.el6"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:2.6.32-220.23.1.el6"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.32-220.23.1.el6"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.32-220.23.1.el6"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-firmware is earlier than 0:2.6.32-220.23.1.el6"
                  },
                  {
                    "Comment": "kernel-firmware is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:2.6.32-220.23.1.el6"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:2.6.32-220.23.1.el6"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:2.6.32-220.23.1.el6"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perf is earlier than 0:2.6.32-220.23.1.el6"
                  },
                  {
                    "Comment": "perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "python-perf is earlier than 0:2.6.32-220.23.1.el6"
                  },
                  {
                    "Comment": "python-perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-0044"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-1179"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-2119"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-2121"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-2123"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-2136"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-2137"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-2372"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-2373"
    }
  ],
  "Issued": {
    "Date": "2012-06-18"
  }
}