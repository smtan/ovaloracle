{
  "Title": "RHSA-2012:0516: thunderbird security update (Critical)",
  "Description": "Mozilla Thunderbird is a standalone mail and newsgroup client.\nA flaw was found in Sanitiser for OpenType (OTS), used by Thunderbird to\nhelp prevent potential exploits in malformed OpenType fonts. Malicious\ncontent could cause Thunderbird to crash or, under certain conditions,\npossibly execute arbitrary code with the privileges of the user running\nThunderbird. (CVE-2011-3062)\nMalicious content could cause Thunderbird to crash or, potentially, execute\narbitrary code with the privileges of the user running Thunderbird.\n(CVE-2012-0467, CVE-2012-0468, CVE-2012-0469)\nContent containing a malicious Scalable Vector Graphics (SVG) image file\ncould cause Thunderbird to crash or, potentially, execute arbitrary code\nwith the privileges of the user running Thunderbird. (CVE-2012-0470)\nA flaw was found in the way Thunderbird used its embedded Cairo library to\nrender certain fonts. Malicious content could cause Thunderbird to crash\nor, under certain conditions, possibly execute arbitrary code with the\nprivileges of the user running Thunderbird. (CVE-2012-0472)\nA flaw was found in the way Thunderbird rendered certain images using\nWebGL. Malicious content could cause Thunderbird to crash or, under certain\nconditions, possibly execute arbitrary code with the privileges of the user\nrunning Thunderbird. (CVE-2012-0478)\nA cross-site scripting (XSS) flaw was found in the way Thunderbird handled\ncertain multibyte character sets. Malicious content could cause Thunderbird\nto run JavaScript code with the permissions of different content.\n(CVE-2012-0471)\nA flaw was found in the way Thunderbird rendered certain graphics using\nWebGL. Malicious content could cause Thunderbird to crash. (CVE-2012-0473)\nA flaw in the built-in feed reader in Thunderbird allowed the Website field\nto display the address of different content than the content the user was\nvisiting. An attacker could use this flaw to conceal a malicious URL,\npossibly tricking a user into believing they are viewing a trusted site, or\nallowing scripts to be loaded from the attacker's site, possibly leading to\ncross-site scripting (XSS) attacks. (CVE-2012-0474)\nA flaw was found in the way Thunderbird decoded the ISO-2022-KR and\nISO-2022-CN character sets. Malicious content could cause Thunderbird\nto run JavaScript code with the permissions of different content.\n(CVE-2012-0477)\nA flaw was found in the way the built-in feed reader in Thunderbird handled\nRSS and Atom feeds. Invalid RSS or Atom content loaded over HTTPS caused\nThunderbird to display the address of said content, but not the content.\nThe previous content continued to be displayed. An attacker could use this\nflaw to perform phishing attacks, or trick users into thinking they are\nvisiting the site reported by the Website field, when the page is actually\ncontent controlled by an attacker. (CVE-2012-0479)\nRed Hat would like to thank the Mozilla project for reporting these issues.\nUpstream acknowledges Mateusz Jurczyk of the Google Security Team as the\noriginal reporter of CVE-2011-3062; Aki Helin from OUSPG as the original\nreporter of CVE-2012-0469; Atte Kettunen from OUSPG as the original\nreporter of CVE-2012-0470; wushi of team509 via iDefense as the original\nreporter of CVE-2012-0472; Ms2ger as the original reporter of\nCVE-2012-0478; Anne van Kesteren of Opera Software as the original reporter\nof CVE-2012-0471; Matias Juntunen as the original reporter of\nCVE-2012-0473; Jordi Chancel and Eddy Bordi, and Chris McGowen as the\noriginal reporters of CVE-2012-0474; Masato Kinugawa as the original\nreporter of CVE-2012-0477; and Jeroen van der Gun as the original reporter\nof CVE-2012-0479.\nNote: All issues except CVE-2012-0470, CVE-2012-0472, and CVE-2011-3062\ncannot be exploited by a specially-crafted HTML mail message as JavaScript\nis disabled by default for mail messages. It could be exploited another way\nin Thunderbird, for example, when viewing the full remote content of an\nRSS feed.",
  "Platform": [
    "Red Hat Enterprise Linux 5",
    "Red Hat Enterprise Linux 6"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2012:0516",
      "ID": "RHSA-2012:0516"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-3062",
      "ID": "CVE-2011-3062"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-0467",
      "ID": "CVE-2012-0467"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-0468",
      "ID": "CVE-2012-0468"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-0469",
      "ID": "CVE-2012-0469"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-0470",
      "ID": "CVE-2012-0470"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-0471",
      "ID": "CVE-2012-0471"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-0472",
      "ID": "CVE-2012-0472"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-0473",
      "ID": "CVE-2012-0473"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-0474",
      "ID": "CVE-2012-0474"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-0477",
      "ID": "CVE-2012-0477"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-0478",
      "ID": "CVE-2012-0478"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-0479",
      "ID": "CVE-2012-0479"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [

        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          },
          {
            "Comment": "thunderbird is earlier than 0:10.0.4-1.el6_2"
          },
          {
            "Comment": "thunderbird is signed with Red Hat redhatrelease2 key"
          }
        ]
      },
      {
        "Operator": "AND",
        "Criterias": [

        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 5 is installed"
          },
          {
            "Comment": "thunderbird is earlier than 0:10.0.4-1.el5_8"
          },
          {
            "Comment": "thunderbird is signed with Red Hat redhatrelease2 key"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 5 is installed"
      }
    ]
  },
  "Severity": "Critical",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-3062"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-0467"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-0468"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-0469"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-0470"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-0471"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-0472"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-0473"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-0474"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-0477"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-0478"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-0479"
    }
  ],
  "Issued": {
    "Date": "2012-04-24"
  }
}