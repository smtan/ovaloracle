{
  "Title": "RHSA-2012:0304: vixie-cron security, bug fix, and enhancement update (Low)",
  "Description": "The vixie-cron package contains the Vixie version of cron. Cron is a\nstandard UNIX daemon that runs specified programs at scheduled times. The\nvixie-cron package adds improved security and more powerful configuration\noptions to the standard version of cron.\nA race condition was found in the way the crontab program performed file\ntime stamp updates on a temporary file created when editing a user crontab\nfile. A local attacker could use this flaw to change the modification time\nof arbitrary system files via a symbolic link attack. (CVE-2010-0424)\nRed Hat would like to thank Dan Rosenberg for reporting this issue.\nThis update also fixes the following bugs:\n* Cron jobs of users with home directories mounted on a Lightweight\nDirectory Access Protocol (LDAP) server or Network File System (NFS) were\noften refused because jobs were marked as orphaned (typically due to a\ntemporary NSS lookup failure, when NIS and LDAP servers were unreachable).\nWith this update, a database of orphans is created, and cron jobs are\nperformed as expected. (BZ#455664)\n* Previously, cron did not log any errors if a cron job file located in the\n/etc/cron.d/ directory contained invalid entries. An upstream patch has\nbeen applied to address this problem and invalid entries in the cron job\nfiles now produce warning messages. (BZ#460070)\n* Previously, the \"@reboot\" crontab macro incorrectly ran jobs when the\ncrond daemon was restarted. If the user used the macro on multiple\nmachines, all entries with the \"@reboot\" option were executed every time\nthe crond daemon was restarted. With this update, jobs are executed only\nwhen the machine is rebooted. (BZ#476972)\n* The crontab utility is now compiled as a position-independent executable\n(PIE), which enhances the security of the system. (BZ#480930)\n* When the parent crond daemon was stopped, but a child crond daemon was\nrunning (executing a program), the \"service crond status\" command\nincorrectly reported that crond was running. The source code has been\nmodified, and the \"service crond status\" command now correctly reports that\ncrond is stopped. (BZ#529632)\n* According to the pam(8) manual page, the cron daemon, crond, supports\naccess control with PAM (Pluggable Authentication Module). However, the PAM\nconfiguration file for crond did not export environment variables correctly\nand, consequently, setting PAM variables via cron did not work. This update\nincludes a corrected /etc/pam.d/crond file that exports environment\nvariables correctly. Setting pam variables via cron now works as documented\nin the pam(8) manual page. (BZ#541189)\n* Previously, the mcstransd daemon modified labels for the crond daemon.\nWhen the crond daemon attempted to use the modified label and mcstransd was\nnot running, crond used an incorrect label. Consequently, Security-Enhanced\nLinux (SELinux) denials filled up the cron log, no jobs were executed, and\ncrond had to be restarted. With this update, both mcstransd and crond use\nraw SELinux labels, which prevents the problem. (BZ#625016)\n* Previously, the crontab(1) and cron(8) manual pages contained multiple\ntypographical errors. This update fixes those errors. (BZ#699620,\nBZ#699621)\nIn addition, this update adds the following enhancement:\n* Previously, the crontab utility did not use the Pluggable Authentication\nModule (PAM) for verification of users. As a consequence, a user could\naccess crontab even if access had been restricted (usually by being denied\nin the access.conf file). With this update, crontab returns an error\nmessage that the user is not allowed to access crontab because of PAM\nconfiguration. (BZ#249512)\nAll vixie-cron users should upgrade to this updated package, which resolves\nthese issues and adds this enhancement.",
  "Platform": [
    "Red Hat Enterprise Linux 5"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2012:0304",
      "ID": "RHSA-2012:0304"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2010-0424",
      "ID": "CVE-2010-0424"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [

        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 5 is installed"
          },
          {
            "Comment": "vixie-cron is earlier than 4:4.1-81.el5"
          },
          {
            "Comment": "vixie-cron is signed with Red Hat redhatrelease2 key"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 5 is installed"
      }
    ]
  },
  "Severity": "Low",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2010-0424"
    }
  ],
  "Issued": {
    "Date": "2012-02-21"
  }
}