{
  "Title": "RHSA-2012:1258: quagga security update (Moderate)",
  "Description": "Quagga is a TCP/IP based routing software suite. The Quagga bgpd daemon\nimplements the BGP (Border Gateway Protocol) routing protocol. The Quagga\nospfd and ospf6d daemons implement the OSPF (Open Shortest Path First)\nrouting protocol.\nA heap-based buffer overflow flaw was found in the way the bgpd daemon\nprocessed malformed Extended Communities path attributes. An attacker could\nsend a specially-crafted BGP message, causing bgpd on a target system to\ncrash or, possibly, execute arbitrary code with the privileges of the user\nrunning bgpd. The UPDATE message would have to arrive from an explicitly\nconfigured BGP peer, but could have originated elsewhere in the BGP\nnetwork. (CVE-2011-3327)\nA NULL pointer dereference flaw was found in the way the bgpd daemon\nprocessed malformed route Extended Communities attributes. A configured\nBGP peer could crash bgpd on a target system via a specially-crafted BGP\nmessage. (CVE-2010-1674)\nA stack-based buffer overflow flaw was found in the way the ospf6d daemon\nprocessed malformed Link State Update packets. An OSPF router could use\nthis flaw to crash ospf6d on an adjacent router. (CVE-2011-3323)\nA flaw was found in the way the ospf6d daemon processed malformed link\nstate advertisements. An OSPF neighbor could use this flaw to crash\nospf6d on a target system. (CVE-2011-3324)\nA flaw was found in the way the ospfd daemon processed malformed Hello\npackets. An OSPF neighbor could use this flaw to crash ospfd on a\ntarget system. (CVE-2011-3325)\nA flaw was found in the way the ospfd daemon processed malformed link state\nadvertisements. An OSPF router in the autonomous system could use this flaw\nto crash ospfd on a target system. (CVE-2011-3326)\nAn assertion failure was found in the way the ospfd daemon processed\ncertain Link State Update packets. An OSPF router could use this flaw to\ncause ospfd on an adjacent router to abort. (CVE-2012-0249)\nA buffer overflow flaw was found in the way the ospfd daemon processed\ncertain Link State Update packets. An OSPF router could use this flaw to\ncrash ospfd on an adjacent router. (CVE-2012-0250)\nRed Hat would like to thank CERT-FI for reporting CVE-2011-3327,\nCVE-2011-3323, CVE-2011-3324, CVE-2011-3325, and CVE-2011-3326; and the\nCERT/CC for reporting CVE-2012-0249 and CVE-2012-0250. CERT-FI acknowledges\nRiku Hietamäki, Tuomo Untinen and Jukka Taimisto of the Codenomicon CROSS\nproject as the original reporters of CVE-2011-3327, CVE-2011-3323,\nCVE-2011-3324, CVE-2011-3325, and CVE-2011-3326. The CERT/CC acknowledges\nMartin Winter at OpenSourceRouting.org as the original reporter of\nCVE-2012-0249 and CVE-2012-0250.\nUsers of quagga should upgrade to these updated packages, which contain\nbackported patches to correct these issues. After installing the updated\npackages, the bgpd, ospfd, and ospf6d daemons will be restarted\nautomatically.",
  "Platform": [
    "Red Hat Enterprise Linux 5"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2012:1258",
      "ID": "RHSA-2012:1258"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2010-1674",
      "ID": "CVE-2010-1674"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-3323",
      "ID": "CVE-2011-3323"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-3324",
      "ID": "CVE-2011-3324"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-3325",
      "ID": "CVE-2011-3325"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-3326",
      "ID": "CVE-2011-3326"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-3327",
      "ID": "CVE-2011-3327"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-0249",
      "ID": "CVE-2012-0249"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-0250",
      "ID": "CVE-2012-0250"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "quagga is earlier than 0:0.98.6-7.el5_8.1"
                  },
                  {
                    "Comment": "quagga is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "quagga-contrib is earlier than 0:0.98.6-7.el5_8.1"
                  },
                  {
                    "Comment": "quagga-contrib is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "quagga-devel is earlier than 0:0.98.6-7.el5_8.1"
                  },
                  {
                    "Comment": "quagga-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 5 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 5 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2010-1674"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-3323"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-3324"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-3325"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-3326"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-3327"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-0249"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-0250"
    }
  ],
  "Issued": {
    "Date": "2012-09-12"
  }
}