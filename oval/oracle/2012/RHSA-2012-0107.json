{
  "Title": "RHSA-2012:0107: kernel security and bug fix update (Important)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux\noperating system.\nThis update fixes the following security issues:\n* Using the SG_IO ioctl to issue SCSI requests to partitions or LVM volumes\nresulted in the requests being passed to the underlying block device. If a\nprivileged user only had access to a single partition or LVM volume, they\ncould use this flaw to bypass those restrictions and gain read and write\naccess (and be able to issue other SCSI commands) to the entire block\ndevice. Refer to Red Hat Knowledgebase article DOC-67874, linked to in the\nReferences, for further details about this issue. (CVE-2011-4127,\nImportant)\n* A flaw was found in the way the Linux kernel handled robust list pointers\nof user-space held futexes across exec() calls. A local, unprivileged user\ncould use this flaw to cause a denial of service or, eventually, escalate\ntheir privileges. (CVE-2012-0028, Important)\n* A flaw was found in the Linux kernel in the way splitting two extents in\next4_ext_convert_to_initialized() worked. A local, unprivileged user with\nthe ability to mount and unmount ext4 file systems could use this flaw to\ncause a denial of service. (CVE-2011-3638, Moderate)\n* A flaw was found in the way the Linux kernel's journal_unmap_buffer()\nfunction handled buffer head states. On systems that have an ext4 file\nsystem with a journal mounted, a local, unprivileged user could use this\nflaw to cause a denial of service. (CVE-2011-4086, Moderate)\n* A divide-by-zero flaw was found in the Linux kernel's igmp_heard_query()\nfunction. An attacker able to send certain IGMP (Internet Group Management\nProtocol) packets to a target system could use this flaw to cause a denial\nof service. (CVE-2012-0207, Moderate)\nRed Hat would like to thank Zheng Liu for reporting CVE-2011-3638, and\nSimon McVittie for reporting CVE-2012-0207.\nThis update also fixes the following bugs:\n* When a host was in recovery mode and a SCSI scan operation was initiated,\nthe scan operation failed and provided no error output. This bug has been\nfixed and the SCSI layer now waits for recovery of the host to complete\nscan operations for devices. (BZ#772162)\n* SG_IO ioctls were not implemented correctly in the Red Hat Enterprise\nLinux 5 virtio-blk driver. Sending an SG_IO ioctl request to a virtio-blk\ndisk caused the sending thread to enter an uninterruptible sleep state (\"D\"\nstate). With this update, SG_IO ioctls are rejected by the virtio-blk\ndriver: the ioctl system call will simply return an ENOTTY (\"Inappropriate\nioctl for device\") error and the thread will continue normally. (BZ#773322)\nUsers should upgrade to these updated packages, which contain backported\npatches to correct these issues. The system must be rebooted for this\nupdate to take effect.",
  "Platform": [
    "Red Hat Enterprise Linux 5"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2012:0107",
      "ID": "RHSA-2012:0107"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-3638",
      "ID": "CVE-2011-3638"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-4086",
      "ID": "CVE-2011-4086"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2011-4127",
      "ID": "CVE-2011-4127"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-0028",
      "ID": "CVE-2012-0028"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2012-0207",
      "ID": "CVE-2012-0207"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.18-274.18.1.el5 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.18-274.18.1.el5 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.18-274.18.1.el5"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-PAE is earlier than 0:2.6.18-274.18.1.el5"
                  },
                  {
                    "Comment": "kernel-PAE is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-PAE-devel is earlier than 0:2.6.18-274.18.1.el5"
                  },
                  {
                    "Comment": "kernel-PAE-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:2.6.18-274.18.1.el5"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:2.6.18-274.18.1.el5"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.18-274.18.1.el5"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.18-274.18.1.el5"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:2.6.18-274.18.1.el5"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:2.6.18-274.18.1.el5"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:2.6.18-274.18.1.el5"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xen is earlier than 0:2.6.18-274.18.1.el5"
                  },
                  {
                    "Comment": "kernel-xen is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-xen-devel is earlier than 0:2.6.18-274.18.1.el5"
                  },
                  {
                    "Comment": "kernel-xen-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 5 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 5 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-3638"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-4086"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2011-4127"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-0028"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2012-0207"
    }
  ],
  "Issued": {
    "Date": "2012-02-09"
  }
}