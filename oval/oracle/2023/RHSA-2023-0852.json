{
  "Title": "RHSA-2023:0852: httpd:2.4 security and bug fix update (Moderate)",
  "Description": "The httpd packages provide the Apache HTTP Server, a powerful, efficient, and extensible web server.\nSecurity Fix(es):\n* httpd: mod_dav: out-of-bounds read/write of zero byte (CVE-2006-20001)\n* httpd: mod_proxy_ajp: Possible request smuggling (CVE-2022-36760)\n* httpd: mod_proxy: HTTP response splitting (CVE-2022-37436)\nFor more details about the security issue(s), including the impact, a CVSS score, acknowledgments, and other related information, refer to the CVE page(s) listed in the References section.\nBug Fix(es):\n* httpd-init fails to create localhost.crt, localhost.key due to \"sscg\" default now creates a /dhparams.pem and is not idempotent if the file /dhparams.pem already exists. (BZ#2165967)",
  "Platform": [
    "Red Hat Enterprise Linux 8"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2023:0852",
      "ID": "RHSA-2023:0852"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2006-20001",
      "ID": "CVE-2006-20001"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2022-36760",
      "ID": "CVE-2022-36760"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2022-37436",
      "ID": "CVE-2022-37436"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "Oracle Linux 8 is installed"
              },
              {
                "Comment": "Red Hat CoreOS 4 is installed"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [
                  {
                    "Operator": "OR",
                    "Criterias": [
                      {
                        "Operator": "AND",
                        "Criterias": [

                        ],
                        "Criterions": [
                          {
                            "Comment": "httpd is earlier than 0:2.4.37-51.module+el8.7.0+18026+7b169787.1"
                          },
                          {
                            "Comment": "httpd is signed with Red Hat redhatrelease2 key"
                          }
                        ]
                      },
                      {
                        "Operator": "AND",
                        "Criterias": [

                        ],
                        "Criterions": [
                          {
                            "Comment": "httpd-devel is earlier than 0:2.4.37-51.module+el8.7.0+18026+7b169787.1"
                          },
                          {
                            "Comment": "httpd-devel is signed with Red Hat redhatrelease2 key"
                          }
                        ]
                      },
                      {
                        "Operator": "AND",
                        "Criterias": [

                        ],
                        "Criterions": [
                          {
                            "Comment": "httpd-filesystem is earlier than 0:2.4.37-51.module+el8.7.0+18026+7b169787.1"
                          },
                          {
                            "Comment": "httpd-filesystem is signed with Red Hat redhatrelease2 key"
                          }
                        ]
                      },
                      {
                        "Operator": "AND",
                        "Criterias": [

                        ],
                        "Criterions": [
                          {
                            "Comment": "httpd-manual is earlier than 0:2.4.37-51.module+el8.7.0+18026+7b169787.1"
                          },
                          {
                            "Comment": "httpd-manual is signed with Red Hat redhatrelease2 key"
                          }
                        ]
                      },
                      {
                        "Operator": "AND",
                        "Criterias": [

                        ],
                        "Criterions": [
                          {
                            "Comment": "httpd-tools is earlier than 0:2.4.37-51.module+el8.7.0+18026+7b169787.1"
                          },
                          {
                            "Comment": "httpd-tools is signed with Red Hat redhatrelease2 key"
                          }
                        ]
                      },
                      {
                        "Operator": "AND",
                        "Criterias": [

                        ],
                        "Criterions": [
                          {
                            "Comment": "mod_http2 is earlier than 0:1.15.7-5.module+el8.6.0+13996+01710940"
                          },
                          {
                            "Comment": "mod_http2 is signed with Red Hat redhatrelease2 key"
                          }
                        ]
                      },
                      {
                        "Operator": "AND",
                        "Criterias": [

                        ],
                        "Criterions": [
                          {
                            "Comment": "mod_ldap is earlier than 0:2.4.37-51.module+el8.7.0+18026+7b169787.1"
                          },
                          {
                            "Comment": "mod_ldap is signed with Red Hat redhatrelease2 key"
                          }
                        ]
                      },
                      {
                        "Operator": "AND",
                        "Criterias": [

                        ],
                        "Criterions": [
                          {
                            "Comment": "mod_md is earlier than 1:2.0.8-8.module+el8.3.0+6814+67d1e611"
                          },
                          {
                            "Comment": "mod_md is signed with Red Hat redhatrelease2 key"
                          }
                        ]
                      },
                      {
                        "Operator": "AND",
                        "Criterias": [

                        ],
                        "Criterions": [
                          {
                            "Comment": "mod_proxy_html is earlier than 1:2.4.37-51.module+el8.7.0+18026+7b169787.1"
                          },
                          {
                            "Comment": "mod_proxy_html is signed with Red Hat redhatrelease2 key"
                          }
                        ]
                      },
                      {
                        "Operator": "AND",
                        "Criterias": [

                        ],
                        "Criterions": [
                          {
                            "Comment": "mod_session is earlier than 0:2.4.37-51.module+el8.7.0+18026+7b169787.1"
                          },
                          {
                            "Comment": "mod_session is signed with Red Hat redhatrelease2 key"
                          }
                        ]
                      },
                      {
                        "Operator": "AND",
                        "Criterias": [

                        ],
                        "Criterions": [
                          {
                            "Comment": "mod_ssl is earlier than 1:2.4.37-51.module+el8.7.0+18026+7b169787.1"
                          },
                          {
                            "Comment": "mod_ssl is signed with Red Hat redhatrelease2 key"
                          }
                        ]
                      }
                    ],
                    "Criterions": [

                    ]
                  }
                ],
                "Criterions": [
                  {
                    "Comment": "Module httpd:2.4 is enabled"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [

        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 8 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2006-20001"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2022-36760"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2022-37436"
    }
  ],
  "Issued": {
    "Date": "2023-02-21"
  }
}