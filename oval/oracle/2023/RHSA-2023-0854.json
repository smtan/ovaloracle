{
  "Title": "RHSA-2023:0854: kernel-rt security and bug fix update (Important)",
  "Description": "The kernel-rt packages provide the Real Time Linux Kernel, which enables fine-tuning for systems with extremely high determinism requirements.\nSecurity Fix(es):\n* kernel: mm/mremap.c use-after-free vulnerability (CVE-2022-41222)\n* kernel: nfsd buffer overflow by RPC message over TCP with garbage data (CVE-2022-43945)\n* kernel: an out-of-bounds vulnerability in i2c-ismt driver (CVE-2022-2873)\nFor more details about the security issue(s), including the impact, a CVSS score, acknowledgments, and other related information, refer to the CVE page(s) listed in the References section.\nBug Fix(es):\n* RHEL8-RT: Backport use of a dedicate thread for timer wakeups (BZ#2127204)\n* SNO Crashed twice - kernel BUG at lib/list_debug.c:28 (BZ#2132062)\n* Cannot trigger kernel dump using NMI on SNO node running PAO and RT kernel  [RT-8] (BZ#2139851)\n* scheduling while atomic in fpu_clone() -> fpu_inherit_perms() (BZ#2154469)\n* The latest RHEL 8.7.z2 kernel changes need to be merged into the RT source tree to keep source parity between the two kernels. (BZ#2159806)",
  "Platform": [
    "Red Hat Enterprise Linux 8"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2023:0854",
      "ID": "RHSA-2023:0854"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2022-2873",
      "ID": "CVE-2022-2873"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2022-41222",
      "ID": "CVE-2022-41222"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2022-43945",
      "ID": "CVE-2022-43945"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "Oracle Linux 8 is installed"
              },
              {
                "Comment": "Red Hat CoreOS 4 is installed"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel-rt earlier than 0:4.18.0-425.13.1.rt7.223.el8_7 is currently running"
              },
              {
                "Comment": "kernel-rt earlier than 0:4.18.0-425.13.1.rt7.223.el8_7 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt is earlier than 0:4.18.0-425.13.1.rt7.223.el8_7"
                  },
                  {
                    "Comment": "kernel-rt is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-core is earlier than 0:4.18.0-425.13.1.rt7.223.el8_7"
                  },
                  {
                    "Comment": "kernel-rt-core is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug is earlier than 0:4.18.0-425.13.1.rt7.223.el8_7"
                  },
                  {
                    "Comment": "kernel-rt-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug-core is earlier than 0:4.18.0-425.13.1.rt7.223.el8_7"
                  },
                  {
                    "Comment": "kernel-rt-debug-core is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug-devel is earlier than 0:4.18.0-425.13.1.rt7.223.el8_7"
                  },
                  {
                    "Comment": "kernel-rt-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug-kvm is earlier than 0:4.18.0-425.13.1.rt7.223.el8_7"
                  },
                  {
                    "Comment": "kernel-rt-debug-kvm is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug-modules is earlier than 0:4.18.0-425.13.1.rt7.223.el8_7"
                  },
                  {
                    "Comment": "kernel-rt-debug-modules is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug-modules-extra is earlier than 0:4.18.0-425.13.1.rt7.223.el8_7"
                  },
                  {
                    "Comment": "kernel-rt-debug-modules-extra is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-devel is earlier than 0:4.18.0-425.13.1.rt7.223.el8_7"
                  },
                  {
                    "Comment": "kernel-rt-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-kvm is earlier than 0:4.18.0-425.13.1.rt7.223.el8_7"
                  },
                  {
                    "Comment": "kernel-rt-kvm is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-modules is earlier than 0:4.18.0-425.13.1.rt7.223.el8_7"
                  },
                  {
                    "Comment": "kernel-rt-modules is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-modules-extra is earlier than 0:4.18.0-425.13.1.rt7.223.el8_7"
                  },
                  {
                    "Comment": "kernel-rt-modules-extra is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [

        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 8 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2022-2873"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2022-41222"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2022-43945"
    }
  ],
  "Issued": {
    "Date": "2023-02-21"
  }
}