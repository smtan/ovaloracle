{
  "Title": "RHSA-2016:2079: java-1.8.0-openjdk security update (Critical)",
  "Description": "The java-1.8.0-openjdk packages provide the OpenJDK 8 Java Runtime Environment and the OpenJDK 8 Java Software Development Kit.\nSecurity Fix(es):\n* It was discovered that the Hotspot component of OpenJDK did not properly check arguments of the System.arraycopy() function in certain cases. An untrusted Java application or applet could use this flaw to corrupt virtual machine's memory and completely bypass Java sandbox restrictions. (CVE-2016-5582)\n* It was discovered that the Hotspot component of OpenJDK did not properly check received Java Debug Wire Protocol (JDWP) packets. An attacker could possibly use this flaw to send debugging commands to a Java program running with debugging enabled if they could make victim's browser send HTTP requests to the JDWP port of the debugged application. (CVE-2016-5573)\n* It was discovered that the Libraries component of OpenJDK did not restrict the set of algorithms used for Jar integrity verification. This flaw could allow an attacker to modify content of the Jar file that used weak signing key or hash algorithm. (CVE-2016-5542)\nNote: After this update, MD2 hash algorithm and RSA keys with less than 1024 bits are no longer allowed to be used for Jar integrity verification by default. MD5 hash algorithm is expected to be disabled by default in the future updates. A newly introduced security property jdk.jar.disabledAlgorithms can be used to control the set of disabled algorithms.\n* A flaw was found in the way the JMX component of OpenJDK handled classloaders. An untrusted Java application or applet could use this flaw to bypass certain Java sandbox restrictions. (CVE-2016-5554)\n* A flaw was found in the way the Networking component of OpenJDK handled HTTP proxy authentication. A Java application could possibly expose HTTPS server authentication credentials via a plain text network connection to an HTTP proxy if proxy asked for authentication. (CVE-2016-5597)\nNote: After this update, Basic HTTP proxy authentication can no longer be used when tunneling HTTPS connection through an HTTP proxy. Newly introduced system properties jdk.http.auth.proxying.disabledSchemes and jdk.http.auth.tunneling.disabledSchemes can be used to control which authentication schemes can be requested by an HTTP proxy when proxying HTTP and HTTPS connections respectively.\nNote: If the web browser plug-in provided by the icedtea-web package was installed, the issues exposed via Java applets could have been exploited without user interaction if a user visited a malicious website.",
  "Platform": [
    "Red Hat Enterprise Linux 6",
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2016:2079",
      "ID": "RHSA-2016:2079"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-10165",
      "ID": "CVE-2016-10165"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-5542",
      "ID": "CVE-2016-5542"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-5554",
      "ID": "CVE-2016-5554"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-5573",
      "ID": "CVE-2016-5573"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-5582",
      "ID": "CVE-2016-5582"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-5597",
      "ID": "CVE-2016-5597"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk is earlier than 1:1.8.0.111-1.b15.el7_2"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-accessibility is earlier than 1:1.8.0.111-1.b15.el7_2"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-accessibility is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-accessibility-debug is earlier than 1:1.8.0.111-1.b15.el7_2"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-accessibility-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-debug is earlier than 1:1.8.0.111-1.b15.el7_2"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-demo is earlier than 1:1.8.0.111-1.b15.el7_2"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-demo is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-demo-debug is earlier than 1:1.8.0.111-1.b15.el7_2"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-demo-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-devel is earlier than 1:1.8.0.111-1.b15.el7_2"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-devel-debug is earlier than 1:1.8.0.111-1.b15.el7_2"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-devel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-headless is earlier than 1:1.8.0.111-1.b15.el7_2"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-headless is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-headless-debug is earlier than 1:1.8.0.111-1.b15.el7_2"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-headless-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-javadoc is earlier than 1:1.8.0.111-1.b15.el7_2"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-javadoc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-javadoc-debug is earlier than 1:1.8.0.111-1.b15.el7_2"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-javadoc-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-src is earlier than 1:1.8.0.111-1.b15.el7_2"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-src is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-src-debug is earlier than 1:1.8.0.111-1.b15.el7_2"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-src-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      },
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk is earlier than 1:1.8.0.111-0.b15.el6_8"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-debug is earlier than 1:1.8.0.111-0.b15.el6_8"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-demo is earlier than 1:1.8.0.111-0.b15.el6_8"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-demo is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-demo-debug is earlier than 1:1.8.0.111-0.b15.el6_8"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-demo-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-devel is earlier than 1:1.8.0.111-0.b15.el6_8"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-devel-debug is earlier than 1:1.8.0.111-0.b15.el6_8"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-devel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-headless is earlier than 1:1.8.0.111-0.b15.el6_8"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-headless is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-headless-debug is earlier than 1:1.8.0.111-0.b15.el6_8"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-headless-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-javadoc is earlier than 1:1.8.0.111-0.b15.el6_8"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-javadoc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-javadoc-debug is earlier than 1:1.8.0.111-0.b15.el6_8"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-javadoc-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-src is earlier than 1:1.8.0.111-0.b15.el6_8"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-src is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-src-debug is earlier than 1:1.8.0.111-0.b15.el6_8"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-src-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Critical",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-10165"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-5542"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-5554"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-5573"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-5582"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-5597"
    }
  ],
  "Issued": {
    "Date": "2016-10-19"
  }
}