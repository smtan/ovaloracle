{
  "Title": "RHSA-2016:0611: samba security update (Critical)",
  "Description": "Samba is an open-source implementation of the Server Message Block (SMB) protocol and the related Common Internet File System (CIFS) protocol, which allow PC-compatible machines to share files, printers, and various information.\nSecurity Fix(es):\n* Multiple flaws were found in Samba's DCE/RPC protocol implementation. A remote, authenticated attacker could use these flaws to cause a denial of service against the Samba server (high CPU load or a crash) or, possibly, execute arbitrary code with the permissions of the user running Samba (root). This flaw could also be used to downgrade a secure DCE/RPC connection by a man-in-the-middle attacker taking control of an Active Directory (AD) object and compromising the security of a Samba Active Directory Domain Controller (DC). (CVE-2015-5370)\nNote: While Samba packages as shipped in Red Hat Enterprise Linux do not support running Samba as an AD DC, this flaw applies to all roles Samba implements.\n* A protocol flaw, publicly referred to as Badlock, was found in the Security Account Manager Remote Protocol (MS-SAMR) and the Local Security Authority (Domain Policy) Remote Protocol (MS-LSAD). Any authenticated DCE/RPC connection that a client initiates against a server could be used by a man-in-the-middle attacker to impersonate the authenticated user against the SAMR or LSA service on the server. As a result, the attacker would be able to get read/write access to the Security Account Manager database, and use this to reveal all passwords or any other potentially sensitive information in that database. (CVE-2016-2118)\n* Several flaws were found in Samba's implementation of NTLMSSP authentication. An unauthenticated, man-in-the-middle attacker could use this flaw to clear the encryption and integrity flags of a connection, causing data to be transmitted in plain text. The attacker could also force the client or server into sending data in plain text even if encryption was explicitly requested for that connection. (CVE-2016-2110)\n* It was discovered that Samba configured as a Domain Controller would establish a secure communication channel with a machine using a spoofed computer name. A remote attacker able to observe network traffic could use this flaw to obtain session-related information about the spoofed machine. (CVE-2016-2111)\n* It was found that Samba's LDAP implementation did not enforce integrity protection for LDAP connections. A man-in-the-middle attacker could use this flaw to downgrade LDAP connections to use no integrity protection, allowing them to hijack such connections. (CVE-2016-2112)\n* It was found that Samba did not enable integrity protection for IPC traffic by default. A man-in-the-middle attacker could use this flaw to view and modify the data sent between a Samba server and a client. (CVE-2016-2115)\nRed Hat would like to thank the Samba project for reporting these issues. Upstream acknowledges Jouni Knuutinen (Synopsis) as the original reporter of CVE-2015-5370; and Stefan Metzmacher (SerNet) as the original reporter of CVE-2016-2118, CVE-2016-2110, CVE-2016-2112, and CVE-2016-2115.",
  "Platform": [
    "Red Hat Enterprise Linux 6"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2016:0611",
      "ID": "RHSA-2016:0611"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-5370",
      "ID": "CVE-2015-5370"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-2110",
      "ID": "CVE-2016-2110"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-2111",
      "ID": "CVE-2016-2111"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-2112",
      "ID": "CVE-2016-2112"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-2115",
      "ID": "CVE-2016-2115"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-2118",
      "ID": "CVE-2016-2118"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libsmbclient is earlier than 0:3.6.23-30.el6_7"
                  },
                  {
                    "Comment": "libsmbclient is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "libsmbclient-devel is earlier than 0:3.6.23-30.el6_7"
                  },
                  {
                    "Comment": "libsmbclient-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "samba is earlier than 0:3.6.23-30.el6_7"
                  },
                  {
                    "Comment": "samba is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "samba-client is earlier than 0:3.6.23-30.el6_7"
                  },
                  {
                    "Comment": "samba-client is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "samba-common is earlier than 0:3.6.23-30.el6_7"
                  },
                  {
                    "Comment": "samba-common is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "samba-doc is earlier than 0:3.6.23-30.el6_7"
                  },
                  {
                    "Comment": "samba-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "samba-domainjoin-gui is earlier than 0:3.6.23-30.el6_7"
                  },
                  {
                    "Comment": "samba-domainjoin-gui is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "samba-glusterfs is earlier than 0:3.6.23-30.el6_7"
                  },
                  {
                    "Comment": "samba-glusterfs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "samba-swat is earlier than 0:3.6.23-30.el6_7"
                  },
                  {
                    "Comment": "samba-swat is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "samba-winbind is earlier than 0:3.6.23-30.el6_7"
                  },
                  {
                    "Comment": "samba-winbind is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "samba-winbind-clients is earlier than 0:3.6.23-30.el6_7"
                  },
                  {
                    "Comment": "samba-winbind-clients is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "samba-winbind-devel is earlier than 0:3.6.23-30.el6_7"
                  },
                  {
                    "Comment": "samba-winbind-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "samba-winbind-krb5-locator is earlier than 0:3.6.23-30.el6_7"
                  },
                  {
                    "Comment": "samba-winbind-krb5-locator is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Critical",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-5370"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-2110"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-2111"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-2112"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-2115"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-2118"
    }
  ],
  "Issued": {
    "Date": "2016-04-12"
  }
}