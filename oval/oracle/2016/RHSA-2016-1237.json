{
  "Title": "RHSA-2016:1237: ImageMagick security update (Important)",
  "Description": "ImageMagick is an image display and manipulation tool for the X Window System that can read and write multiple image formats.\nSecurity Fix(es):\n* It was discovered that ImageMagick did not properly sanitize certain input before using it to invoke processes. A remote attacker could create a specially crafted image that, when processed by an application using ImageMagick or an unsuspecting user using the ImageMagick utilities, would lead to arbitrary execution of shell commands with the privileges of the user running the application. (CVE-2016-5118)\n* It was discovered that ImageMagick did not properly sanitize certain input before passing it to the gnuplot delegate functionality. A remote attacker could create a specially crafted image that, when processed by an application using ImageMagick or an unsuspecting user using the ImageMagick utilities, would lead to arbitrary execution of shell commands with the privileges of the user running the application. (CVE-2016-5239)\n* Multiple flaws have been discovered in ImageMagick. A remote attacker could, for example, create specially crafted images that, when processed by an application using ImageMagick or an unsuspecting user using the ImageMagick utilities, would result in a memory corruption and, potentially, execution of arbitrary code, a denial of service, or an application crash. (CVE-2015-8896, CVE-2015-8895, CVE-2016-5240, CVE-2015-8897, CVE-2015-8898)",
  "Platform": [
    "Red Hat Enterprise Linux 6",
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2016:1237",
      "ID": "RHSA-2016:1237"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-8895",
      "ID": "CVE-2015-8895"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-8896",
      "ID": "CVE-2015-8896"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-8897",
      "ID": "CVE-2015-8897"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-8898",
      "ID": "CVE-2015-8898"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-5118",
      "ID": "CVE-2016-5118"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-5239",
      "ID": "CVE-2016-5239"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-5240",
      "ID": "CVE-2016-5240"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ImageMagick is earlier than 0:6.7.8.9-15.el7_2"
                  },
                  {
                    "Comment": "ImageMagick is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ImageMagick-c++ is earlier than 0:6.7.8.9-15.el7_2"
                  },
                  {
                    "Comment": "ImageMagick-c++ is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ImageMagick-c++-devel is earlier than 0:6.7.8.9-15.el7_2"
                  },
                  {
                    "Comment": "ImageMagick-c++-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ImageMagick-devel is earlier than 0:6.7.8.9-15.el7_2"
                  },
                  {
                    "Comment": "ImageMagick-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ImageMagick-doc is earlier than 0:6.7.8.9-15.el7_2"
                  },
                  {
                    "Comment": "ImageMagick-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ImageMagick-perl is earlier than 0:6.7.8.9-15.el7_2"
                  },
                  {
                    "Comment": "ImageMagick-perl is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      },
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ImageMagick is earlier than 0:6.7.2.7-5.el6_8"
                  },
                  {
                    "Comment": "ImageMagick is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ImageMagick-c++ is earlier than 0:6.7.2.7-5.el6_8"
                  },
                  {
                    "Comment": "ImageMagick-c++ is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ImageMagick-c++-devel is earlier than 0:6.7.2.7-5.el6_8"
                  },
                  {
                    "Comment": "ImageMagick-c++-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ImageMagick-devel is earlier than 0:6.7.2.7-5.el6_8"
                  },
                  {
                    "Comment": "ImageMagick-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ImageMagick-doc is earlier than 0:6.7.2.7-5.el6_8"
                  },
                  {
                    "Comment": "ImageMagick-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "ImageMagick-perl is earlier than 0:6.7.2.7-5.el6_8"
                  },
                  {
                    "Comment": "ImageMagick-perl is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-8895"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-8896"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-8897"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-8898"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-5118"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-5239"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-5240"
    }
  ],
  "Issued": {
    "Date": "2016-06-16"
  }
}