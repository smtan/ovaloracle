{
  "Title": "RHSA-2016:0494: kernel security, bug fix, and enhancement update (Moderate)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux\noperating system.\n* It was found that the fix for CVE-2015-1805 incorrectly kept buffer\noffset and buffer length in sync on a failed atomic read, potentially\nresulting in a pipe buffer state corruption. A local, unprivileged user\ncould use this flaw to crash the system or leak kernel memory to user\nspace. (CVE-2016-0774, Moderate)\nThe security impact of this issue was discovered by Red Hat.\nThis update also fixes the following bugs:\n* In the anon_vma structure, the degree counts number of child anon_vmas\nand of VMAs which points to this anon_vma. Failure to decrement the\nparent's degree in the unlink_anon_vma() function, when its list was empty,\npreviously triggered a BUG_ON() assertion. The provided patch makes sure\nthe anon_vma degree is always decremented when the VMA list is empty, thus\nfixing this bug. (BZ#1318364)\n* When running Internet Protocol Security (IPSEC) on external storage\nencrypted with LUKS under a substantial load on the system, data\ncorruptions could previously occur. A set of upstream patches has been\nprovided, and data corruption is no longer reported in this situation.\n(BZ#1298994)\n* Due to prematurely decremented calc_load_task, the calculated load\naverage was off by up to the number of CPUs in the machine. As a\nconsequence, job scheduling worked improperly causing a drop in the system\nperformance. This update keeps the delta of the CPU going into NO_HZ idle\nseparately, and folds the pending idle delta into the global active count\nwhile correctly aging the averages for the idle-duration when leaving NO_HZ\nmode. Now, job scheduling works correctly, ensuring balanced CPU load.\n(BZ#1300349)\n* Due to a regression in the Red Hat Enterprise Linux 6.7 kernel, the\ncgroup OOM notifier accessed a cgroup-specific internal data structure\nwithout a proper locking protection, which led to a kernel panic. This\nupdate adjusts the cgroup OOM notifier to lock internal data properly,\nthus fixing the bug. (BZ#1302763)\n* GFS2 had a rare timing window that sometimes caused it to reference an\nuninitialized variable. Consequently, a kernel panic occurred. The code has\nbeen changed to reference the correct value during this timing window, and\nthe kernel no longer panics. (BZ#1304332)\n* Due to a race condition whereby a cache operation could be submitted\nafter a cache object was killed, the kernel occasionally crashed on systems\nrunning the cachefilesd service. The provided patch prevents the race\ncondition by adding serialization in the code that makes the object\nunavailable. As a result, all subsequent operations targetted on the object\nare rejected and the kernel no longer crashes in this scenario.\n(BZ#1308471)\nThis update also adds this enhancement:\n* The lpfc driver has been updated to version 11.0.0.4. (BZ#1297838)\nAll kernel users are advised to upgrade to these updated packages, which\ncontain backported patches to correct these issues and add this\nenhancement. The system must be rebooted for this update to take effect.",
  "Platform": [
    "Red Hat Enterprise Linux 6"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2016:0494",
      "ID": "RHSA-2016:0494"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-0774",
      "ID": "CVE-2016-0774"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:2.6.32-573.22.1.el6 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:2.6.32-573.22.1.el6 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:2.6.32-573.22.1.el6"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-abi-whitelists is earlier than 0:2.6.32-573.22.1.el6"
                  },
                  {
                    "Comment": "kernel-abi-whitelists is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-bootwrapper is earlier than 0:2.6.32-573.22.1.el6"
                  },
                  {
                    "Comment": "kernel-bootwrapper is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:2.6.32-573.22.1.el6"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:2.6.32-573.22.1.el6"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:2.6.32-573.22.1.el6"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:2.6.32-573.22.1.el6"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-firmware is earlier than 0:2.6.32-573.22.1.el6"
                  },
                  {
                    "Comment": "kernel-firmware is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:2.6.32-573.22.1.el6"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:2.6.32-573.22.1.el6"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:2.6.32-573.22.1.el6"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perf is earlier than 0:2.6.32-573.22.1.el6"
                  },
                  {
                    "Comment": "perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "python-perf is earlier than 0:2.6.32-573.22.1.el6"
                  },
                  {
                    "Comment": "python-perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Moderate",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-0774"
    }
  ],
  "Issued": {
    "Date": "2016-03-22"
  }
}