{
  "Title": "RHSA-2016:0301: openssl security update (Important)",
  "Description": "OpenSSL is a toolkit that implements the Secure Sockets Layer (SSL v2/v3)\nand Transport Layer Security (TLS v1) protocols, as well as a\nfull-strength, general purpose cryptography library.\nA padding oracle flaw was found in the Secure Sockets Layer version 2.0\n(SSLv2) protocol. An attacker can potentially use this flaw to decrypt\nRSA-encrypted cipher text from a connection using a newer SSL/TLS protocol\nversion, allowing them to decrypt such connections. This cross-protocol\nattack is publicly referred to as DROWN. (CVE-2016-0800)\nNote: This issue was addressed by disabling the SSLv2 protocol by default\nwhen using the 'SSLv23' connection methods, and removing support for weak\nSSLv2 cipher suites. For more information, refer to the knowledge base\narticle linked to in the References section.\nA flaw was found in the way malicious SSLv2 clients could negotiate SSLv2\nciphers that have been disabled on the server. This could result in weak\nSSLv2 ciphers being used for SSLv2 connections, making them vulnerable to\nman-in-the-middle attacks. (CVE-2015-3197)\nA side-channel attack was found that makes use of cache-bank conflicts on\nthe Intel Sandy-Bridge microarchitecture. An attacker who has the ability\nto control code in a thread running on the same hyper-threaded core as the\nvictim's thread that is performing decryption, could use this flaw to\nrecover RSA private keys. (CVE-2016-0702)\nA double-free flaw was found in the way OpenSSL parsed certain malformed\nDSA (Digital Signature Algorithm) private keys. An attacker could create\nspecially crafted DSA private keys that, when processed by an application\ncompiled against OpenSSL, could cause the application to crash.\n(CVE-2016-0705)\nAn integer overflow flaw, leading to a NULL pointer dereference or a\nheap-based memory corruption, was found in the way some BIGNUM functions of\nOpenSSL were implemented. Applications that use these functions with large\nuntrusted input could crash or, potentially, execute arbitrary code.\n(CVE-2016-0797)\nRed Hat would like to thank the OpenSSL project for reporting these issues.\nUpstream acknowledges Nimrod Aviram and Sebastian Schinzel as the original\nreporters of CVE-2016-0800 and CVE-2015-3197; Adam Langley\n(Google/BoringSSL) as the original reporter of CVE-2016-0705; Yuval Yarom\n(University of Adelaide and NICTA), Daniel Genkin (Technion and Tel Aviv\nUniversity), Nadia Heninger (University of Pennsylvania) as the original\nreporters of CVE-2016-0702; and Guido Vranken as the original reporter of\nCVE-2016-0797.\nAll openssl users are advised to upgrade to these updated packages,\nwhich contain backported patches to correct these issues. For the update \nto take effect, all services linked to the OpenSSL library must be \nrestarted, or the system rebooted.",
  "Platform": [
    "Red Hat Enterprise Linux 6",
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2016:0301",
      "ID": "RHSA-2016:0301"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2015-3197",
      "ID": "CVE-2015-3197"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-0702",
      "ID": "CVE-2016-0702"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-0705",
      "ID": "CVE-2016-0705"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-0797",
      "ID": "CVE-2016-0797"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2016-0800",
      "ID": "CVE-2016-0800"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "openssl is earlier than 0:1.0.1e-42.el6_7.4"
                  },
                  {
                    "Comment": "openssl is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "openssl-devel is earlier than 0:1.0.1e-42.el6_7.4"
                  },
                  {
                    "Comment": "openssl-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "openssl-perl is earlier than 0:1.0.1e-42.el6_7.4"
                  },
                  {
                    "Comment": "openssl-perl is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "openssl-static is earlier than 0:1.0.1e-42.el6_7.4"
                  },
                  {
                    "Comment": "openssl-static is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 6 is installed"
          }
        ]
      },
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "openssl is earlier than 1:1.0.1e-51.el7_2.4"
                  },
                  {
                    "Comment": "openssl is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "openssl-devel is earlier than 1:1.0.1e-51.el7_2.4"
                  },
                  {
                    "Comment": "openssl-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "openssl-libs is earlier than 1:1.0.1e-51.el7_2.4"
                  },
                  {
                    "Comment": "openssl-libs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "openssl-perl is earlier than 1:1.0.1e-51.el7_2.4"
                  },
                  {
                    "Comment": "openssl-perl is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "openssl-static is earlier than 1:1.0.1e-51.el7_2.4"
                  },
                  {
                    "Comment": "openssl-static is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 6 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2015-3197"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-0702"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-0705"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-0797"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2016-0800"
    }
  ],
  "Issued": {
    "Date": "2016-03-01"
  }
}