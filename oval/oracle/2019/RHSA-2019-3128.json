{
  "Title": "RHSA-2019:3128: java-1.8.0-openjdk security update (Important)",
  "Description": "The java-1.8.0-openjdk packages provide the OpenJDK 8 Java Runtime Environment and the OpenJDK 8 Java Software Development Kit.\nSecurity Fix(es):\n* OpenJDK: Improper handling of Kerberos proxy credentials (Kerberos, 8220302) (CVE-2019-2949)\n* OpenJDK: Unexpected exception thrown during regular expression processing in Nashorn (Scripting, 8223518) (CVE-2019-2975)\n* OpenJDK: Incorrect handling of nested jar: URLs in Jar URL handler (Networking, 8223892) (CVE-2019-2978)\n* OpenJDK: Incorrect handling of HTTP proxy responses in HttpURLConnection (Networking, 8225298) (CVE-2019-2989)\n* OpenJDK: Missing restrictions on use of custom SocketImpl (Networking, 8218573) (CVE-2019-2945)\n* OpenJDK: NULL pointer dereference in DrawGlyphList (2D, 8222690) (CVE-2019-2962)\n* OpenJDK: Unexpected exception thrown by Pattern processing crafted regular expression (Concurrency, 8222684) (CVE-2019-2964)\n* OpenJDK: Unexpected exception thrown by XPathParser processing crafted XPath expression (JAXP, 8223505) (CVE-2019-2973)\n* OpenJDK: Unexpected exception thrown by XPath processing crafted XPath expression (JAXP, 8224532) (CVE-2019-2981)\n* OpenJDK: Unexpected exception thrown during Font object deserialization (Serialization, 8224915) (CVE-2019-2983)\n* OpenJDK: Missing glyph bitmap image dimension check in FreetypeFontScaler (2D, 8225286) (CVE-2019-2987)\n* OpenJDK: Integer overflow in bounds check in SunGraphics2D (2D, 8225292) (CVE-2019-2988)\n* OpenJDK: Excessive memory allocation in CMap when reading TrueType font (2D, 8225597) (CVE-2019-2992)\n* OpenJDK: Insufficient filtering of HTML event attributes in Javadoc (Javadoc, 8226765) (CVE-2019-2999)\nFor more details about the security issue(s), including the impact, a CVSS score, acknowledgments, and other related information, refer to the CVE page(s) listed in the References section.",
  "Platform": [
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2019:3128",
      "ID": "RHSA-2019:3128"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-2945",
      "ID": "CVE-2019-2945"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-2949",
      "ID": "CVE-2019-2949"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-2962",
      "ID": "CVE-2019-2962"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-2964",
      "ID": "CVE-2019-2964"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-2973",
      "ID": "CVE-2019-2973"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-2975",
      "ID": "CVE-2019-2975"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-2978",
      "ID": "CVE-2019-2978"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-2981",
      "ID": "CVE-2019-2981"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-2983",
      "ID": "CVE-2019-2983"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-2987",
      "ID": "CVE-2019-2987"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-2988",
      "ID": "CVE-2019-2988"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-2989",
      "ID": "CVE-2019-2989"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-2992",
      "ID": "CVE-2019-2992"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-2999",
      "ID": "CVE-2019-2999"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk is earlier than 1:1.8.0.232.b09-0.el7_7"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-accessibility is earlier than 1:1.8.0.232.b09-0.el7_7"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-accessibility is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-accessibility-debug is earlier than 1:1.8.0.232.b09-0.el7_7"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-accessibility-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-debug is earlier than 1:1.8.0.232.b09-0.el7_7"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-demo is earlier than 1:1.8.0.232.b09-0.el7_7"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-demo is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-demo-debug is earlier than 1:1.8.0.232.b09-0.el7_7"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-demo-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-devel is earlier than 1:1.8.0.232.b09-0.el7_7"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-devel-debug is earlier than 1:1.8.0.232.b09-0.el7_7"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-devel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-headless is earlier than 1:1.8.0.232.b09-0.el7_7"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-headless is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-headless-debug is earlier than 1:1.8.0.232.b09-0.el7_7"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-headless-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-javadoc is earlier than 1:1.8.0.232.b09-0.el7_7"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-javadoc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-javadoc-debug is earlier than 1:1.8.0.232.b09-0.el7_7"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-javadoc-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-javadoc-zip is earlier than 1:1.8.0.232.b09-0.el7_7"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-javadoc-zip is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-javadoc-zip-debug is earlier than 1:1.8.0.232.b09-0.el7_7"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-javadoc-zip-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-src is earlier than 1:1.8.0.232.b09-0.el7_7"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-src is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "java-1.8.0-openjdk-src-debug is earlier than 1:1.8.0.232.b09-0.el7_7"
                  },
                  {
                    "Comment": "java-1.8.0-openjdk-src-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 7 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-2945"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-2949"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-2962"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-2964"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-2973"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-2975"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-2978"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-2981"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-2983"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-2987"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-2988"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-2989"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-2992"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-2999"
    }
  ],
  "Issued": {
    "Date": "2019-10-16"
  }
}