{
  "Title": "RHSA-2019:1176: kernel-rt security update (Important)",
  "Description": "The kernel-rt packages provide the Real Time Linux Kernel, which enables fine-tuning for systems with extremely high determinism requirements.\nSecurity Fix(es):\n* A flaw was found in the implementation of the \"fill buffer\", a mechanism used by modern CPUs when a cache-miss is made on L1 CPU cache.  If an attacker can generate a load operation that would create a page fault, the execution will continue speculatively with incorrect data from the fill buffer while the data is fetched from higher level caches.  This response time can be measured to infer data in the fill buffer. (CVE-2018-12130)\n* Modern Intel microprocessors implement hardware-level micro-optimizations to improve the performance of writing data back to CPU caches. The write operation is split into STA (STore Address) and STD (STore Data) sub-operations. These sub-operations allow the processor to hand-off address generation logic into these sub-operations for optimized writes. Both of these sub-operations write to a shared distributed processor structure called the 'processor store buffer'.  As a result, an unprivileged attacker could use this flaw to read private data resident within the CPU's processor store buffer. (CVE-2018-12126)\n* Microprocessors use a ‘load port’ subcomponent to perform load operations from memory or IO. During a load operation, the load port receives data from the memory or IO subsystem and then provides the data to the CPU registers and operations in the CPU’s pipelines. Stale load operations results are stored in the 'load port' table until overwritten by newer operations. Certain load-port operations triggered by an attacker can be used to reveal data about previous stale requests leaking data back to the attacker via a timing side-channel. (CVE-2018-12127)\n* Uncacheable memory on some microprocessors utilizing speculative execution may allow an authenticated user to potentially enable information disclosure via a side channel with local access. (CVE-2019-11091)\nFor more details about the security issue(s), including the impact, a CVSS score, acknowledgments, and other related information, refer to the CVE page(s) listed in the References section.",
  "Platform": [
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2019:1176",
      "ID": "RHSA-2019:1176"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-12126",
      "ID": "CVE-2018-12126"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-12127",
      "ID": "CVE-2018-12127"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-12130",
      "ID": "CVE-2018-12130"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-11091",
      "ID": "CVE-2019-11091"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel-rt earlier than 0:3.10.0-957.12.2.rt56.929.el7 is currently running"
              },
              {
                "Comment": "kernel-rt earlier than 0:3.10.0-957.12.2.rt56.929.el7 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt is earlier than 0:3.10.0-957.12.2.rt56.929.el7"
                  },
                  {
                    "Comment": "kernel-rt is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug is earlier than 0:3.10.0-957.12.2.rt56.929.el7"
                  },
                  {
                    "Comment": "kernel-rt-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug-devel is earlier than 0:3.10.0-957.12.2.rt56.929.el7"
                  },
                  {
                    "Comment": "kernel-rt-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug-kvm is earlier than 0:3.10.0-957.12.2.rt56.929.el7"
                  },
                  {
                    "Comment": "kernel-rt-debug-kvm is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-devel is earlier than 0:3.10.0-957.12.2.rt56.929.el7"
                  },
                  {
                    "Comment": "kernel-rt-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-doc is earlier than 0:3.10.0-957.12.2.rt56.929.el7"
                  },
                  {
                    "Comment": "kernel-rt-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-kvm is earlier than 0:3.10.0-957.12.2.rt56.929.el7"
                  },
                  {
                    "Comment": "kernel-rt-kvm is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-trace is earlier than 0:3.10.0-957.12.2.rt56.929.el7"
                  },
                  {
                    "Comment": "kernel-rt-trace is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-trace-devel is earlier than 0:3.10.0-957.12.2.rt56.929.el7"
                  },
                  {
                    "Comment": "kernel-rt-trace-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-trace-kvm is earlier than 0:3.10.0-957.12.2.rt56.929.el7"
                  },
                  {
                    "Comment": "kernel-rt-trace-kvm is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 7 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-12126"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-12127"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-12130"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-11091"
    }
  ],
  "Issued": {
    "Date": "2019-05-14"
  }
}