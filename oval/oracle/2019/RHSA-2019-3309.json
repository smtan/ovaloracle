{
  "Title": "RHSA-2019:3309: kernel-rt security and bug fix update (Important)",
  "Description": "The kernel-rt packages provide the Real Time Linux Kernel, which enables fine-tuning for systems with extremely high determinism requirements.\nSecurity Fix(es):\n* kernel: nfs: use-after-free in svc_process_common() (CVE-2018-16884)\n* Kernel: vhost_net: infinite loop while receiving packets leads to DoS (CVE-2019-3900)\n* Kernel: page cache side channel attacks (CVE-2019-5489)\n* hardware: bluetooth: BR/EDR encryption key negotiation attacks (KNOB) (CVE-2019-9506)\n* kernel: Heap overflow in mwifiex_uap_parse_tail_ies function in drivers/net/wireless/marvell/mwifiex/ie.c (CVE-2019-10126)\n* Kernel: KVM: OOB memory access via mmio ring buffer (CVE-2019-14821)\n* kernel: Information Disclosure in crypto_report_one in crypto/crypto_user.c (CVE-2018-19854)\n* kernel: usb: missing size check in the __usb_get_extra_descriptor() leading to DoS (CVE-2018-20169)\n* kernel: Heap address information leak while using L2CAP_GET_CONF_OPT (CVE-2019-3459)\n* kernel: Heap address information leak while using L2CAP_PARSE_CONF_RSP (CVE-2019-3460)\n* kernel: SCTP socket buffer memory leak leading to denial of service (CVE-2019-3874)\n* kernel: denial of service vector through vfio DMA mappings (CVE-2019-3882)\n* kernel: null-pointer dereference in hci_uart_set_flow_control (CVE-2019-10207)\n* kernel: fix race condition between mmget_not_zero()/get_task_mm() and core dumping (CVE-2019-11599)\n* kernel: fs/ext4/extents.c leads to information disclosure (CVE-2019-11833)\n* kernel: sensitive information disclosure from kernel stack memory via HIDPCONNADD command (CVE-2019-11884)\n* kernel: use-after-free in arch/x86/lib/insn-eval.c (CVE-2019-13233)\n* kernel: memory leak in register_queue_kobjects() in net/core/net-sysfs.c leads to denial of service (CVE-2019-15916)\n* kernel: oob memory read in hso_probe in drivers/net/usb/hso.c (CVE-2018-19985)\n* Kernel: KVM: leak of uninitialized stack contents to guest (CVE-2019-7222)\n* Kernel: net: weak IP ID generation leads to remote device tracking (CVE-2019-10638)\nFor more details about the security issue(s), including the impact, a CVSS score, acknowledgments, and other related information, refer to the CVE page(s) listed in the References section.\nAdditional Changes:\nFor detailed information on changes in this release, see the Red Hat Enterprise Linux 8.1 Release Notes linked from the References section.",
  "Platform": [
    "Red Hat Enterprise Linux 8"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2019:3309",
      "ID": "RHSA-2019:3309"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-16884",
      "ID": "CVE-2018-16884"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-19854",
      "ID": "CVE-2018-19854"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-19985",
      "ID": "CVE-2018-19985"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-20169",
      "ID": "CVE-2018-20169"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-10126",
      "ID": "CVE-2019-10126"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-10207",
      "ID": "CVE-2019-10207"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-10638",
      "ID": "CVE-2019-10638"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-11599",
      "ID": "CVE-2019-11599"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-11833",
      "ID": "CVE-2019-11833"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-11884",
      "ID": "CVE-2019-11884"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-13233",
      "ID": "CVE-2019-13233"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-14821",
      "ID": "CVE-2019-14821"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-15666",
      "ID": "CVE-2019-15666"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-15916",
      "ID": "CVE-2019-15916"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-15921",
      "ID": "CVE-2019-15921"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-15924",
      "ID": "CVE-2019-15924"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-16994",
      "ID": "CVE-2019-16994"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-3459",
      "ID": "CVE-2019-3459"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-3460",
      "ID": "CVE-2019-3460"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-3874",
      "ID": "CVE-2019-3874"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-3882",
      "ID": "CVE-2019-3882"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-3900",
      "ID": "CVE-2019-3900"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-5489",
      "ID": "CVE-2019-5489"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-7222",
      "ID": "CVE-2019-7222"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-9506",
      "ID": "CVE-2019-9506"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2020-10720",
      "ID": "CVE-2020-10720"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "Oracle Linux 8 is installed"
              },
              {
                "Comment": "Red Hat CoreOS 4 is installed"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel-rt earlier than 0:4.18.0-147.rt24.93.el8 is currently running"
              },
              {
                "Comment": "kernel-rt earlier than 0:4.18.0-147.rt24.93.el8 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt is earlier than 0:4.18.0-147.rt24.93.el8"
                  },
                  {
                    "Comment": "kernel-rt is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-core is earlier than 0:4.18.0-147.rt24.93.el8"
                  },
                  {
                    "Comment": "kernel-rt-core is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug is earlier than 0:4.18.0-147.rt24.93.el8"
                  },
                  {
                    "Comment": "kernel-rt-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug-core is earlier than 0:4.18.0-147.rt24.93.el8"
                  },
                  {
                    "Comment": "kernel-rt-debug-core is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug-devel is earlier than 0:4.18.0-147.rt24.93.el8"
                  },
                  {
                    "Comment": "kernel-rt-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug-kvm is earlier than 0:4.18.0-147.rt24.93.el8"
                  },
                  {
                    "Comment": "kernel-rt-debug-kvm is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug-modules is earlier than 0:4.18.0-147.rt24.93.el8"
                  },
                  {
                    "Comment": "kernel-rt-debug-modules is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-debug-modules-extra is earlier than 0:4.18.0-147.rt24.93.el8"
                  },
                  {
                    "Comment": "kernel-rt-debug-modules-extra is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-devel is earlier than 0:4.18.0-147.rt24.93.el8"
                  },
                  {
                    "Comment": "kernel-rt-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-kvm is earlier than 0:4.18.0-147.rt24.93.el8"
                  },
                  {
                    "Comment": "kernel-rt-kvm is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-modules is earlier than 0:4.18.0-147.rt24.93.el8"
                  },
                  {
                    "Comment": "kernel-rt-modules is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-rt-modules-extra is earlier than 0:4.18.0-147.rt24.93.el8"
                  },
                  {
                    "Comment": "kernel-rt-modules-extra is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [

        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 8 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-16884"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-19854"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-19985"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-20169"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-10126"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-10207"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-10638"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-11599"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-11833"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-11884"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-13233"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-14821"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-15666"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-15916"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-15921"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-15924"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-16994"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-3459"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-3460"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-3874"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-3882"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-3900"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-5489"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-7222"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-9506"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2020-10720"
    }
  ],
  "Issued": {
    "Date": "2019-11-05"
  }
}