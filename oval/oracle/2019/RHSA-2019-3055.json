{
  "Title": "RHSA-2019:3055: kernel security and bug fix update (Important)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux operating system.\nSecurity Fix(es):\n* kernel: Use-after-free in __blk_drain_queue() function in block/blk-core.c (CVE-2018-20856)\n* kernel: Heap overflow in mwifiex_update_bss_desc_with_ie function in marvell/mwifiex/scan.c (CVE-2019-3846)\n* hardware: bluetooth: BR/EDR encryption key negotiation attacks (KNOB) (CVE-2019-9506)\n* kernel: Heap overflow in mwifiex_uap_parse_tail_ies function in drivers/net/wireless/marvell/mwifiex/ie.c (CVE-2019-10126)\nFor more details about the security issue(s), including the impact, a CVSS score, acknowledgments, and other related information, refer to the CVE page(s) listed in the References section.\nBug Fixes:\n* gfs2: Fix iomap write page reclaim deadlock (BZ#1737373)\n* [FJ7.6 Bug]: [REG] kernel: ipc: ipc_free should use kvfree (BZ#1740178)\n* high update_cfs_rq_blocked_load contention (BZ#1740180)\n* [Hyper-V][RHEL 7] kdump fails to start on a Hyper-V guest of Windows Server 2019. (BZ#1740188)\n* kvm: backport cpuidle-haltpoll driver (BZ#1740192)\n* Growing unreclaimable slab memory (BZ#1741920)\n* [bnx2x] ping failed from pf to vf which has been attached to vm (BZ#1741926)\n* [Hyper-V]vPCI devices cannot allocate IRQs vectors in a Hyper-V VM with > 240 vCPUs (i.e., when in x2APIC mode) (BZ#1743324)\n* Macsec: inbound MACSEC frame is unexpectedly dropped with InPktsNotValid (BZ#1744442)\n* RHEL 7.7 Beta - Hit error when trying to run nvme connect with IPv6 address (BZ#1744443)\n* RHEL 7.6 SS4 - Paths lost when running straight I/O on NVMe/RoCE system (BZ#1744444)\n* NFSv4.0 client sending a double CLOSE (leading to EIO application failure) (BZ#1744946)\n* [Azure] CRI-RDOS | [RHEL 7.8] Live migration only takes 10 seconds, but the VM was unavailable for 2 hours (BZ#1748239)\n* NFS client autodisconnect timer may fire immediately after TCP connection setup and may cause DoS type reconnect problem in complex network environments (BZ#1749290)\n* [Inspur] RHEL7.6 ASPEED graphic card display issue (BZ#1749296)\n* Allows macvlan to operated correctly over the active-backup mode to support bonding events. (BZ#1751579)\n* [LLNL 7.5 Bug] slab leak causing a crash when using kmem control group (BZ#1752421)\nUsers of kernel are advised to upgrade to these updated packages, which fix these bugs.",
  "Platform": [
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2019:3055",
      "ID": "RHSA-2019:3055"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-20856",
      "ID": "CVE-2018-20856"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-10126",
      "ID": "CVE-2019-10126"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-3846",
      "ID": "CVE-2019-3846"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2019-9506",
      "ID": "CVE-2019-9506"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:3.10.0-1062.4.1.el7 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:3.10.0-1062.4.1.el7 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "bpftool is earlier than 0:3.10.0-1062.4.1.el7"
                  },
                  {
                    "Comment": "bpftool is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:3.10.0-1062.4.1.el7"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-abi-whitelists is earlier than 0:3.10.0-1062.4.1.el7"
                  },
                  {
                    "Comment": "kernel-abi-whitelists is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-bootwrapper is earlier than 0:3.10.0-1062.4.1.el7"
                  },
                  {
                    "Comment": "kernel-bootwrapper is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:3.10.0-1062.4.1.el7"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:3.10.0-1062.4.1.el7"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:3.10.0-1062.4.1.el7"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:3.10.0-1062.4.1.el7"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:3.10.0-1062.4.1.el7"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:3.10.0-1062.4.1.el7"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:3.10.0-1062.4.1.el7"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-tools is earlier than 0:3.10.0-1062.4.1.el7"
                  },
                  {
                    "Comment": "kernel-tools is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-tools-libs is earlier than 0:3.10.0-1062.4.1.el7"
                  },
                  {
                    "Comment": "kernel-tools-libs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-tools-libs-devel is earlier than 0:3.10.0-1062.4.1.el7"
                  },
                  {
                    "Comment": "kernel-tools-libs-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perf is earlier than 0:3.10.0-1062.4.1.el7"
                  },
                  {
                    "Comment": "perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "python-perf is earlier than 0:3.10.0-1062.4.1.el7"
                  },
                  {
                    "Comment": "python-perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 7 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-20856"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-10126"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-3846"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2019-9506"
    }
  ],
  "Issued": {
    "Date": "2019-10-16"
  }
}