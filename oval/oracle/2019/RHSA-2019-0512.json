{
  "Title": "RHSA-2019:0512: kernel security, bug fix, and enhancement update (Important)",
  "Description": "The kernel packages contain the Linux kernel, the core of any Linux operating system.\nSecurity Fix(es):\n* kernel: Memory corruption due to incorrect socket cloning (CVE-2018-9568)\n* kernel: Unprivileged users able to inspect kernel stacks of arbitrary tasks (CVE-2018-17972)\n* kernel: Faulty computation of numberic bounds in the BPF verifier (CVE-2018-18445)\nFor more details about the security issue(s), including the impact, a CVSS score, acknowledgments, and other related information, refer to the CVE page(s) listed in the References section.\nBug Fix(es) and Enhancement(s):\n* kernel fuse invalidates cached attributes during reads (BZ#1657921)\n* [NetApp-FC-NVMe] RHEL7.6: nvme reset gets hung indefinitely (BZ#1659937)\n* Memory reclaim deadlock calling __sock_create() after memalloc_noio_save() (BZ#1660392)\n* hardened usercopy is causing crash (BZ#1660815)\n* Backport: xfrm: policy: init locks early (BZ#1660887)\n* AWS m5 instance type loses NVMe mounted volumes [was: Unable to Mount StatefulSet PV in AWS EBS] (BZ#1661947)\n* RHEL 7.6 running on a VirtualBox guest with a GUI has a mouse problem (BZ#1662848)\n* Kernel bug report in cgroups on heavily contested 3.10 node (BZ#1663114)\n* [PCIe] SHPC probe crash on Non-ACPI/Non-SHPC ports (BZ#1663241)\n* [Cavium 7.7 Feat] qla2xxx: Update to latest upstream. (BZ#1663508)\n* Regression in lpfc and the CNE1000 (BE2 FCoE) adapters that no longer initialize (BZ#1664067)\n* [csiostor] call trace after command: modprobe csiostor (BZ#1665370)\n* libceph: fall back to sendmsg for slab pages (BZ#1665814)\n* Deadlock between stop_one_cpu_nowait() and stop_two_cpus() (BZ#1667328)\n* Soft lockups occur when the sd driver passes a device size of 1 sector to string_get_size() (BZ#1667989)\n* [RHEL7.7] BUG: unable to handle kernel paging request at ffffffffffffffff (BZ#1668208)\n* RHEL7.6 - powerpc/pseries: Disable CPU hotplug across migrations / powerpc/rtas: Fix a potential race between CPU-Offline & Migration (LPM) (BZ#1669044)\n* blk-mq: fix corruption with direct issue (BZ#1670511)\n* [RHEL7][patch] iscsi driver can block reboot/shutdown (BZ#1670680)\n* [DELL EMC 7.6 BUG] Unable to create-namespace over Dell NVDIMM-N (BZ#1671743)\n* efi_bgrt_init fails to ioremap error during boot (BZ#1671745)\n* Unable to mount a share on kernel- 3.10.0-957.el7. The share can be mounted on kernel-3.10.0-862.14.4.el7 (BZ#1672448)\n* System crash with RIP nfs_readpage_async+0x43 -- BUG: unable to handle kernel NULL pointer dereference (BZ#1672510)\nUsers of kernel are advised to upgrade to these updated packages, which fix these bugs and add this enhancement.",
  "Platform": [
    "Red Hat Enterprise Linux 7"
  ],
  "References": [
    {
      "Source": "RHSA",
      "URI": "https://access.redhat.com/errata/RHSA-2019:0512",
      "ID": "RHSA-2019:0512"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-17972",
      "ID": "CVE-2018-17972"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-18445",
      "ID": "CVE-2018-18445"
    },
    {
      "Source": "CVE",
      "URI": "https://access.redhat.com/security/cve/CVE-2018-9568",
      "ID": "CVE-2018-9568"
    }
  ],
  "Criteria": {
    "Operator": "OR",
    "Criterias": [
      {
        "Operator": "AND",
        "Criterias": [
          {
            "Operator": "OR",
            "Criterias": [

            ],
            "Criterions": [
              {
                "Comment": "kernel earlier than 0:3.10.0-957.10.1.el7 is currently running"
              },
              {
                "Comment": "kernel earlier than 0:3.10.0-957.10.1.el7 is set to boot up on next boot"
              }
            ]
          },
          {
            "Operator": "OR",
            "Criterias": [
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "bpftool is earlier than 0:3.10.0-957.10.1.el7"
                  },
                  {
                    "Comment": "bpftool is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel is earlier than 0:3.10.0-957.10.1.el7"
                  },
                  {
                    "Comment": "kernel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-abi-whitelists is earlier than 0:3.10.0-957.10.1.el7"
                  },
                  {
                    "Comment": "kernel-abi-whitelists is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-bootwrapper is earlier than 0:3.10.0-957.10.1.el7"
                  },
                  {
                    "Comment": "kernel-bootwrapper is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug is earlier than 0:3.10.0-957.10.1.el7"
                  },
                  {
                    "Comment": "kernel-debug is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-debug-devel is earlier than 0:3.10.0-957.10.1.el7"
                  },
                  {
                    "Comment": "kernel-debug-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-devel is earlier than 0:3.10.0-957.10.1.el7"
                  },
                  {
                    "Comment": "kernel-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-doc is earlier than 0:3.10.0-957.10.1.el7"
                  },
                  {
                    "Comment": "kernel-doc is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-headers is earlier than 0:3.10.0-957.10.1.el7"
                  },
                  {
                    "Comment": "kernel-headers is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump is earlier than 0:3.10.0-957.10.1.el7"
                  },
                  {
                    "Comment": "kernel-kdump is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-kdump-devel is earlier than 0:3.10.0-957.10.1.el7"
                  },
                  {
                    "Comment": "kernel-kdump-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-tools is earlier than 0:3.10.0-957.10.1.el7"
                  },
                  {
                    "Comment": "kernel-tools is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-tools-libs is earlier than 0:3.10.0-957.10.1.el7"
                  },
                  {
                    "Comment": "kernel-tools-libs is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "kernel-tools-libs-devel is earlier than 0:3.10.0-957.10.1.el7"
                  },
                  {
                    "Comment": "kernel-tools-libs-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "perf is earlier than 0:3.10.0-957.10.1.el7"
                  },
                  {
                    "Comment": "perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "Operator": "AND",
                "Criterias": [

                ],
                "Criterions": [
                  {
                    "Comment": "python-perf is earlier than 0:3.10.0-957.10.1.el7"
                  },
                  {
                    "Comment": "python-perf is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": [

            ]
          }
        ],
        "Criterions": [
          {
            "Comment": "Oracle Linux 7 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "Comment": "Oracle Linux 7 is installed"
      }
    ]
  },
  "Severity": "Important",
  "Cves": [
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-17972"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-18445"
    },
    {
      "Impact": "",
      "Href": "",
      "ID": "CVE-2018-9568"
    }
  ],
  "Issued": {
    "Date": "2019-03-13"
  }
}